<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('lib/utils.php');
  require_once ('api_layout.php');
  echo makeHTML_begin(true);
  includeStyleSheet();
  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  require_once ('api_playlist.php');
  require_once ('api_feedback.php');
  require_once ('api_mediabase.php');
  require_once ('lib/class_cache.php');

  require_once ('lib/getid3/getid3.php');


  // -- head --
  echo '<div class="commonHead">';
    echo '<div class="commonHeadText">'.PROJECT_NAME.' ('.LNG_BACKEND.') action</div>';
    echo '<a class="linkNaviNice" style="float: right;" href="frontend.php">back to '.PROJECT_NAME.'</a>';
    echo '<a class="linkNaviNice" href="upload.php">'.LNG_ONE_MORE_UPLOAD.'</a>';
  echo '</div>';


  $feedback = LFH;
  if (!($cfg_songUpload !== false && isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] == true))
  {
    $feedback .= 'upload not allowed or no nickname given' . LFH;
  }
  else
  {
    // -- upload and import a dreamplayer valueoperator.ini --
    $uploadDir = $cfg_absPathToSongs . $cfg_songUpload . SLASH . $_SESSION['str_nickname'];// important! wihtout . SLASH;
    if (!file_exists($uploadDir))
      mkdir($uploadDir, 0777, true);


    if (isset($_POST['btnUploadSong']))
    {
      $uploadBaseFile = $_FILES['uploadSong']['name'];
      $uploadFile = $uploadDir . SLASH . $uploadBaseFile;
      if (!move_uploaded_file($_FILES['uploadSong']['tmp_name'], $uploadFile))
      {
        $feedback .= 'upload...<span style="color: red;">NOK</span>' . LFH;
      }
      else
      {
        $feedback .= 'upload...<span style="color: green;">OK</span>' . LFH;

        $mediaBaseUpdateStr = file_get_contents($cfg_mediabaseFile);
        $mediaBaseUpdate = simplexml_load_file($cfg_mediabaseFile);
        $myFeedback = new class_feedback();

        $feedback .= 'check if exists...' . LFH;
        if (strpos($mediaBaseUpdateStr, $uploadBaseFile) === false)
        {
          // -- core --
          updateMP3TagsToMediabase($uploadFile);
          $feedback .= 'song imported...<span style="color: green;">OK</span>' . LFH;
        }
        else
          $feedback .= 'song already exists...<span style="color: red;">NOK</span>' . LFH;

        // -- ... write to XML file now --
        $handle = fopen($cfg_mediabaseFile, 'w');
        if (!fwrite($handle, $mediaBaseUpdate->asXML()))
          echo 'failure while saving mediabase file' . LFC;
        fclose($handle);
      }
    }
  }

?>
<div style="position: relative; margin-left: 2em; margin-top: 3em;">
  <pre>
    <?php
      echo $feedback;
    ?>
  </pre>
</div>
<?php
  echo makeHTML_end();
