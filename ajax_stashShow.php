<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_playlist.php';


  $msg = NULLSTR;

  // -- is admin? --
  //$admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  if (!isset($_SESSION['str_nickname']))
  {
    $msg = LNG_ERR_UNKNOWN_FUNNY;
  }
  else
  {
    $nickname = $_SESSION['str_nickname'];

    $myPlaylist = new class_playlist();

    $stashContent = $myPlaylist->stashGetContent($nickname);

    if (empty($stashContent))
    {
      $msg = '<span id="stashIsEmpty">'.$myPlaylist->getLastMsg().'</span>';
    }
    else
    {
      $msg .= '<div style="text-align: center; color: green;">'.LNG_STASH.'</div>'; // <span style="color: black; font-size: smaller; margin-left: 1em;"><sup>X</sup></span>
      foreach ($stashContent as $songID => $song)
      {
        $msg .= '<a class="grpLstItmL" href="javascript:stashPopSong(\''.$songID.'\')">
                     <div style="margin-top: 1em;">
                       <div class="listItemInterpret">'.$song['i'].'</div>
                       <div class="listItemTitle">
                         '.$song['t'].'</div>
                     </div>
                   </a>';
      }
    }

    //eventLog('removed from playlist [index]: ' . $_GET['index']);
    unset($myPlaylist);
  }
  echo $msg;
