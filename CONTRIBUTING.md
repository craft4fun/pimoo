First off all, thanks for taking the time to contribute!

All types of contributions are encouraged and valued. The community and melooks forward to your contributions.

And if you like the project, but just don’t have time to contribute, that’s fine. There are other easy ways to support the project and show your appreciation, which we would also be very happy about:

- Star the project
- Refer this project-page or main developer's page https://craft4fun.de in your project’s readme
- Mention the project at local meetups and tell your friends/colleagues.

