<?php
  // -- this script releases the forcing to mobile or not --

  date_default_timezone_set('UTC'); // -- does not matter, no need to include all defines and config --
  if (!isset($_SESSION)) // -- session starting here is an exception as an emergency case --
    session_start();

  unset($_SESSION['forceIsMobile']);

  if (isset($_SERVER['HTTP_REFERER']))
    header('location: ' . $_SERVER['HTTP_REFERER']);
  else
    header('location: frontend.php');
