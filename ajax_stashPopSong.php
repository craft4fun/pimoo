<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_playlist.php';


  $msg = NULLSTR;

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  // -- either a certain song picked out from dialog or simply the bottom one --
  $certainSong = (isset($_GET['index'])) ? $_GET['index'] : false;

  if (!isset($_SESSION['str_nickname']))
  {
    $msg = LNG_ERR_UNKNOWN_FUNNY;
  }
  else
  {
    $nickname = $_SESSION['str_nickname'];

    $myPlaylist = new class_playlist();

    $songInfo = $myPlaylist->stashPopSong($nickname, $certainSong);

    if (empty($songInfo))
    {
      $msg = $myPlaylist->getLastMsg();
    }
    else
    {
      $myPlaylist->setWishedBy($nickname);
      $check = $myPlaylist->addSong($songInfo['a'],
                                    $songInfo['i'],
                                    $songInfo['t'],
                                    $songInfo['p'],
                                    $songInfo['a'],
                                    $admin);

      if (!$check)
      {
        $msg = $myPlaylist->getLastMsg();
      }
      else
      {
        $msg = LNG_SUC_ADDED;

        // -- prepared, but to have this, the stashed song must not be showed in a bubble rather than a result hulk --
        // if ($admin && $myPlaylist->getPlaylistCount() > 1)
        //   $msg .= '<div style="margin-top: 1em;">'.cntMBL('<a style="color: red;" href="javascript:songMoveTopAfterAdding()">'.LNG_MOVE_TOP.'</a>').'</div>';
      }
    }

    //eventLog('removed from playlist [index]: ' . $_GET['index']);
    unset($myPlaylist);
  }
  echo $msg;
