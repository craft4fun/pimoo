<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');
  require_once ('lib/utils.php');
  require_once ('api_mediabase.php');
  require_once ('api_playlist.php');
  //require_once ('api_feedback.php');

  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();

  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">myMoo</div>';

    echo '<span><a class="linkNaviNice"                       href="index.php">'.LNG_BACK_TO_.PROJECT_NAME.'</a></span>';
    echo '<span style="float: right;"><a class="linkNaviNice" href="myMooMore.php">'.LNG_MORE.'</a></span>';

  echo '</div>';
?>
<div style="padding: 0.5em;">
  <?php

    // -- --------------- --
    // -- user "nickname" --
    // -- --------------- --
    echo '<div style="margin: 1.5em 0em 1em 0em;">';
      if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
      {
        echo '<span class="textShadowBlur">'.LNG_HELLO.'</span>'.BLANK.'<span style="font-weight: bold;"><nobr>'.$_SESSION['str_nickname'].'</nobr></span>';
        echo '<span style="float: right;">'.cntMBL('<a class="linkNice" href="javascript:promptNickname(\''.$_SESSION['str_nickname'].'\')">'.LNG_CHANGE_NICKNAME.'</a>').'</span>';
        echo '<br><br>';
        echo '<span style="float: right;">'.cntMBL('<a onclick="return confirm(\''.LNG_SURE.'\')" style="color: red;" href="javascript:logout()">'.LNG_LOGOUT.'</a>').'</span>';
      }
      else
      {
        // $_SESSION['str_nickname'] = $_SERVER['REMOTE_ADDR'];
        echo '<span><a href="javascript:promptNickname();">'.LNG_GIMMENICKNAME.'</a></span>';
      }
    echo '</div>';




    // -- ----------- --
    // -- UI language --
    // -- ----------- --
    function _isSetLanguage($aValue)
    {
      if (isset($_COOKIE[STR_LANG]) && $_COOKIE[STR_LANG] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 3em 0em 1em 0em;">';
      echo '<label for="mm_language" class="textShadowBlur">'.LNG_LANGUAGE.'</label>';
      echo '<select id="mm_language" onchange="setLanguage(this.value)" class="myMooOption">
            <option '._isSetLanguage(LANG_EN).' value="'.LANG_EN.'">'.LANG_EN_PLAIN.'</option>
            <option '._isSetLanguage(LANG_DE).' value="'.LANG_DE.'">'.LANG_DE_PLAIN.'</option>
          </select>';
    echo '</div>';



    // -- --------- --
    // -- Dark mode --
    // -- --------- --

    function _isSetDarkMode($aValue)
    {
      if (isset($_COOKIE['bool_darkMode']) && $_COOKIE['bool_darkMode'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_darkMode" class="textShadowBlur">'.LNG_DARKMODE.'</label>';
      echo '<select id="mm_darkMode" onchange="setDarkMode(this)" class="myMooOption '._isSetDarkMode(YES).'">
              <option value="'.NO.'">'.LNG_NO.'</option>
              <option '._isSetDarkMode(YES).' value="'.YES.'">'.LNG_YES.'</option>
           </select>';
    echo '</div>';



    // -- --------------------- --
    // -- Display zoom --
    // -- --------------------- --
    function _isSetDisplayZoom($aValue)
    {
      if (!isset($_COOKIE['str_displayZoom']) && MEDIUM == $aValue)
        return 'selected';
      elseif (isset($_COOKIE['str_displayZoom']) && $_COOKIE['str_displayZoom'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_displayZoom" class="textShadowBlur">'.LNG_DISPLAY_ZOOM.'</label>';
      echo '<select id="mm_displayZoom" onchange="setDisplayZoom(this)" class="myMooOption '._isSetDisplayZoom(MEDIUM).'">
              <option '._isSetDisplayZoom(MEDIUM).' value="'.LOW.'">'.LNG_LOW.'</option>
              <option '._isSetDisplayZoom(MEDIUM).' value="'.MEDIUM.'">'.LNG_MEDIUM.'</option>
              <option '._isSetDisplayZoom(HIGH).'   value="'.HIGH.'">'.LNG_HIGH.'</option>
          </select>';
    echo '</div>';



    // -- ---------------------------------- --
    // -- Show song title on top omnipresent --
    // -- ---------------------------------- --

    // -- without any reason as cookie :o) - So the settings last longer in browser as a convenience for the user --

    function _isSetSongOmnipresent($aValue)
    {
      if (isset($_COOKIE['bool_SongOmnipresent']) && $_COOKIE['bool_SongOmnipresent'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_songOmnipresent" class="textShadowBlur">'.LNG_SHOW_SONG_OMNIPRESENT.'</label>';
      echo '<select id="mm_songOmnipresent" onchange="setSongOmnipresent(this)" class="myMooOption '._isSetSongOmnipresent(YES).'">
              <option value="'.NO.'">'.LNG_NO.'</option>
              <option '._isSetSongOmnipresent(YES).' value="'.YES.'">'.LNG_YES.'</option>
           </select>';
    echo '</div>';



    // -- ------------------------------------------------------ --
    // -- Show album instead of "i"nfo whereever it is possible  --
    // -- ------------------------------------------------------ --

    // -- without any reason as cookie :o) - So the settings last longer in browser as a convenience for the user --

    function _isSetShowAlbumCoverInPlaylist($aValue)
    {
      if (isset($_COOKIE['bool_ShowAlbumCoverInPlaylist']) && $_COOKIE['bool_ShowAlbumCoverInPlaylist'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_albumInPlaylist" class="textShadowBlur">'.LNG_SHOW_ALBUM_IN_PLAYLIST.'</label>';
      echo '<select id="mm_albumInPlaylist" onchange="setShowAlbumCoverInPlaylist(this)" class="myMooOption '._isSetShowAlbumCoverInPlaylist(YES).'">
              <option value="'.NO.'">'.LNG_NO.'</option>
              <option '._isSetShowAlbumCoverInPlaylist(YES).' value="'.YES.'">'.LNG_YES.'</option>
            </select>';
    echo '</div>';



    // -- --------------------- --
    // -- Random search results --
    // -- --------------------- --
    function _isSetRandomSearchResults($aValue)
    {
      if (isset($_COOKIE['int_RandSrchResults']) && $_COOKIE['int_RandSrchResults'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_randomSearchResults" class="textShadowBlur">'.LNG_RANDOM_SEARCH_RESULTS.'</label>';
      echo '<select id="mm_randomSearchResults" onchange="setRandomSearchResults(this.value)" class="myMooOption">
              <option value="'.GRP_RAND10.'">'.GRP_RAND10.'</option>
              <option '._isSetRandomSearchResults(GRP_RAND20).' value="'.GRP_RAND20.'">'.GRP_RAND20.'</option>
          </select>';
    echo '</div>';



    // -- ---------------------------- --
    // -- Day difference for newcomers --
    // -- Default: 60 ---------------- --
    // -- ---------------------------- --
    function _isSetNewcomerDays($aValue)
    {
      if (isset($_COOKIE['int_NewcomerDays']))
      {
        if ($_COOKIE['int_NewcomerDays'] == $aValue)
          return 'selected';
      }
      else
      {
        if (GRP_NEWCOMERDAYS180 == $aValue)
          return 'selected';
      }
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_daySongsNewcomer" class="textShadowBlur">'.LNG_DAYS_SONG_NEWCOMMER.'</label>';
      echo '<select id="mm_daySongsNewcomer" onchange="setNewcomerDays(this.value)" class="myMooOption">
              <option '._isSetNewcomerDays(GRP_NEWCOMERDAYS14).' value="'.GRP_NEWCOMERDAYS14.'">'.GRP_NEWCOMERDAYS14.'</option>
              <option '._isSetNewcomerDays(GRP_NEWCOMERDAYS30).' value="'.GRP_NEWCOMERDAYS30.'">'.GRP_NEWCOMERDAYS30.'</option>
              <option '._isSetNewcomerDays(GRP_NEWCOMERDAYS60).' value="'.GRP_NEWCOMERDAYS60.'">'.GRP_NEWCOMERDAYS60.'</option>
              <option '._isSetNewcomerDays(GRP_NEWCOMERDAYS90).' value="'.GRP_NEWCOMERDAYS90.'">'.GRP_NEWCOMERDAYS90.'</option>
              <option '._isSetNewcomerDays(GRP_NEWCOMERDAYS180).' value="'.GRP_NEWCOMERDAYS180.'">'.GRP_NEWCOMERDAYS180.'</option>
              <option '._isSetNewcomerDays(GRP_NEWCOMERDAYS365).' value="'.GRP_NEWCOMERDAYS365.'">'.GRP_NEWCOMERDAYS365.'</option>
          </select>';
    echo '</div>';



    // -- ----------------------- --
    // -- Search results as table --
    // -- ----------------------- --

    // -- because needed in javascript environment in clientside a cookie is required --

    function _isSetSearchResultsAsTable($aValue)
    {
      if (isset($_COOKIE['bool_SrchRsltsAsTable']) && $_COOKIE['bool_SrchRsltsAsTable'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_resultAsTable" class="textShadowBlur">'.LNG_SEARCH_RESULT_AS_TABLE.'</label>';
      echo '<select id="mm_resultAsTable" onchange="setSearchResultsAsTable(this)" class="myMooOption '._isSetSearchResultsAsTable(YES).'">
              <option value="'.NO.'">'.LNG_NO.'</option>
              <option '._isSetSearchResultsAsTable(YES).' value="'.YES.'">'.LNG_YES.'</option>
            </select>';
    echo '</div>';



    // -- --------------- --
    // -- cut blank --
    // -- --------------- --
    function _isSetCutBlank($aValue)
    {
      if (isset($_COOKIE['bool_cutBlank']) && $_COOKIE['bool_cutBlank'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_cutBlank" class="textShadowBlur">'.LNG_CUT_BLANK.'<label>';
      echo '<select id="mm_cutBlank" onchange="setCutBlank(this)" class="myMooOption '._isSetCutBlank(YES).'">
              <option '._isSetCutBlank(YES).' value="'.YES.'">'.LNG_YES.'</option>
              <option '._isSetCutBlank(NO).' value="'.NO.'">'.LNG_NO.'</option>
            </select>';
    echo '</div>';



    // -- --------------- --
    // -- tolerant search --
    // -- --------------- --
    function _isSetTolerantSearch($aValue)
    {
      if (isset($_COOKIE['int_DL_level']) && $_COOKIE['int_DL_level'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_tolerantSearch" class="textShadowBlur">'.LNG_TOLERANT_SEARCH.'<label>';
      echo '<select id="mm_tolerantSearch" onchange="setTolerantSearch(this.value)" class="myMooOption">
            <option '._isSetTolerantSearch(DLD_OFF).' value="'.DLD_OFF.'">'.$DLD[DLD_OFF].'</option>
            <option '._isSetTolerantSearch(DLD_LOW).' value="'.DLD_LOW.'">'.$DLD[DLD_LOW].'</option>
            <option '._isSetTolerantSearch(DLD_MEDIUM).' value="'.DLD_MEDIUM.'">'.$DLD[DLD_MEDIUM].'</option>
            <option '._isSetTolerantSearch(DLD_HIGH).' value="'.DLD_HIGH.'">'.$DLD[DLD_HIGH].'</option>
          </select>';
    echo '</div>';



    // -- --------------- --
    // -- case senstive --
    // -- --------------- --
    function _isSetCaseSensitive($aValue)
    {
      if (isset($_COOKIE['bool_caseSensitive']) && $_COOKIE['bool_caseSensitive'] == $aValue)
        return 'selected';
    }

    echo '<div style="margin: 2em 0em 1em 0em;">';
      echo '<label for="mm_caseSensitive" class="textShadowBlur">'.LNG_CASE_SENSTIVE.'<label>';
      echo '<select id="mm_caseSensitive" onchange="setCaseSensitivity(this)" class="myMooOption '._isSetCaseSensitive(YES).'">
              <option '._isSetCaseSensitive(NO).' value="'.NO.'">'.LNG_NO.'</option>
              <option '._isSetCaseSensitive(YES).' value="'.YES.'">'.LNG_YES.'</option>
            </select>';
    echo '</div>';


  ?>
</div>
<?php
  echo makeBottom();
  echo makeHTML_end();
