<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  require_once ('lib/utils.php');
  require_once ('api_mediabase.php');


  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin('onbeforeunload="return HandleOnClose()"');

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();


  function _makePreviewNaviHeader($addSongBlock)
  {
    $result = '<div style="opacity: 0.75;" class="commonHead">';
      $result .= '<span><a class="linkNaviNice" href="index.php">'.LNG_BACK_TO_ . PROJECT_NAME.'</a></span>';
      $result .=  '<div class="commonHeadText">'.$addSongBlock.'</div>';
    $result .=  '</div>';
    return $result;
  }


  // -- this top, because pt is required in javascript libs --
  if (!isset($_GET['song']))
  {
    // echo LNG_ERR_PARAM_MISSING;
    echo _makePreviewNaviHeader(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    $song = base64_decode($_GET['song']);
    $myMediabase = new class_mediabase(); // $myFeedback = new class_feedback();

    $in = $_GET['i'];
    $ti = $_GET['t'];
    $pt = $_GET['p'];
    $al = $_GET['l'];

    // -- calculate pure seconds for javascript using later --
    $playtimePlain = base64_decode($pt);
    $pos = strpos($playtimePlain, COLON);
    $minutes = substr($playtimePlain, 0, $pos);
    $seconds = substr($playtimePlain, $pos + 1);
    $duration = intval($minutes * 60 + $seconds);
    //echo   $duration;
?>
<script type="text/javascript">

  // -- some globals --
  previewPlayerRunning = false;



  // -- When preview player is playing leaving page will ask for confirmation --
  function HandleOnClose()
  {
    if (previewPlayerRunning)
    {
      if (!confirm("<?php echo LNG_LEAVE_PAGE_WHILE_PLAYING; ?>"))
        return false;
    }
  }



  // -- ----------------------- --
  // -- ----------------------- --
  // -- begin of preview player --
  // -- ----------------------- --
  // -- ----------------------- --


  // -- reset --
  function previewPlayerOnEnded()
  {
    //alert('huh1');

    previewPlayerRunning = false;

    window.clearInterval(previewPlayerInterval);

    // -- "rewind" is required to start again after finishing --
    $("previewPlayer").load();

    $("btn_ppPlay").style.visibility = "visible";
    $("btn_ppPause").style.visibility = "hidden";

    $("btn_ppRewind").style.visibility = "hidden";
    $("btn_ppForward").style.visibility = "hidden";

    $('songCurTime').textContent = "<?php echo $playtimePlain; ?>";

    //alert('huh2');
  }





  function previewPlayerProgress()
  {
    var previewPlayer = $("previewPlayer");
    var progressBar = $("progressBar");
    if (progressBar && previewPlayer)
    {
      // -- in this context, the audio element does not know the duration of the song for any reason --
      // -- it should be: percentage = Math.floor((previewPlayer.currentTime * 100) / previewPlayer.duration); but that does not work --
      // -- therefore (null problemo) it is taken the known duration by mediabase --
      // -- ok paddy, but why this inconvenient? --
//      var percentage = Math.floor((previewPlayer.currentTime * 100) / < ?php echo $duration; ?>); // should be: previewPlayer.duration
//      progressBar.value = percentage + 2; // -- + 2 empirical found out --
      // -- do simply --
      progressBar.value = previewPlayer.currentTime;

      // -- display current time as text --
      var songCurTime = $('songCurTime');
      if (songCurTime)
        songCurTime.innerHTML = secondsToMusicTime( previewPlayer.currentTime + 1 ); // -- + 1 empirical found out --
    }
  }

  function previewPlayerPlay()
  {
    var previewPlayer = $("previewPlayer");
    if (previewPlayer)
    {
      previewPlayer.play();
      previewPlayerRunning = true;

      previewPlayerInterval = window.setInterval("previewPlayerProgress()", 1000);

      // -- set eventlistener to be informed about ready --
      previewPlayer.addEventListener("ended", previewPlayerOnEnded, true); // -- this should work, but does not - why? --

      $("btn_ppPlay").style.visibility = "hidden";
      $("btn_ppPause").style.visibility = "visible";

      $("btn_ppRewind").style.visibility = "visible";
      $("btn_ppForward").style.visibility = "visible";
    }
  }

  function previewPlayerPause()
  {
    var previewPlayer = $("previewPlayer");
    if (previewPlayer)
    {
      previewPlayer.pause();
      previewPlayerRunning = false;
      window.clearInterval(previewPlayerInterval);

      // -- required at all to remove an eventlistener ?! --
      previewPlayer.removeEventListener("ended", previewPlayerOnEnded);

      $("btn_ppPlay").style.visibility = "visible";
      $("btn_ppPause").style.visibility = "hidden";

      $("btn_ppRewind").style.visibility = "hidden";
      $("btn_ppForward").style.visibility = "hidden";
    }
  }




// -- this still does not work on every browser --

  // -- Rewinds the audio file by 30 seconds --
  function previewPlayerRewind()
  {
    var tenPercent = (<?php echo $duration; ?> / 100) * 10;
      $("previewPlayer").currentTime -= tenPercent;
  }

   // -- Fast forwards the audio file by 30 seconds --
   function previewPlayerForward()
   {
     var tenPercent = (<?php echo $duration; ?> / 100) * 10;
       $("previewPlayer").currentTime += tenPercent;
   }




  // -- --------------------- --
  // -- --------------------- --
  // -- end of preview player --
  // -- --------------------- --
  // -- --------------------- --


</script>
<div style="padding: 0.5em;">
<?php
    // -- --------------------------- --
    // -- begin of rating and comment --
    $result = NULLSTR;
     if (
         (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
         ||
         (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
      )
    {
      if ($myMediabase->findSong($song)) // -- without basename() --
      {
        $curComment = $myMediabase->getComment() . ' (by: ' . $myMediabase->getCommentBy() . ')';
        $curRating = $myMediabase->getRating() . ' (by: ' . $myMediabase->getRatingBy() . ')';
      }
      else
      {
        $curComment = NULLSTR;
        $curRating = NULLSTR;
      }
      function _isRateSel5($aRate) {
        if ($aRate == RATE5)
          return BLANK.'selected'.BLANK; }
      function _isRateSel4($aRate) {
      if ($aRate == RATE4)
        return BLANK.'selected'.BLANK; }
      function _isRateSel3($aRate) {
        if ($aRate == RATE3)
          return BLANK.'selected'.BLANK; }
      function _isRateSel2($aRate) {
        if ($aRate == RATE2)
          return BLANK.'selected'.BLANK; }
      function _isRateSel1($aRate) {
        if ($aRate == RATE1)
          return BLANK.'selected'.BLANK; }
      $escapedFile = base64_encode(basename(str_replace('\'', '\\\'', $song)));
      $result .= '<span style="float: right;">';
        // -- comment --
        $curComment = ($myMediabase->getComment() != NULLSTR) ? $myMediabase->getComment() : LNG_NOCOMMENT_DO;
        $result .= '<span style="border-style: outset; border-width: 1px; margin-left: 2em; padding: 0.5em 0.75em 0.5em 0px; border-radius: 3px;">';
          $result .= '<a class="playListItemR comment" href="javascript:songSetComment(\''.$escapedFile.'\');">';
          $result .= '<nobr><span id="curComment" style="font-style: italic; font-weight: normal; margin-left: 0.5em;">'.$curComment.'</span></nobr></a>';
        $result .= '</span>';
        // -- rating --
        $result .= '<select class="playListItemR rating" style="margin-right: 2.5em; margin-top: 1em;" onchange="songSetRating(\''.$escapedFile.'\', this.value);">';
        $curRating = $myMediabase->getRating();
        if ($curRating != RATE1 && $curRating != RATE2 && $curRating != RATE3 && $curRating != RATE4 && $curRating != RATE5)
          $result .= '<option value="">'.LNG_NORATING_VOTE.'</option>';
        $result .= '
                      <option value="'.RATE5.'" '._isRateSel5($myMediabase->getRating()).'>'.RATE5_HUMAN.'</option>
                      <option value="'.RATE4.'" '._isRateSel4($myMediabase->getRating()).'>'.RATE4_HUMAN.'</option>
                      <option value="'.RATE3.'" '._isRateSel3($myMediabase->getRating()).'>'.RATE3_HUMAN.'</option>
                      <option value="'.RATE2.'" '._isRateSel2($myMediabase->getRating()).'>'.RATE2_HUMAN.'</option>
                      <option value="'.RATE1.'" '._isRateSel1($myMediabase->getRating()).'>'.RATE1_HUMAN.'</option>
                    </select>';
        $result .= '</span>';
      //is used later unset($myMediabase);
    }
    // -- end of rating and comment --
    // -- ------------------------- --

       $progressbar = '<div style="margin-top: 1em;">
                        <span id="songCurTime" style="color: white; margin-right: 0.2em;">0:00</span>
                          <progress id="progressBar" min="0" max="'.$duration.'" value="0"></progress>
                        <span style="color: white; margin-left: 0.2em;">'.base64_decode($pt).'</span>
                      </div>';

      // -- add to playlist --
      $addSongBlock = '<div style="float: left; margin-top: 1em; width: 99.9%;">
                        <div style="margin-top: 1.5em;">'.

                          $result.
                          '<a class="grpLstItmL" style="text-align: center;" href="javascript:songAdd(\''.$_GET['song'].'\',\''.$in.'\',\''.$ti.'\',\''.$pt.'\',\''.$al.'\', \''.cntSubsituteCriticalJSChars(base64_decode($ti)).'\')" >'.
                            '<div class="grpLstContL commonStyleBGcolor">
                              <div>
                                <div class="listItemInterpret">'.base64_decode($in).'</div>
                                <div class="listItemTitle">'.base64_decode($ti).'</div>
                              </div>'.
                              $progressbar.
                            '</div>
                          </a>
                        </div>
                       </div>';



    // -- head --
    echo _makePreviewNaviHeader($addSongBlock);



    if (!file_exists($cfg_absPathToSongs . $song))
    {
      echo LNG_ERR_NOSONGINFILESYSTEM;
    }
    else
    {
      // -- songs with '&' causes trouble, because the filesystem needs the '&', but the HTML player the &amp; --
      // -- clearly, the '&' is the divider for URL params --
      // -- in combination with songDisp.php the issue occurs, because it needs the right filename for readfile() --
      // -- here comes the solution: --
      // -- and if ever problems occur, simply use the &base64 parameter and encode the song URL before transmit --
      $songHTML = str_replace('&', '%26', $song);

      $songLink = 'lib/songDisp.php?song='.$songHTML.'&hash='.md5($_SESSION['str_nickname']);



      // -- ----------------------- --
      // -- begin of preview player --
      // -- ----------------------- --

      $mobileSuffix = (cntIsMobile()) ? 'M' : NULLSTR;
      $controls = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true) ? 'controls' : NULLSTR;

      echo '
        <div style="float: right; padding: 1em;">
          <audio id="previewPlayer" src="'.$songLink.'" '.$controls.' preload="none">
            '.LNG_ERR_NOAUDIOELEM.'
          </audio>
          <img id="btn_ppPlay"    src="res/play'.$mobileSuffix.'.png"     onclick="previewPlayerPlay()"    alt="play"    />
          <img id="btn_ppPause"   src="res/pause'.$mobileSuffix.'.png"    onclick="previewPlayerPause()"   alt="pause"   style="visibility: hidden;" />
          <img id="btn_ppRewind"  src="res/rewind'.$mobileSuffix.'.png"   onclick="previewPlayerRewind()"  alt="rewind"  style="visibility: hidden; margin-left: 1em;s" />
          <img id="btn_ppForward" src="res/forward'.$mobileSuffix.'.png"  onclick="previewPlayerForward()" alt="forward" style="visibility: hidden;" />
       </div>
      ';
      // onended="previewPlayerOnEnded()"
      // display: block | none

      // -- --------------------- --
      // -- end of preview player --
      // -- --------------------- --
    }


    // -- ---------------------- --
    // -- begin of cover N lyric --
    // -- ---------------------- --

    $songInfo = $myMediabase->getSongInfo($song);
    echo '<style type="text/css">
        body {
          background-image: url("'.cntGetAlbumPicURL($song, $songInfo['l']).'");
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-position: center center;
          background-size: cover;
        }
        h1 {
          font-family: "Yanone Kaffeesatz";
          font-size: 700%;
          margin: 5% 5% 10px 5%;
          text-shadow: 1px 1px 0 #fe3;
        }
        h2 {
          margin-left: 5%;
          text-shadow: 1px 1px 0 #fe3;
        }
        #lyric {
          width: 50%;
          max-width: 600px;
          color: white;
          text-shadow: 2px 1px 0 #333;
          margin-left: 5%;

          margin-top: 1em;
          float: left;
        }
      </style>';
    unset($songInfo);

    $pathToSong = dirname($cfg_absPathToSongs . $song) . SLASH;
    $lyricPrep = $pathToSong . basename($song, DOT . $cfg_musicFileType) . DOT . $cfg_textFileType;
    $lyric = (file_exists($lyricPrep)) ? '<pre>' . file_get_contents($lyricPrep)  . '</pre>' : LNG_NOLYRIC_AVAILABLE; // file_get_contents($lyricPrep)

    echo '<article id="lyric">'.$lyric.'</article>';

    // -- -------------------- --
    // -- end of cover N lyric --
    // -- -------------------- --

    unset($myMediabase);
  }

?>
</div>
<?php
  echo makeBottom();
  echo makeHTML_end();
