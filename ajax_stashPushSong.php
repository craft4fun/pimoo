<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_playlist.php';

  $msg = NULLSTR;

  if (    !isset($_GET['index'])
       || !isset($_GET['wishedBy'])

       || !isset($_GET['a'])
       || !isset($_GET['i'])
       || !isset($_GET['t'])
       || !isset($_GET['p'])
       || !isset($_GET['l'])
     )
  {
    $msg = LNG_ERR_PARAM_MISSING;
  }
  else
  {
    $index = $_GET['index'];
    $wishedBy = $_GET['wishedBy'];
    $a = $_GET['a'];
    $i = $_GET['i'];
    $t = $_GET['t'];
    $p = $_GET['p'];
    $l = $_GET['l'];

    if (!isset($_SESSION['str_nickname']))
    {
      $msg = LNG_ERR_UNKNOWN_FUNNY;
    }
    else
    {
      $nickname = $_SESSION['str_nickname'];
      // -- for moving a song from queue to stash, it must be deleted from queue. And that's only possible for an admin. So we must fake admin rights --
      $adminFake = ($nickname == $wishedBy) ? true : false;

      $myPlaylist = new class_playlist();
      if (!$myPlaylist->stashPushSong($index, $nickname, base64_decode($a), base64_decode($i), base64_decode($t), $p, base64_decode($l)))
      {
        $msg = LNG_ERR_UNKNOWN_FUNNY;
      }
      else
      {
        // -- in this case the song has been stashed successfully and can be deleted from playlist --
        if (!$myPlaylist->removeSong($index, $adminFake))
        {
          $msg = $myPlaylist->getLastMsg();
        }
        else
        {
          $msg = LNG_STASH_PUSH_TO;
        }
      }

      //eventLog('removed from playlist [index]: ' . $_GET['index']);
      unset($myPlaylist);
    }
  }

  echo $msg;
