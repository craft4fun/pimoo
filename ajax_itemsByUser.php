<?php
  // -- special piMoo stuff --
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('lib/utils.php');
  require_once ('lib/class_cache.php');
  require_once ('api_layout.php');


  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  // -- is it allowed to add titles as guest ? --
  if ($cfg_allowExternControl || $admin)
  {
    // -- standard check, if the required parameter is given --
    if (!isset($_GET[USER]))
    {
      echo cntErrMsg(LNG_ERR_PARAM_MISSING);
    }
    else
    {
      // -- core of this file --
      $caseSensitive = (isset($_COOKIE['bool_caseSensitive']) && $_COOKIE['bool_caseSensitive'] == YES) ? true : false; // -- this is still required for cache handling --
      $user = $_GET[USER];

      $cutBlank = (isset($_COOKIE['bool_cutBlank'])) ? $_COOKIE['bool_cutBlank'] : YES;

      $offset = (isset($_GET['offset'])) ? $_GET['offset'] : NULLINT;
      // -- limit to positive offset --
      if ($offset < NULLINT)
        $offset = NULLINT;

      //$viewForm = (isset($_COOKIE['bool_SrchRsltsAsTable']) && $_COOKIE['bool_SrchRsltsAsTable'] == YES) ? CACHE_RSLT_TABLE : CACHE_RSLT_BLOCKS;
      $viewForm = CACHE_RSLT_BLOCKS;
      if (isset($_COOKIE['bool_SrchRsltsAsTable']))
      {
        if ($_COOKIE['bool_SrchRsltsAsTable'] == YES)
          $viewForm = CACHE_RSLT_TABLE;
      }

      $output = NULLSTR;

      // -- cache handling --
      $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
      $myCache->setCachePath($cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_USER . $user . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $cfg_maxSearchResultsPerPage . CACHE_RSLT_VIEWFORM . $viewForm . CACHE_RSLT_LANG . $lang . CACHE_RSLT_CASESENSITIVITY . $caseSensitive . CACHE_RSLT_CUTBLANK . $cutBlank);

      // -- take it from cache --
      if ($myCache->getIsCacheFileAlive())
      {
        echo '<div style="text-align: center; color: grey;"><nobr>... ' . LNG_LOADED_FROM_CACHE . ' ...</nobr></div>';
        echo file_get_contents($myCache->getCacheFile());
        unset($myCache);
      }

      // -- otherwise show it native and build cache --
      else
      {
        //unset($myCache);

        // -- init navigation --
        $resultsPrevious = '<div class="resultNavi" style="float: left;">
                              <div class="grpLstItmL" title="'.LNG_PREVIOUS_RESULTS.'">
                                <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$user.'\', '.intval($offset - intval($cfg_maxSearchResultsPerPage*5)).');">'.HTML_LESS.HTML_LESS.'</a>
                                <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$user.'\', '.intval($offset - $cfg_maxSearchResultsPerPage).');">'.HTML_LESS.'</a>
                              </div>
                            </div>';
        $resultsNext = '<div class="resultNavi" style="float: right;">
                          <div class="grpLstItmL" title="'.LNG_NEXT_RESULTS.'">
                            <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$user.'\', '.intval($offset + $cfg_maxSearchResultsPerPage).');">'.HTML_GREATER.'</a>
                            <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$user.'\', '.intval($offset + intval($cfg_maxSearchResultsPerPage*5)).');">'.HTML_GREATER.HTML_GREATER.'</a>
                          </div>
                        </div>';

        // -- ------------------------------- --
        // -- begin of the the very main core --
        // -- ------------------------------- --
        //moved upper: $DL_Level = (isset($_COOKIE['int_DL_level'])) ? $_COOKIE['int_DL_level'] : 0;

        require_once ('api_mediabase.php');
        $myMediaBase = new class_mediaBase();
        $tmp = $myMediaBase->getSearchResultByUser($user, $offset, $cfg_maxSearchResultsPerPage, $caseSensitive);
        $count = count($tmp);

        // -- upper navigation --
        $output .= '<div style="width: 100%; float: left;">';
        if ($cfg_maxSearchResultsPerPage > NULLINT)
        {
          if ($count > NULLINT && $count >= $cfg_maxSearchResultsPerPage)
            $output .= $resultsNext;
          if ($offset > NULLINT)
            $output .= $resultsPrevious;
          else
            $output .= '<div class="resultNavi" style="float: left;"></div>';
        }
        $output .= '</div>';

        // -- these are the actually results --
        $output .= makeResultItems($tmp);

        // -- lower navigation --
        $output .= '<div style="clear: both;">';
          if ($cfg_maxSearchResultsPerPage > NULLINT)
          {
            if ($count > NULLINT && $count >= $cfg_maxSearchResultsPerPage)
              $output .= $resultsNext;
            if ($offset > NULLINT)
              $output .= $resultsPrevious;
            else
              $output .= '<div class="resultNavi" style="float: left;"></div>';
          }
        $output .= '</div>';

        // -- ---------------------------------------------- --
        // -- beg of offer possibilty to add all to playlist --
        if ($admin)
        {
          //-- get nickame --
          $wishedBy = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : $_SERVER['REMOTE_ADDR'];
          // -- result file reference --
          $resultFile = $cfg_cachePath . 'lastResult' . UNDSCR . $myCache->getCacheFile(true);
          // -- use api_playlist --
          require_once ('api_playlist.php');
          $myPlaylist = new class_playlist;
          $myPlaylist::stat_lastResult_pushSongs($resultFile, $tmp, $wishedBy);
          // -- output "button" --
          $output .= '<a class="linkNice grpLstContR" style="float: right; margin-top: 1em;" href="javascript:addResultToPlaylist(\''.$resultFile.'\')">'.LNG_ADD_ALL_TO_PLAYLIST.'</a>';
        }
        // -- end of offer possibilty to add all to playlist --
        // -- ---------------------------------------------- --

        unset($tmp);
        unset($myMediaBase);
        // -- ----------------------------- --
        // -- end of the the very main core --
        // -- ----------------------------- --


        // -- "execute" normal way --
        echo $output;
        // -- build cache for next time --
        $myCache = new class_outputCache();
        $myCache->setCachePath($cfg_cachePath);
        $myCache->setCacheFile(CACHE_ITEMS_BY_USER . $user . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $cfg_maxSearchResultsPerPage . CACHE_RSLT_VIEWFORM . $viewForm . CACHE_RSLT_LANG . $lang . CACHE_RSLT_CASESENSITIVITY . $caseSensitive . CACHE_RSLT_CUTBLANK . $cutBlank);
        $myCache->putCacheContent($output);
        unset($myCache);
        unset($output);
      }
    }
  }
  // that is done in frontend
  //else
    //echo '<div style="margin: 5px;">the song navigation has been disabled by admin</div>';

