#!/usr/bin/php
<?php
  require ('defines.inc.php');
  require ('keep/config.php');
  require ('lib/utils.php');
  require ('api_player.php');

  // -- needed, if it is to be controlled by runlevel scripts --
  define ('PLAYERSCRIPT', 'cli_player.php');

  $path = getCLIpathDependOnConfig($cfg_isDocker, $cfg_system, __DIR__);
  // -- finally set path - anyhow --
  chdir($path);


  // -- stop player before, that's clean --
  $myPlayer = new class_player();
  if ($myPlayer->getPlayerState() == PS_PLAYING)
    $myPlayer->cmdTurnOff();

  // -- called by external trigger -> mouse event --
  util_systemShutdown();
