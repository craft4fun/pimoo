<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  if (!isset($_GET['value']))
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    if (setcookie('int_RandSrchResults', $_GET['value'], strtotime('+365 days')))
      $msg = LNG_RANDOM_SEARCH_RESULTS . ARROW . '<span style="font-weight: bold;">' . $_GET['value'] . '</span>';
  }

  echo $msg;