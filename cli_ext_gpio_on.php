#!/usr/bin/php
<?php
  require 'defines.inc.php';
  require 'keep/config.php';
  require 'lib/utils.php';
  require 'api_player.php'; // class_player

  // -- needed, if it is to be controlled by runlevel scripts --
  define ('PLAYERSCRIPT', 'cli_player.php');

  $path = getCLIpathDependOnConfig($cfg_isDocker, $cfg_system, __DIR__);
  // -- finally set path - anyhow --
  chdir($path);

  // -- ----------------------------- --
  // -- turn ON HiFi over gpio relais --
  // -- ----------------------------- --

  // -- pigpio since 2022 --
  //system("pigs w $cfg_GPIO_HiFi 1");
  $myPlayer = new class_player();
  $myPlayer->GPIO_ON();

