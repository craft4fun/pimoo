<?php
  /**
   * admin_controlPlayerForward
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_controlPlayerForward.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_player.php';

  require_once '_coreBeg.php';

  $json[JSON_RC_ADDINFO] = NULLSTR;

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $json[JSON_RC] = JSON_RC_NO_ADMIN;
  }
  else
  {
    $myPlayer = new class_player(TWODIRUP);

    $seconds = (isset($_GET['seconds'])) ? intval($_GET['seconds']) : 30;
    if (!$myPlayer->cmdForward($seconds))
    {
      $json[JSON_RC] = JSON_RC_UNDEF_ERR;
      $json[JSON_RC_ADDINFO] .= $myPlayer->getLastMsg();
    }
    else
      $json[JSON_RC] = JSON_RC_OK;

    unset($myPlayer);
  }

  require_once '_coreEnd.php';
