<?php
  /**
   * getSongsByRandom
   *
   * standard call
   * http://localhost/piMoo/api/1/getSongsByRandom.php?a=R2
   *
   * &a= string -> amount of songs returned by random (MUST have the prefix: "R")
   *
   *
   * examples of JSON response code
   * {"RC":0,"byRandom":[{"a":"Albums\/Genesis\/Selling England by the pound\/Genesis - (06) After The Ordeal.mp3","i":"Genesis","t":"After The Ordeal","p":"4:16","l":"Selling England By The Pound"},{"a":"Singles\/Secret Service - Ten oclock postman.mp3","i":"Secret Service","t":"Ten o'clock postman","p":"3:32","l":""}]}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_mediabase.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  if (!isset($_GET['a']))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/getSongsByRandom.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $json[JSON_RC] = JSON_RC_OK;

    $myMediaBase = new class_mediaBase(TWODIRUP);
    $results = $myMediaBase->getRandomSongs(intval(substr($_GET['a'], 1)));

    //$json[JSON_RC] = $results[0];

    $data = array();
    foreach ($results as $result)
    {
      $tmp = array('a' => (string)$result['a'], 'i' => (string)$result['i'], 't' => (string)$result['t'], 'p' => (string)$result['p'], 'l' => (string)$result['l']);
      $data[] = $tmp;
    }
    $json['data'] = $data;

    unset($myMediaBase);
  }

  $intermediateResult =  json_encode($json);
  echo $intermediateResult;

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
