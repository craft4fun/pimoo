<?php
  /**
   * admin_dropNickname
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_dropNickname.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();
  // -- normaly $_SESSION['str_nickname'] would be enough, but this way I can delete other nicks by admin area

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $json[JSON_RC] = JSON_RC_NO_ADMIN;
  }
  else
  {
    if (!isset($_GET['nickname']))
    {
      $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
    }
    else
    {
      $myNickname = new class_nickname(TWODIRUP);
      if ($myNickname->dropForeign($_GET['nickname']))
      {
        // -- iMoo --
        $iMooPl = TWODIRUP . $cfg_tmpPathStatic . SLASH . 'iMoo' . SLASH . $_GET['nickname'] . DOT . 'json';
        if (file_exists($iMooPl))
        {
          unlink($iMooPl);
        }
        $json[JSON_RC] = JSON_RC_OK;
      }
      unset($myNickname);
    }
  }


  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks (only for write access) --
  usleep(ANTI_DOS_DELAY);
