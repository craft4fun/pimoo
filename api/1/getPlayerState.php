<?php
  /**
   * getPlayerState
   *
   * standard call
   * http://localhost/piMoo/api/1/getPlayerState.php
   *
   *
   * examples of JSON response code
   * {"RC":0,"state":"playing"}
   *
   * 0 -> JSON_RC_OK
   *
   * possible player states as string
   * [unknown|off|paused|playing]
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_player.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  $myPlayer = new class_player(TWODIRUP);

  $json[JSON_RC] = JSON_RC_OK;
  $json[JSON_RC_STATE] = $myPlayer->getPlayerState();

  unset($myPlayer);

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
