<?php
  /**
   * songAdd
   *
   * standard call
   * http://localhost/piMoo/api/1/songAdd.php?a=Albums/Metallica/Ride the lightning/Metallica - (03) For Whom The Bell Tolls.mp3&i=Metallica&t=For Whom The Bell Tolls&p=5:25&w=devel&l=Ride The Lightning
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once '_coreBeg.php';

  if (
         !isset($_GET['a'])
      || !isset($_GET['i'])
      || !isset($_GET['t'])
      || !isset($_GET['p'])
      || !isset($_GET['w'])
      || !isset($_GET['l'])
    )
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING, TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    include_once '../../keep/config.php';
    require_once '../../lib/utils.php';
    include_once '../../api_playlist.php';

    $base64 = (isset($_GET['base64'])) ? true : false;

    $toTop = (isset($_GET['toTop'])) ? true : false;


    $myPlaylist = new class_playlist(TWODIRUP);

    // TODO paddy This should come by session or in future by token
    if ($base64)
      $myPlaylist->setWishedBy(base64_decode($_GET['w']));
    else
      $myPlaylist->setWishedBy($_GET['w']);


    // -- is admin? --
    $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

    if (!isset($_SESSION['str_nickname']))
    {
      eventLog('Partypooper/ nickname is required in api/1/getCurrentPlaylist.php', TWODIRUP);
      $json[JSON_RC] = JSON_RC_PARTYPOOPER;
    }
    else
    {
      if ($base64)
      {
        $myPlaylist->addSong(base64_decode( $_GET['a'] ), base64_decode($_GET['i']), base64_decode($_GET['t']), $_GET['p'], base64_decode($_GET['l']), $admin);
        $msg = $myPlaylist->getLastMsg();
      }
      else
      {
        $myPlaylist->addSong(( $_GET['a'] ), ($_GET['i']), ($_GET['t']), $_GET['p'], ($_GET['l']), $admin);
        $msg = $myPlaylist->getLastMsg();
      }



      // -- if wished set top at all then do it and check ... --
      $checkToTop = false;
      if ($toTop)
      {
        if ($admin)
          $checkToTop = $myPlaylist->moveBottomTopSong($admin);
      }
      // -- ... otherwise its okay anyway --
      else
        $checkToTop = true;


      eventLog($myPlaylist->getLastMsg() . COLON . BLANK .  $_GET['t'], TWODIRUP);
      if ($msg !== false && $checkToTop === true)
        $json[JSON_RC] = JSON_RC_OK;
      else
      {
        $json[JSON_RC] = JSON_RC_UNDEF_ERR;
        $json[JSON_RC_ADDINFO] = $myPlaylist->getLastMsg();
      }

      unset($myPlaylist);
    }
  }

  require_once '_coreEnd.php';
