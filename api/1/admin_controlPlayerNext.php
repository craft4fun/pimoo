<?php
  /**
   * admin_controlPlayerNext
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_controlPlayerNext.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_player.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $json[JSON_RC] = JSON_RC_NO_ADMIN;
  }
  else
  {
    $myPlayer = new class_player(TWODIRUP);

    if (!$myPlayer->cmdNext())
    {
      $json[JSON_RC] = JSON_RC_UNDEF_ERR;
      $json[JSON_RC_ADDINFO] = $myPlayer->getLastMsg();
    }
    else
    {
      $json[JSON_RC] = JSON_RC_OK;
    }

    unset($myPlayer);

  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
