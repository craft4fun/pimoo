<?php
  /**
   * admin_controlPlayerStop
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_controlPlayerStop.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_player.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $json[JSON_RC] = JSON_RC_NO_ADMIN;
  }
  else
  {
    $myPlayer = new class_player(TWODIRUP);

    $json[JSON_RC_ADDINFO] = LNG_TRYING_TO_STOP_PLAYER;

     // -- turn on the cli_player.php and cli_common.php --
    $json[JSON_RC_ADDINFO] .= $myPlayer->cmdTurnOff();

    // -- turn on HiFi via hardware - relais --
    $json[JSON_RC_ADDINFO] .= $myPlayer->GPIO_OFF();

    unset($myPlayer);
    $json[JSON_RC] = JSON_RC_OK;
  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);

