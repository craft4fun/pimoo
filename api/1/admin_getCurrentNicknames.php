<?php
  /**
   * admin_getCurrentNicknames
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_getCurrentNicknames.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":0,"currentNicknames":[{"nickname":"tester","ip":"192.168.0.111\n"},{"nickname":"test","ip":"::1"}]}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $json[JSON_RC] = JSON_RC_NO_ADMIN;
  }
  else
  {
    $nicknames = TWODIRUP . $cfg_tmpPath . 'nicknames/';
    if (is_dir($nicknames))
    {
      $json[JSON_RC] = JSON_RC_OK;

      $files = scandir($nicknames);
      $files = array_reverse($files); // -- change order ascending --
      foreach($files as $nick)
      {
        if (substr($nick, 0, 1) != DOT) // -- show no hidden file or dir like .svn :o)
        {
          $tmp = array('nickname' => $nick, 'ip' => file_get_contents(TWODIRUP . $cfg_tmpPath . 'nicknames/' . $nick));
          $json['currentNicknames'][] = $tmp;
        }
      }
    }
  }


  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks (only for write access) --
  usleep(ANTI_DOS_DELAY);

