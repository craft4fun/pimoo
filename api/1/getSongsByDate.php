<?php
  /**
   * getSongsByDate
   *
   * standard call
   * http://localhost/piMoo/api/1/getSongsByDate.php?d=N180
   * http://localhost/piMoo/api/1/getSongsByDate.php?d=R180&o=50&m=100
   *
   * &d= string -> something like N180. That means songs which comes in in the last 180 days.
   * &o= integer -> offset
   * &m= integer -> max search results
   *
   *
   * examples of JSON response code
   * {"RC":0,"bySearch":[{"a":"Singles\/John Farnham - You\u00b4re the voice.mp3","i":"John Farnham","t":"You&#180;re the voice","p":"4:49","l":""},{"a":"Singles\/John Farnham - Thats freedom.mp3","i":"John Farnham","t":"That's freedom","p":"4:19","l":""}]}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 1 -> JSON_RC_OK_BYCACHE
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../lib/class_cache.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  if (!isset($_GET['d']))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/getSongsByDate.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $days     = (isset($_GET['d'])) ? $_GET['d'] : GRP_NEWCOMERDAYS365;
    $offset   = (isset($_GET['o'])) ? $_GET['o'] : NULLINT;
    $max      = (isset($_GET['m'])) ? $_GET['m'] : $cfg_maxSearchResultsPerPage;

    // -- cache handling --
    $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
    $myCache->setCachePath(TWODIRUP . $cfg_cachePath);
    $myCache->setCacheFile(CACHE_ITEMS_BY_DATE . $days . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $max . CACHE_RSLT_VIEWFORM . CACHE_RSLT_JSON . CACHE_RSLT_LANG);

    // -- take it from cache --
    if ($myCache->getIsCacheFileAlive())
    {
      //-- add an info for this would mean to decode add info and encode again: where's the speed enhancement, huh? --
      // therefore NO: $json[JSON_RC_ADDINFO] = JSON_RC_BYCACHE;
      $output = file_get_contents($myCache->getCacheFile());
      unset($myCache);
    }
    // -- otherwise show it native and build cache --
    else
    {
      unset($myCache);

      // -- ------------------------------- --
      // -- begin of the the very main core --
      // -- ------------------------------- --

      require_once '../../api_mediabase.php';
      $myMediaBase = new class_mediaBase(TWODIRUP);

      // -- calculate the days --
      $days2 = intval(substr($days, 1)); // -- N60 -> 60 --
      $daysAsSeconds = $days2 * 86400; // 24 * 60 * 60;
      $diff = time() - $daysAsSeconds;  // -- today minus x days --
      $past = date('Ymd', $diff);
      $today = date('Ymd', time());
      $results = $myMediaBase->getSongsByDate($past, $today, $offset, $max);


      $json[JSON_RC] = JSON_RC_OK;

      $data = array();
      foreach ($results as $result)
      {
        $tmp = array('a' => (string)$result['a'], 'i' => (string)$result['i'], 't' => (string)$result['t'], 'p' => (string)$result['p'], 'l' => (string)$result['l']);
        $data[] = $tmp;
      }
      $json['data'] = $data;

      unset($myMediaBase);

      $output = json_encode($json);

      // -- build cache for next time --
      $myCache = new class_outputCache();
      $myCache->setCachePath(TWODIRUP . $cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_DATE . $days . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $max . CACHE_RSLT_VIEWFORM . CACHE_RSLT_JSON . CACHE_RSLT_LANG);
      $myCache->putCacheContent($output);
      unset($myCache);
    }

    // -- "execute" normal way --
    echo $output;
    unset($output);
  }

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
