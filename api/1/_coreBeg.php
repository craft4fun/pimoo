<?php

  cntCreateJSONheader();
  cntCreateCORSheader();

  // -- prepare api response --
  $json = array();



  require_once '../../keep/accessTokens.php';

  function getIsGrantedByAccessToken($aAccessToken)
  {
    global $accessTokens;

    $result = false;

    if (in_array($aAccessToken, $accessTokens))
      $result = true;


    // NEW version of tokens prepared here ...



    return $result;
  }

  function getNicknameByAccessToken($aAccessToken)
  {
    global $accessTokens;

    /* NEW version of tokens prepared here ...
    foreach ($accessTokens as $key => $value)
    {
      if ($aAccessToken == $key)
      {
        $nick = $value;
        break;
      }
    }
    return $nick; */

    return array_search($aAccessToken, $accessTokens);
  }
