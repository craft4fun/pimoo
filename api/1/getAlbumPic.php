<?php
  /**
   * getAlbumPic
   *
   * standard call
   * http://localhost/piMoo/api/1/getAlbumPic.php?song=aSong
   * http://localhost/piMoo/api/1/getAlbumPic.php?base64&song=' + aSong
   *
   *
   * examples of JSON response code
   * {"RC":0,"state":false}
   *
   * 0 -> JSON_RC_OK
   *
   */



  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once '../../api_mediabase.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  //if (isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR)
  if (true)
  {
    if (isset($_GET['song']))
    {
      // -- switches if the URL comes base64 encoded and has to be decoded before --
      $song = (isset($_GET['base64'])) ? base64_decode($_GET['song']) : $_GET['song'];


      // -- first attempt: take the song path and file name --

      $pathToSong = dirname($song) . SLASH; //old: $cfg_absPathToSongs . $aSong

      $cover = $pathToSong . basename($song, DOT . $cfg_musicFileType) . DOT . $cfg_pictureFileType;
      if (file_exists($cfg_absPathToSongs . $cover))
      {
//         $cover = str_replace('&', '%26', $cover);
//         $picLink = '<img class="albumPic" src="../lib/picDisp.php?pic='.$cover.'&hash='.md5($_SESSION['str_nickname']).'" />';

        $json[JSON_RC] = JSON_RC_OK;

        // -- return the requested file name as an info --
        $json['pic'] = $cover;

        // -- build the requested picture base64 encoded --
        $picRAWContent = @file_get_contents($cfg_absPathToSongs . $cover);
        if ($picRAWContent !== false)
        {
          $base64EncodedPic = base64_encode( $picRAWContent );
          $json['base64'] = $base64EncodedPic;
        }
        else
          $json[JSON_RC] = JSON_RC_FILE_NOT_FOUND_OR_WRITEPROTECTED;


      }
      else
      {
        // -- second attempt: take the album name --
        $myMediaBase = new class_mediaBase(TWODIRUP);
        $info = $myMediaBase->getSongInfo($song); //, true

        // -- check for the album name --
        $cover = $pathToSong . $info['l'] . DOT . $cfg_pictureFileType;

        // -- finally take something --
        if (file_exists($cfg_absPathToSongs . $cover))
        {
//           $cover = str_replace('&', '%26', $cover);
//           $picLink = '<img class="albumPic" src="../lib/picDisp.php?pic='.$cover.'&hash='.md5($_SESSION['str_nickname']).'" />';

          $json[JSON_RC] = JSON_RC_OK;

          // -- return the requested file name as an info --
          $json['pic'] = $song;

          // -- build the requested picture base64 encoded --
          $picRAWContent = @file_get_contents($cfg_absPathToSongs . $cover);
          if ($picRAWContent !== false)
          {
            $base64EncodedPic = base64_encode( $picRAWContent );
            $json['base64'] = $base64EncodedPic;
          }
          else
            $json[JSON_RC] = JSON_RC_FILE_NOT_FOUND_OR_WRITEPROTECTED;

        }
        else
          $json[JSON_RC] = $info['l'];

        unset($myMediaBase);
      }
    }
    else
      $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
    $json[JSON_RC] = LNG_ERR_UNKNOWN_FUNNY;

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks (only for write access) --
  usleep(ANTI_DOS_DELAY);

