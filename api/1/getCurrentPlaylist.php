<?php
  /**
   * getCurrentPlaylist
   *
   * standard call
   * http://localhost/piMoo/api/1/getCurrentPlaylist.php
   *
   *
   * examples of JSON response code
   * {"RC":0, "onAir":{"id":"0","a":"Albums\/Genesis\/Invisible touch\/Genesis - (06) Domino.mp3","i":"Genesis","t":"Domino","p":"10:45","w":"devel","l":"Invisible Touch"},"playlist":[{"id":"1","a":"Albums\/Coldplay\/A rush of blood to the head\/Coldplay - (04) The scientist.mp3","i":"Coldplay","t":"The Scientist","p":"5:10","w":"devel","l":"A rush of blood to the head"},{"id":"2","a":"Albums\/Metallica\/Metallica\/Metallica - (02) Sad But True.mp3","i":"Metallica","t":"Sad But True","p":"5:25","w":"devel","l":"Metallica"}]}
   * {"RC":3}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 3 -> JSON_RC_PARTYPOOPER
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_playlist.php';
  require_once '../../api_layout.php';

  require_once '_coreBeg.php';

  $json[JSON_RC_ADDINFO] = NULLSTR;
  $skipToken = false;
  $json[JSON_RC_ADMIN] = false;

  // -- ---------------------------- --
  // -- -1- is admin by password ? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if ($admin)
  {
    $json[JSON_RC_ADMIN] = true;
    $json[JSON_RC_ADDINFO] .= '<admin by password>';
    $skipToken = true;
  }
  else
  {
    // -- ----------------------------------------------------------------------------- --
    // -- -2- or by token ? -> order is important, must be after admin password check --
    if (!empty($_GET['token']) && !$skipToken)
    {
      $token = $_GET['token'];
      if (!getIsGrantedByAccessToken($token))
      {
        $json[JSON_RC_ADDINFO] .= '<provided a token, but it is not valid>';
      }
      else
      {
        $_SESSION['bool_admin'] = true;
        $json[JSON_RC_ADMIN] = true;
        $json[JSON_RC_ADDINFO] .= '<admin by token>';

        $current = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : NULLSTR;
        $nick = getNicknameByAccessToken($token);
        $myNickname = new class_nickname(TWODIRUP);
        if (!$myNickname->push($nick, $current))
          $json[JSON_RC] = LNG_TAKE_ANOTHER_NICK;
        else
          $json[JSON_RC] = JSON_RC_OK;

      }
    }
  }


  // -- player state --
  $myPlayer = new class_player(TWODIRUP);
  $json[JSON_RC_STATE] = $myPlayer->getPlayerState();
  unset($myPlayer);


  // -- --------------- --
  // -- beg of playlist --
  if (!isset($_SESSION['str_nickname']))
  {
    eventLog('Partypooper/ nickname is required in api/1/getCurrentPlaylist.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_PARTYPOOPER;
    //$json[JSON_RC_ADDINFO] = 'party pooper!';
  }
  else
  {
    $json[JSON_RC] = JSON_RC_OK;

    $myPlaylist = new class_playlist(TWODIRUP);
    $songs = $myPlaylist->getWholePlaylist();

    $i = 0;
    foreach ($songs as $song)
    {
      if ($i == 0)
      {
        $onAirRealRemainTime = parseSecondsToHumanMinSecString($myPlaylist->getPlayerRemainTime());
        if ($onAirRealRemainTime !== false)
        {
          $tmp = array('id' => (string)$song[XML_INDEX], 'a' => (string)$song['a'], 'i' => (string)$song['i'], 't' => (string)$song['t'], 'p' => (string)$onAirRealRemainTime, 'w' => (string)$song['w'], 'l' => (string)$song['l']);
          $curPos = $onAirRealRemainTime;
        }
        else
        {
          $tmp = array('id' => (string)$song[XML_INDEX], 'a' => (string)$song['a'], 'i' => (string)$song['i'], 't' => (string)$song['t'], 'p' => (string)$song['p'], 'w' => (string)$song['w'], 'l' => (string)$song['l']);
          $curPos = 0;
        }
        $json['onAir'] = $tmp;
        // -- current position for visualizing --
        $json['onAirCurPos'] = getSecondsPerMinSec($curPos);
        $json['onAirLength'] = getSecondsPerMinSec($song['p']);
      }
      else
      {
        $eta = $myPlaylist->getEstimatedPlaytime($song[XML_INDEX]);
        $tmp = array('id' => (string)$song[XML_INDEX], 'a' => (string)$song['a'], 'i' => (string)$song['i'], 't' => (string)$song['t'], 'p' => (string)$eta, 'w' => (string)$song['w'], 'l' => (string)$song['l']);
        $playlist[] = $tmp;
      }
      $i++;
    }
    if (!empty($playlist))
      $json['playlist'] = $playlist;

    unset($myPlaylist);
  }
  // -- end of playlist --
  // -- --------------- --


  require_once '_coreEnd.php';
