<?php
  /**
   * getIsLoggedIn
   *
   * Development endpoint to check WHO is HOW logged in
   *
   * standard call
   * http://localhost/piMoo/api/1/getIsLoggedIn.php
   *
   *
   * examples of JSON response code
   * {"RC":0,"nickname":"devel"}
   * {"RC":12}
   *
   * 0 -> JSON_RC_OK
   * 12 -> JSON_RC_NO_MATCHING_VALUE
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once '_coreBeg.php';

  $json[JSON_RC_ADDINFO] = NULLSTR;
  $skipToken = false;


  // -- ------------------------ --
  // -- -1- logged in by session --
  if (!isset($_SESSION['str_nickname']))
  {
    $json[JSON_RC] = JSON_RC_NO_MATCHING_VALUE;
  }
  else
  {
    $json[JSON_RC] = JSON_RC_OK;
    $json['nickname'] = $_SESSION['str_nickname'];
    $json[JSON_RC_ADDINFO] .= '<logged in by session>';
    $skipToken = true;
  }

  // -- ----------------------------------------------------------------------------- --
  // -- -2- logged in by token -> order is important, must be after admin password check --
  if (!empty($_GET['token']) && !$skipToken)
  {
    if (!getIsGrantedByAccessToken($_GET['token']))
    {
      $json[JSON_RC] = JSON_RC_NO_MATCHING_VALUE;
      $json[JSON_RC_ADDINFO] .= '<provided a token, but it is not valid>';
    }
    else
    {
      $json[JSON_RC] = JSON_RC_OK;
      $_SESSION['str_nickname'] = getNicknameByAccessToken($_GET['token']);
      $json['nickname'] = $_SESSION['str_nickname'];
      $json[JSON_RC_ADDINFO] .= '<logged in by token>';
    }
  }


  // -- require_once '_coreEnd.php'; not here because I don't need the delay --
  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks (only for write access) --
  //usleep(ANTI_DOS_DELAY);
