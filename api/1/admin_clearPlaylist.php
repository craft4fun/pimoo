<?php
  /**
   * admin_clearPlaylist
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_clearPlaylist.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  include_once '../../api_playlist.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  // -- is admin? --
  if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
  {
    $myPlaylist = new class_playlist(TWODIRUP);
    $json[JSON_RC] = JSON_RC_OK;
    $json[JSON_RC_ADDINFO] = $myPlaylist->clearPlaylist();
    unset($myPlaylist);
  }
  else
    $json[JSON_RC] = JSON_RC_NO_ADMIN;

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
