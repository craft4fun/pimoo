This is version 1 of the piMoo API. 

It has mainly been designed to serve as an interface for the piMooApp.

There are some ugly workarounds to get some things working easily on app side.

This version is going to be inherited by version 2 which will be a pure JSON REST API.

2022-10-14: FREEZE development of this api-version working with piMooApp-version 2020-12-26 V1.0.3.

