<?php
/**
   * getSongInfo
   *
   * standard call
   * http://localhost/piMoo/api/1/getSongInfo.php?song=Albums/Genesis/Invisible touch/Genesis - (06) Domino.mp3
   * http://localhost/piMoo/api/1/getSongInfo.php?song=Genesis -(06) Domino.mp3&firstHit
   *
   * examples of JSON response code
   * {"RC":0,"songInfo":{"i":"Genesis","t":"Domino","p":"10:45","l":"Invisible Touch","y":"1986","g":"Progressive Rock","r":"~5~","rby":"paddy","c":"","cby":"","d":"2009-03-16","a":"Albums\/Genesis\/Invisible touch\/Genesis - (06) Domino.mp3"}}
   * {"RC":0,"addInfo":"first hit","songInfo":{"i":"Genesis","t":"Domino","p":"10:45","l":"Invisible Touch","y":"1986","g":"Progressive Rock","r":"~5~","rby":"paddy","c":"","cby":"","d":"2009-03-16","a":"Albums\/Genesis\/Invisible touch\/Genesis - (06) Domino.mp3"}}
   * {"RC":3}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 3 -> JSON_RC_PARTYPOOPER
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  include_once '../../api_mediabase.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();


  if (!isset($_GET['song']))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/getCurrentPlaylist.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $base64 = (isset($_GET['base64'])) ? true : false;

    if (!isset($_SESSION['str_nickname']))
    {
      eventLog('Partypooper/ nickname is required in api/1/getCurrentPlaylist.php', TWODIRUP);
      $json[JSON_RC] = JSON_RC_PARTYPOOPER;
    }
    else
    {
      if ($base64)
        $song = base64_decode($_GET['song']);
      else
        $song = $_GET['song'];

      $json[JSON_RC] = JSON_RC_OK;


      // -- build lyrics --
      $pathToSong = dirname($cfg_absPathToSongs . $song) . SLASH;
      $lyricPrep = $pathToSong . basename($song, DOT . $cfg_musicFileType) . DOT . $cfg_textFileType;
      $lyrics = (file_exists($lyricPrep)) ? file_get_contents($lyricPrep) : NULLSTR;
      $lyrics = str_replace(LFC, LFH, $lyrics);


      // -- preview --
//           <span style="float: right;">
//             <a href="preview.php?song='.$_REQUEST['song'].'&i='.base64_encode($info['i']).'&t='.base64_encode($info['t']).'&p='.base64_encode($info['p']).'&l='.base64_encode($info['l']).'">
//               <img '.$thumgSize.' src="'.cntGetAlbumPicURL($song, $info['l']).'" title="preview" />
//             </a>
//           </span>';


      // -- needed if called e.g. by valuedSong.php to take the first hit of song name without intern mediabase path --
      $firstHit = (isset($_GET['firstHit'])) ? true : false;

      $myMediabase = new class_mediaBase(TWODIRUP);
      $info = $myMediabase->getSongInfo($song, $firstHit);

      // -- check if the song can be found in the mediabase --
      if ($info)
      {
        // -- if found by luckiness, the first hit (without intern mediabase path, only filename) --
        if ($firstHit)
          $json[JSON_RC_ADDINFO] = LNG_FIRST_HIT;

        // -- ------------------ --
        // -- begin of song info --
        // -- ------------------ --

        // -- escape the apostrophe for further javascript issues --
        $escapedInterpret = str_replace('\'', '\\\'', $info['i']);
        $escapedTitle = str_replace('\'', '\\\'', $info['t']);
        $escapedAlbum = str_replace('\'', '\\\'', $info['l']);

        $escapedRatingBy = str_replace('\'', '\\\'', $info['rby']);
        $escapedComment = str_replace('\'', '\\\'', $info['c']);
        $escapedCommentBy = str_replace('\'', '\\\'', $info['cby']);

        $tmp = array(
                      'i' => (string)$escapedInterpret,
                      't' => (string)$escapedTitle,
                      'p' => (string)$info['p'],
                      'l' => (string)$escapedAlbum,
                      'y' => (string)$info['y'],
                      'g' => (string)$info['g'],

                      'r' => (string)$info['r'],
                      'rby' => (string)$escapedRatingBy,
                      'c' => (string)$escapedComment,
                      'cby' => (string)$escapedCommentBy,

                      'd' => (string)$info['d'],
                      'a' => (string)$info['a'],

                      'lyrics' => $lyrics,
                    );
        $json['songInfo'] = $tmp;
      }
      else
      {
        $json[JSON_RC] = JSON_RC_NO_MATCHING_VALUE;
        $json[JSON_RC_ADDINFO] = LNG_ERR_NOSONGINFO;
      }
      unset($myMediabase);
    }
  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);

