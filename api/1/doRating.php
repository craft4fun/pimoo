<?php
  /**
   * doRating
   *
   * standard call
   * http://localhost/piMoo/api/1/doRating.php?song=Albums/Genesis/Invisible touch/Genesis - (06) Domino.mp3&rating=~5~
   *
   * &song= song to rate
   * &base64 (song base64 encoded or not
   * &rating= ~1~ up to ~5~ where 1 is worst and 5 is best
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once ('../../api_feedback.php');
  require_once ('../../api_mediabase.php');

  cntCreateJSONheader();
  cntCreateCORSheader();

  if (
         !isset($_GET['song'])
      || !isset($_GET['rating'])
      || !$_GET['rating'] != NULLSTR
    )
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/doRating.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $song = (isset($_GET['base64'])) ?  base64_decode($_GET['song']) : $_GET['song'];
    $rating = $_GET['rating'];

    $json = array();

    //-- get nickame --
    if (isset($_SESSION['str_nickname']))
      $wishedBy = $_SESSION['str_nickname'];
    else
      $wishedBy = NULLSTR; // -- $_SERVER['REMOTE_ADDR'] --> do not fill the feedback.xml with obsolete information --

    // -- is admin or registered user? --
    if (
            !(isset($_SESSION['bool_admin']) AND $_SESSION['bool_admin'] == true)
         || !(isset($_SESSION['str_nickname']) AND $_SESSION['str_nickname'] != NULLSTR)
       )
    {
      $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
      eventLog('ERROR in api/1/doRating, need to be logged in or being admin', TWODIRUP);
    }
    else
    {
      $myFeedback = new class_feedback(TWODIRUP);
      $myFeedback->setSong($song); // -- feedback must make an entry in EVERY case --
      if (!$myFeedback->setRating($rating, $wishedBy))
      {
        $json[JSON_RC] = JSON_RC_UNDEF_ERR;
        eventLog('ERROR in api/1/doRating while myFeedback->setRating()', TWODIRUP);
      }
      else
      {
        if (!$myFeedback->saveFeedback())
        {
          $json[JSON_RC] = JSON_RC_UNDEF_ERR;
          eventLog('ERROR in api/1/doRating while myFeedback->saveFeedback()', TWODIRUP);
        }
        else
        {
          // -- CLONE INTO MEDIABASE FOR DIRECT USE--
          $myMediaBase = new class_mediaBase(TWODIRUP);
          $myMediaBase->setSong(basename($song)); // -- on mediabase it is ok to check if it exists --
          if (!$myMediaBase->setRating($rating, $wishedBy))
          {
            $json[JSON_RC] = JSON_RC_UNDEF_ERR;
            eventLog('ERROR in api/1/doRating while $myMediaBase->setRating()', TWODIRUP);
          }
          else
          {
            if (!$myMediaBase->saveFeedback())
            {
              $json[JSON_RC] = JSON_RC_UNDEF_ERR;
              eventLog('ERROR in api/1/doRating while $myMediaBase->saveFeedback()', TWODIRUP);
            }
            else
            {
              // -- as a result of all --
              $json[JSON_RC] = JSON_RC_OK;
            }
          }
        }
      }
    }
  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);

