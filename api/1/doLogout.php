<?php
  /**
   * doLogout
   *
   * standard call
   * http://localhost/piMoo/api/1/doLogout.php
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":11}
   * {"RC":12}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 12 -> JSON_RC_NO_MATCHING_VALUE
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once '_coreBeg.php';

//   $json[JSON_RC_ADDINFO] = NULLSTR;
//   $skipToken = false;

  $myNickname = new class_nickname(TWODIRUP);
  if (!isset($_SESSION['str_nickname']))
  {
    $json[JSON_RC] = JSON_RC_NO_MATCHING_VALUE;
  }
  else
  {
    if (!$myNickname->drop($_SESSION['str_nickname']))
      $json[JSON_RC] = JSON_RC_UNDEF_ERR;
    else
      $json[JSON_RC] = JSON_RC_OK;
    unset($myNickname);
  }

  require_once '_coreEnd.php';