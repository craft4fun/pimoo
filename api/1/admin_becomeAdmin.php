<?php
  /**
   * admin_becomeAdmin
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_becomeAdmin.php
   * &token=
   * &paswd= //later md5
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once '_coreBeg.php';


  $json[JSON_RC_ADDINFO] = NULLSTR;
  $skipToken = false;

  // -- ---------------------------- --
  // -- -1- Access by admin password --
  if (!isset($_GET['passwd'])) //   isset($_GET['user']) // -- wherefore? There is only one admin --
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/admin_becomeAdmin.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    if ($_GET['passwd'] != $cfg_admin_passwd) // md5
    {
      $json[JSON_RC] = JSON_RC_NO_ADMIN;
      $json[JSON_RC_ADDINFO] .= '<wrong admin password>';
    }
    else
    {
      $_SESSION['bool_admin'] = true;

      $json[JSON_RC] = JSON_RC_OK;
      $json[JSON_RC_ADDINFO] .= '<access granted by admin password>';

      $skipToken = true;
    }
  }

  // -- ----------------------------------------------------------------------------- --
  // -- -2- Access by token -> order is important, must be after admin password check --
  if (!empty($_GET['token']) && !$skipToken)
  {
    if (!getIsGrantedByAccessToken($_GET['token']))
    {
      $json[JSON_RC] = JSON_RC_NO_ADMIN;
      $json[JSON_RC_ADDINFO] .= '<provided a token, but it is not valid>';
    }
    else
    {
      $_SESSION['bool_admin'] = true;

      $json[JSON_RC] = JSON_RC_OK;
      $json[JSON_RC_ADDINFO] .= '<access granted by token>';
    }
  }


  require_once '_coreEnd.php';
