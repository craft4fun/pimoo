<?php
  /**
   * setLanguage
   *
   * standard call
   * http://localhost/piMoo/api/1/setLanguage.php?lng=EN
   *
   * Supported languages are:
   * EN,DE
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 11 -> JSON_RC_UNKNOWN_VALUE
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  if (!isset($_GET['lng']))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/setLanguage.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $json = array();

    switch ($_GET['lng']) // -- do not use the cookie here, it is not updated yet --
    {
      case LANG_EN:
        {
          if (setcookie(STR_LANG, LANG_EN, time() + 2592000)) // -- time()+60*60*24*30 -> one month --
            $json[JSON_RC] = JSON_RC_OK;
          else
            $json[JSON_RC] = JSON_RC_UNDEF_ERR;
          break;
        }
      case LANG_DE:
        {
          if (setcookie(STR_LANG, LANG_DE, time() + 2592000)) // -- time()+60*60*24*30 -> one month --
            $json[JSON_RC] = JSON_RC_OK;
          else
            $json[JSON_RC] = JSON_RC_UNDEF_ERR;
          break;
        }

      default:
        {
          $json[JSON_RC] = JSON_RC_UNKNOWN_VALUE;
        }
    }
  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
