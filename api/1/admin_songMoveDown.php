<?php
  /**
   * admin_songMoveDown
   *
   * standard call
   * http://localhost/piMoo/api/1/admin_songMoveDown.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_playlist.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  if (!isset($_GET['index']))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/admin_songMoveDown.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $json = array();

    // -- is admin? --
    $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
    if (!$admin)
    {
      $json[JSON_RC] = JSON_RC_NO_ADMIN;
    }
    else
    {
      $myPlaylist = new class_playlist(TWODIRUP);
      $check = $myPlaylist->moveDownSong($_GET['index'], $admin);
      unset($myPlaylist);
      if ($check !== false)
        $json[JSON_RC] = JSON_RC_OK;
      else
        $json[JSON_RC] = JSON_RC_UNDEF_ERR;
    }
  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
