<?php
  /**
   * admin_songMoveUp
   *
   * standard call
   * http://localhost/piMoo/api/2/admin_songMoveUp.php?index=7
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '_coreBeg.php';
  require_once $apidepth.'defines.inc.php';
  require_once $apidepth.'keep/config.php';
  require_once $apidepth.'lib/utils.php';
  require_once $apidepth.'api_playlist.php';

  $scriptName = basename(__FILE__);

  if (!$isAdminByToken)
  {
    $json[JSON_RC] = JSON_RC_NO_ADMIN;
    $json[JSON_RC_ADDINFO] = 'NO_ADMIN';
  }
  else
  {
    if (!isset($_GET['index']))
    {
      eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. $scriptName, $apidepth);
      $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
      $json[JSON_RC_ADDINFO] = 'REQUIRED_PARAM_MISSING';
    }
    else
    {
      $json = array();

      $myPlaylist = new class_playlist(TWODIRUP);
      $check = $myPlaylist->moveUpSong($_GET['index'], $isAdminByToken);
      unset($myPlaylist);
      if ($check === false)
      {
        $json[JSON_RC] = JSON_RC_UNDEF_ERR;
      }
      else
      {
        $json[JSON_RC] = JSON_RC_OK;
      }
    }
  }

  require_once '_coreEnd.php';
