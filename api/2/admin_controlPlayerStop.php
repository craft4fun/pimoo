<?php
  /**
   * admin_controlPlayerStop
   *
   * standard call
   * http://localhost/piMoo/api/2/admin_controlPlayerStop.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '_coreBeg.php';

  require_once $apidepth.'defines.inc.php';
  require_once $apidepth.'keep/config.php';
  require_once $apidepth.'lib/utils.php';
  require_once $apidepth.'api_player.php';


  if ($isAdminByToken)
  {
    $myPlayer = new class_player(TWODIRUP);

    $json[JSON_RC_ADDINFO] = LNG_TRYING_TO_STOP_PLAYER;

     // -- turn on the cli_player.php and cli_common.php --
    $json[JSON_RC_ADDINFO] .= $myPlayer->cmdTurnOff();

    // -- turn on HiFi via hardware - relais --
    $json[JSON_RC_ADDINFO] .= $myPlayer->GPIO_OFF();

    unset($myPlayer);
    $json[JSON_RC] = JSON_RC_OK;
  }

  require_once '_coreEnd.php';

