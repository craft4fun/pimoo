<?php

  // -- return api response --
  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks (only for write access) --
  usleep(ANTI_DOS_DELAY);
