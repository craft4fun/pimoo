<?php
  /**
   * releaseNickname
   *
   * Release own nickname, addresses by token
   * TODO clarify the use case here!
   *
   * standard call
   * http://localhost/piMoo/api/2/releaseNickname.php?token=abc
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":11}
   * {"RC":12}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 12 -> JSON_RC_NO_MATCHING_VALUE
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '_coreBeg.php';

//   $json[JSON_RC_ADDINFO] = NULLSTR;
//   $skipToken = false;

  if ($accessGranted)
  {
    $myNickname = new class_nickname($apidepth);
    if (!$myNickname->drop($nickname))
      $json[JSON_RC] = JSON_RC_UNDEF_ERR;
    else
      $json[JSON_RC] = JSON_RC_OK;
    unset($myNickname);
  }

  require_once '_coreEnd.php';
