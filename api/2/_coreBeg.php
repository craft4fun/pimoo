<?php
  $apidepth = '../../'; // -- TWODIRUP is not defined yet --

  require_once $apidepth.'defines.inc.php';
  require_once $apidepth.'keep/config.php';
  require_once $apidepth.'lib/utils.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  require_once $apidepth.'keep/accessTokens.php';

  // -- prepare api response --
  $json = array();

  // -- common results --
  $json[JSON_RC] = JSON_RC_UNDEF_ERR;
  $json[JSON_RC_ADDINFO] = NULLSTR;
  $json[JSON_RC_ADMIN] = false;


  /**
   * If user (by provied token) is in this list, he is an admin!
   * string $token
   */
  function getIsAdminGrantedByAccessToken($token)
  {
    global $accessTokens;
    $result = false;
    if (in_array($token, $accessTokens))
      $result = true;
    return $result;
  }

  /**
   * Provide user's nickname by given token
   * string $token
   */
  function getNicknameByAccessToken($token)
  {
    global $accessTokens;
    return array_search($token, $accessTokens);
  }


  $isAdminByToken = false;
  $accessGranted = false;

  if (!isset($_GET['token']))
  {
    $json[JSON_RC] = JSON_RC_TOKEN_MISSING;
    $json[JSON_RC_ADDINFO] = 'access token required';
  }
  else
  {
    $token = $_GET['token'];

    $myNickname = new class_nickname($apidepth);

    // -- -------------------------------------------- --
    // -- check for admin -> nickname by configuration --
    // -- -------------------------------------------- --
    if (getIsAdminGrantedByAccessToken($token))
    {
      $isAdminByToken = true;
      $accessGranted = true;
      $json[JSON_RC_ADMIN] = true;

      $nickname = getNicknameByAccessToken($token);
      // -- this nickname cannot be faked or rotated because it comes from internal, pre-configured structure --
      $myNickname->push($nickname);
      $json[JSON_RC_NICKNAME] = $nickname;
    }
    // -- ----------------------------------------------------- --
    // -- common user -> claimed token turns to plain nickname! --
    // -- ----------------------------------------------------- --
    else
    {
      if ($trustedEnvironment) // -- Former gitlab-issue#98 check if new nickname comes from known IP, if so switch the nickname silently! --
      {
        if (!$myNickname->push($token))
        {
          $json[JSON_RC] = JSON_RC_RESOURCE_IN_USE;
          $json[JSON_RC_ADDINFO] .= 'nickname is already in use';
        }
        else
        {
          $nickname = $token;
          $json[JSON_RC_NICKNAME] = $nickname;
          $accessGranted = true;
        }
      }
    }
  }


