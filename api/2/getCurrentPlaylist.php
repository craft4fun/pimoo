<?php
  /**
   * getCurrentPlaylist
   *
   * standard call
   * http://localhost/piMoo/api/2/getCurrentPlaylist.php?token=abc
   *
   *
   * examples of JSON response code
   * {"RC":0, "onAir":{"id":"0","a":"Albums\/Genesis\/Invisible touch\/Genesis - (06) Domino.mp3","i":"Genesis","t":"Domino","p":"10:45","w":"devel","l":"Invisible Touch"},"playlist":[{"id":"1","a":"Albums\/Coldplay\/A rush of blood to the head\/Coldplay - (04) The scientist.mp3","i":"Coldplay","t":"The Scientist","p":"5:10","w":"devel","l":"A rush of blood to the head"},{"id":"2","a":"Albums\/Metallica\/Metallica\/Metallica - (02) Sad But True.mp3","i":"Metallica","t":"Sad But True","p":"5:25","w":"devel","l":"Metallica"}]}
   * {"RC":3}
   * {"RC":12}
   * {"RC":13}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 3 -> JSON_RC_PARTYPOOPER
   * 12 -> JSON_RC_NO_MATCHING_VALUE
   * 13 -> JSON_RC_RESOURCE_IN_USE
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '_coreBeg.php';

  require_once $apidepth.'api_playlist.php';
  require_once $apidepth.'api_layout.php';


  if ($accessGranted)
  {
    $myPlaylist = new class_playlist($apidepth);
    $myPlayer = new class_player($apidepth);
    $json = $myPlaylist->getWholePlaylistAsJson($myPlayer);


    /* -- this worked well, but I swapped out code into class --
    $json[JSON_RC] = JSON_RC_OK;

    $myPlaylist = new class_playlist($apidepth);
    $songs = $myPlaylist->getWholePlaylist();

    $i = 0;
    foreach ($songs as $song)
    {
      // -- on air --
      if ($i == 0)
      {
        $onAirRealRemainTime = parseSecondsToHumanMinSecString($myPlaylist->getPlayerRemainTime());
        if ($onAirRealRemainTime !== false)
        {
          $tmp = array('id' => intval($song[XML_INDEX]),
                        'a' => (string)$song['a'],
                        'i' => (string)$song['i'],
                        't' => (string)$song['t'],
                        'p' => (string)$onAirRealRemainTime,
                        'w' => (string)$song['w'],
                        'l' => (string)$song['l']
                      );
          $curPos = $onAirRealRemainTime;
        }
        else
        {
          $tmp = array('id' => intval($song[XML_INDEX]),
                        'a' => (string)$song['a'],
                        'i' => (string)$song['i'],
                        't' => (string)$song['t'],
                        'p' => (string)$song['p'],
                        'w' => (string)$song['w'],
                        'l' => (string)$song['l']
                      );
          $curPos = 0;
        }
        $json['onAir'] = $tmp;

        // -- player state --
        $myPlayer = new class_player($apidepth);
        $json[JSON_RC_STATE] = $myPlayer->getPlayerState();
        unset($myPlayer);

        // -- current position for visualizing --
        $json['onAirCurPos'] = getSecondsPerMinSec($curPos);
        $json['onAirLength'] = getSecondsPerMinSec($song['p']);
        $json['count'] = count($songs);
      }
      // -- playlist --
      else
      {
        $eta = $myPlaylist->getEstimatedPlaytime($song[XML_INDEX]);
        $tmp = array('id' => intval($song[XML_INDEX]),
                     'a' => (string)$song['a'],
                     'i' => (string)$song['i'],
                     't' => (string)$song['t'],
                     'p' => (string)$eta,
                     'w' => (string)$song['w'],
                     'l' => (string)$song['l']
                    );
        $playlist[] = $tmp;
      }
      $i++;
    }
    if (!empty($playlist))
      $json['playlist'] = $playlist;

    unset($myPlaylist);
    */
  }

  require_once '_coreEnd.php';
