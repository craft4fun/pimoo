<?php
  /**
   * admin_systemShutdown
   *
   * standard call
   * http://localhost/piMoo/api/2/admin_systemShutdown.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   * {"RC":99}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_player.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $json[JSON_RC] = JSON_RC_NO_ADMIN;
  }
  else
  {
    // -- shutdown player before, that's clean --
    $myPlayer = new class_player(TWODIRUP);
    if ($myPlayer->getPlayerState() == PS_PLAYING)
      $myPlayer->cmdTurnOff();
    unset($myPlayer);

    $json[JSON_RC_ADDINFO] = 'try to shutdown the system...<br>';
    //$json[JSON_RC_ADDINFO] .= (string)system ('sudo /sbin/shutdown -h now');
    $json[JSON_RC_ADDINFO] .= util_systemShutdown(TWODIRUP);
    $json[JSON_RC] = JSON_RC_OK;
  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);

