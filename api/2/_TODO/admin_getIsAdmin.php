<?php
  /**
   * admin_getIsAdmin (currently used at all?)
   *
   * standard call
   * http://localhost/piMoo/api/2/admin_getIsAdmin.php
   *
   *
   * examples of JSON response code
   * {"RC":0,"state":false}
   *
   * 0 -> JSON_RC_OK
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once '_coreBeg.php';

  $json[JSON_RC] = JSON_RC_OK;

  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
    $json[JSON_RC_STATE] = false;
  else
    $json[JSON_RC_STATE] = true;

  require_once '_coreEnd.php';
