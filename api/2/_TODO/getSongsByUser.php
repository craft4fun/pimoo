<?php
  /**
   * getSongsByUser
   *
   * standard call
   * http://localhost/piMoo/api/2/getSongsByUser.php?user=nane
   * http://localhost/piMoo/api/2/getSongsByUser.php?user=nane&o=50&m=100
   * http://localhost/piMoo/api/2/getSongsByUser.php?user=Tim&cs // case sensitive search for "Tim". Prevend finding user "tim"
   *
   * &u= string -> the search string
   * &o= integer -> offset
   * &m= integer -> max search results
   * $cs= boolean -> case sensitivity
   *
   *
   * examples of JSON response code
   * {"RC":0,"byUser":[{"a":"Singles\/Depeche Mode - People are people.mp3","i":"Depeche Mode","t":"People are people","p":"3:44","l":""},{"a":"Singles\/Marky Mark - No mercy.mp3","i":"Marky Mark","t":"No mercy","p":"3:41","l":""},{"a":"Singles\/Secret Service - Ten oclock postman.mp3","i":"Secret Service","t":"Ten o'clock postman","p":"3:32","l":""},{"a":"Singles\/Van Halen - Why cant this love.mp3","i":"Van Halen","t":"Why can&#180;t this love","p":"3:48","l":""},{"a":"Singles\/Olly Murs - Dear Darling.mp3","i":"Olly Murs","t":"Dear Darling","p":"3:26","l":"Right place right time"},{"a":"Singles\/Klingande - Jubel.mp3","i":"Klingande","t":"Jubel","p":"3:29","l":""}]}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 1 -> JSON_RC_OK_BYCACHE
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../lib/class_cache.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  if (!isset($_GET[USER]))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/getSongsByUser.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $offset   = (isset($_GET['o'])) ? $_GET['o'] : NULLINT;
    $max      = (isset($_GET['m'])) ? $_GET['m'] : $cfg_maxSearchResultsPerPage;
    $caseSens = (isset($_GET['cs'])) ? true : false;

    // -- cache handling --
    $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
    $myCache->setCachePath(TWODIRUP . $cfg_cachePath);
    $myCache->setCacheFile(CACHE_ITEMS_BY_USER . $_GET[USER] . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $max . CACHE_RSLT_VIEWFORM . CACHE_RSLT_JSON . CACHE_RSLT_LANG . $lang . CACHE_RSLT_CASESENSITIVITY . $caseSens);

    // -- take it from cache --
    if ($myCache->getIsCacheFileAlive())
    {
      //-- add an info for this would mean to decode add info and encode again: where's the speed enhancement, huh? --
      // therefore NO: $json[JSON_RC_ADDINFO] = JSON_RC_BYCACHE;
      $output = file_get_contents($myCache->getCacheFile());
      unset($myCache);
    }
    // -- otherwise show it native and build cache --
    else
    {
      unset($myCache);

      // -- ------------------------------- --
      // -- begin of the the very main core --
      // -- ------------------------------- --

      require_once '../../api_mediabase.php';
      $myMediaBase = new class_mediaBase(TWODIRUP);

      $results = $myMediaBase->getSearchResultByUser($_GET[USER], $offset, $max, $caseSens);

      $json[JSON_RC] = JSON_RC_OK;

      $data = array();
      foreach ($results as $result)
      {
        $tmp = array('a' => (string)$result['a'], 'i' => (string)$result['i'], 't' => (string)$result['t'], 'p' => (string)$result['p'], 'l' => (string)$result['l']);
        $data[] = $tmp;
      }
      $json['data'] = $data;

      unset($myMediaBase);

      $output = json_encode($json);

      // -- build cache for next time --
      $myCache = new class_outputCache();
      $myCache->setCachePath(TWODIRUP . $cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_USER . $_GET[USER] . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $max . CACHE_RSLT_VIEWFORM . CACHE_RSLT_JSON . CACHE_RSLT_LANG . $lang . CACHE_RSLT_CASESENSITIVITY . $caseSens);
      $myCache->putCacheContent($output);
      unset($myCache);
    }

    // -- "execute" normal way --
    echo $output;
    unset($output);
  }

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
