<?php
  /**
   * admin_leaveAdmin
   *
   * standard call
   * http://localhost/piMoo/api/2/admin_leaveAdmin.php
   * &user=
   * &paswd= //later md5
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":2}
   *
   * 0 -> JSON_RC_OK
   * 2 -> JSON_RC_NO_ADMIN
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  $_SESSION['bool_admin'] = false;
  unset($_SESSION['bool_admin']);

  $json[JSON_RC] = JSON_RC_OK;


  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks (only for write access) --
  usleep(ANTI_DOS_DELAY);
