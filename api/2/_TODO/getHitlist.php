<?php
  /**
   * getHitlist
   *
   * This is NOT a pure JSON endpoint BUT returns HTML code
   *
   * standard call
   * http://localhost/piMoo/api/2/getCurrentPlaylist.php
   *
   *
   * examples of JSON response code
   * {"RC":0, "onAir":{"id":"0","a":"Albums\/Genesis\/Invisible touch\/Genesis - (06) Domino.mp3","i":"Genesis","t":"Domino","p":"10:45","w":"devel","l":"Invisible Touch"},"playlist":[{"id":"1","a":"Albums\/Coldplay\/A rush of blood to the head\/Coldplay - (04) The scientist.mp3","i":"Coldplay","t":"The Scientist","p":"5:10","w":"devel","l":"A rush of blood to the head"},{"id":"2","a":"Albums\/Metallica\/Metallica\/Metallica - (02) Sad But True.mp3","i":"Metallica","t":"Sad But True","p":"5:25","w":"devel","l":"Metallica"}]}
   * {"RC":3}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 3 -> JSON_RC_PARTYPOOPER
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../api_playlist.php';
  require_once '../../api_layout.php';

  cntCreateJSONheader();
  cntCreateCORSheader();


  // -- full does mean with all extra info. This is slow, because these info must be fetched from the mediabase --
  $full = (isset($_GET['full'])) ? true : false;


  $hitlistFilename = '../../keep/partyReport/hitlist.txt';

  if (!file_exists($hitlistFilename))
  {
    $json[JSON_RC] = LNG_HITLIST_EMPTY;
  }
  else
  {
    if ($full)
      $myMediabase = new class_mediaBase(TWODIRUP);

    $hitlistHTMLStr = NULLSTR;

    $file = file($hitlistFilename);
    $grad = cntGradientFromTo('019e1a', 'd0218f', count($file));  // 00DF20 to BF0040
    unset($file);

    $hitlist = fopen($hitlistFilename, 'r');
    $i = 0;
    while(!feof($hitlist))
    {
      $song = fgets($hitlist, 1024);
      $song = str_replace(LFC, NULLSTR, $song);
      if (strlen($song) > 0)
      {
        if (!$full)
        {
          // -- add title to playlist without additional info is not possible :o( --
          $hitlistHTMLStr .= '<div class="textShadowBlur" style="color: #'.$grad[$i].';">'.basename($song, '.'.$cfg_musicFileType).'</div>';
        }
        else
        {
          $info = $myMediabase->getSongInfo($song);
          if ($info !== false)
          {
            // -- escape the apostrophe for further javascript issues --
            $escapedInterpret = str_replace('\'', '\\\'', $info['i']);
            $escapedTitle = str_replace('\'', '\\\'', $info['t']);

            if (isset($_SESSION['str_nickname']))
              $hitlistHTMLStr .=  '<div id="hli_'.$i.'"><a class="textShadowBlur" style="text-decoration: none; color: #'.$grad[$i].';" href="javascript:addSong($(\'hli_'.$i.'\'), \''.base64_encode($song).'\',\''.base64_encode($info['i']).'\',\''.base64_encode($info['t']).'\',\''.$info['p'].'\',\''.base64_encode($_SESSION['str_nickname']).'\',\''.base64_encode($info['l']).'\')" ><span style="float: right;">'.$escapedInterpret.'</span>'.$escapedTitle.'</a></div>';
            else
              $hitlistHTMLStr .= '<div id="hli_'.$i.'"><a class="textShadowBlur" style="text-decoration: none; color: #'.$grad[$i].';" ><span style="float: right;">'.$escapedInterpret.'</span>'.$escapedTitle.'</a></div>';
          }
          // -- not available in the current mediaBase --
          else
            $hitlistHTMLStr .= '<div class="textShadowBlur" style="text-decoration: line-through; color: #'.$grad[$i].';">'.$song.'</div>';
        }
      }
      $i++;

      //if ($i >= $cfg_hitlistLimit)
        //break;

      $json[JSON_RC] = JSON_RC_OK;
      $json['hitlistHTMLStr'] = $hitlistHTMLStr;
    }
    fclose($hitlist);
    if ($full)
      unset($myMediabase);

  }


  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
