<?php
  /**
   * doComment
   *
   * standard call
   * http://localhost/piMoo/api/2/doComment.php?song=Albums/Genesis/Invisible touch/Genesis - (06) Domino.mp3&comment=One of the greatest songs ever
   *
   * &song= song to rate
   * &base64 (song base64 encoded or not
   * &comment=
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   * 99 -> JSON_RC_UNDEF_ERR
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';

  require_once ('../../api_feedback.php');
  require_once ('../../api_mediabase.php');

  cntCreateJSONheader();
  cntCreateCORSheader();

  if (
         !isset($_GET['song'])
      || !isset($_GET['comment'])
      || !$_GET['comment'] != NULLSTR
    )
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/doComment.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $song = (isset($_GET['base64'])) ?  base64_decode($_GET['song']) : $_GET['song'];
    $comment = $_GET['comment'];

    $json = array();

    //-- get nickame --
    if (isset($_SESSION['str_nickname']))
      $wishedBy = $_SESSION['str_nickname'];
    else
      $wishedBy = NULLSTR; // -- $_SERVER['REMOTE_ADDR'] --> do not fill the feedback.xml with obsolete information --

     // -- is admin or registered user? --
     if (
            !(isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
         || !(isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
       )
     {
       eventLog('ERROR in api/1/doComment, need to be logged in or being admin', TWODIRUP);
       $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
     }
     else
     {
       $myFeedback = new class_feedback(TWODIRUP);
       $myFeedback->setSong($song); // -- feedback must make an entry in EVERY case --
      if (!$myFeedback->setComment($comment, $wishedBy))
      {
        eventLog('ERROR in api/1/doComment in function myFeedback->setComment()', TWODIRUP);
        $json[JSON_RC] = JSON_RC_UNDEF_ERR;
      }
      else
      {
        if (!$myFeedback->saveFeedback())
        {
          eventLog('ERROR in api/1/doComment in function myFeedback->saveFeedback()', TWODIRUP);
          $json[JSON_RC] = JSON_RC_UNDEF_ERR;
        }
        else
        {
          // -- CLONE INTO MEDIABASE FOR DIRECT USE--
          $myMediaBase = new class_mediaBase(TWODIRUP);
          $myMediaBase->setSong(basename($song)); // -- on mediabase it is ok to check if it exists --
          if (!$myMediaBase->setComment($comment, $wishedBy))
          {
            eventLog('ERROR in api/1/doComment in function $myMediaBase->setComment()', TWODIRUP);
            $json[JSON_RC] = JSON_RC_UNDEF_ERR;
          }
          else
          {
            if (!$myMediaBase->saveFeedback())
            {
              eventLog('ERROR in api/1/doComment in function $myMediaBase->saveFeedback()', TWODIRUP);
              $json[JSON_RC] = JSON_RC_UNDEF_ERR;
            }
            else
            {
              // -- as a result of all --
              $json[JSON_RC] = JSON_RC_OK;
            }
          }
        }
      }
    }
  }

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);

