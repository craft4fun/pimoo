<?php
  /**
   * SUCH AN ENDPOINT IS NOT REQUIRED ANYMORE, THE TOKEN SHALL BE PROVIDED AT EVERY REQUEST AND IS HANDLED IN _corebeg.php!
   *
   *
   * setNickname
   *
   * For default user without token
   *
   * standard call
   * http://localhost/piMoo/api/2/setNickname.php?nick=paddy
   *
   * &nick= the requested nickname
   * &current= the current "old" nickname // if possible use this, so the current hook can be released clearly
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '_coreBeg.php';

  require_once $apidepth.'defines.inc.php';
  require_once $apidepth.'keep/config.php';
  require_once $apidepth.'lib/utils.php';


  if (!isset($_GET['nick']))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/2/setNickname.php', $apidepth);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    $current = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : NULLSTR;

    $myNickname = new class_nickname($apidepth);
    if (!$myNickname->push($_GET['nick'], $current))
      $json[JSON_RC] = LNG_TAKE_ANOTHER_NICK;
    else
      $json[JSON_RC] = JSON_RC_OK;
    unset($myNickname);
  }

  require_once '_coreEnd.php';