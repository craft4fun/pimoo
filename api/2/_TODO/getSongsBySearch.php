<?php
  /**
   * getSongsBySearch
   *
   * standard call
   * http://localhost/piMoo/api/2/getSongsBySearch.php?s=farnham
   * http://localhost/piMoo/api/2/getSongsBySearch.php?s=genesis&o=50&m=100
   * http://localhost/piMoo/api/2/getSongsBySearch.php?s=appa&o=0&m=1&dl=3 // returns some junk but also "ABBA"
   * http://localhost/piMoo/api/2/getSongsBySearch.php?s=Sia&cs // case sensitive search for "Sia". Prevend finding stuff like "Asia"
   *
   * &s= string -> the search string
   * &o= integer -> offset
   * &m= integer -> max search results
   * &dl= damerau levenstein algo to have tolerant search
   * $cs= boolean -> case sensitivity
   *
   * examples of JSON response code
   * {"RC":0,"bySearch":[{"a":"Singles\/John Farnham - You\u00b4re the voice.mp3","i":"John Farnham","t":"You&#180;re the voice","p":"4:49","l":""},{"a":"Singles\/John Farnham - Thats freedom.mp3","i":"John Farnham","t":"That's freedom","p":"4:19","l":""}]}
   * {"RC":23}
   *
   * 0 -> JSON_RC_OK
   * 1 -> JSON_RC_OK_BYCACHE
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   *
   */

  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';
  require_once '../../lib/utils.php';
  require_once '../../lib/class_cache.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  if (!isset($_GET['s']))
  {
    eventLog(JSON_RC_REQUIRED_PARAM_MISSING . COLON . BLANK. 'in api/1/getSongsBySearch.php', TWODIRUP);
    $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
  }
  else
  {
    // -- that has to be decided and handled by the app -> the user of the API/1 interface --
    // -- no, in fact that is handled by the search engine itself and cannot decided here --
    //$search = strtolower(($_GET['s']));

    $offset   = (isset($_GET['o'])) ? $_GET['o'] : NULLINT;
    $max      = (isset($_GET['m'])) ? $_GET['m'] : $cfg_maxSearchResultsPerPage;
    $DL_Level = (isset($_GET['dl'])) ? $_GET['dl'] : false;
    $caseSens = (isset($_GET['cs'])) ? true : false;

    // -- cache handling --
    $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
    $myCache->setCachePath(TWODIRUP . $cfg_cachePath);
    $myCache->setCacheFile(CACHE_ITEMS_BY_SEARCH . $_GET['s'] . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $max . CACHE_RSLT_VIEWFORM . CACHE_RSLT_JSON . CACHE_RSLT_LANG . $lang . CACHE_RSLT_DAMLEV . $DL_Level . CACHE_RSLT_CASESENSITIVITY . $caseSens);

    // -- take it from cache --
    if ($myCache->getIsCacheFileAlive())
    {
      //-- add an info for this would mean to decode add info and encode again: where's the speed enhancement, huh? --
      // therefore NO: $json[JSON_RC_ADDINFO] = JSON_RC_BYCACHE;
      $output = file_get_contents($myCache->getCacheFile());
      unset($myCache);
    }
    // -- otherwise show it native and build cache --
    else
    {
      unset($myCache);
      // -- ------------------------------- --
      // -- begin of the the very main core --
      // -- ------------------------------- --
      //moved upper: $DL_Level = (isset($_COOKIE['int_DL_level'])) ? $_COOKIE['int_DL_level'] : 0;
      require_once '../../api_mediabase.php';

      $myMediaBase = new class_mediaBase(TWODIRUP);
      $results = $myMediaBase->getSearchResult($_GET['s'], $offset, $max, $DL_Level, $caseSens);

      $json[JSON_RC] = JSON_RC_OK;

      $data = array();
      foreach ($results as $result)
      {
        $tmp = array('a' => (string)$result['a'], 'i' => (string)$result['i'], 't' => (string)$result['t'], 'p' => (string)$result['p'], 'l' => (string)$result['l']);
        $data[] = $tmp;
      }
      $json['data'] = $data;

      unset($myMediaBase);

      $output = json_encode($json);

      // -- build cache for next time --
      $myCache = new class_outputCache();
      $myCache->setCachePath(TWODIRUP . $cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_SEARCH . $_GET['s'] . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $max . CACHE_RSLT_VIEWFORM . CACHE_RSLT_JSON . CACHE_RSLT_LANG . $lang . CACHE_RSLT_DAMLEV . $DL_Level . CACHE_RSLT_CASESENSITIVITY . $caseSens);
      $myCache->putCacheContent($output);
      unset($myCache);
    }

    // -- "execute" normal way --
    echo $output;
    unset($output);
  }

  // -- make a delay to avoid DOS-attacks --
  usleep(ANTI_DOS_DELAY);
