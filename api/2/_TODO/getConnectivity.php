<?php
  /**
   * getConnectivity
   *
   * standard call
   * http://localhost/piMoo/api/2/getConnectivity.php
   *
   *
   * examples of JSON response code
   * {"RC":0}
   *
   */
  require_once '../../defines.inc.php';
  require_once '../../keep/config.php';

  cntCreateJSONheader();
  cntCreateCORSheader();

  $json = array();

  $json[JSON_RC] = JSON_RC_OK;

  echo json_encode($json);

  // -- make a delay to avoid DOS-attacks (only for write access) --
  usleep(ANTI_DOS_DELAY);

