<?php
  /**
   * songAdd
   *
   * standard call
   * http://localhost/piMoo/api/2/songAdd.php?a=Albums/Metallica/Ride the lightning/Metallica - (03) For Whom The Bell Tolls.mp3&i=Metallica&t=For Whom The Bell Tolls&p=5:25&w=devel&l=Ride The Lightning
   *
   *
   * examples of JSON response code
   * {"RC":0}
   * {"RC":23}
   * {"RC":30}
   *
   * 0 -> JSON_RC_OK
   * 23 -> JSON_RC_REQUIRED_PARAM_MISSING
   * 30 -> JSON_RC_FAILED_SONG_ADD_PLAYLIST
   *
   */

  require_once '_coreBeg.php';


  if ($accessGranted)
  {
    if (
           !isset($_GET['a'])
        || !isset($_GET['i'])
        || !isset($_GET['t'])
        || !isset($_GET['p'])
        || !isset($_GET['l'])
      )
    {
      eventLog(JSON_RC_REQUIRED_PARAM_MISSING, $apidepth);
      $json[JSON_RC] = JSON_RC_REQUIRED_PARAM_MISSING;
    }
    else
    {
      require_once $apidepth.'api_playlist.php';
      $myPlaylist = new class_playlist($apidepth);

      $base64 = (isset($_GET['base64'])) ? true : false;

      $toTop = (isset($_GET['toTop'])) ? true : false;

      $myPlaylist->setWishedBy($myNickname->getNickname());

      if ($base64)
      {
        $myPlaylist->addSong(base64_decode( $_GET['a'] ), base64_decode($_GET['i']), base64_decode($_GET['t']), $_GET['p'], base64_decode($_GET['l']), $isAdminByToken);
        $msg = $myPlaylist->getLastMsg();
      }
      else
      {
        $myPlaylist->addSong(( $_GET['a'] ), ($_GET['i']), ($_GET['t']), $_GET['p'], ($_GET['l']), $isAdminByToken);
        $msg = $myPlaylist->getLastMsg();
      }

      // -- if wished set top at all then do it and check ... --
      $checkToTop = false;
      if ($toTop)
      {
        if ($isAdminByToken)
          $checkToTop = $myPlaylist->moveBottomTopSong($isAdminByToken);
      }
      // -- ... otherwise its okay anyway --
      else
        $checkToTop = true;


      eventLog($myPlaylist->getLastMsg() . COLON . BLANK .  $_GET['t'], $apidepth);
      if ($msg !== false && $checkToTop === true)
      {
        $json[JSON_RC] = JSON_RC_OK;
      }
      else
      {
        $json[JSON_RC] = JSON_RC_FAILED_SONG_ADD_PLAYLIST;
        // -- strip_tags -> removes html tags from the message --
        $json[JSON_RC_ADDINFO] = strip_tags($myPlaylist->getLastMsg());
      }

      unset($myPlaylist);
    }
  }

  require_once '_coreEnd.php';
