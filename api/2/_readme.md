# This is version 2 of the piMoo API.


## 2023-01-18:
- Add trustedEnviroment to avoid flooding the system by using anonymous nicknames.

## 2022-12-16:
- Some fundamental endpoints are working: start, stop, add song, get playlist and so on.

## 2022-10-14:
- Drops support for piMooApp-version 2020-12-26 V1.0.3.
- This version is going to be pure JSON REST API.
- Also a goal is to have a pure token based authentication working (without session).








