<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  if (!isset($_GET['value']))
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    // -- without any reason as cookie :o) --
    setcookie('bool_SongOmnipresent', $_GET['value'], strtotime('+365 days'));

    switch ($_GET['value']) // -- do not use the cookie here, it is not updated yet --
    {
      case YES:
        {
          $msg =  LNG_SHOW_SONG_OMNIPRESENT . ARROW . '<span style="font-weight: bold;">' . LNG_YES . '</span>';
          break;
        }
      case NO:
        {
          $msg = LNG_SHOW_SONG_OMNIPRESENT . ARROW . '<span style="font-weight: bold;">' . LNG_NO . '</span>';
          break;
        }
    }
    //$msg = LNG_SHOW_SONG_OMNIPRESENT . ARROW . $_GET['value'];
  }

  echo $msg;