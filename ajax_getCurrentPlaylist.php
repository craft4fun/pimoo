<?php
  // -- special piMoo stuff --
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('lib/utils.php');
  require_once ('api_playlist.php');
  require_once ('api_layout.php');

  $myPlaylist = new class_playlist();
  echo makeCurrentPlaylistView($myPlaylist->getWholePlaylist());
  unset($myPlaylist);
