<?php
  // -- feedback is stored directly in the mediabase which is faster in the following usage --
  // -- BUT, to be able to import a mediabase again and again without loosing given feedback - the feedback is always stored here, too --

  //require_once ('defines.inc.php');
  require_once ('lib/utils.php');

  class class_feedback
  {
    private string $depth;
    private string $feedbackXMLFile;
    private SimpleXMLElement $xml;
    private string $curSong;
    private mixed $curSongFound;
    private string $lastMsg;
    private string $curSongComment;
    private string $curSongCommentBy;
    private string $curSongRating;
    private string $curSongRatingBy;


    function __construct($aDepth = NULLSTR)
    {
      global $cfg_feedbackFile;

      $this->depth = $aDepth;

      $this->feedbackXMLFile = $this->depth . $cfg_feedbackFile;

      // -- if no feedback file exists, create a new one --
      if (!file_exists($this->feedbackXMLFile))
      {
        $this->_createNew($this->feedbackXMLFile);
      }

      $this->xml = simplexml_load_file($this->feedbackXMLFile);


      // -- for writing --
      $this->curSong = NULLSTR;
      $this->curSongFound = false;

      // -- for reading --
      $this->findSongReset();

      $this->lastMsg = NULLSTR;
    }


    // -- ---------------- --
    // -- begin of helpers --
    // -- ---------------- --
    private function _createNew($aFile)
    {
      $newfile = fopen($aFile, 'w');
      fwrite($newfile, '<?xml version="1.0" encoding="UTF-8"?>'.LFC.'<feedback></feedback>');
      fclose($newfile);
      unset($newfile);
    }
    // -- -------------- --
    // -- end of helpers --
    // -- -------------- --

    function getLastMsg()
    {
      return $this->lastMsg;
    }

    // -- until now only counting --
    function getAllValued()
    {
      return count($this->xml->i);
    }


    // -- --------------------------------- --
    // -- begin of save a comment or rating --

    /**
     * set the song which should be commented or rated
     * if it could not be found a new enty will be made
     * stores $i in $this->curSongFound if it has been found, false if it has been created
     */
    function setSong($aSong)
    {
      //$result = false;
      $this->curSongFound = false; // -- if it could not be found, create a new entry in the following functions "setComment and/or setRating" --
      $this->curSong = $aSong;
      if ($this->xml)
      {
        $i = 0;
        foreach ($this->xml->i as $item)
        {
          //if ($item->a == $aSong) -> not good enough on real compare! better with strcmp()

          //if (strcmp(basename($item->a), $this->curSong) == 0) // -- only the filename matters without album-path --
          //if (strcmp($item->a, $this->curSong) == 0) // -- but I want to be specific to differ between singles and albums --
          // -- The simple file matters, whereever in which album! Works also for piMoo AND piMooReverse. --
          if (strcmp(basename($item->a), basename($this->curSong)) == 0)
          {
            $this->curSongFound = $i;
            //$result = true;
            break;
          }
          $i++;
        }
      }
      //eventLog('DEBUG: SONG (feedback): ' . $aSong, $this->depth);
      //return $result;
    }

    /**
     *
     * @param string $aComment
     * @param string $aBy
     * @return boolean
     */
    function setComment($aComment, $aBy = NULLSTR)
    {
      $result = false;

      //never, bad on empty xmls if ($this->xml)
      {
        $attr = XML_BY;
        // -- take the existing entry --
        if ($this->curSongFound !== false)   //if ($this->curSongFound)
        {
          // -- why searching a twice? :o) --
          // foreach ($this->xml->i as $item) {
          //   if (strcmp($item->a, $this->curSong) == 0) {
          //      $item->c = htmlspecialchars( trim($aComment) );
          //      $item->c->attributes()->$attr = $aBy;
          //      eventLog('CHANGED COMMENT: "' . trim($aComment) . '" for song: ' . $this->curSong);
          //      break; } }
          $this->xml->i[$this->curSongFound]->c = htmlspecialchars( trim($aComment) );
          $this->xml->i[$this->curSongFound]->c->attributes()->$attr = $aBy;

          eventLog('CHANGED COMMENT in feedback: "' . trim($aComment) . '" for song: ' . $this->curSong, $this->depth);
          $this->lastMsg = LNG_SUC_COMMENTED;
          $result = true;
        }
        // -- create a new --
        else
        {
          $new = $this->xml->addchild('i');
          $new->addchild('a', htmlspecialchars( $this->curSong ));
          $new->addchild('c', htmlspecialchars( trim($aComment) ));
          $new->c->addAttribute(XML_BY, $aBy);
          // -- prepare the "friend" --
          $new->addchild('r', NULLSTR);
          $new->r->addAttribute(XML_BY, NULLSTR);
          // -- that's important on import issues and does not affect common stuff --
          $this->curSongFound = true;

          eventLog('SET COMMENT in feedback: "' . trim($aComment) . '" for song: ' . $this->curSong, $this->depth);
          $this->lastMsg = LNG_SUC_COMMENTED;
          $result = true;
        }
      }

      return $result;
    }


    /**
     *
     * @param string $aRating
     * @param string $aBy
     * @return boolean
     */
    function setRating($aRating, $aBy = NULLSTR)
    {
      $result = false;

      //never, bad on empty xmls if ($this->xml)
      //{
        $attr = XML_BY;
        // -- take the existing entry --
        if ($this->curSongFound !== false)   //if ($this->curSongFound)
        {
          $this->xml->i[$this->curSongFound]->r = $aRating;
          $this->xml->i[$this->curSongFound]->r->attributes()->$attr = $aBy;

          eventLog('CHANGED RATING in feedback: "' . $aRating . '" for song: ' . $this->curSong, $this->depth);
          $this->lastMsg = LNG_SUC_RATED;
          $result = true;
        }
        // -- create a new --
        else
        {
          $new = $this->xml->addchild('i');
          $new->addchild('a', htmlspecialchars( $this->curSong ));
          $new->addchild('r', $aRating);
          $new->r->addAttribute(XML_BY, $aBy);
          // -- prepare the "friend" --
          $new->addchild('c', NULLSTR);
          $new->c->addAttribute(XML_BY, NULLSTR);
          // -- that's important on import issues and does not affect common stuff --
          $this->curSongFound = true;

          eventLog('SET RATING in feedback: "' . $aRating . '" for song: ' . $this->curSong, $this->depth);
          $this->lastMsg = LNG_SUC_RATED;
          $result = true;
        }
      //}

      return $result;
    }


    /**
     * saves the feedback file, after comment or rating has been set
     * @return boolean
     */
    function saveFeedback()
    {
      $result = false; //$result = 'huh?';

      // -- ... write to XML file now --
      $handle = fopen($this->feedbackXMLFile, 'wb');
      if (!fwrite($handle, $this->xml->asXML()))
      {
        $this->lastMsg = LNG_ERR_UNKNOWN;
        eventLog('ERROR in function saveFeedback() in file api_feedback.php');
      }
      else
      {
        $result = true;
      }
      fclose($handle);

      // -- Note: large feedback.xml slow down the whole searching --
      if ($result && CFG_HUMAN_READABLE_FEEDBACK)
      {
        cntMakeHumanXML($this->feedbackXMLFile);
      }

      return $result;
    }

    // -- end of save a comment or rating --
    // -- ------------------------------- --



    // -- --------------------------------- --
    // -- begin of find a comment or rating --

    /**
     * finds a wished song and returns rating and value in the following functions getComment... and so far
     * if it could not be found the following code by the caller has to abort the rest by negative result here
     * @param string $aSong
     * @return boolean
     */
    function findSong($aSong)
    {
      $result = false;

      if ($this->xml) //$xml = simplexml_load_file($this->feedbackXMLFile);
      {
        $attr = XML_BY;
        foreach ($this->xml->i as $item)
        {
          if (strcmp($item->a, $aSong) == 0) //if ($item->a == $aSong)
          {
            if (isset($item->c))
            {
              $this->curSongComment = $item->c;
              $this->curSongCommentBy = $item->c->attributes()->$attr;
            }
            if (isset($item->r))
            {
              $this->curSongRating = $item->r;
              $this->curSongRatingBy = $item->r->attributes()->$attr;
            }
            $result = true;
            break;
          }
        }
      }

      return $result;
    }


    function getComment()
    {
      return $this->curSongComment;
    }
    function getCommentBy()
    {
      return $this->curSongCommentBy;
    }

    function getRating()
    {
      return $this->curSongRating;
    }
    function getRatingBy()
    {
      return $this->curSongRatingBy;
    }


    function findSongReset()
    {
      $this->curSongComment = NULLSTR;
      $this->curSongCommentBy = NULLSTR;
      $this->curSongRating = NULLSTR;
      $this->curSongRatingBy = NULLSTR;
    }


    // -- end of find a comment or rating --
    // -- ------------------------------- --


    // -- ------------------ --
    // -- additional helpers --
    function getCount()
    {
      return $this->xml->count();
    }

  }

