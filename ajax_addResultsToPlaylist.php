<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  include_once ('api_playlist.php');


  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  if (!$admin)
  {
    echo LNG_ERR_ONLYADMIN;
  }
  else
  {
    if (isset($_GET['filename']))
    {
      $resultFile = $_GET['filename'];

      $msg = NULLSTR;

      if (!file_exists($resultFile))
      {
        $msg .= 'file not available - strange, but no stress';
      }
      else
      {
        //-- get nickame --
        $wishedBy = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : $_SERVER['REMOTE_ADDR'];

        $myPlaylist = new class_playlist();
        $myPlaylist->setWishedBy($wishedBy);

        $historyPlaylist = simplexml_load_file($resultFile);
        foreach ($historyPlaylist as $song)
        {
          $check = $myPlaylist->addSong($song->a, $song->i, $song->t, $song->p, $song->l, $admin);
          eventLog($myPlaylist->getLastMsg() . COLON . BLANK .  $song->t); // -- no longer required: strip_tags() --

          if ($check === true)
            $msg .= $myPlaylist->getLastMsg() . BLANK . '"' . $song->t . '"' . LFH;
          else
            $msg .= cntErrMsg($myPlaylist->getLastMsg() . BLANK . '"' . $song->t . '"' . LFH);

        }
      }
      unset($historyPlaylist);
      unset($myPlaylist);
      echo $msg;
    }
  }


