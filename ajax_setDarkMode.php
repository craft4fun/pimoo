<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  if (!isset($_GET['value']))
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    // -- because needed in javascript environment in clientside a cookie is required --
    setcookie('bool_darkMode', $_GET['value'], strtotime('+365 days'));

    switch ($_GET['value']) // -- do not use the cookie here, it is not updated yet --
    {
      case NO:
        {
          $msg = LNG_DARKMODE.ARROW.'<span style="font-weight: bold;">'.LNG_NO.'</span>';
          break;
        }
      case YES:
        {
          $msg = LNG_DARKMODE.ARROW.'<span style="font-weight: bold;">'.LNG_YES.'</span>';
          break;
        }
    }
    //$msg = LNG_SEARCH_RESULT_AS_TABLE . ARROW . $_GET['value'];
  }

  echo $msg;