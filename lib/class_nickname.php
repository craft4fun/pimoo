<?php
  /**
   * a class to manage the nicknames
   * avoid using already occupied nicknames
   * @author paddy
   *
   */
  class class_nickname
  {
    private $depth = NULLSTR;
    private $nickname = NULLSTR;
    private $nicknamePath = NULLSTR;
    private $allowMultiNickname = NULLSTR;
    private $clientIP = NULLSTR;
    private $lastDetectedNickIP = NULLSTR;
    private $silent = false; // -- oppress some loggings, e.g. coming from api and have something to log every call --

    function __construct($depth = NULLSTR)
    {
      global $cfg_tmpPath;
      global $cfg_allowMultiNickname;

      $this->depth = $depth;
      $this->allowMultiNickname = $cfg_allowMultiNickname;

      $this->nicknamePath = $this->depth . $cfg_tmpPath . DIR_NICKNAMES;
      if (!is_dir($this->nicknamePath))
        mkdir($this->nicknamePath);

      $this->clientIP = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : QUESTMARK;
      $this->lastDetectedNickIP = NULLSTR; // -- internal memory for logging --

      $this->nickname = NULLSTR;
    }


    function setSilent($value)
    {
      $this->silent = $value;
    }

    function getSilent()
    {
      return $this->silent;
    }



    /**
     * check for senseful nicknames and block obviously kidding
     * @param string $aNickname
     * @return boolean
     */
    private function _isValid($aNickname)
    {
      if (
             $aNickname != '::1' && strtolower($aNickname) != 'localhost' && $aNickname != '127.0.0.1'
          && $aNickname != NICK_AUTO && $aNickname != NICK_TRIGGER
          && trim($aNickname) != NULLSTR
        )
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    private function _isFree($aNickname)
    {
      // -- check if nickname is free --
      $check = scandir($this->nicknamePath);
      $nickIP = (file_exists($this->nicknamePath . $aNickname)) ? file_get_contents($this->nicknamePath . $aNickname) : false;
      $this->lastDetectedNickIP = $nickIP;
      if (
              $this->allowMultiNickname
           || !in_array($aNickname, $check)
           || ($this->clientIP === $nickIP)
           || (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] == $aNickname) // -- done twice? --
         )
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    /**
     * returns the current set nickname
     * @return string
     */
    function getNickname()
    {
      return $this->nickname;
    }

    /**
     * pushes the nickname to the nickname path "register"
     * @param string $nickname
     * @param string $current
     */
    function push($nickname, $current = NULLSTR)
    {
      $result = false;

      $nickname = trim($nickname);

      if (!$this->_isValid($nickname))
      {
        eventLog(LNG_NICK_NOT_VALID . COLON . BLANK . $nickname, $this->depth);
      }
      else
      {
        if (!$this->_isFree($nickname))
        {
          eventLog(LNG_NICK_ALREADY_USED.COLON.BLANK.$nickname.', on IP: '.$this->lastDetectedNickIP, $this->depth);
        }
        else
        {
          // -- delete the old --
          if ($current != NULLSTR)
            $this->dropForeign($current);

          if (!isset($_SESSION))
            session_start();

          // -- NOTE: this is for the special http caller only --
          $_SESSION['str_nickname'] = $nickname;
          // -- hold current nickname in object --
          $this->nickname = $nickname;

          // -- push (new) nickname --
          file_put_contents($this->nicknamePath . $nickname, $this->clientIP);

          if (!$this->silent)
            eventLog('set nickname: ' . $nickname . ' (old: ' . $current . ')', $this->depth);

          $result = true;
        }
      }
      return $result;
    }

    /**
     * in difference to drop, this does not destroy the OWN session
     * @param string $aNickname
     * @param string $aDepth
     * return boolean
     */
    function dropForeign($aNickname, $aDepth = NULLSTR)
    {
      $result = false;

      if (file_exists($this->nicknamePath . $aNickname) && !is_dir($this->nicknamePath . $aNickname))
      {
        if (unlink($this->nicknamePath . $aNickname))
          $result = true;
        else
          eventLog('failed to unlink hook from filesystem while loggin out: ' . $aNickname, $this->depth);
      }
      else
        eventLog('logged out: ' . $aNickname . ' (but nickname hook in filesystem was not available)', $this->depth);

      return $result;
    }



    /**
     * deletes the nickname from the nickname path "unregister"
     * @param string $aNickname
     * return boolean
     */
    function drop($nickname)
    {
      $result = false;

      session_unset();
      if (!session_destroy())
      {
        eventLog('failed to destroy session while loggin out: '.$nickname, $this->depth);
      }
      else
      {
        if (!file_exists($this->nicknamePath.$nickname) || is_dir($this->nicknamePath.$nickname))
        {
          eventLog('logged out: '.$nickname.' (but nickname hook in filesystem was not available)', $this->depth);
          // -- as an exception, this is also okay - would only confuse the user to show a bad message --
          $result = true;
        }
        else
        {
          if (!unlink($this->nicknamePath.$nickname))
          {
            eventLog('failed to unlink hook from filesystem while loggin out: '.$nickname, $this->depth);
          }
          else
          {
            eventLog('logged out: ' .$nickname, $this->depth);
            $result = true;
          }
        }
      }

      return $result;
    }

    /**
     * show already logged in users by their nickname
     */
    static function showCurrentNicknames($depth = NULLSTR)
    {
      global $cfg_tmpPath;

      $output = NULLSTR;

      $nicknames = $depth.$cfg_tmpPath.DIR_NICKNAMES;
      if (is_dir($nicknames))
      {
        $files = scandir($nicknames);
        if (count($files) > 2) // -- . and .. are always there --
        {
          $output .= '<div class="takenNicknameArea centerAlignedBlock">';
            $output .= '<div style="margin: 0em 0em 1em 1em; font-style: italic;">'.LNG_TAKEN_NICKNAMES.'</div>';
            foreach($files as $nicks)
            {
              if (substr($nicks, 0, 1) != DOT) // -- show no hidden file or dir like .git --
              {
                // -- call "promptNickname" and following "setNickname" WITH DELAY 10 times faster 200 instead of default 2000 --
                // -- downside: there is a risk, that the nickname is already taken and then one would see no error message --
                // -- one would return to the login page and has to find another nickname by trying --
                $output .= '<div id="nickID'.$nicks.'" class="takenNicknames">'.cntMBL('<a class="linkNice" href="javascript:promptNickname(\''.$nicks.'\', 200)">'.$nicks.'</a>').'</div>';
              }
            }
          $output .= '</div>';
        }
      }

      return $output;
    }


  } // -- end of class_nickname --


  /* -- usage of class_nickname --
    $myNickname = new class_nickname();
    if ($myNickname->push($_GET['nickname'], 'theOldNickname'))
      echo 'Thanks <span style="font-weight: bold;">' . $_GET['nickname'] . '</span>';
    else
      echo cntErrMsg(LNG_TAKE_ANOTHER_NICK);
    unset($myNickname);
  */
