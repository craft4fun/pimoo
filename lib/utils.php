<?php
/* -- table of content --

  -- utils -> former named "classes n tools". Thats the reason for the partly used prefix "cnt" --

  function cntPSS($aString)
  function cntGetFileExt($aFilename)
  function cntAsynchCommand($aCommand, $aPath = SLASH, $aCLI = true)
  function cntSubsituteCriticalJSChars($aString)
  function cntSubsituteAmpersand($aStr)
  function cntCCSC($aStr)
  function cntMakeHumanXML($aXMLFile)
  function cntGetTimeDiffAsSecondsFromUnix($aTime)
  function cntDelTree($path, $aDirToo = false)
  function cntFilecount($aFolderPath)
  function cntIsMobile()
  function cntIsConsole()
  function cntStdMsg ($aMessage)
  function cntOkMsg ($aMessage)
  function cntErrMsg($aMessage)
  function cntMBL($aLink)
  function cntTrim($aStr)
  function util_coverStringsInApostrophes()
  function util_coverArrayItemsInApostrophes()
  function eventLog($aEvent, $aDepth = NULLSTR)
  function moveElementInArray($array, $toMove, $targetIndex)
  function cntGradientFromTo($aFromCol, $aToCol, $aGraduations = 10)
  function cntGetHumanRating($aRating)
  function cntGetEmojiRating($rating)
  class_nickname.php // -- swapped out in a separate file 2022-12-16 --
  function cntGetAlbumPicURL($aSong, $aAlbum, $aSmallDummy = false)
  function cntGetDriveUsageInPercent($aDriveORPath)
  function cntGetServerMemoryUsage()
  function cntGetServerCPU_usage()
  function cntGetCPU_temperature()
  function util_systemShutdown()
  function cntCreateJSONheader()
  function cntCreateCORSheader()
  function getPLRI()
  function writeMP3TagsToMediabase($aFile)
  function recursive_writeMP3TagsToMediabase
  function updateMP3TagsToMediabase($aFile)
  function recursive_updateMP3TagsToMediabase
  function createShadowConfig()
  function cli_progressbar($overall, $current, $bar_length = 77)
  function utf8encode($strToEncode, $encoding=UTF8)


*/

  /**
   * prepare song string -> add blank before and pack into apostrophe
   * wow paddy, you surpassed yourself ;)
   *
   * @param string $string
   * @return string
   */
  function cntPSS(string $string): string
  {
    $prep = str_replace('\'', '\'\\\'\'', $string);
    return ' \'' . $prep . '\'';
  }


  /**
   * Returns file extension (and if not exists false)
   *
   * @param string $filename
   * @return string|false
   */
  function cntGetFileExt(string $filename): string|false
  {
    $path_parts = pathinfo($filename);
    if (isset($path_parts['extension']))
    {
      return $path_parts['extension'];
    }
    else
    {
      return false;
    }
  }



  /**
   * do asynchronous line command
   * returns false if given path does not exists
   * @param string $aCommand
   * @param string $aPath
   * @param boolean $aCLI
   * @return string
   */
  function cntAsynchCommand($aCommand, $aPath = SLASH, $aCLI = true)
  {
    // this works, new php feature? yes the backticks are the same than shell_exec: echo `whoami`;
    $result = false;
    $old = getcwd();
    if (file_exists($aPath))
    {
      chdir($aPath);
      $result = exec(CURDIR . $aCommand . ' > /dev/null &');
      chdir($old); // -- this solves some problems --
      return (string)$result;
    }
  }



  // -- substitute critical chars for javascript (e.g.alerts) --
  function cntSubsituteCriticalJSChars($aString)
  {
    $result = str_replace('\'', '&acute;', $aString);
    return $result;
  }


  /**
   * converts a string to a HTML and javascript usable URL string
   * used by iMoo
   * @param string $aStr

  function cntStrToHTMLStr($aStr)
  {
      $tmp = str_replace('&', '%26', $aStr);
      return str_replace('\'', '&acute;', $tmp);
  }
  */

/*
  function cntSubsituteAmpersand($aStr)
  {
    return str_replace(chr(38), '&amp;', $aStr);
  }

  // -- correct confused htmlSpecialChar like: &amp;amp; --
  function cntCCSC($aStr)
  {
    return str_replace('&amp;amp;', '&amp;', $aStr);
  }
*/

/*
  // -- substitute critical chars for XML --
  function cntSubsituteCriticalXMLChars($aString)
  {
return str_replace('&', '+', $aString);
return htmlspecialchars($aString);
    $result = str_replace('\'', '&acute;', $aString);
    $result = str_replace('"', '&quot;', $result);
    $result = str_replace('&', '&amp;', $result);
    return $result;
  }
*/

  function cntMakeHumanXML($aXMLFile)
  {
  /*
      $dom = new DOMDocument('1.0');
      //$dom->preserveWhiteSpace = false; // -- this leads to error where a second click is needed to move a song in the playlist --
      //$dom->preserveWhiteSpace = true; // -- this not, but the human readable does not work then --
      $dom->formatOutput = true;
      $dl = @$dom->load($aXMLFile); // remove error control operator (@) to print any error message generated while loading.
      if (!$dl)
        return false; //('Error while parsing the document: ' . $xmlFile);
      else
      {
        $dom->save($aXMLFile);
        return true;
      }
  */
    // -- this works fine --
    $result = false;
    $dom = new DomDocument();
    $dom->formatOutput = true;
    $dom->preserveWhiteSpace = false;
    $dom->load($aXMLFile);
    if ($dom->save($aXMLFile))
      $result = true;
    return $result;
  }




  // -- Liefert die Differenz in Sekunden zwischen einen Unix Timestamp und dem aktuellen --
  function cntGetTimeDiffAsSecondsFromUnix($aTime)
  {
    return time() - $aTime;
  }

  /**
   * parse a given string like 2:30 into 160 seconds --
   * @param mixed (string) $MinSecString
   * @return int|float
   */
  function parseHumanMinSecStringToSeconds(mixed $MinSecString, bool $asInt = false): int|float
  {
    $time = explode(COLON, $MinSecString);
    $min = (isset($time[0])) ? intval($time[0]) : '0';
    $sec = (isset($time[1])) ? intval($time[1]) : '00';

    if (!$asInt)
    {
      return $min * 60 + $sec;
    }
    else
    {
      return intval($min * 60 + $sec);
    }
  }

  /**
   * parse seconds to human MinSec string
   *
   * @param [mixed] $aSeconds
   * @return string
   */
  function parseSecondsToHumanMinSecString(mixed $aSeconds): string
  {
    $seconds = (int)$aSeconds;
    $min = floor($seconds / 60);
    $sec = $seconds % 60;
    if ($min >= 0 && $sec >= 0)
    {
      return $min . COLON . str_pad($sec, 2, 0, STR_PAD_LEFT);
    }
    else
    {
      return '-0:00';
    }
  }



  // -- kill recursively all files and sub folders --
  function cntDelTree($path, $aDirToo = false)
  {
    $result = false;
    $path = rtrim($path, SLASH).SLASH;
    $handle = opendir($path);
    if (is_dir($path)) // -- avoids big trouble occuring in while(false !== ($file = readdir($handle))) --
    {
      while(false !== ($file = readdir($handle))) // Warning: readdir() expects parameter 1 to be resource, boolean given in /home/paddy/workspace_svn/KAMCOSnet/lib/utils.php on line 359
      {
        if($file != DOT and $file != DDOT )
        {
          $fullpath = $path.$file;
          if(is_dir($fullpath))
            cntDelTree($fullpath, $aDirToo);
          else
            unlink($fullpath);
        }
      }
      closedir($handle);
      if ($aDirToo)
        $result = rmdir($path);
      else
        $result = true;
    }
    // -- pseudo --
    return $result;
  }


  function cntFilecount($aFolderPath)
  {
    $filescount = 0;
    // Open the directory
    $dir = opendir($aFolderPath);
    // if the directory doesn't exist  return 0
    if (!$dir){return 0;}
    // Read the directory and get the files
    while (($file = readdir($dir)) !== false)
    {

      if ($file[0] == DOT)
        continue;

      //if '.' it is a sub folder and call the function recursively
      if (is_dir($aFolderPath.$file))
      {
        // Call the function if it is a folder type
        $filescount += cntFilecount($aFolderPath.$file.DIRECTORY_SEPARATOR);
      }
      else
      {
        // Increment the File Count.
        $filescount++;
      }
    }
    // close the directory
    closedir($dir);
    return $filescount;
  }



  function cntIsMobile()
  {
    global $cfg_timeZone;

    $timeZone = (isset($cfg_timeZone)) ? $cfg_timeZone : TZ_UTC;
    date_default_timezone_set($timeZone);

    // -- check for not running on  cli --
    $sapi_type = php_sapi_name();
    if (strtolower(substr($sapi_type, 0, 3)) != 'cli')
    {
      // -- begin of core --
      if (!isset($_SESSION)) // -- session starting here is an exception as an emergency case --
        session_start();
      if (isset($_SESSION['forceIsMobile']))
      {
        // -- if it was forced to be a mobile or not --
        return $_SESSION['forceIsMobile'];
      }
      else
      {
        // -- let the browser choose --
        $useragent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        return $useragent && (preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sandroid, iphone, handybrowser, mobilbrowser, mobil, handy, iemobile, symbiana(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)));
      }
      // -- end of core --
    }
  }


  function cntIsConsole()
  {
    $useragent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
    // return $useragent && (preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sandroid, iphone, handybrowser, mobilbrowser, mobil, handy, iemobile, symbiana(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)));
    return $useragent && (preg_match('/android|lynx|m1\-w|m3ga|zte\-/i', substr($useragent, 0, 4)));
  }



  /**
   * make a standard message inside the target_feedback
   * usually done directly by the html/javascript code
   * using this should be an exception
   */
  function cntStdMsg ($aMessage)
  {
    if ($aMessage != NULLSTR)
      return '<span id="StdMsg">'.$aMessage.'</span>';
  }

  /**
   * make a ok message inside the target_feedback
   */
  function cntOkMsg ($aMessage)
  {
    if ($aMessage != NULLSTR)
      return '<span id="okMsg">'.$aMessage.'</span>';
  }

  /**
   * make a error message inside the target_feedback
   */
  function cntErrMsg($aMessage)
  {
    if ($aMessage != NULLSTR)
      return '<span id="errorMsg">'.$aMessage.'</span>';
  }



  /**
   * make a link embedded in brackets indepentent from style
   * @param string $aLink
   * @return string
   */
  function cntMBL($aLink)
  {
    return '['.BLANK.$aLink.BLANK.']';
  }

  /**
   * optionaly do not trim the search string
   * not trimming the blank is special, e.g. to find "Sia" and NOT "Asia" with a search pattern like " Sia"
   * @param string $aStr
   * @return mixed
   */
  function cntTrim($aStr)
  {
    $cutBlank = (isset($_COOKIE['bool_cutBlank'])) ? $_COOKIE['bool_cutBlank'] : YES;

    // -- trim the blank is default --
    if ($cutBlank == YES)
      return trim($aStr);
    // -- not trimming the blank is special, e.g. to find "Sia" and NOT "Asia" with a search pattern like " Sia" --
    else
      return $aStr;
  }

  /**
   * convert single, comma separated strings in to apostrophe covered
   * e.g.: Rock, Pop, Progressive Rock -> 'Rock','Pop','Progressive Rock'
   */
  function util_coverStringsInApostrophes($strings)
  {
    if (trim($strings) == NULLSTR)
      return NULLSTR;
    $tmp1 = explode(COMMA, $strings);
    $tmp2 = "'".implode("','",$tmp1)."'";
    //return $tmp2;
    return str_replace(",' " , ",'", $tmp2);
  }

  /**
   * convert single, comma separated strings in to apostrophe covered
   * e.g.: ['Rock','Pop','Progressive Rock'] -> 'Rock','Pop','Progressive Rock'
   */
  function util_coverArrayItemsInApostrophes($array)
  {
    if (empty($array))
      return NULLSTR;
    $tmp2 = "'".implode("','",$array)."'";
    return $tmp2;
    //return str_replace(",' " , ",'", $tmp2);
  }

  /**
   * calculate the number of retries to pick a song that fullfills the conditions (black- whitelist and rating)
   *
   * @param integer $mediabaseCount
   * @return integer
   */
  function util_calcMaxRetryCounter($mediabaseCount)
  {
    //TODO paddy the higher the number of mediabase items AND voted song, the smaller the retry max is required

    if ($mediabaseCount < 1000)
    {
      return $mediabaseCount * 100;
    }
    // -- that's my current case, approx 5000 songs --
    if ($mediabaseCount > 1000 && $mediabaseCount < 10000)
    {
      return $mediabaseCount * 10;
    }
    if ($mediabaseCount > 10000)
    {
      return $mediabaseCount * 2;
    }
  }


  /**
   * -- a log file (old: with the newest entry at top) --
   * -- make ring memory --
   * @param string $event
   * @param string $depth
   */
  function eventLog($event, $depth = NULLSTR)
  {
    global $cfg_logFile;

    // $abs_logFile = __DIR__ . SLASH . $cfg_logFile;

    if ($cfg_logFile !== false) // -- false would disable the logging at all --
    {
      $time = date('Y-m-d H:i:s');
      $loc = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : 'unknown/CLIscript';
      $causer = (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR) ? $_SESSION['str_nickname'] : QUESTMARK;

      $logEntry = $time.SEMICOLON.$loc.SEMICOLON.$causer.SEMICOLON.$event.LFC;
      // -- if not existing, create one with first entry --
      if (!file_exists($depth.$cfg_logFile))
      {
        // file_put_contents($depth.$cfg_logFile, $logEntry);
        try
        {
          $check = @file_put_contents($depth.$cfg_logFile, $logEntry);
          if (!$check)
          {
            throw new Exception("Failed to put content in logfile");
          }
        }
        catch (Exception $e)
        {
          // -- NO, never: eventLog('Failure in eventLog(): ' . $e->getMessage(), $depth); --
          if (cntIsConsole())
          {
            echo 'Failure in eventLog(): ' . $e->getMessage();
          }
        }
      }
      else
      {
        // -- a log file with the newest entry at top --
        // $tmp = $logEntry . file_get_contents($aDepth . $cfg_logFile, false, NULL, 0, 100000); // 0.1 MB
        // file_put_contents($aDepth . $cfg_logFile, $tmp);
        // -- linux like normal: watch with tail -f --
        $tmp = file_get_contents($depth.$cfg_logFile, false, NULL, 0, 100000).$logEntry; // 0.1 MB
        file_put_contents($depth.$cfg_logFile, $tmp);
      }
    }

    // -- simply return the event, so the eventLog() can be used to return frontend output in a smarter way (in one line)  --
    return $event;
  }


  /**
   * A function that preserves keys:
   * @param array $array
   * @param integer $toMove
   * @param integer $targetIndex
   * @return mixed <unknown, multitype:unknown >
   */
  function moveElementInArray($array, $toMove, $targetIndex)
  {
    if (is_int($toMove))
    {
      $tmp = array_splice($array, $toMove, 1);
      array_splice($array, $targetIndex, 0, $tmp);
      $output = $array;
    }
    elseif (is_string($toMove))
    {
      $indexToMove = array_search($toMove, array_keys($array));
      $itemToMove = $array[$toMove];
      array_splice($array, $indexToMove, 1);
      $i = 0;
      $output = Array();
      foreach($array as $key => $item)
      {
        if ($i == $targetIndex)
        {
          $output[$toMove] = $itemToMove;
        }
        $output[$key] = $item;
        $i++;
      }
    }
    return $output;
  }




  /**
   * generates a gradient from color A to color B over X steps
   * @param string $aFromCol -> e.g. #ff0000
   * @param string $aToCol -> e.g. #00ff00
   * @param string $aGraduations  -> e.g. 10
   * @return string
   */
  function cntGradientFromTo($aFromCol, $aToCol, $aGraduations = 10)
  {
    // -- catch division by zero --
    if (empty($aGraduations) || $aGraduations < 2)
    {
      $aGraduations = 2;
    }

    $aGraduations--;

    $RedOrigin = hexdec(substr($aFromCol, 0, 2));
    $GrnOrigin = hexdec(substr($aFromCol, 2, 2));
    $BluOrigin = hexdec(substr($aFromCol, 4, 2));

    $GradientSizeRed = (hexdec(substr($aToCol, 0, 2)) - $RedOrigin) / $aGraduations; //Graduation Size Red
    $GradientSizeGrn = (hexdec(substr($aToCol, 2, 2)) - $GrnOrigin) / $aGraduations;
    $GradientSizeBlu = (hexdec(substr($aToCol, 4, 2)) - $BluOrigin) / $aGraduations;

    $RetVal = array();
    for($i=0; $i <= $aGraduations; $i++)
    {
      $RetVal[$i] =
      str_pad(dechex($RedOrigin + ($GradientSizeRed * $i)), 2, '0', STR_PAD_LEFT) .
      str_pad(dechex($GrnOrigin + ($GradientSizeGrn * $i)), 2, '0', STR_PAD_LEFT) .
      str_pad(dechex($BluOrigin + ($GradientSizeBlu * $i)), 2, '0', STR_PAD_LEFT);
    }
    return $RetVal;
  }

  // function cntGradientFromOverTo($aFromCol, $aOverCol ,$aToCol, $aGraduations = 10)
  // lime 00ff00
  // red ff0000


  /**
   * Transform a value like ~4~ in a human readable word like great
   * @param string $aRating
   * @return mixed <string, mixed>
   */
  function cntGetHumanRating($aRating)
  {
  /*      would work fine, but: see below!
      switch ($aRating)
      {
        case RATE1: return RATE1_HUMAN;
        case RATE2: return RATE2_HUMAN;
        case RATE3: return RATE3_HUMAN;
        case RATE4: return RATE4_HUMAN;
        case RATE5: return RATE5_HUMAN;
      }
  */
    // -- must be handled this way, because of style formating to show "old" comments --
    $result = NULLSTR;
    if (strpos($aRating, RATE1) !== false)
      $result = str_replace(RATE1, RATE1_HUMAN, $aRating);
        elseif (strpos($aRating, RATE2) !== false)
          $result = str_replace(RATE2, RATE2_HUMAN, $aRating);
            elseif (strpos($aRating, RATE3) !== false)
              $result = str_replace(RATE3, RATE3_HUMAN, $aRating);
                elseif (strpos($aRating, RATE4) !== false)
                  $result = str_replace(RATE4, RATE4_HUMAN, $aRating);
                    elseif (strpos($aRating, RATE5) !== false)
                      $result = str_replace(RATE5, RATE5_HUMAN, $aRating);
    return $result;
  }

  function cntGetEmojiRating($rating)
  {
    switch ($rating)
    {
      case RATE1:
        return RATE1_DINGBAT;
        break;
      case RATE2:
        return RATE2_DINGBAT;
        break;
      case RATE3:
        return RATE3_DINGBAT;
        break;
      case RATE4:
        return RATE4_DINGBAT;
        break;
      case RATE5:
        return RATE5_DINGBAT;
        break;
    }
  }


  function cntMakeCommentBubble($comment)
  {
    //return (trim($comment) == NULLSTR) ? NULLSTR : '<img src="res/comment.png" title="'.$comment.'" />';
    return (trim($comment) == NULLSTR) ? NULLSTR : '<span title="'.$comment.'">'.COMMENT_DINGBAT.'</span>';
  }


  /**
   * returns the previous directory
   */
  function cntGetPreviousDir($aCurDir, $aHandleLastSlash = true)
  {
    if ($aHandleLastSlash)
    {
      // -- wenn das letzte Zeichen ein Slash ist, schneide es ab --
      $length = strlen($aCurDir);
      if (substr($aCurDir, $length - 1, 1) == SLASH)
        $aCurDir = substr($aCurDir, 0, $length - 1);
    }
    $tmp = explode(SLASH, $aCurDir);
    array_pop($tmp);
    return implode(SLASH, $tmp);
  }

  /**
   * try to get the url to an album picture
   * by songname or album tag
   * @param string $aSong -> Path
   * @param string $aAlbum -> Album by tag name
   * @param boolean $aSmallDummy ->  return a small dummy picture
   * @param boolean $negativeResultAsBool -> return false as boolean when no pictures has been found
   */
  function cntGetAlbumPicURL($aSong, $aAlbum, $aSmallDummy = false, $negativeResultAsBool = false)
  {
    global $cfg_absPathToSongs;
    global $cfg_musicFileType;
    global $cfg_pictureFileType;

    $nickname = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : NULLSTR;

    // -- ----------------------------- --
    // -- first attempt: song file name --

    $pathToSong = dirname($aSong) . SLASH; //old: $cfg_absPathToSongs . $aSong

    $cover = $pathToSong . basename($aSong, DOT . $cfg_musicFileType) . DOT . $cfg_pictureFileType;
    if (file_exists($cfg_absPathToSongs . $cover))
    {
      $cover = str_replace('&', '%26', $cover);
      $picLink = 'lib/picDisp.php?pic='.$cover.'&hash='.md5($nickname);
    }
    else
    {
      // -- ------------------------------------------ --
      // -- second attempt: take the album name by tag --

      $cover = $pathToSong . $aAlbum . DOT . $cfg_pictureFileType;
      if (file_exists($cfg_absPathToSongs . $cover))
      {
        $cover = str_replace('&', '%26', $cover);
        $picLink = 'lib/picDisp.php?pic='.$cover.'&hash='.md5($nickname);
      }
      else
      {
        // -- ------------------------------------------------ --
        // -- third attempt: take the last dirname before song --

        $cover = $pathToSong . basename(cntGetPreviousDir($aSong)) . DOT . $cfg_pictureFileType;
        if (file_exists($cfg_absPathToSongs . $cover))
        {
          $cover = str_replace('&', '%26', $cover);
          $picLink = 'lib/picDisp.php?pic='.$cover.'&hash='.md5($nickname);
        }
        else
        {
          if ($negativeResultAsBool)
            return false;

          // -- ---------------------------- --
          // -- nothing found - take a dummy --
          $darkMode = (isset($_COOKIE['bool_darkMode']) && $_COOKIE['bool_darkMode'] == YES) ? true : false;

          if ($aSmallDummy)
          {
            $picLink = (!$darkMode) ? 'res/piMoo_logo.png' : 'res/piMoo_logoDark.png';
          }
          else
          {
            $picLink = (!$darkMode) ? 'res/piMoo_bg.png' : 'res/piMoo_bgDark.png';
          }

        }
      }
    }

    return $picLink;
  }




  /**
   * get usage for a given drive or path in percent
   */
  function cntGetDriveUsageInPercent($aDriveORPath)
  {
    if (is_dir($aDriveORPath))
    {
      $diskTotalSpace = disk_total_space ($aDriveORPath); // -- avoids division by zero --
      return ($diskTotalSpace > 0) ? intval((100 - (disk_free_space ($aDriveORPath) * 100 / $diskTotalSpace))) : 100;
    }
    else
    {
      //NEVER cntLog (E0_0022 . 'in cntGetDriveUsageInPercent: "' .  $aDriveORPath . '"', LOG_COMMON, true);
      echo 'Not a drive or directory in cntGetDriveUsageInPercent: "' .  $aDriveORPath . '"';
      return 100;
    }
  }

  /**
   * use like: echo round(cntGetServerMemoryUsage())
   * @return float
   */
  function cntGetServerMemoryUsage()
  {
    $free = shell_exec('free');
    $free = (string)trim($free);
    $free_arr = explode(LFC, $free);
    $mem = explode(BLANK, $free_arr[1]);
    $mem = array_filter($mem);
    $mem = array_merge($mem);
    $memory_usage = $mem[2]/$mem[1]*100;
    return $memory_usage;
  }


  /**
   * use like: echo round(cntGetServerCPU_usage())
   * @return number
   */
  function cntGetServerCPU_usage()
  {
    $load = sys_getloadavg();
    return $load[0];
  }


  function cntGetCPU_temperature()
  {
    /* did not work because only root can run vcgencmd...
    $raspiCurCPUTempStr = (string)exec('vcgencmd measure_temp'); // -- result like: temp=44.8'C --
    $pos = strpos($raspiCurCPUTempStr, 'C'); //$pos = strpos($raspiCurCPUTempStr, "'C");
    $raspiCurCPUTempStr = substr($raspiCurCPUTempStr, $pos - 5, 5); //$raspiCurCPUTempStr = substr($raspiCurCPUTempStr, $pos - 4, 4);
    $raspiCurCPUTempInt = round($raspiCurCPUTempStr);
    return ($raspiCurCPUTempInt * RASPI_CPU_MAX_TEMP) / RASPI_CPU_MAX_TEMP; */

    $temp = intval( exec('cat /sys/class/thermal/thermal_zone0/temp') ); // -- result like: 44800 --
    $raspiCurCPUTemp = round ($temp / 1000 );
    return ($raspiCurCPUTemp * RASPI_CPU_MAX_TEMP) / RASPI_CPU_MAX_TEMP;
  }

  /**
   * Tries to shudown the system and gives feedback as string
   * Requires following lines added to suders with visudo in native environment
   *
   * == Debian (Raspi) ==
   * www-data ALL=(ALL) NOPASSWD: /sbin/reboot
   *
   * == Docker ==
   * or setup host-pipe with inotify
   *
   * Depending on other cicumstances it could be, that $result won't be shown.
  */
  function util_systemShutdown($depth = NULLSTR)
  {
    global $cfg_isDocker;
    global $cfg_dockerHostPipeShutdown;

    $result = 'try to shutdown the system with: ';

    // -- --------------------------------- --
    // -- native environment and sudo rules --
    // -- --------------------------------- --
    if ($cfg_isDocker !== true)
    {
      $cmd = 'sudo /sbin/shutdown -h now';
      $result .= $cmd.LFH;
    }

    // -- ------------------------------- --
    // -- Docker environment command pipe --
    // -- requies inotify pre set up in host --
    // -- apt install inotify-tools ... --
    // -- ... and observing script started after boot --
    // -- ------------------------------- --
    if ($cfg_isDocker === true)
    {
      $timestamp = date("Y-m-d H:i:s");
      file_put_contents($cfg_dockerHostPipeShutdown, $timestamp);
      // -- show analogous what has been done --
      $cmd = "echo '$timestamp' > $cfg_dockerHostPipeShutdown";
      $result .= $cmd.LFH;
    }

    eventLog($result, $depth);
    $result .= (string)system($cmd);

    return $result;
  }


  function cntCreateJSONheader()
  {
    header('Cache-Control: no-cache, must-revalidate');
    //header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
  }

  /**
   * create CORS header
   */
  function cntCreateCORSheader()
  {
    /* VERSUCH
     header('Access-Control-Allow-Origin: *'); // only test, but not with "Access-Control-Allow-Credentials: true"
     //orig: header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
     header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
     oder header('Access-Control-Allow-Methods: GET, POST');
     header("Access-Control-Allow-Headers: Origin, Accept, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
     */
    $http_origin = (isset($_SERVER['HTTP_ORIGIN'])) ? $_SERVER['HTTP_ORIGIN'] : '*';
    header('Access-Control-Allow-Origin: '.$http_origin);
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
    header('Access-Control-Allow-Credentials: true');
  }


  /**
   * returns the playlist refresh interval
   */
  function getPLRI()
  {
    global $cfg_playlistRefreshInterval;
    return (string)intval($cfg_playlistRefreshInterval * 1000);
  }

  /**
   * get seconds by minutes and seconds
   * @param string $aTime
   * @return number
   */
  function getSecondsPerMinSec($aTime)
  {
    $time = str_pad($aTime, 4 ,'0', STR_PAD_LEFT);
    // -- 04:57 --
    // -- 12:34 --
    $sec = intval(substr($time, 2));
    $min = intval(substr($time, 0,2));
    // eventLog("min: $min", TWODIRUP);
    // eventLog("sec: $sec", TWODIRUP);
    return ($sec + $min * 60);
  }



  /**
   * write mp3 tag details into mediabase
   * @param string $aFile
   */
  function writeMP3TagsToMediabase($aFile)
  {
    global $cfg_absPathToSongs;
    global $cfg_musicFileType;
    global $mediaBase;
    global $myFeedback;
    // global $cfg_musicFileType;

    // if (cntGetFileExt($aFile) == $cfg_musicFileType) // -- done in caller function --
    {
      // -- Initialize getID3 engine --
      $getID3 = new getID3;
      $ThisFileInfo = $getID3->analyze($aFile);
      getid3_lib::CopyTagsToComments($ThisFileInfo);

      $curFile = str_replace($cfg_absPathToSongs, NULLSTR, $aFile);

      if (isset($ThisFileInfo['comments_html']['artist'][0]))
        $interpret = htmlspecialchars_decode(utf8encode($ThisFileInfo['comments_html']['artist'][0]));
      else
        $interpret = QUESTMARK;

      // -- each song inside this element --
      $mediaBase->startElement(XML_STRDITEM);

        // -- the absolute song adress --
        $mediaBase->startElement('a');
        //$mediaBase->text( $curFile );
        //$mediaBase->text( utf8encode( $curFile ) );
        //$mediaBase->text( htmlspecialchars( $curFile ) );
        $mediaBase->text(htmlspecialchars_decode($curFile)); // best so far!
        //$curFileDecoded = html_entity_decode($curFile, ENT_QUOTES | ENT_IGNORE, 'UTF-8');
        //$mediaBase->text( ( $curFileDecoded ) ); // htmlspecialchars
        $mediaBase->endElement();

        // -- interpret --
        $mediaBase->startElement('i');
        $mediaBase->text($interpret);
        $mediaBase->endElement();

        // -- title --
        $mediaBase->startElement('t');
        if (isset($ThisFileInfo['comments_html']['title'][0]))
          $mediaBase->text(htmlspecialchars_decode(utf8encode($ThisFileInfo['comments_html']['title'][0])));
        else
          $mediaBase->text( basename($aFile, DOT.$cfg_musicFileType));
        $mediaBase->endElement();

        // -- album --
        $mediaBase->startElement('l');
        if (isset($ThisFileInfo['id3v2']['album']))
          $mediaBase->text(htmlspecialchars_decode (utf8encode($ThisFileInfo['id3v2']['album'])));
        elseif (isset($ThisFileInfo['id3v1']['album']))
          $mediaBase->text( htmlspecialchars_decode (utf8encode($ThisFileInfo['id3v1']['album'])));
        $mediaBase->endElement();

        // -- play time --
        $mediaBase->startElement('p');
          if (isset($ThisFileInfo['playtime_string']))
            $mediaBase->text($ThisFileInfo['playtime_string']);
        $mediaBase->endElement();

        // -- genre --
        $mediaBase->startElement('g');
          if (isset($ThisFileInfo['comments_html']['genre'][0]))
            $mediaBase->text( htmlspecialchars_decode (utf8encode($ThisFileInfo['comments_html']['genre'][0])));
        $mediaBase->endElement();

        $mediaBase->startElement('y');
          @$year = $ThisFileInfo['id3v2']['year'];
          if (empty($year))
          {
            @$year = $ThisFileInfo['id3v1']['year'];
          }
          // -- avoid: PHP Deprecated: XMLWriter::text(): Passing null to parameter #1 ($content) of type string is deprecated --
          if (!empty($year))
          {
            $mediaBase->text($year);
          }
        $mediaBase->endElement();

        $mediaBase->startElement('d');
          if (isset($ThisFileInfo['filenamepath']))
            $mediaBase->text(date('Y-m-d', filemtime($ThisFileInfo['filenamepath'])));
        $mediaBase->endElement();

        // -- --------------- --
        // -- beg of feedback --
        // -- --------------- --
        $myFeedback->findSong(basename($aFile));

        // -- rating --
        $mediaBase->startElement('r');
          $mediaBase->startAttribute(XML_BY);
            $mediaBase->text($myFeedback->getRatingBy());
          $mediaBase->endAttribute();
          $mediaBase->text($myFeedback->getRating());
        $mediaBase->endElement();

        // -- comment --
        $mediaBase->startElement('c');
          $mediaBase->startAttribute(XML_BY);
            $mediaBase->text($myFeedback->getCommentBy());
          $mediaBase->endAttribute();
          $mediaBase->text($myFeedback->getComment());
        $mediaBase->endElement();

        $myFeedback->findSongReset();
        // -- --------------- --
        // -- end of feedback --
        // -- --------------- --

      $mediaBase->endElement();

      unset($getID3);
    }
  }


  /**
   * fresh creation of database - find all files
   * @param string $path
   * @param boolean $html
   */
  function recursive_writeMP3TagsToMediabase($path, $html)
  {
    global $mediaBase; // -- DO NOT DELETE, it's used inside sub function writeMP3TagsToMediabase --
    global $cfg_isDocker;
    global $cfg_musicFileType;
    if ($html)
    {
      global $progBar;
    }
    else
    {
      global $cliProgBar;
    }

    // -- either html or console linefeed --
    $LF = ($html) ? LFH : LFC;

    $handle =  opendir($path);
    while ($file = readdir($handle))
    {
      if ($file != DOT && $file != DDOT && substr($file, 0, 1) != DOT)
      {
        if (is_dir($path.$file))
        {
          // -- Renewed function call to read out the current directory --
          recursive_writeMP3TagsToMediabase($path.$file.SLASH, $html);
        }
        else
        {
          if ($cfg_musicFileType == strtolower(cntGetFileExt($file)))
          {
            if ($html || $cfg_isDocker)
            {
              echo $file.$LF;
            }
            writeMP3TagsToMediabase($path.$file);
          }
          if ($html)
          {
            $progBar->step();
          }
          elseif(!$cfg_isDocker)
          {
            $cliProgBar->setCaption($file);
            $cliProgBar->progress();
            // usleep(50000); // -- test --
          }
        }
      }
    }
    closedir($handle);
  }





  /**
   * update mp3 tag details into mediabase
   * @param string $aFile
   */
  function updateMP3TagsToMediabase($aFile)
  {
    global $cfg_absPathToSongs;
    global $cfg_musicFileType;
    global $mediaBaseUpdate;
    // global $cfg_musicFileType;

    // if (cntGetFileExt($aFile) == $cfg_musicFileType) // -- done in caller function --
    {
      $curFile = str_replace($cfg_absPathToSongs, NULLSTR, $aFile);
      // -- create a new song item for xml --
      $newItem = $mediaBaseUpdate->addChild(XML_STRDITEM);

      // -- Initialize getID3 engine --
      $getID3 = new getID3;
      $ThisFileInfo = $getID3->analyze($aFile);
      getid3_lib::CopyTagsToComments($ThisFileInfo);

      if (isset($ThisFileInfo['comments_html']['artist'][0]))
        $interpret = htmlspecialchars_decode(utf8encode($ThisFileInfo['comments_html']['artist'][0]));
      else
        $interpret = QUESTMARK;

      // -- the absolute song adress --
      $newItem->addChild('a', htmlspecialchars_decode( $curFile ));// best so far!

      // -- interpret --
      $newItem->addChild('i', $interpret);

      // -- title --
      if (isset($ThisFileInfo['comments_html']['title'][0]))
        $newItem->addChild('t', htmlspecialchars_decode ( utf8encode( $ThisFileInfo['comments_html']['title'][0]) ));
      else
        $newItem->addChild('t', basename( $aFile, $cfg_musicFileType));

      // -- album --
      if (isset($ThisFileInfo['id3v2']['album']))
        $newItem->addChild('l', htmlspecialchars_decode ( utf8encode( $ThisFileInfo['id3v2']['album'] ) ));
      elseif (isset($ThisFileInfo['id3v1']['album']))
        $newItem->addChild('l', htmlspecialchars_decode ( utf8encode( $ThisFileInfo['id3v1']['album'] ) ));

      // -- play time --
      if (isset($ThisFileInfo['playtime_string']))
        $newItem->addChild('p', $ThisFileInfo['playtime_string']);

      // -- genre --
      if (isset($ThisFileInfo['comments_html']['genre'][0]))
        $newItem->addChild('g', htmlspecialchars_decode(utf8encode( $ThisFileInfo['comments_html']['genre'][0])));

      @$year = $ThisFileInfo['id3v2']['year'];
      if (empty($year))
        @$year = $ThisFileInfo['id3v1']['year'];
      $newItem->addChild('y', $year);

      if (isset($ThisFileInfo['filenamepath']))
        $newItem->addChild('d', date('Y-m-d', filemtime($ThisFileInfo['filenamepath'])));

      // -- -------- --
      // -- feedback --
      // -- ok, if it is new, it cannot have a feedback entry already! --
      // -- so only the body is created for this --
      $newItem->addChild('r');
      $newItem->addChild('c');

      unset($getID3);
    }
  }


  /**
   * find all files in time range - update of existing database
   * @param string $path
   * @param string $updateDaysUnix
   * @param boolean $html
   */
  function recursive_updateMP3TagsToMediabase($path, $updateDaysUnix, $html)
  {
    global $mediaBaseUpdate; // -- DO NOT DELETE, it's used inside sub function updateMP3TagsToMediabase --
    global $cfg_musicFileType;
    global $mediaBaseUpdateStr;
    // if ($html)
    // {
      global $progBar;
    // }
    // else
    // {
    //   global $cliProgBar;
    // }

    // -- either html or console linefeed --
    $LF = ($html) ? LFH : LFC;

    $handle =  opendir($path);
    while ($file = readdir($handle))
    {
      if ($file != DOT && $file != DDOT && substr($file, 0, 1) != DOT)
      {
        if (is_dir($path . $file))
        {
          // -- Renewed function call to read out the current directory --
          recursive_updateMP3TagsToMediabase($path.$file.SLASH, $updateDaysUnix, $html);
        }
        else
        {
          if ($cfg_musicFileType == strtolower(cntGetFileExt($file)))
          {
            if (filectime($path.$file) >= $updateDaysUnix)
            {
              echo 'found - check if not exists: '.$file.$LF;
              if (strpos($mediaBaseUpdateStr, $file) === false)
              {
                if ($html)
                {
                  echo '<span style="color: red;">not existing - adding now: '.$file.'</span>'.$LF;
                  $progBar->step();
                }
                else
                {
                  echo 'not existing - adding now: '.$file.$LF;
                }
                updateMP3TagsToMediabase($path.$file);
              }
            }
          }
        }
      }
    }
    closedir($handle);
  }


  /**
   * create a default config shadow file
   * $depth = '../' when used inside admin for example
   */
  function createShadowConfig($depth,
                              $allowMultiNickname,
                              $allowMultiblePlaylistEntries,
                              $partyMode,
                              $genreWhitelist,
                              $mqtt,
                              $mqttaddr,
                              $caching)
          {
    global $cfg_configShadow;
    $content = '<?php
    //$cfg_codeWordQuestion=false;
    //$cfg_codeWordAnswer="an answer";
    $cfg_allowMultiNickname='.$allowMultiNickname.';
    $cfg_allowMultiblePlaylistEntries='.$allowMultiblePlaylistEntries.';
    //$cfg_partyReport=true;
    $cfg_mqtt='.$mqtt.';
    $cfg_mqtt_broker_unified='.$mqttaddr.';
    $cfg_cache='.$caching.';
    $cfg_partyMode='.$partyMode.';
    //$cfg_maxPlaylistEntries=10;
    $cfg_genreWhitelist=array('.$genreWhitelist.');
    //$cfg_genreBlacklist=array("UltraStar","Spoken Word","Comedy");
    ';
    file_put_contents($depth.$cfg_configShadow, $content);
  }


  /**
   * a cli progressbar in Linux console style
   *
   * @param [integer] $overall
   * @param [integer] $current
   * @param integer $bar_length
   * @return void
   */
  function cli_progressbar($overall, $current, $bar_length = 77)
  {
    $percent = $current / $overall;
    $filled = (int)($bar_length * $percent);
    $empty = $bar_length - $filled;
    $bar = str_repeat('#', $filled) . str_repeat('_', $empty);
    printf("\r[%s] %d%%", $bar, $percent * 100);
    flush();
    if ($current == $overall)
    {
        echo LFC;
    }
  }
  // -- example usage --
  // for ($i = 0; $i <= 100; $i++)
  // {
  //   cli_progressbar(100, $i);
  //   usleep(50000);  // Sleep for a short time to simulate progress
  // }


  /**
   * wrapper function for deprecated utf8_encode
   *
   * @param [string] $strToEncode
   * @param [string] $encoding
   * @return string
   */
  function utf8encode($strToEncode, $encoding=UTF8)
  {
    // -- depreacted in php 8.2 --
    // return utf8_encode($strToEncode);
    return mb_convert_encoding($strToEncode, $encoding);
  }


  /**
   * escape chritical chars like " and '
   * Mostly useful for data base queries
   * @param string $string
   * @return string
   */
  function utils_escapeDBCriticalChars(string $string): string
  {
    $tmp1 = str_replace('\'', '`', $string);

    $tmp2 = str_replace('´', '`', $tmp1); // -- found at The Who Album: "who´s next" --
    $tmp3 = str_replace('\xB4', '`', $tmp2);

    $tmp4 = str_replace('"', '″', $tmp3);

    return $tmp4;
  }


  /**
   * get cli path depeding on what is configured
   *
   * @param boolean $isDocker
   * @param string $system
   * @param string $magicDir
   * @return string
   */
  function getCLIpathDependOnConfig(bool $isDocker, string $system, string $magicDir): string
  {
    // -- set most presumably path for Debian or Raspi --
    $path = DEBIAN_WEBROOT_FULL; // -- like /var/www/html/piMoo/ --

    if ($isDocker === true)
    {
      if ($system == SYSTEM_DEBIAN || $system == SYSTEM_RASPI)
      {
        $path = DEBIAN_WEBROOT_BASIC; // -- like /var/www/html/ --
      }
      if ($system == SYSTEM_ALPINE)
      {
        $path = ALPINE_WEBROOT_BASIC; // -- like /var/www/localhost/htdocs/ --
      }
    }
    // -- if nothing previously set is okay we have a last hope --
    if (!file_exists($path . PLAYERSCRIPT))
    {
      $path = $magicDir;
    }

    return $path;
  }


  // -- ---------- --
  // -- includings --
  // -- ---------- --

  require 'class_nickname.php'; // -- swapped out in a separate file 2022-12-16 --



  /**
   * search a value in a two dimensional array with key validation
   * @param array $aArray
   * @param $aValue
   * @param string $aKey
   * @return boolean
   * /
  function cntIsIn2DimArrayByKey($aArray, $aValue, $aKey)
  {
    $result = false;

    foreach ($aArray as $arr)
    {
      $key = array_search($aValue, $arr);
      if ($key === $aKey)
      {
        $result = true;
        break;
      }
    }

    return $result;
  } */


  /*
   * Funktionsdefinition:
   * Die Funktion entfernt leere und nicht-leere Array-Elemente.
   * Nicht-leere Elemente werden entfernt, wenn die Elemente nur
   * aus Whitespaces bestehen. Die Funktionsargumente werden
   * 'passed by reference' uebergeben, der Rückgabewert der
   * Funktion ist immer vom Typ Boolean.
   * Funktionsprototyp:
   * boolean cleanUpArray ( array &$ar [, integer &$removal_counter] )
   * @param      array    Das Array welches gesaeubert werden soll.
   * @param      integer  (Optional) Enthaelt nach Funktionsdurchlauf
   *                      die Anzahl der Veraenderungen im Array.
   * @return     boolean  true wenn die Funktion korrekt durchlaufen
   *                      wurde oder false wenn kein Array uebergeben
   *                      worden ist.
   * @author              Daniel Kreßler, daniel.kressler@selfphp.de
   * @copyright           (c) 2008 Daniel Kreßler
   * addition: renew index by Patrick Höf
   * call like this: cntCleanUpArray($textsAsSepStr, $counter);
   *
  function cntCleanUpArray(&$ar, &$removal_counter, $aRenewIndex = true)
  {
    // Wurde ein Array uebergeben?
    if (is_array($ar))
    {
        // Groesse des uebergeben Arrays speichern.
        $ar_original_size = count($ar);
        // Alle nicht leeren und nicht nur aus Whitespaces
        // bestehenden Array-Elemente - als neues Array - aus
        // dem originalen Array ziehen und das originale Array
        // mit dem neuen Array ueberschreiben.
        $ar = preg_grep('/^\s*$/s', $ar, PREG_GREP_INVERT);
        // Differenz zwischen der originalen Arraygroesse und
        // dem neuen Array berechnen.
        $removal_counter = $ar_original_size - count($ar);
        if ($aRenewIndex)
          $ar = array_merge($ar); // -- renew index --
        return true;
    }
    // Wenn kein Array übergeben wurde:
    return false;
  }


  // --------------------------------------------
  // -- Liefert das Betriebssystem des Servers --
  // --------------------------------------------

  function cntGetServerOS()
  {
    if(strpos($_SERVER['DOCUMENT_ROOT'],'/') === 0)
    {
      return LINUX;
    }
    elseif(strpos($_SERVER['DOCUMENT_ROOT'],':') !== false)
    // -- alternatively: substr(PHP_OS, 0, 3) == 'WIN' --
    {
      return WINDOWS;
    }
  }


  // -- auf der Konsole --
  function cntGetServerOSCLI()
  {
    return (DIRECTORY_SEPARATOR == SLASH) ? LINUX : WINDOWS;
  }


  // -- get the mobile device --
  function cntGetMobileDevice($UserAgent)
  {
    // -- find the device --
    if (preg_match("/BLACKBERRY/i", $UserAgent))
      return 'BBTorch';
    if (preg_match("/IPHONE/i", $UserAgent))
      return IPHONE;
    if (preg_match("/IPAD/i", $UserAgent))
      return IPHONE; //later: IPAD;
    if (preg_match("/ANDROID/i", $UserAgent)) // -- no chance to handle all possible Android devices --
      return ANDROID;

    // -- if not found --
    return UNKNOWN;

  }



  // -- special use here, but DOES NOT belong to cache. It belongs to mayer_param_cache! --
  // -- normally it should be in mayer_tools, but anyway --
  function cntGetIsCacheFileAlive($aFile, $aLifetime = CACHELIFETIME)
  {
    global $cfgset;
    if ($cfgset->cacheParams)
    {
      if (file_exists($aFile))
        return (cntGetTimeDiffAsSecondsFromUnix(filemtime($aFile)) < $aLifetime) ? true : false;
      return false;
    }
    else
      // -- it is not "alive", if output is deactivated --
      return false;
  }



  // -- returns a substring from begin to a delimiter --
  // echo substr2delimiter($_SERVER['PHP_SELF'], '/', 1); returns the projectname in document root
  function cntSubstr2delimiter($aString, $aDelimiter, $aStart = 0)
  {
    $str = substr($aString, $aStart, strlen($aString));
    //return (substr($str, 0, (int) strpos($str, $aDelimiter)));
    return (substr($str, 0, intval( strpos($str, $aDelimiter)) ));
  }

  // -- returns the project folder name like "KAMCOSnet" --
  // -- to optimize speed in always called scripts like frame_x, it is often used the define "PROJECTMASTERFOLDER" instead --
  function cntGetProjectFolderName() // $_SERVER['PHP_SELF'] // $aScript is only reqiered if another location is checked
  {
    return cntSubstr2delimiter($_SERVER['PHP_SELF'], '/', 1);
  }




  // -- loads a file in any level of project until level 15 --
  // -- clone_of_used_one_in_defines, there called "cULFAL" --
  function cntUniLoadFileAnyLevel($aAnyFile, $aAsBool = false)
  {
    $file = $aAnyFile;
    $prefix = ''; // -- before windows expirience: $prefix = ''; --
    while (!file_exists($prefix . $file))
    {
      $prefix .= '../';
      if (strlen($prefix) >= 64) // -- before windows expirience: if ($prefix == '../../../../../../../../../../../../../../../'); --
      {
        if ($aAsBool == true)
        {
          return false;
        }
        else
          die(E0_0002 . ': ' . $prefix . $aAnyFile);
      }
    }
    return $prefix . $aAnyFile;
  }



  // -- Liefert die Differenz in Sekunden zwischen einen Datum und dem aktuellen --
  function cntGetTimeDiffAsSeconds($aYear, $aMonth, $aDay, $aHour, $aMinute, $aSecond)
  {
    // Get current date in UNIX format
    $date1 = time();
    // Make a date in UNIX format
    $date2 = mktime($aHour, $aMinute, $aSecond, $aMonth, $aDay, $aYear);
    $dateDiff = $date1 - $date2;
    return $dateDiff;
  }

  // -- Liefert die Differenz in Tagen zwischen einen Datum und dem aktuellen --
  // http://www.phpf1.com/tutorial/php-date-difference.html
  function cntGetTimeDiffAsDays($aYear, $aMonth, $aDay)
  {
    // Get current date in UNIX format
    $date1 = time();
    // Make a date in UNIX format
    $date2 = mktime(0, 0, 0, $aMonth, $aDay, $aYear);
    $dateDiff = $date1 - $date2;
    $fullDays = floor($dateDiff/(60 * 60 * 24));
    return $fullDays;
  }


  // -- checks if a var is numeric --
  function cntCheckIsNumeric($aNum)
  {
    //return (preg_match("=^[0-9\-]+$=i", $aNum)) ? true : false;
    return (is_numeric($aNum)) ? true : false;
  }


*/

















  /*
   * this is an initially function swaped out from lib/config.php
  * !!! DO NOT MODIFY ANYTHING HERE !!!
  */
  function _init()
  {
    global $cfg_maxSearchResultsPerPage;
    global $cfg_nonMobileMultiplier;

    if (!cntIsMobile())
    {
      $cfg_maxSearchResultsPerPage = $cfg_maxSearchResultsPerPage * $cfg_nonMobileMultiplier;
    }

    global $cfg_playlistRefreshInterval;
    $cfg_playlistRefreshInterval = ($cfg_playlistRefreshInterval >= 1) ? $cfg_playlistRefreshInterval : 1;
    global $cfg_minimumSearchChars;
    $cfg_minimumSearchChars  = ($cfg_minimumSearchChars >= 1) ? $cfg_minimumSearchChars : 1;
  }
  _init();


