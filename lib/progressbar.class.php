<?php
/*
*	PHP Fortschrittsbalken v1.3
*	(c) 2010 by Fabian Schlieper
*	fabian@fabi.me
*	http://fabi.me/
*
* modified 2023 by paddy to work with php8.2
*/

class ProgressBar
{
  private static string $jsPrefix = "_pbr_";

  private static function echo(string $string): void
  {
    echo $string;
    @ob_flush();
    @flush();
  }

  private static function executeJs(string $code): void
  {
    $code = trim($code);
    self::echo("<script>$code</script>");
  }

  private int $id = 0;
  private int $value = 0;
  private int $steps = 0;
  private int $width = 0;
  private int $height = 0;
  private string $color = '';
  private string $backgroundColor = '';
  private string $innerStyleClass = '';
  private string $outerStyleClass = '';
  private bool $showDigits = true;

  public function __construct(
    int $value = 0,
    int $steps = 100,
    int $width = 100,
    int $height = 20,
    string $color = '#0C0',
    string $backgroundColor = '#FFF',
    string $innerStyleClass = '',
    string $outerStyleClass = ''
  ) {
    $this->id = ProgressBar::getProgressBarCount();
    $this->value = $value;
    $this->steps = $steps;
    $this->width = $width;
    $this->height = $height;
    $this->color = $color;
    $this->backgroundColor = $backgroundColor;
    $this->innerStyleClass = $innerStyleClass;
    $this->outerStyleClass = $outerStyleClass;
    ProgressBar::incrementProgressBarCount();
  }

  public function setShowDigits(bool $show = true): void
  {
    $this->showDigits = $show;
  }

  public function print_code(): void
  {
    static $initPrinted;

    $jsp = self::$jsPrefix;

    if (empty($initPrinted)) {
      self::executeJs(
        "var {$jsp}bs=[];var {$jsp}ds=[];var {$jsp}ss=[];var {$jsp}ws=[];var {$jsp}dc=[];" .
        "function {$jsp}s(f,d){var e=(d/{$jsp}ss[f]);" .
        "if({$jsp}ds[f]){{$jsp}ds[f].innerHTML=\"\"+Math.round(e*100)+\" %\";}" .
        "{$jsp}bs[f].style.width=\"\"+Math.round(e*{$jsp}ws[f])+\"px\";" .
        "var c=(e>=0.5);if({$jsp}ds[f]&&{$jsp}dc[f]!=c)" .
        "{var a=(c?document.getElementById(\"{$jsp}b\"+f):{$jsp}bs[f]);" .
        "{$jsp}ds[f].style.color=a.style.backgroundColor;{$jsp}dc[f]=c}}" .
        "function {$jsp}i(e,d,a,b){{$jsp}bs[e]=document.getElementById(\"{$jsp}\"+e);" .
        "{$jsp}ds[e]=document.getElementById(\"{$jsp}d\"+e);{$jsp}dc[e]=false;{$jsp}ss[e]=a;" .
        "{$jsp}ws[e]=b;{$jsp}s(e,d)};"
      );
      $initPrinted = true;
    }

    $id = $this->id;
    $w = "width:{$this->width}px;";
    $h = "height:{$this->height}px;";
    $osc = empty($this->outerStyleClass) ? false : $this->outerStyleClass;
    $isc = empty($this->innerStyleClass) ? false : $this->innerStyleClass;
    $cl = "color:{$this->color};";

    self::echo("<div id=\"{$jsp}b{$id}\" style=\"{$w}{$h}text-align:left;background-color:{$this->backgroundColor};overflow:hidden;" . ($osc ? "\" class=\"{$osc}\"" : "border:1px solid #000;\"") . ">");
    if ($this->showDigits) {
      self::echo("\n<div id=\"{$jsp}d{$id}\" style=\"{$w}{$h}text-align:center;line-height:{$this->height}px;position:absolute;z-index:3;{$cl}\"></div>");
    }
    self::echo("<div id=\"{$jsp}{$id}\" style=\"width:0px;{$h}background-{$cl}" . ($isc ? " class=\"{$isc}\"" : "\"") . "\"></div>");
    self::echo("\n</div>");
    self::executeJs("{$jsp}i({$this->id},{$this->value},{$this->steps},{$this->width});");
  }

  public function set(int $v): void
  {
    if ($v < 0) {
      $v = 0;
    } elseif ($v >= $this->steps) {
      $v = $this->steps;
    }

    if ($this->value != $v) {
      $this->value = $v;
      self::executeJs(self::$jsPrefix . 's(' . $this->id . ',' . $v . ');');
    }
  }

  public function step(int $d = 1): void
  {
    $this->set($this->value + $d);
  }

  public function reset(): void
  {
    $this->set(0);
  }

  public function complete(): void
  {
    $this->set($this->steps);
  }

  private static function getProgressBarCount(): int
  {
    static $progressBars = 0;

    return $progressBars;
  }

  private static function incrementProgressBarCount(): void
  {
    static $progressBars = 0;
    $progressBars++;
  }
}

