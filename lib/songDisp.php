<?php
  // -- song dispatcher to deliver songs as preview outside of htdocs --

  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');

  if (!$cfg_allowStreaming)
  {
    die('streaming is not allowed');
  }
  else
  {
    $hashUser = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : NULLSTR;

    // -- only logged in can see preview? --
    if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
    {
      if (!isset($_GET['hash']))
      {
        die('hash missing');
      }
      else
      {
        if (md5($hashUser) != $_GET['hash'])
        {
          die('hash failure');
        }
        else
        {
          if (!isset($_GET['song']))
          {
            die('song missing');
          }
          else
          {
            header("Content-Type: audio/$cfg_musicFileType"); // -- modify the header to mp3 --

            // -- switches if the URL comes base64 encoded and has to be decoed before --
            $song = (isset($_GET['base64'])) ? base64_decode($_GET['song']) : $_GET['song'];

            $songAbs = $cfg_absPathToSongs.$song;

            // -- give a name for the download, otherwise it would be named "songDisp.php.mp3" --
            if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
              $attachment = NULLSTR;
            else
              $attachment = 'attachment;';
            header("Content-Disposition:$attachment filename=\"{$songAbs}\"");

            if ($songAbs !== NULLSTR)
            {
              readfile($songAbs); // -- deliver the song --
            }
          }
        }
      }
    }
  }

