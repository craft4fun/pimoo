<?php
  /**
   * class_outputCache -> handles the output cache by putting content and getting cache file
   * @author Patrick Höf
   * @param __construct($aLifetime = CACHELIFETIME)
   * @access public
   * @example $myCache = new class_outputCache;
   */
  class class_outputCache
  {
    private $allowed = false;
    private $lifetime = CACHELIFETIME_NORM;
    private $cachePath = NULLSTR;
    private $cacheFile = NULLSTR;
    private $fallback = NULLSTR;

    function __construct($aLifetime = CACHELIFETIME_LONG) // NOT CFG_SEARCH_CACHELIFETIME
    {
      global $cfg_cachePath;
      global $cfg_cache;

      $this->allowed = ($cfg_cache) ? true : false;
      $this->lifetime = $aLifetime;

      // -- that does a lot --
      $this->setCachePath($cfg_cachePath);
    }


    // -- ------ --
    // -- common --
    // -- ------ --

    function getCacheAllowed()
    {
      return $this->allowed;
    }

    function getLifeTime()
    {
      return $this->lifetime;
    }
    function setLifeTime($aLifetime)
    {
      $this->lifetime = $aLifetime;
    }

    // -- in case of cache file should e.g. only available for a single user --
    function getCachePath()
    {
      return $this->cachePath;
    }
    function setCachePath($aCachePath)
    {
      $this->cachePath = $aCachePath;
    }

    // -- set cache file without path, that is intern --
    function setCacheFile($aCacheFile) // gerade nicht: , $aCaseIgnore = false
    {
      // if ($aCaseIgnore)
        $this->cacheFile = $this->cachePath . $aCacheFile;
      // else
      // $this->cacheFile = $this->cachePath . strtolower($aCacheFile);
    }

    // -- this is a one way use, if set, it is set! --
    function addLanguage($aLang = LANG_EN)
    {
      if (isset($aLang) && !empty($aLang))
        $this->setCachePath($this->cachePath .= $aLang . SLASH);
      else
        $this->setCachePath($this->cachePath .= LANG_EN . SLASH);
    }
    // -- somethis like "killLanguage" should be only implemented if really needed --



    // -- ----------- --
    // -- set content --
    // -- ----------- --

    // -- use when putting cache content or adding language --
    function _initCachePath($aCachePath)
    {
      if (!defined('RIGHTSOWNERONLY'))
        define('RIGHTSOWNERONLY', 0700);

      if (!is_Dir($aCachePath)) // normally: $cfgset->tmppath_absolute . 'cache/'
        mkdir($aCachePath, RIGHTSOWNERONLY, true);
    }

    // -- put the cache content --
    function putCacheContent($aContent)
    {
      if ($this->allowed)
      {
        $this->_initCachePath($this->cachePath);
        @file_put_contents($this->cacheFile, $aContent);
      }
    }



    // -- ---------------------- --
    // -- get, NOT the "content" --
    // -- ---------------------- --


    // -- this can be used, if no cache is allowed or no cache file is valid !!! with full path !!! --
    function setFallback($aFallback)
    {
      $this->fallback = $aFallback;
    }
    function getFallback()
    {
      return $this->fallback;
    }

    // -- this returns NOT the content --     info: setter is above --
    function getCacheFile($basename = false)
    {
      if (!$basename)
        return $this->cacheFile;
      else
        return basename($this->cacheFile);
    }


    // -- main functions --
    function getIsCacheFileAlive()
    {
      // -- it is not "alive", if output is deactivated -> "not allowed" --
       if (!$this->allowed) // -- to set the cache handling temporarily inactive --
       {
         return false;
       }
       else
       {
        if (!isset($this->cacheFile))
        {
          return false;
        }
        else
        {
          if (!file_exists($this->cacheFile))
          {
            return false;
          }
          else
          {
            return (cntGetTimeDiffAsSecondsFromUnix(filemtime($this->cacheFile)) < $this->lifetime) ? true : false;
          }
        }
       }
    }




    // -- ------------------------------------------------------- --
    // -- helpers (are used by settings and overview for example) --
    // -- ------------------------------------------------------- --

    // -- shoud be good for almost all issues --

    // -- class_outputCache::stat_killCacheFile($aCacheFile) --
    public static function stat_killCacheFile($aCacheFile)
    {
      global $cfg_cachePath;

      $fileToKill = $cfg_cachePath . $aCacheFile;

      if (file_exists($fileToKill))
      {
        unlink($fileToKill);
      }
      else
      {
        $lang = (isset($_COOKIE[STR_LANG])) ? $_COOKIE[STR_LANG] : LANG_EN;

        $fileToKill = $cfg_cachePath . $lang . SLASH . $aCacheFile;
        if (file_exists($fileToKill))
          unlink($fileToKill);
      }
    }


    // -- if special path modifications are needed --

    // -- class_outputCache::stat_killSpecialCacheFile($aCacheFile, $aCachePath = NULLSTR) --
    public static function stat_killSpecialCacheFile($aCacheFile, $aCachePath = NULLSTR)
    {
      global $cfg_cachePath;

      if ($cfg_cachePath != NULLSTR)
        $fileToKill = $cfg_cachePath . SLASH  . $aCacheFile;
      else
        $fileToKill = $cfg_cachePath . $aCacheFile;

      if (file_exists($fileToKill))
        unlink($fileToKill);
    }


  }

