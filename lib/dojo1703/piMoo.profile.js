dependencies = {
  layers: [
      {
      name: "dojo.js",
      customBase: true,
      dependencies: [
	"dojo.main",
	"dojo.selector.acme",
        "dojo.cookie",

        "dojo.dnd.Container",
        "dojo.dnd.Manager",
        "dojo.dnd.Source",
        "dojo.dnd.Mover",
        "dojo.dnd.Moveable",
        "dojo.dnd.move",
        "dojo.dnd.AutoSource",
	"dojo.dnd.Target",

       ]
      },
    ],


}
