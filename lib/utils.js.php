<script>

  <?php
    $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true) ? true : false;
    $songOmnipres = (isset($_COOKIE['bool_SongOmnipresent']) && $_COOKIE['bool_SongOmnipresent'] == YES) ? true : false;
    $isMobile = (cntIsMobile());

    if ($isMobile)
    {
  ?>
    var isMobile = true;
  <?php
    }
    else
    {
  ?>
    var isMobile = false;
  <?php
    }
  ?>

  // -- ------------- --
  // -- begin of core --
  // -- ------------- --

  dojo.require ("dojo/cookie");


  // -- init --

  // -- table sorting --
  var sortBlock = false;
  var omnipresentInterval;



  /*function DojoAjax(aActionFile, aTargetLocation, aHandleAs, aSync)
  {

    // -- set default, possible values are: text (default), json (old: javascript, xml) HTML is NOT allowed! --
    if (!aHandleAs)
      aHandleAs = 'text';
    if (!aSync)
      aSync = false;

    //never here: show_waitDlg();

    // -- the date suffix avoids cache problems --
    // -- a time (or random) praefix or suffix forces the browser to reload the requested file and not fetching it from cache --
    var UBBs = new Date();
    // -- check if a param divider is already set "?" --
    // -- if so, use "&", instead "?" --
    var divSign = '?'; // -- divider sign --
    var divCheck = aActionFile.indexOf('?');
    if (divCheck > -1)
      divSign = '&';

    // -- Look up the node we´ll stick the text under --
    var targetNode = dojo.byId(aTargetLocation);
    // -- The parameters to pass to xhrGet, the url, how to handle it, and the callbacks --
    var xhrArgs =
    {
      url: aActionFile + divSign + UBBs.getTime(), // -- time since unix big bang in seconds --
      handleAs: aHandleAs,
      sync: aSync, // -- that is very imortant to have the result in the following functions by the caller --
      timeout: < ?php echo $cfg_playlistRefreshInterval * 1000;  ?>,

      load: function(data)
      {
        if (aHandleAs == 'text')
        {
          if (targetNode)
            targetNode.innerHTML = data;
        }
        if (aHandleAs == 'json')
        {
          if (targetNode)
          {
            // -- this functions needs a hidden elem like <span id="AJAXJSONRESP"> to hold the received data --
            var jsonStr = JSON.stringify(data);
            targetNode.textContent = jsonStr;
          }
        }
      },
      error: function(error)
      {
        < ?php if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
          echo 'if (targetNode) targetNode.innerHTML = ": " + error;';
        ?>
      }
    };
    // Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
  }*/


  function DojoAjax(aActionFile, aTargetLocation, aHandleAs, aSync, aPreventCache)
  {
    // -- set default, possible values are: text (default), json (old: javascript, xml) HTML is NOT allowed! --
    if (!aHandleAs)
      aHandleAs = 'text';
    if (!aSync)
      aSync = false;
    if (!aPreventCache)
      aPreventCache = true; // -- default is true --

    // Look up the node we´ll stick the text under.
    var targetNode = dojo.byId(aTargetLocation);
    // The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
    var xhrArgs =
    {
      url: aActionFile,
      handleAs: aHandleAs,
      sync: aSync, // -- that is very imortant to have the result in the following functions by the caller --
      preventCache: aPreventCache,
      timeout: <?php echo $cfg_playlistRefreshInterval * 1000; /* 1000 -> give up after 10 seconds */ ?>,

      load: function(data)
      {
        if (aHandleAs == 'text')
        {
          if (targetNode)
            targetNode.innerHTML = data;
        }
        if (aHandleAs == 'json')
        {
           // NOPE: if (targetNode)
          {
            // NOPE: // -- this functions needs a hidden elem like <span id="AJAXJSONRESP"> to hold the received data --
            var jsonStr = JSON.stringify(data);
            // NOPE: targetNode.textContent = jsonStr;
            window.sessionStorage.setItem('AJAXJSONRESP', jsonStr);
          }
        }
      },
      error: function(error)
      {
          <?php if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
            echo 'if (targetNode) targetNode.innerHTML = ": " + error;';
          ?>
      }
    };
    // Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
  }



  // -- deal with browser´s history --
  /*dojo.require("dojo.back");
  function _setBrowserHistory(aRef)
  {
    var state =
    {
      back: function()
      {
        //alert(aRef);
        window.location.href = aRef;
      },
      forward: function()
      {
        //alert("Forward was clicked!");
      }//,
      //changeUrl: true
    };
    dojo.back.setInitialState(state);
    dojo.back.addToHistory(state);
  }*/

  // -- uses DojoAjax to call inner sites with writing URL to browsers history --
  function callHulk(aPage, aRef)
  {
    // if (!aRef)
    //   aRef = document.URL;
    // _setBrowserHistory(aRef);

    // -- set the current page to browser´s  URL bar (HTML5) --
    //  window.history.pushState(new Object(), "Titel", "/neue-url");

    // var callURL = "frontend" + aPage.substr(9);
    // var callURL = aPage; // "frontend.php?hulk=" +

    if (!aRef)
      aRef = aPage;

    // -- show wait "dialog" --
    show_waitDlg();

    try
    {
      //window.history.replaceState(new Object(), "", callURL);
      window.history.replaceState(new Object(), "", aRef);
    }
    catch (e)
    {
      // alert("no HTML5 history object");
    }

    DojoAjax(aPage, "frame_hulk");
    // window.scrollTo(0, 0);

    // -- table sorting --
    sortBlock = false;
  }

  // -- ----------- --
  // -- end of core --
  // -- ----------- --


  // -- tools --

  // Utilities like jQuery :o)
  function $(aID)
  {
    var elem = document.getElementById(aID);
    if (elem)
      return elem;
    else
      return false;
  }

  // -- Delete a DOM node --
  function killElement(aElem)
  {
    if (aElem)
    {
      var papa = aElem.parentNode;
      if (papa)
        papa.removeChild(aElem);
    }
  }


  // -- --------------------------------- --
  // -- begin of helpers and common stuff --
  // -- --------------------------------- --

  function resetAjaxCurPlaylistCall()
  {
    if (typeof ajaxInterval !== typeof undefined)  // -- more safe for foreign callers like iMoo --
    {
      window.clearInterval(ajaxInterval);
    }
  }


  /*function showElemByStr(aElemStr)
  {
    var elem = document.getElementById(aElemStr);
    if (elem)
      elem.style.display = "block";
  }

  function hideElemByStr(aElemStr)
  {
    var elem = document.getElementById(aElemStr);
    if (elem)
      elem.style.display = "none";
  }*/


  function showNavi()
  {
    var frame_navi_content = $("frame_navi_content");
    if (frame_navi_content)
      frame_navi_content.style.display = "block";

    var ctl_showNavi = $("ctl_showNavi");
    if (ctl_showNavi)
      ctl_showNavi.style.display = "none";

    var ctl_hideNavi = $("ctl_hideNavi");
    if (ctl_hideNavi)
      ctl_hideNavi.style.display = "block";

  }
  // -- So it is saved, what the user wished combinated with automatich of search results --
  function showNaviClick()
  {
    localStorage["naviOpen"] = 1;
    showNavi();
  }


  function hideNavi()
  {
    var frame_navi_content = $("frame_navi_content");
    if (frame_navi_content)
      frame_navi_content.style.display = "none";

    var ctl_showNavi = $("ctl_showNavi");
    if (ctl_showNavi)
      ctl_showNavi.style.display = "block";

    var ctl_hideNavi = $("ctl_hideNavi");
    if (ctl_hideNavi)
      ctl_hideNavi.style.display = "none";

  }

  // -- So it is saved, what the user wished combinated with automatich of search results --
  function hideNaviClick()
  {
    localStorage["naviOpen"] = 0;
    hideNavi();
  }


  function songSearchClean()
  {
    var edt_songSearch = $("edt_songSearch");
    if (edt_songSearch)
    {
      localStorage["edt_songSearch"] = '';
      edt_songSearch.value = localStorage["edt_songSearch"];
      edt_songSearch.focus();
    }
  }

  function openAdminPartyReport()
  {
    var adminPartyReportBar = $("adminPartyReportBar");
    if (adminPartyReportBar)
      adminPartyReportBar.style.display = "none";

      var adminPartyReport = $("adminPartyReport");
    if (adminPartyReport)
      adminPartyReport.style.display = "block";
  }


  function scrollTopCollapseNavi()
  {
    window.scrollTo(0, 0);
    //hideNavi();
  }



  // -- table sorting --
  function makeTableSortable()
  {
    var overview_table = document.getElementById("searchResultTable");
    if (!sortBlock)
    {
      new JB_Table(overview_table);
    }
    sortBlock = true;
  }



  // -- pure seconds to music used time format --
  // -- e.g. needed for preview player's progressbar --
  function secondsToMusicTime(aSeconds)
  {
    var date = new Date(1970,0,1);
    date.setSeconds(aSeconds);
    //return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1"); // -- orig found --
    return date.toTimeString().replace(/.*(\d{1}:\d{2}).*/, "$1"); // -- could be the would fuck up if time is longer than 10 minutes (10:00) --
  }



  // -- --------------------------------- --
  // -- begin of omnipresent current song --

  /**
  * element operating on, e.g. omniPresentSong
  * curPos current point in song
  * songLength song length
  */
  function visualizeProgress(element, curPos, songLength)
  {
    // -- make songLength to 255 and curPos to x --
    let x = (255 * curPos) / songLength;

    // -- create a div element and assign it inside omniPresentSong, that's similar to ajax_getCurrentSong.php: echo '<div style="'.$style.'">'.$onAirSong[0]['t'].'</div>'; --
    let subDiv = document.createElement("div");

    // -- move the content into the newly created div --
    //subDiv.innerHTML = element.innerHTML;
    //element.innerHTML = '';
    subDiv.textContent = element.textContent;
    element.textContent = '';

    // -- style basics --
    subDiv.style.color = 'white';
    subDiv.style.textShadow = 'none';
    subDiv.style.paddingTop = '0.25em';
    subDiv.style.height = '120%'; // -- because of the new sub div this works --

    // -- this does fade from red (#ff0000) to lime (#00ff00) --
    subDiv.style.backgroundColor = 'rgb('+Math.floor(255 - x).toString()+', '+Math.floor(x).toString()+', 0)';
    subDiv.style.background = 'radial-gradient(' + 'black,' +
                              'rgb('+Math.floor(255 - x).toString()+', '+Math.floor(x).toString()+', 0)' + ')';
    subDiv.style.background = '-moz-radial-gradient(' + 'black,' +
                        			'rgb('+Math.floor(255 - x).toString()+', '+Math.floor(x).toString()+', 0)' + ')';
    subDiv.style.background = '-webkit-radial-gradient(' + 'black,' +
                              'rgb('+Math.floor(255 - x).toString()+', '+Math.floor(x).toString()+', 0)' + ')';

    // -- assign to father element --
    element.appendChild(subDiv);
  }

  function _omnipresentCallerInterval(aDepth)
  {
    // -- if index.php or frontend.php is active - a clone is enough, no extra Ajax-Call is required --
    var curOnAirSongTitle = $("curOnAirSongTitle");
    if (curOnAirSongTitle)
    {
      var omniPresentSong = $('omniPresentSong');
      if (omniPresentSong)
      {
        // -- song title --
        omniPresentSong.textContent = curOnAirSongTitle.textContent;

        // -- progress visualization --
        let hid_onAirSongCurPos = $('hid_onAirSongCurPos');
        if (hid_onAirSongCurPos)
        {
          let hid_onAirSongLength = $('hid_onAirSongLength');
          if (hid_onAirSongLength)
          {
            let curPos = hid_onAirSongCurPos.textContent;
            let songLength = hid_onAirSongLength.textContent;
            visualizeProgress(omniPresentSong, curPos, songLength);
            //visualizeProgress(omniPresentSong, songLength, curPos);
          }
        }
      }
    }
    // -- all the others need a real ajax-request --
    else
    {
      // -- progress visualization is inside this script --
      if (localStorage.getItem("bool_noPlaylistRefresh") != "<?php echo YES; ?>")
        DojoAjax(aDepth + "ajax_getCurrentSong.php", "omniPresentSong");
    }
  }

  function omnipresentCallerDelay(aDepth)
  {
    // if (!omnipresentInterval) // -- should work but doesn´t (and it´s not required) --
    omnipresentInterval = window.setInterval("_omnipresentCallerInterval(\""+aDepth+"\")", <?php echo (string)($cfg_playlistRefreshInterval * 1000); ?>);
  }

  function omnipresentCallerStart(aDepth)
  {
    if (!aDepth)
      aDepth = '';

    $('omniPresentSong').style.display = 'block';
    <?php
      if ($admin)
      {
        echo 'var adminControlBar = $("adminControlBar"); if (adminControlBar) adminControlBar.style.top = "2.5em";';
        echo 'var contentWrapper = $("contentWrapper"); if (contentWrapper) contentWrapper.style.marginTop = "4em";';
      }
      else
        echo 'var contentWrapper = $("contentWrapper"); if (contentWrapper) contentWrapper.style.marginTop = "2.5em";';
    ?>


    // -- have a result immediatelly and do the main job --
    if (localStorage.getItem("bool_noPlaylistRefresh") != "<?php echo YES; ?>")
      DojoAjax(aDepth + "ajax_getCurrentSong.php", "omniPresentSong");

    // ($cfg_playlistRefreshInterval * 500) is the trick to time shift it with the normal playlist request
    window.setTimeout("omnipresentCallerDelay(\""+aDepth+"\")", <?php echo (string)($cfg_playlistRefreshInterval * 500); ?>);
  }

  function omnipresentCallerStop()
  {
    $('omniPresentSong').style.display = 'none';
    <?php
      if ($admin)
      {
        echo 'var adminControlBar = $("adminControlBar"); if (adminControlBar) adminControlBar.style.top = "0em";';
        echo 'var contentWrapper = $("contentWrapper"); if (contentWrapper) contentWrapper.style.marginTop = "2.5em";';
      }
      else
        echo 'var contentWrapper = $("contentWrapper"); if (contentWrapper) contentWrapper.style.marginTop = "0em";';
    ?>


    if (omnipresentInterval)
      window.clearInterval(omnipresentInterval);
  }

  // -- end of omnipresent current song --
  // -- ------------------------------- --



  // -- ------------------------------- --
  // -- end of helpers and common stuff --
  // -- ------------------------------- --






  // -- ----------------------------- --
  // -- begin of ajax command callers --
  // -- ----------------------------- --

  // -- add song --
  function songAdd(aA, aIn, aTi, aPt, aAl, aConfStr)
  {
    <?php
      if (!$admin)
        echo 'if (confirm("add ´" + aConfStr + "´ to playlist?"))';
    ?>
    {
      DojoAjax("ajax_songAdd.php?a=" + aA + "&i=" + aIn + "&t="+ aTi + "&p="+ aPt + "&l="+ aAl, "target_feedback");
      show_feedback();
    }
  }

  // -- remove a song --
  function songKill(aIndex, aWishedBy)
  {
    if (!aWishedBy)
      aWishedBy = '';
    else
      aWishedBy = '&wishedBy=' + aWishedBy;

    DojoAjax("ajax_songKill.php?index=" + aIndex + aWishedBy, "target_feedback", 'text', true);
    //show_feedback();
    //window.setTimeout("ajaxCurPlaylistCall()", 100); // -- old: 1000, for long playlists 100 could be to fast but it has been tested with raspberry over the internet --
    ajaxCurPlaylistCall();
  }


  // -- push a song from queue to stash --
  function stashPushSong(aIndex, aWishedBy, aA, aIn, aTi, aPt, aAl)
  {
    DojoAjax("ajax_stashPushSong.php?index=" + aIndex + "&wishedBy=" + aWishedBy + "&a=" + aA + "&i=" + aIn + "&t="+ aTi + "&p="+ aPt + "&l="+ aAl, "target_feedback", 'text', false);
    show_feedback();
    window.setTimeout("ajaxCurPlaylistCall()", 100); // -- old: 1000, for long playlists 100 could be to fast but it has been tested with raspberry over the internet --
    //ajaxCurPlaylistCall();
  }

  /*/ -- pop the last song from stash to queue --
  function stashPopLastSong()
  {
     DojoAjax("ajax_stashPopSong.php", "target_feedback", 'text', false);
     show_feedback();
     window.setTimeout("ajaxCurPlaylistCall()", 100); // -- old: 1000, for long playlists 100 could be to fast but it has been tested with raspberry over the internet --
     //ajaxCurPlaylistCall();
  }*/

  // -- pop a certain song from stash to queue --
  function stashPopSong(aIndex)
  {
     DojoAjax("ajax_stashPopSong.php?index=" + aIndex, "target_feedback"); // , 'text', false
     show_feedback();
     window.setTimeout("ajaxCurPlaylistCall()", 100); // -- old: 1000, for long playlists 100 could be to fast but it has been tested with raspberry over the internet --
     //ajaxCurPlaylistCall();
  }

  // -- this is a helper construction to close the feedback message from outside --
  // -- because to decision to close or not is first known only inside the loaded content --
  function _checkEmptyStash()
  {
    var stashIsEmpty = $('stashIsEmpty');
    if (stashIsEmpty)
      quit_feedback();
  }

  // -- show the stash content --
  function stashShow()
  {
    // -- this is in the feedback bubble --
    DojoAjax("ajax_stashShow.php", "target_feedback");
    show_feedback(true);
    window.setTimeout('_checkEmptyStash()', 2000);


    // -- this is a real result page --
    // resetAjaxCurPlaylistCall();
    // callHulk("ajax_stashShow.php");
  }


  // -- get a song´s detailed info --
  function songGetInfo(aSong, aFirstHit, aNoPreview)
  {
    // -- needed if called e.g. by valuedSong.php to take the first hit of song name without intern mediabase path --
    if (!aFirstHit)
      aFirstHit = "";
    else
      if (aFirstHit == true)
        aFirstHit = "&firstHit";

    if (!aNoPreview)
      aNoPreview = "";
    else
      if (aNoPreview == true)
        aNoPreview = "&noPreview";

    DojoAjax("ajax_songGetInfo.php?song=" + aSong + aFirstHit + aNoPreview, "target_feedback");
    show_feedback(true);
  }



  // -- move up a song --
  function songMoveUp(aIndex)
  {
    DojoAjax("ajax_songMoveUp.php?index=" + aIndex, "target_feedback", 'text', true);
    ajaxCurPlaylistCall();
  }

  // -- move down a song --
  function songMoveDown(aIndex)
  {
    DojoAjax("ajax_songMoveDown.php?index=" + aIndex, "target_feedback", 'text', true);
    ajaxCurPlaylistCall();
  }

  // -- move up to top of the playlist --
  function songMoveTop(aIndex)
  {
    DojoAjax("ajax_songMoveTop.php?index=" + aIndex, "target_feedback", 'text', true);
    show_feedback(); // -- message here is ok --
    ajaxCurPlaylistCall();
  }


  // -- this is called inside a result after adding to playlist --
  function songMoveTopAfterAdding()
  {
    DojoAjax("ajax_songMoveTopAfterAdding.php", "target_feedback"); // target_feedback
    show_feedback();
  }


  // -- set comment --
  function songSetComment(aSong)
  {
    var curComment = $('curComment');
    if (curComment)
    {
      if (curComment.innerHTML != "<?php echo LNG_NOCOMMENT_DO; ?>")
        oldComment = curComment.innerHTML;
      else
        oldComment = '';
    }

    var comment = prompt("<?php echo LNG_YOUR_COMMENT; ?>", oldComment);
    if (comment)
    {
      DojoAjax("ajax_feedbackSetComment.php?song=" + aSong + "&comment=" + base64_encode( comment ), "target_feedback");
      show_feedback();
      //window.setTimeout("ajaxCurPlaylistCall()", 1000);

      if (curComment)
        curComment.innerHTML = comment;
    }
  }

  // -- set rating --
  function songSetRating(aSong, aRating)
  {
    DojoAjax("ajax_feedbackSetRating.php?song=" + aSong + "&rating=" + aRating, "target_feedback");
    show_feedback();
  }


  function restartPlaylistInterval()
  {
    // -- reset and reload the interval playlist refresh --
    resetAjaxCurPlaylistCall();
    ajaxInterval = window.setInterval("ajaxCurPlaylistCall();", <?php echo getPLRI(); ?>);

  }


  // -- answer the code word of the day --
  // -- "login" step 0 --
  function answerCodeword()
  {
    //var nickname = window.prompt("your wished nickname", aCurrent); // -- window. does not work on mobile devices --
    var codeword = prompt("<?php echo LNG_CODEWORD_OF_THE_DAY; ?>" + "\n(<?php echo $cfg_codeWordQuestion; ?>)");
    if (codeword)
    {
      quit_StdMsg();

      DojoAjax("ajax_answerCodeword.php?codeword=" + codeword, "target_feedback");
      show_feedback();
      //window.location.reload();
      window.setTimeout("window.location.reload()", 2000);
    }
  }


  // -- the very main ajax call for claiming or chaning a nickname --
  function setNickname(nickname, current)
  {
    DojoAjax("ajax_setNickname.php?nickname=" + nickname + "&current=" + current, "target_feedback");
  }

  // -- actually a user´s free name --
  // -- "login" step 1 --
  function promptNickname(aCurrent, delay = 2000)
  {
    if (!aCurrent)
      aCurrent = "";

    var nickname = prompt("<?php echo LNG_WISHED_NICKNAME; ?>", aCurrent); // -- window. does not work on mobile devices --
    if (nickname)
    {
      quit_StdMsg();
      setNickname(nickname, aCurrent);
      show_feedback();
      window.setTimeout("window.location.reload()", delay);
    }
  }

  // -- "logout" --
  function logout(aDepth)
  {
    if (!aDepth)
      aDepth = "";

    DojoAjax(aDepth + "ajax_logout.php", "target_feedback");
    show_feedback();

    <?php
      // -- find the right way back when running as demo in project page --
      if (isset($_SESSION['isDemo']) && $_SESSION['isDemo'] == true)
      {
    ?>
      window.setTimeout("window.location.href='"+aDepth+"index.php'", 2000);
    <?php
      }
      // -- the common way back, even from iframes like special admin page by using "parent...." --
      else
      {
    ?>
      window.setTimeout("parent.location.href='"+aDepth+"index.php'", 2000);
    <?php
      }
    ?>
  }


  // -- stop piMoo cli_player and logout --
  function stopNlogout(aDepth)
  {
    if (!aDepth)
      aDepth = "";

    show_feedback(true, '<?php echo LNG_TRYING_TO_STOP_PLAYER; ?>');

    // -- stop cli_player synchronously --
    if (aDepth == "")
      ////DojoAjax("admin/ajax_controlPlayerStop.php", "target_feedback", "text", true);
      DojoAjax("admin/ajax_controlPlayerStop.php");
    else
      ////DojoAjax("ajax_controlPlayerStop.php", "target_feedback", "text", true); // -- already in admins area --
      DojoAjax("ajax_controlPlayerStop.php"); // -- already in admins area --

    // -- logout --
    //DojoAjax(aDepth + "ajax_logout.php", "target_feedback", "text", true);
    window.setTimeout(function(){DojoAjax(aDepth + "ajax_logout.php", "target_feedback", "text", true);}, 1000);
    //show_feedback();

    <?php
      // -- find the right way back when running as demo in project page --
      if (isset($_SESSION['isDemo']) && $_SESSION['isDemo'] == true)
      {
    ?>
      window.setTimeout("window.location.href='"+aDepth+"index.php'", 2000);
    <?php
      }
      // -- the common way back, even from iframes like special admin page by using "parent...." --
      else
      {
    ?>
      window.setTimeout("parent.location.href='"+aDepth+"index.php'", 2000);
    <?php
      }
    ?>
  }


  // -- ---------------------- --
  // -- begin of backend stuff --

  function clearQueue()
  {
    if (confirm('<?php echo LNG_SURE; ?>'))
    {
      DojoAjax("ajax_killPlaylist.php", "target_feedback");
      show_feedback();
    }
  }

  function removeDoubles()
  {
    if (confirm('<?php echo LNG_SURE; ?>'))
    {
      DojoAjax("ajax_removeDoubles.php", "target_feedback");
      show_feedback();
    }
  }

  function removeCurNotAvail()
  {
    //if (confirm('<?php echo LNG_SURE; ?>'))
    {
      DojoAjax("ajax_removeCurNotAvail.php", "target_feedback");
      show_feedback();
    }
  }


  // -- drop nickname manually by admin --
  function dropNickname(aNickname)
  {
    if (aNickname)
    {
      var nickID = $('nickID' + aNickname);
      if (nickID)
        nickID.innerHTML = '';

      DojoAjax("ajax_dropNickname.php?nickname=" + aNickname, "target_feedback");
      show_feedback();
    }
  }




  // -- start cli player --
  function controlPlayerStart()
  {
    target_btnPlayerStart.innerHTML = '<img alt="please wait" src="../res/pleaseWait2.gif" class="pleaseWaitAdmin" />';

    DojoAjax("ajax_controlPlayerStart.php", "target_feedback");
    show_feedback();

    // -- read back the real state, did it work or not?! --
    window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_PLAYERSTATE; ?>", "target_playerState")', 1500);
    window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_BTNSTART; ?>", "target_btnPlayerStart")', 1500);
    window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_BTNSTOP; ?>", "target_btnPlayerStop")', 1500);
    window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_ADMINCONTROLBAR; ?>", "targetPlayerControlTab")', 1600);

    const collection = document.getElementsByClassName('toBlink');
    for (let i = 0; i < collection.length; i++)
    {
      collection[i].classList.remove('blinking');
    }
  }

  // -- stop cli player --
  function controlPlayerStop()
  {
    if (confirm("<?php echo LNG_SURE; ?>"))
    {
      target_btnPlayerStop.innerHTML = '<img alt="please wait" src="../res/pleaseWait2.gif" class="pleaseWaitAdmin" />';

      DojoAjax("ajax_controlPlayerStop.php", "target_feedback");
      show_feedback();

      // -- read back the real state, did it work or not?! --
      window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_PLAYERSTATE; ?>", "target_playerState")', 1500);
      window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_BTNSTART; ?>", "target_btnPlayerStart")', 1500);
      window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_BTNSTOP; ?>", "target_btnPlayerStop")', 1500);
      window.setTimeout('DojoAjax("ajax_getPlayerState.php?issue=<?php echo ISSUE_ADMINCONTROLBAR; ?>", "targetAdminControlBar")', 1600);
    }
  }


  // -- end of backend stuff --
  // -- -------------------- --


  function controlPlayerRewind(aSeconds, aDepth)
  {
    function _enableCtrlRewind(aElem)
    {
      aElem.style.opacity = '1';
      aElem.disabled = false;
    }

    if (!aDepth)
      aDepth = '';

    var ctrlElemRewind = $('ctrlElemRewind');
    {
      ctrlElemRewind.style.opacity = '0.2';
      ctrlElemRewind.disabled = true;
      window.setTimeout(function() { _enableCtrlRewind(ctrlElemRewind) }, 1000);
    }

    //DojoAjax(aDepth + "ajax_controlPlayerRewind.php?seconds=" + aSeconds, "target_feedback");
    ////show_feedback(false, aSeconds);
    //show_feedback();

    show_feedback();
    DojoAjax(aDepth + "ajax_controlPlayerRewind.php?seconds=" + aSeconds, "target_feedback", '', true);
    // -- because the function is set up in php environment (for any reason) I must check if exists --
    if (typeof ajaxCurPlaylistCall === "function") {
      ajaxCurPlaylistCall();
    }
  }

  function controlPlayerForward(aSeconds, aDepth)
  {
    function _enableCtrlForward(aElem)
    {
      aElem.style.opacity = '1';
      aElem.disabled = false;
    }

    if (!aDepth)
      aDepth = '';

    var ctrlElemForward = $('ctrlElemForward');
    {
      ctrlElemForward.style.opacity = '0.2';
      ctrlElemForward.disabled = true;
      window.setTimeout(function() { _enableCtrlForward(ctrlElemForward) }, 1000);
    }

    //DojoAjax(aDepth + "ajax_controlPlayerForward.php?seconds=" + aSeconds, "target_feedback");
    ////show_feedback(false, '+' + aSeconds);
    //show_feedback();

    show_feedback();
    DojoAjax(aDepth + "ajax_controlPlayerForward.php?seconds=" + aSeconds, "target_feedback", '', true);
    // -- because the function is set up in php environment (for any reason) I must check if exists --
    if (typeof ajaxCurPlaylistCall === "function") {
      ajaxCurPlaylistCall();
    }
  }



  //-- currently playing -> pause now --
  function controlPlayerPause(aDepth)
  {
    if (!aDepth)
      aDepth = '';

    // -- do not simply remove, it is realy used ;o) --
    function _isPlayingFlag()
    {
      isPlaying = false;
    }


    ctrlElemPlay = $('ctrlElemPlay');
    if (ctrlElemPlay)
    {
      ctrlElemPlay.style.opacity = '1';
      ctrlElemPlay.disabled = false;
    }

    ctrlElemPause = $('ctrlElemPause');
    if (ctrlElemPause)
    {
      ctrlElemPause.style.opacity = '0.2';
      ctrlElemPause.disabled = true;
    }


    // -- do not remove simply, it is realy used ;o) --
    window.setTimeout(_isPlayingFlag, 1000);

    DojoAjax(aDepth + "ajax_controlPlayerPause.php", "target_feedback");
    //show_feedback();
  }
  //-- is paused -> play again --
  function controlPlayerPlay(aDepth)
  {
    if (!aDepth)
      aDepth = '';

    ctrlElemPlay = $('ctrlElemPlay');
    if (ctrlElemPlay)
    {
      ctrlElemPlay.style.opacity = '0.2';
      ctrlElemPlay.disabled = true;
    }

    ctrlElemPause = $('ctrlElemPause');
    if (ctrlElemPause)
    {
      ctrlElemPause.style.opacity = '1';
      ctrlElemPause.disabled = false;
    }

    // -- do not simply remove, it is realy used ;o) --
    isPlaying = true;

    DojoAjax(aDepth + "ajax_controlPlayerPlay.php", "target_feedback");
    //show_feedback();
  }


  // -- next song --
  function controlPlayerNext(aDepth) // aIndex,
  {
    if (!aDepth)
      aDepth = '';

    ctrlElemPlay = $('ctrlElemPlay');
    if (ctrlElemPlay)
    {
      ctrlElemPlay.style.opacity = '0.2';
      ctrlElemPlay.disabled = true;
    }

    ctrlElemPause = $('ctrlElemPause');
    if (ctrlElemPause)
    {
      ctrlElemPause.style.opacity = '1';
      ctrlElemPause.disabled = false;
    }

    // -- do not simply remove, it is really used ;o) --
    isPlaying = true;

    show_feedback();
    DojoAjax(aDepth + "ajax_controlPlayerNext.php", "target_feedback", '', true); // ?index=" + aIndex
    // -- because the function is set up in php environment (for any reason) I must check if exists --
    if (typeof ajaxCurPlaylistCall === "function") {
      ajaxCurPlaylistCall();
    }
  }

  // -- volume down --
  function controlVolumeDown(aDepth)
  {
    function _enableCtrlRewind(aElem)
    {
      aElem.style.opacity = '1';
      aElem.disabled = false;
    }

    var ctrlElemVolumeDown = $('ctrlElemVolumeDown');
    {
      ctrlElemVolumeDown.style.opacity = '0';
      ctrlElemVolumeDown.disabled = true;
      window.setTimeout(function() { _enableCtrlRewind(ctrlElemVolumeDown) }, 100);
    }

    if (!aDepth)
      aDepth = '';

    DojoAjax(aDepth + "ajax_controlVolumeDown.php", "target_feedback");
    show_feedback();
  }

  // -- volume up --
  function controlVolumeUp(aDepth)
  {
    function _enableCtrlRewind(aElem)
    {
      aElem.style.opacity = '1';
      aElem.disabled = false;
    }

    var ctrlElemVolumeUp = $('ctrlElemVolumeUp');
    {
      ctrlElemVolumeUp.style.opacity = '0';
      ctrlElemVolumeUp.disabled = true;
      window.setTimeout(function() { _enableCtrlRewind(ctrlElemVolumeUp) }, 100);
    }

    if (!aDepth)
      aDepth = '';

    DojoAjax(aDepth + "ajax_controlVolumeUp.php", "target_feedback");
    show_feedback();
  }



  function setTolerantSearch(aValue)
  {
    DojoAjax("ajax_setTolerantSearch.php?value=" + aValue, "target_feedback");
    show_feedback();
  }

  function setCaseSensitivity(elem)
  {
    DojoAjax("ajax_setCaseSensitivity.php?value=" + elem.value, "target_feedback");
    show_feedback();

    if (elem.value == "<?php echo YES; ?>")
      elem.classList.add("selected");
    if (elem.value != "<?php echo YES; ?>")
      elem.classList.remove("selected");
  }

  function setCutBlank(elem)
  {
    DojoAjax("ajax_setCutBlank.php?value=" + elem.value, "target_feedback");
    show_feedback();

    if (elem.value == "<?php echo YES; ?>")
      elem.classList.add("selected");
    if (elem.value != "<?php echo YES; ?>")
      elem.classList.remove("selected");
  }

  function setLanguage(aValue)
  {
    DojoAjax("ajax_setLanguage.php?value=" + aValue, "target_feedback");
    show_feedback();
    window.setTimeout("window.location.reload()", 2000);
  }

  function setRandomSearchResults(aValue)
  {
    DojoAjax("ajax_setRandomSearchResults.php?value=" + aValue, "target_feedback");
    show_feedback();
  }

  function setNewcomerDays(aValue)
  {
    DojoAjax("ajax_setNewcomerDays.php?value=" + aValue, "target_feedback");
    show_feedback();
  }

  function setSearchResultsAsTable(elem)
  {
    DojoAjax("ajax_setSearchResultsAsTable.php?value=" + elem.value, "target_feedback");
    show_feedback();

    if (elem.value == "<?php echo YES; ?>")
      elem.classList.add("selected");
    if (elem.value != "<?php echo YES; ?>")
      elem.classList.remove("selected");

  }


  function setSongOmnipresent(elem)
  {
    DojoAjax("ajax_setSongOmnipresent.php?value=" + elem.value, "target_feedback");

    // -- turn on --
    if (elem.value == "<?php echo YES; ?>")
    {
      //omnipresentCallerInit();
      omnipresentCallerStart();
      elem.classList.add("selected");
    }

    // -- turn off --
    if (elem.value != "<?php echo YES; ?>")
    {
      omnipresentCallerStop();
      $("omniPresentSong").innerHTML = "";
      elem.classList.remove("selected");
    }

    show_feedback();
  }

  function setShowAlbumCoverInPlaylist(elem)
  {
    DojoAjax("ajax_setShowAlbumCoverInPlaylist.php?value=" + elem.value, "target_feedback");
    show_feedback();

    if (elem.value == "<?php echo YES; ?>")
      elem.classList.add("selected");
    if (elem.value != "<?php echo YES; ?>")
      elem.classList.remove("selected");
  }

  function setDarkMode(elem)
  {
    DojoAjax("ajax_setDarkMode.php?value=" + elem.value, "target_feedback");
    show_feedback();

    if (elem.value == '<?php echo YES; ?>')
      elem.classList.add("selected");
    if (elem.value == '<?php echo NO; ?>')
      elem.classList.remove("selected");

    window.setTimeout("window.location.reload()", 1000);
  }

  function setDisplayZoom(elem)
  {
    DojoAjax("ajax_setDisplayZoom.php?value=" + elem.value, "target_feedback");
    show_feedback();

    if (elem.value == '<?php echo MEDIUM; ?>')
      elem.classList.add("selected");
    if (elem.value == '<?php echo LOW; ?>' || elem.value == '<?php echo HIGH; ?>')
      elem.classList.remove("selected");

    window.setTimeout("window.location.reload()", 1000);
  }


  // -- ---------------------- --
  // -- settings in admin area --
  // -- ---------------------- --

  function resetShadowConfig()
  {
    if (confirm("<?php echo LNG_SURE; ?>\n\nDepending on which settings has been changed, you may have to restart (red and green button)!"))
    {
      DojoAjax("ajax_resetShadowConfig.php", "target_feedback");
      show_feedback();
      window.setTimeout("window.location.reload()", 2000);
    }
  }

  function setMQTT(elem)
  {
    DojoAjax("ajax_setMQTT.php?value=" + elem.value, "target_feedback");
    show_feedback();
    window.setTimeout("window.location.reload()", 3000); // -- give system more time to take over settings --
  }
  function setMQTTaddr(elem)
  {
    DojoAjax("ajax_setMQTTaddr.php?value=" + elem.value, "target_feedback");
    show_feedback();
    window.setTimeout("window.location.reload()", 3000);
  }

  function setCaching(elem)
  {
    DojoAjax("ajax_setCaching.php?value=" + elem.value, "target_feedback");
    show_feedback();
  }
  function setMultiNick(elem)
  {
    DojoAjax("ajax_setMultiNick.php?value=" + elem.value, "target_feedback");
    show_feedback();
  }
  function setMultiEntries(elem)
  {
    DojoAjax("ajax_setMultiEntries.php?value=" + elem.value, "target_feedback");
    show_feedback();
  }
  function setPartyMode(elem)
  {
    DojoAjax("ajax_setPartyMode.php?value=" + elem.value, "target_feedback");
    show_feedback();

    const collection = document.getElementsByClassName('toBlink');
    for (let i = 0; i < collection.length; i++)
    {
      collection[i].classList.add('blinking');
    }
  }
  function setNoPlaylistRefresh(aValue)
  {
    localStorage.setItem('bool_noPlaylistRefresh', aValue);
    show_feedback();
    let msg = '<?php echo LNG_NO_PLAYLLIST_REFRESH . ARROW; ?>' + '<span style="font-weight: bold;">' + aValue + '</span>';
    $("target_feedback").innerHTML = msg;
  }
  function setGenreWhitelist(elem)
  {
    DojoAjax("ajax_setGenreWhitelist.php?value=" + elem.value, "target_feedback");
    show_feedback();

    const collection = document.getElementsByClassName('toBlink');
    for (let i = 0; i < collection.length; i++)
    {
      collection[i].classList.add('blinking');
    }
  }


  function removeUselessCommasFromStr(str)
  {
    // -- remove first and last comma --
    str = str.replace(/(^,)|(,$)/g,'');
    // -- remove double comma --
    str = str.replace(/,,/g,',');
    //console.log(str);
    return str;
  }

  function addToOverlayWhitelist(genre)
  {
    // -- do nothing --
    if (genre == '-')
      return;

    let sc_genreWhitelist = document.getElementById('sc_genreWhitelist');
    if (sc_genreWhitelist)
    {
      // -- clear and send --
      if (genre == 0)
      {
        sc_genreWhitelist.value = '';
        setGenreWhitelist(sc_genreWhitelist);
      }
      // -- apply and send --
      else if (genre == 1)
      {
        // -- remove first, last and double commas --
        sc_genreWhitelist.value = removeUselessCommasFromStr(sc_genreWhitelist.value);
        setGenreWhitelist(sc_genreWhitelist);
      }
      else
      {
        // -- prepare --
        sc_genreWhitelist.value = sc_genreWhitelist.value + ',' + genre + ',';
      }
    }
  }



  // -- reboot the whole system --
  function systemReboot()
  {
    if (confirm("<?php echo LNG_SURE; ?>"))
    {
      DojoAjax("ajax_systemReboot.php", "target_feedback");
      show_feedback();
    }
  }

  // -- shutdown the whole system --
  function systemShutdown()
  {
    if (confirm("<?php echo LNG_SURE; ?>"))
    {
      DojoAjax("ajax_systemShutdown.php", "target_feedback");
      show_feedback();
    }
  }


  // -- --------------------------- --
  // -- end of ajax command callers --
  // -- --------------------------- --




  // -- ----------------- --
  // -- begin of feedback --
  // -- ----------------- --

  var timerFeedback;

  function show_feedback(aEndless, aPresetMsg)
  {
    if (!aEndless)
      aEndless = false;

    if (!aPresetMsg)
      aPresetMsg = '';

    elem = $("target_feedback");
    if (elem)
    {
      elem.innerHTML = aPresetMsg;

      elem.style.display = "block";

      if (aEndless == false)
      {
        window.setTimeout("elem.style.opacity = '0.75';", 200); //elem.style.opacity = "0.75";

        if (typeof timerFeedback !== typeof undefined) // -- like isset() in PHP --
          window.clearTimeout(timerFeedback);

        timerFeedback = window.setTimeout("quit_feedback()", 5000);  // 3000
      }
      else
      {
        window.clearTimeout(timerFeedback);
        elem.style.opacity = "1";
      }
    }
  }

  function _drain_feedback()
  {
    elem = $("target_feedback");
    if (elem)
    {
      elem.style.display = "none";
      elem.innerHTML = "";
    }
  }

  function quit_feedback()
  {
    elem = $("target_feedback");
    if (elem)
    {
       //if (typeof timerFeedback !== typeof undefined) // -- like isset() in PHP --
      {
        elem.style.opacity = "0";
        //elem.innerHTML = "";
        window.setTimeout("_drain_feedback()", 600);
      }
    }
  }

  // -- --------------- --
  // -- end of feedback --
  // -- --------------- --



  // -- -------------------------- --
  // -- begin of standard feedback --
  // -- -------------------------- --
  function _drain_StdMsg()
  {
    elem = $("StdMsg");
    if (elem)
    {
      elem.style.display = "none";
      elem.innerHTML = "";
    }
  }
  function quit_StdMsg()
  {
    elem = $("StdMsg");
    if (elem)
    {
      elem.style.opacity = "0";
      //elem.innerHTML = "";
      window.setTimeout("_drain_StdMsg", 500); // 600
    }
  }
  // -- ------------------------ --
  // -- end of standard feedback --
  // -- ------------------------ --




  function show_waitDlg()
  {
    elem = $("frame_hulk");
    if (elem)
    {
      <?php
        $size = (cntIsMobile()) ? 'width="150" height="150"' : NULLSTR;
      ?>
      elem.innerHTML = '<div id="dlg_pleaseWait"><img <?php echo $size; ?> src="res/pleaseWait.gif" alt="<?php echo LNG_PLEASE_WAIT;?>" /></div>';
    }
  }



  // -- -------------------------------- --
  // -- begin of all navi search callers --

  // -- this function can be used by any caller --
  function callSearchByStr (aStr, aOffset)
  {
    if (!aOffset)
      aOffset = 0;

    var searchStr = aStr;
    if (searchStr && searchStr.length >= <?php echo (string)$cfg_minimumSearchChars; ?>) // -- usually 3 --
    {
      resetAjaxCurPlaylistCall();

      /* -- that is confusing for normal users. A wish by nane to remove this behaviour. --
      if (isMobile == true)
        hideNavi(); */

      ////DojoAjax(\'ajax_itemsBySearch.php?search=\' + searchStr + \'&offset=\' + aOffset, \'frame_hulk\');
      // -- escape apostrophe (by simply use a double quote, which is normaly not used often in songs :o) hope so) --
      //DojoAjax("ajax_itemsBySearch.php?search=" + searchStr + "&offset=" + aOffset, "frame_hulk");
      callHulk("ajax_itemsBySearch.php?search=" + searchStr + "&offset=" + aOffset, "frontend.php?hulk=ajax_itemsBySearch.php&search=" + searchStr + "&offset=" + aOffset);
    }

    /* -- NOT REALLY USEFUL --
    // -- search with exactly 1 char means searching with an interpret, like the old group buttons --
    if (searchStr && searchStr.length == 1)
      callGroup(searchStr.toUpperCase());
    */
  }

  // -- this function is to only be used by the free field search button --
  function callSearchByEdtSongSearch (aElem)
  {
    var edt_songSearch = $("edt_songSearch").value;
     // -- remember the last search to eventually modify an old search --
          localStorage["edt_songSearch"] = edt_songSearch;
    // -- core --
    callSearchByStr(edt_songSearch);
  }


  // -- searching for a user's comment only, not all other parts like album or title --
  function callSearchByUser (aUser, aOffset)
  {
    if (!aOffset)
      aOffset = 0;

    var userStr = aUser;
    if (userStr && userStr.length >= <?php echo (string)$cfg_minimumSearchChars; ?>) // -- usually 3 --
    {
      resetAjaxCurPlaylistCall();

      /* -- that is confusing for normal users. A wish by nane to remove this behaviour. --
      if (isMobile == true)
        hideNavi(); */

      callHulk("ajax_itemsByUser.php?user=" + userStr + "&offset=" + aOffset, "frontend.php?hulk=ajax_itemsByUser.php&user=" + userStr + "&offset=" + aOffset);
    }
  }

  // -- this function is to only be used by the free field search USER button --
  function callSearchUserByEdtSongSearch (aElem)
  {
    var edt_songSearch = $("edt_songSearch").value;
     // -- remember the last search to eventually modify an old search --
    localStorage["edt_songSearch"] = edt_songSearch;
    // -- core --
    callSearchByUser(edt_songSearch);
  }


  function callNewcomer (aDays, aOffset)
  {
    if (!aOffset)
      aOffset = 0;

    resetAjaxCurPlaylistCall();

    /* -- that is confusing for normal users. A wish by nane to remove this behaviour. --
    if (isMobile == true)
      hideNavi(); */

    callHulk('ajax_itemsByDate.php?days=' + aDays + "&offset=" + aOffset, 'frontend.php?hulk=ajax_itemsByDate.php&days=' + aDays + "&offset=" + aOffset);
  }


  function callRand (aValue)
  {
    resetAjaxCurPlaylistCall();
    // NOT here: hideNavi(); // it is faster to click and click if it stays open
    //DojoAjax(\'ajax_itemsByRandom.php?val=\' + aValue, \'frame_hulk\');
    callHulk('ajax_itemsByRandom.php?val=' + aValue, 'frontend.php?hulk=ajax_itemsByRandom.php&val=' + aValue);
  }


  // -- end of all navi search callers --
  // -- ------------------------------ --


  function addHistoryPlaylistToCurrentPlaylist(aFilename)
  {
    if (confirm("<?php echo LNG_SURE; ?>"))
    {
      DojoAjax("ajax_addHistoryPlaylistToCurrentPlaylist.php?playlist=" + aFilename, "target_feedback");
      show_feedback();
    }
  }


  function addResultToPlaylist(aFilename)
  {
    if (confirm("<?php echo LNG_SURE; ?>"))
    {
      DojoAjax("ajax_addResultsToPlaylist.php?filename=" + aFilename, "target_feedback");
      show_feedback();
    }
  }


  // -- beg of javascript remaintime and eta calculation --
  function _lz2(aNumber)
  {
      var num = '' + aNumber;
      while (num.length < 2) num = '0' + num;
      return num;
  }

  function calcRTtime(aTime)
  {
    var checkLen = aTime.length;
    if (checkLen > 4)
    {
      // -- from 00:00 --
      var mm = parseInt(aTime.substring(0,2));
      var ss = parseInt(aTime.substring(3,5));
    }
    else
    {
      // -- from 0:00 --
      var mm = parseInt(aTime.substring(0,1));
      var ss = parseInt(aTime.substring(2,5));
    }
    var seconds = ss + 60 * mm;


    //console.log('before: ' + seconds);
    if (seconds > 0)
      seconds =  seconds - 1;
    //console.log('after: ' + seconds);

    // -- back to 00:00:00 --
    var second = Math.floor( seconds%60 );
    var minute = Math.floor( (seconds%3600) / 60 );

    var newTime = minute + ':' + _lz2(second);
    //console.log(newTime);

    return newTime;
  }

  function calcETAtime(aTime)
  {
    // -- from 00:00:00 --
    var hh = parseInt(aTime.substring(0,2));
    var mm = parseInt(aTime.substring(3,5));
    var ss = parseInt(aTime.substring(6,8));
    var seconds = ss + 60 * mm + 3600 * hh;

    //console.log('before: ' + seconds);
    if (seconds > 0)
      seconds =  seconds - 1;
    //console.log('after: ' + seconds);

    // -- back to 00:00:00 --
    var second = Math.floor( seconds%60 );
    var minute = Math.floor( (seconds%3600) / 60 );
    var hour = Math.floor( seconds / 3600 );

    var newTime = _lz2(hour) + ':' + _lz2(minute) + ':' + _lz2(second);
    //console.log(newTime);

    return newTime;
  }
  // -- end of javascript remaintime and eta calculation --


  function openOldHistory()
  {
    var iMuhHistory = $('iMuhHistory');
    if (iMuhHistory)
      iMuhHistory.style.display="block";
  }


  function generateRandomString(length = 13) // -- 13 is default in php counterpart uniqid --
  {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';

    for (let i = 0; i < length; i++)
    {
      const randomIndex = Math.floor(Math.random() * characters.length);
      result += characters[randomIndex];
    }

    return result;
  }



  // -- ---------------------------------------------- --
  // -- beg of mqtt and playlist change notifier stuff --

  function isFrontpage()
  {
    // Get the current path from the window location
    const path = window.location.pathname;

    // Check if the path ends with "index.php" or "frontend.php"
    if (path.endsWith("index.php")
        || path.endsWith("frontend.php")
        || path.endsWith("frontend_admin.php")
      )
    {
      // Ensure there are no query parameters or hash fragments
      return window.location.search === "" && window.location.hash === "";
    }

    return false;
  }

  <?php
    if ($cfg_mqtt)
    {
  ?>
    console.log("init MQTT playlist refresh");

    const brokerUrl = 'ws://<?php echo $cfg_mqtt_broker_unified; ?>:9001'; // -- not mqtt, browser can only connect with websocket --
    const clientID = '<?php echo CFG_MQTT_CLIENTID_PREFIX; ?>.frontend.'+generateRandomString();
    console.log('clientID: ' + clientID);
    const options = {
      clientId: clientID,
    };

    // -- create an MQTT client --
    const client = mqtt.connect(brokerUrl, options);
    const topicToSubscribe = '<?php echo CFG_MQTT_TOPIC_PLAYLIST; ?>';

    // -- event handler when the client is connected --
    client.on('connect', () => {
      console.log('MQTT: Connected to broker <?php echo $cfg_mqtt_broker_unified; ?>');

      // Subscribe to a topic
      client.subscribe(topicToSubscribe, (err) => {
        if (!err) {
          console.log(`MQTT: Subscribed to ${topicToSubscribe}`);
        }
        else {
          console.error(`MQTT: Error subscribing to ${topicToSubscribe}: ${err}`);
        }
      });
    });

    // Event handler when a message is received
    client.on('message', (topic, message) => {
      console.log(`MQTT: received message on topic ${topic}: ${message.toString()}`);
      if (topic == topicToSubscribe)
      {
        // -- when not in playlist, do not update --
        if (isFrontpage())
        {
          console.log('reload playlist by MQTT message'); // console.log('ajaxCurPlaylistCall');
          reloadPlaylist(); // ajaxCurPlaylistCall();
        }
      }
    });

    // Event handler when the client is disconnected
    client.on('close', () => {
      console.log('MQTT: disconnected from broker');
    });

    // Handle errors
    client.on('error', (err) => {
      console.error(`MQTT: client error: ${err}`);
    });


  // -- end of mqtt and playlist change notifier stuff --
  // -- ---------------------------------------------- --





  <?php
    }
  ?>



  /*if(typeof(EventSource) === "undefined")
    {
      console.log('NO - server sent events support available');
    }
    else
    {
      console.log('OK - server sent events support available');

  VAR1
      var sse_source = new EventSource("sse_player.php");
      sse_source.onmessage = demo;
      sse_source.addEventListener("ping", demo, false); // ping message update
      function demo(e) {
        console.log(e.data);
        //$('frame_hulk').innerHTML = e.data;
        // -- so wie hier oben zu sehen, geht es schon mal prinzipiell --
        //DojoAjax('ajax_getCurrentPlaylist.php', 'frame_hulk');
      }

  VAR2
      var sse_source = new EventSource('sse_player.php');
      sse_source.onmessage = function (e) {
      console.log(e.data);
        //DojoAjax('ajax_getCurrentPlaylist.php', 'frame_hulk');
      };

  VAR3
      //if no events specified
      var sse_source = new EventSource("sse_player.php");
      sse_source.addEventListner("message", function(e) {
        log(e.data)
      });
    }

  VAR4
    if (!!window.EventSource) {
      var sse_source = new EventSource('sse_player.php');
      sse_source.addEventListener('message', function(e) {
        var data = JSON.parse(e.data);
        console.log(data.username + ' is now ' + data.emotion);
      }, false); }
    else {
      // Result to xhr polling :( }
  */


  /*function sleep(milliseconds)
  {
    var start = new Date().getTime();
    while ((new Date().getTime() - start) < milliseconds)
    {
      // Do nothing
    }
  }*/


</script>
