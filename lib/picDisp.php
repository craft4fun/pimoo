<?php
  // -- picture dispatcher to deliver pics as preview outside of htdocs --

  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');

  if (!$cfg_allowStreaming)
  {
    die('streaming is not allowed');
  }
  else
  {
    $hashUser = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : NULLSTR;

    // -- only logged in can see preview? --
    if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
    {
      if (!isset($_GET['hash']))
      {
        die('hash missing');
      }
      else
      {
        if (md5($hashUser) != $_GET['hash'])
        {
          die('hash failure');
        }
        else
        {
          if (!isset($_GET['pic']))
          {
            die('pic missing');
          }
          else
          {
            header("Content-Type: image/$cfg_pictureFileType"); // -- modify the header - it is a jpg --

            // -- switches if the URL comes base64 encoded and has to be decoed before --
            $pic = (isset($_GET['base64'])) ? base64_decode($_GET['pic']) : $_GET['pic'];

            $picAbs = $cfg_absPathToSongs.$pic;

            // -- give a name for the download, otherwise it would be named "picDisp.php.jpg" --
            if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
              $attachment = NULLSTR;
            else
              $attachment = 'attachment;';
            header("Content-Disposition:$attachment filename=\"{$picAbs}\"");

            if ($picAbs !== NULLSTR)
            {
              readfile($picAbs); // -- deliver the picture --
            }
          }
        }
      }
    }
  }

