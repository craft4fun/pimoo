<?php
  /**
   * cli progressbar
   * Patrick Hoef
   * craft4fun.de
   * issued: 2024-09-25
   * last change: 2024-10-18
   *
   * -- usage --
   * require_once 'class_cli_progressbar.php';
   * $cliProgBar = new cli_ProgressBar(10);
   * (optional) $cliProgBar->setSignDone('+');
   * (optional) $cliProgBar->setSignOpen(' ');
   *
   * for ($i = 0; $i < 10; $i++)
   * {
   *   $cliProgBar->setCaption('currently doing this'); // (optional)
   *   $cliProgBar->setColor($cliProgBar::COLOR_BLUE); // (optional)
   *   $cliProgBar->progress();
   *   sleep(1);
   * }
   * $cliProgBar->finish();
   *
  */

  class cli_ProgressBar
  {
    // -- public constants for colors --
    public const COLOR_WHITE = '37';   // White for default
    public const COLOR_BLUE = '34';    // Blue for information
    public const COLOR_GREEN = '32';   // Green for success
    public const COLOR_YELLOW = '33';  // Yellow for warnings
    public const COLOR_RED = '31';     // Red for errors or failed

    private $maxValue = 100;
    private $barLength = 50;
    private $signDone = '#';
    private $signOpen = '_';
    private $caption = '';
    private $color = self::COLOR_WHITE; // Default color
    private $curValue = 0;

    /**
     * constructor, sets the maxValue
     *
     * @param integer $maxValue
     */
    public function __construct(int $maxValue = 100)
    {
      $this->maxValue = $maxValue;
    }

    /**
     * set the "Done" sign
     *
     * @param string $value
     * @return void
     */
    function setSignDone(string $value):void
    {
      $this->signDone = $value;
    }
    /**
     * set the "Open" sign
     *
     * @param string $value
     * @return void
     */
    function setSignOpen(string $value):void
    {
      $this->signOpen = $value;
    }

    private function clearConsole(): void
    {
      // TODO fix wipe all previous data from console when using caption
      //system('tput reset'); // -- works more gracefully in more environments than system('clear'); --
      // -- clear the console using ANSI escape codes --
      echo "\033[H\033[J";  // -- moves the cursor to the top-left and clears the screen --
    }

    /**
     * colored console output
     *
     * @param string $string
     * @param string $colorCode
     * @return void
     */
    private function echoColored(string $string, string $colorCode = self::COLOR_WHITE): void
    {
      echo "\033[" . $colorCode . "m" . $string . "\033[0m"; #  . PHP_EOL
    }

    /**
     * outputs the current progressbar
     *
     * @param integer $currentValue
     * @return void
     */
    private function output(int $currentValue, string $caption = ''):void
    {
      $percent = $currentValue / $this->maxValue;
      $filled = (int)($this->barLength * $percent);
      $empty = $this->barLength - $filled;
      $bar = str_repeat($this->signDone, $filled) . str_repeat($this->signOpen, $empty);
      if ($caption == '')
      {
        $progressString = sprintf("\r[%s] %d%%", $bar, $percent * 100);
        $this->echoColored($progressString, $this->color);
      }
      else
      {
        $this->clearConsole();  // -- clear the screen before printing new content --
        $progressString = sprintf("\r[%s] %d%% \n %s", $bar, $percent * 100, $caption);
        $this->echoColored($progressString, $this->color);
      }

      flush();

      if ($currentValue == $this->maxValue)
      {
        echo PHP_EOL;
      }
    }

    /**
     * set optional caption for output behind value
     *
     * @param integer $delta
     * @return void
     */
    function setCaption(string $value):void
    {
      $this->caption = $value;
    }

    /**
     * set optional caption for output behind value
     *
     * @param integer $delta
     * @return void
     */
    function setColor(string $value):void
    {
      $this->color = $value;
    }

    /**
     * increment progress and calls output of progressbar
     *
     * @param integer $delta
     * @return void
     */
    function progress(int $delta = 1):void
    {
      $this->curValue = $this->curValue + $delta;
      $this->output($this->curValue, $this->caption);
    }

    /**
     * finishes the progressbar gracefully and optionally to get a defined end state
     *
     * @return void
     */
    function finish():void
    {
      $this->curValue = $this->maxValue;
      $this->output($this->curValue, $this->caption);
      //echo PHP_EOL;
    }

  }
