<script type="text/javascript">

// -- base64 --


  function utf8_encode (argString) {
      // Encodes an ISO-8859-1 string to UTF-8
      // version: 1103.1210
      // discuss at: http://phpjs.org/functions/utf8_encode
      // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
      var string = (argString + '');
      var utftext = "",
          start, end, stringl = 0;

      start = end = 0;
      stringl = string.length;
      for (var n = 0; n < stringl; n++) {
          var c1 = string.charCodeAt(n);
          var enc = null;

          if (c1 < 128) {
              end++;
          } else if (c1 > 127 && c1 < 2048) {
              enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
          } else {
              enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
          }
          if (enc !== null) {
              if (end > start) {
                  utftext += string.slice(start, end);
              }
              utftext += enc;
              start = end = n + 1;
          }
      }

      if (end > start) {
          utftext += string.slice(start, stringl);
      }

      return utftext;
  }


  function base64_encode (data, aURLmode) {
      // Encodes string using MIME base64 algorithm
      // version: 1103.1210
      // discuss at: http://phpjs.org/functions/base64_encode    // +   original by: Tyler Akins (http://rumkin.com)
      // +   improved by: Bayron Guevara
      // +   improved by: Thunder.m
      // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // +   bugfixed by: Pellentesque Malesuada    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // -    depends on: utf8_encode
      // *     example 1: base64_encode('Kevin van Zonneveld');
      // *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
      // mozilla has this native    // - but breaks in 2.0.0.12!
      //if (typeof this.window['atob'] == 'function') {
      //    return atob(data);
      //}

      //-- P.Höf --
      if (!aURLmode)
        aURLmode = false;

      if (aURLmode == false)
        var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
      else
        var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_=";

      var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
      ac = 0,
      enc = "",
      tmp_arr = [];
      if (!data)
        return data;
      data = this.utf8_encode(data + '');
      do {
          o1 = data.charCodeAt(i++);
          o2 = data.charCodeAt(i++);
          o3 = data.charCodeAt(i++);
          bits = o1 << 16 | o2 << 8 | o3;
          h1 = bits >> 18 & 0x3f;
          h2 = bits >> 12 & 0x3f;        h3 = bits >> 6 & 0x3f;
          h4 = bits & 0x3f;
          tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);    } while (i < data.length);
      enc = tmp_arr.join('');
      switch (data.length % 3) {    case 1:
          enc = enc.slice(0, -2) + '==';
          break;
      case 2:
          enc = enc.slice(0, -1) + '=';        break;
      }
      return enc;
  }



   // private method for UTF-8 decoding
   function utf8_decode (utftext) {
       var string = '';
       var i = 0;
       var c = 0, c1 = 0, c2 = 0;

       while (i < utftext.length) {

           c = utftext.charCodeAt(i);

           if (c < 128) {
               string += String.fromCharCode(c);
               i++;
           }
           else if ((c > 191) && (c < 224)) {
               c1 = utftext.charCodeAt(i + 1);
               string += String.fromCharCode(((c & 31) << 6) | (c1 & 63));
               i += 2;
           }
           else {
               c1 = utftext.charCodeAt(i + 1);
               c2 = utftext.charCodeAt(i + 2);
               string += String.fromCharCode(((c & 15) << 12) | ((c1 & 63) << 6) | (c2 & 63));
               i += 3;
           }

       }
       return string;
   };

   function base64_decode (data, aURLmode) {
      // Decodes string using MIME base64 algorithm
      // version: 1103.1210
      // discuss at: http://phpjs.org/functions/base64_decode    // +   original by: Tyler Akins (http://rumkin.com)

      //-- P.Höf --
      if (!aURLmode)
        aURLmode = false;

      if (aURLmode == false)
       var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
      else
         var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_=";

      var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
          ac = 0,        dec = "",
          tmp_arr = [];
      if (!data)
        return data;
      data += '';
      do {
          h2 = b64.indexOf(data.charAt(i++));
          h3 = b64.indexOf(data.charAt(i++));
          h4 = b64.indexOf(data.charAt(i++));
           bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
          o1 = bits >> 16 & 0xff;
          o2 = bits >> 8 & 0xff;
          o3 = bits & 0xff;
          if (h3 == 64) {
              tmp_arr[ac++] = String.fromCharCode(o1);
          } else if (h4 == 64) {
              tmp_arr[ac++] = String.fromCharCode(o1, o2);        } else {
              tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
          }
      } while (i < data.length);
       dec = tmp_arr.join('');
      dec = this.utf8_decode(dec);       //dec = dec;
      return dec;
  }




/*
 *  md5.js 1.0b 27/06/96
 *
 * Javascript implementation of the RSA Data Security, Inc. MD5
 * Message-Digest Algorithm.
 *
 * Copyright (c) 1996 Henri Torgemane. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for any purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies.
 *
 * Of course, this soft is provided "as is" without express or implied
 * warranty of any kind.
 *
 * Modified with german comments and some information about collisions.
 * (Ralf Mieke, ralf@miekenet.de, http://mieke.home.pages.de)
 */
function array(n) {
  for(i=0;i<n;i++) this[i]=0;
  this.length=n;
}
function integer(n) { return n%(0xffffffff+1); }

function shr(a,b) {
  a=integer(a);
  b=integer(b);
  if (a-0x80000000>=0) {
    a=a%0x80000000;
    a>>=b;
    a+=0x40000000>>(b-1);
  } else
    a>>=b;
  return a;
}

function shl1(a) {
  a=a%0x80000000;
  if (a&0x40000000==0x40000000)
  {
    a-=0x40000000;
    a*=2;
    a+=0x80000000;
  } else
    a*=2;
  return a;
}

function shl(a,b) {
  a=integer(a);
  b=integer(b);
  for (var i=0;i<b;i++) a=shl1(a);
  return a;
}

function and(a,b) {
  a=integer(a);
  b=integer(b);
  var t1=(a-0x80000000);
  var t2=(b-0x80000000);
  if (t1>=0)
    if (t2>=0)
      return ((t1&t2)+0x80000000);
    else
      return (t1&b);
  else
    if (t2>=0)
      return (a&t2);
    else
      return (a&b);
}

function or(a,b) {
  a=integer(a);
  b=integer(b);
  var t1=(a-0x80000000);
  var t2=(b-0x80000000);
  if (t1>=0)
    if (t2>=0)
      return ((t1|t2)+0x80000000);
    else
      return ((t1|b)+0x80000000);
  else
    if (t2>=0)
      return ((a|t2)+0x80000000);
    else
      return (a|b);
}

function xor(a,b) {
  a=integer(a);
  b=integer(b);
  var t1=(a-0x80000000);
  var t2=(b-0x80000000);
  if (t1>=0)
    if (t2>=0)
      return (t1^t2);
    else
      return ((t1^b)+0x80000000);
  else
    if (t2>=0)
      return ((a^t2)+0x80000000);
    else
      return (a^b);
}

function not(a) {
  a=integer(a);
  return (0xffffffff-a);
}
    var state = new array(4);
    var count = new array(2);
        count[0] = 0;
        count[1] = 0;
    var buffer = new array(64);
    var transformBuffer = new array(16);
    var digestBits = new array(16);
    var S11 = 7;
    var S12 = 12;
    var S13 = 17;
    var S14 = 22;
    var S21 = 5;
    var S22 = 9;
    var S23 = 14;
    var S24 = 20;
    var S31 = 4;
    var S32 = 11;
    var S33 = 16;
    var S34 = 23;
    var S41 = 6;
    var S42 = 10;
    var S43 = 15;
    var S44 = 21;
    function F(x,y,z) {
        return or(and(x,y),and(not(x),z));
    }
    function G(x,y,z) {
        return or(and(x,z),and(y,not(z)));
    }
    function H(x,y,z) {
        return xor(xor(x,y),z);
    }
    function I(x,y,z) {
        return xor(y ,or(x , not(z)));
    }
    function rotateLeft(a,n) {
        return or(shl(a, n),(shr(a,(32 - n))));
    }
    function FF(a,b,c,d,x,s,ac) {
        a = a+F(b, c, d) + x + ac;
        a = rotateLeft(a, s);
        a = a+b;
        return a;
    }
    function GG(a,b,c,d,x,s,ac) {
        a = a+G(b, c, d) +x + ac;
        a = rotateLeft(a, s);
        a = a+b;
        return a;
    }
    function HH(a,b,c,d,x,s,ac) {
        a = a+H(b, c, d) + x + ac;
        a = rotateLeft(a, s);
        a = a+b;
        return a;
    }
    function II(a,b,c,d,x,s,ac) {
        a = a+I(b, c, d) + x + ac;
        a = rotateLeft(a, s);
        a = a+b;
        return a;
    }
    function transform(buf,offset) {
        var a=0, b=0, c=0, d=0;
        var x = transformBuffer;
        a = state[0];
        b = state[1];
        c = state[2];
        d = state[3];
        for (i = 0; i < 16; i++) {
            x[i] = and(buf[i*4+offset],0xff);
            for (j = 1; j < 4; j++) {
                x[i]+=shl(and(buf[i*4+j+offset] ,0xff), j * 8);
            }
        }
        a = FF ( a, b, c, d, x[ 0], S11, 0xd76aa478);
        d = FF ( d, a, b, c, x[ 1], S12, 0xe8c7b756);
        c = FF ( c, d, a, b, x[ 2], S13, 0x242070db);
        b = FF ( b, c, d, a, x[ 3], S14, 0xc1bdceee);
        a = FF ( a, b, c, d, x[ 4], S11, 0xf57c0faf);
        d = FF ( d, a, b, c, x[ 5], S12, 0x4787c62a);
        c = FF ( c, d, a, b, x[ 6], S13, 0xa8304613);
        b = FF ( b, c, d, a, x[ 7], S14, 0xfd469501);
        a = FF ( a, b, c, d, x[ 8], S11, 0x698098d8);
        d = FF ( d, a, b, c, x[ 9], S12, 0x8b44f7af);
        c = FF ( c, d, a, b, x[10], S13, 0xffff5bb1);
        b = FF ( b, c, d, a, x[11], S14, 0x895cd7be);
        a = FF ( a, b, c, d, x[12], S11, 0x6b901122);
        d = FF ( d, a, b, c, x[13], S12, 0xfd987193);
        c = FF ( c, d, a, b, x[14], S13, 0xa679438e);
        b = FF ( b, c, d, a, x[15], S14, 0x49b40821);
        a = GG ( a, b, c, d, x[ 1], S21, 0xf61e2562);
        d = GG ( d, a, b, c, x[ 6], S22, 0xc040b340);
        c = GG ( c, d, a, b, x[11], S23, 0x265e5a51);
        b = GG ( b, c, d, a, x[ 0], S24, 0xe9b6c7aa);
        a = GG ( a, b, c, d, x[ 5], S21, 0xd62f105d);
        d = GG ( d, a, b, c, x[10], S22,  0x2441453);
        c = GG ( c, d, a, b, x[15], S23, 0xd8a1e681);
        b = GG ( b, c, d, a, x[ 4], S24, 0xe7d3fbc8);
        a = GG ( a, b, c, d, x[ 9], S21, 0x21e1cde6);
        d = GG ( d, a, b, c, x[14], S22, 0xc33707d6);
        c = GG ( c, d, a, b, x[ 3], S23, 0xf4d50d87);
        b = GG ( b, c, d, a, x[ 8], S24, 0x455a14ed);
        a = GG ( a, b, c, d, x[13], S21, 0xa9e3e905);
        d = GG ( d, a, b, c, x[ 2], S22, 0xfcefa3f8);
        c = GG ( c, d, a, b, x[ 7], S23, 0x676f02d9);
        b = GG ( b, c, d, a, x[12], S24, 0x8d2a4c8a);
        a = HH ( a, b, c, d, x[ 5], S31, 0xfffa3942);
        d = HH ( d, a, b, c, x[ 8], S32, 0x8771f681);
        c = HH ( c, d, a, b, x[11], S33, 0x6d9d6122);
        b = HH ( b, c, d, a, x[14], S34, 0xfde5380c);
        a = HH ( a, b, c, d, x[ 1], S31, 0xa4beea44);
        d = HH ( d, a, b, c, x[ 4], S32, 0x4bdecfa9);
        c = HH ( c, d, a, b, x[ 7], S33, 0xf6bb4b60);
        b = HH ( b, c, d, a, x[10], S34, 0xbebfbc70);
        a = HH ( a, b, c, d, x[13], S31, 0x289b7ec6);
        d = HH ( d, a, b, c, x[ 0], S32, 0xeaa127fa);
        c = HH ( c, d, a, b, x[ 3], S33, 0xd4ef3085);
        b = HH ( b, c, d, a, x[ 6], S34,  0x4881d05);
        a = HH ( a, b, c, d, x[ 9], S31, 0xd9d4d039);
        d = HH ( d, a, b, c, x[12], S32, 0xe6db99e5);
        c = HH ( c, d, a, b, x[15], S33, 0x1fa27cf8);
        b = HH ( b, c, d, a, x[ 2], S34, 0xc4ac5665);
        a = II ( a, b, c, d, x[ 0], S41, 0xf4292244);
        d = II ( d, a, b, c, x[ 7], S42, 0x432aff97);
        c = II ( c, d, a, b, x[14], S43, 0xab9423a7);
        b = II ( b, c, d, a, x[ 5], S44, 0xfc93a039);
        a = II ( a, b, c, d, x[12], S41, 0x655b59c3);
        d = II ( d, a, b, c, x[ 3], S42, 0x8f0ccc92);
        c = II ( c, d, a, b, x[10], S43, 0xffeff47d);
        b = II ( b, c, d, a, x[ 1], S44, 0x85845dd1);
        a = II ( a, b, c, d, x[ 8], S41, 0x6fa87e4f);
        d = II ( d, a, b, c, x[15], S42, 0xfe2ce6e0);
        c = II ( c, d, a, b, x[ 6], S43, 0xa3014314);
        b = II ( b, c, d, a, x[13], S44, 0x4e0811a1);
        a = II ( a, b, c, d, x[ 4], S41, 0xf7537e82);
        d = II ( d, a, b, c, x[11], S42, 0xbd3af235);
        c = II ( c, d, a, b, x[ 2], S43, 0x2ad7d2bb);
        b = II ( b, c, d, a, x[ 9], S44, 0xeb86d391);
        state[0] +=a;
        state[1] +=b;
        state[2] +=c;
        state[3] +=d;
    }
    function init() {
        count[0]=count[1] = 0;
        state[0] = 0x67452301;
        state[1] = 0xefcdab89;
        state[2] = 0x98badcfe;
        state[3] = 0x10325476;
        for (i = 0; i < digestBits.length; i++)
            digestBits[i] = 0;
    }
    function update(b) {
        var index,i;

        index = and(shr(count[0],3) , 0x3f);
        if (count[0]<0xffffffff-7)
          count[0] += 8;
        else {
          count[1]++;
          count[0]-=0xffffffff+1;
          count[0]+=8;
        }
        buffer[index] = and(b,0xff);
        if (index  >= 63) {
            transform(buffer, 0);
        }
    }
    function finish() {
        var bits = new array(8);
        var        padding;
        var        i=0, index=0, padLen=0;

        for (i = 0; i < 4; i++) {
            bits[i] = and(shr(count[0],(i * 8)), 0xff);
        }
        for (i = 0; i < 4; i++) {
            bits[i+4]=and(shr(count[1],(i * 8)), 0xff);
        }
        index = and(shr(count[0], 3) ,0x3f);
        padLen = (index < 56) ? (56 - index) : (120 - index);
        padding = new array(64);
        padding[0] = 0x80;
        for (i=0;i<padLen;i++)
          update(padding[i]);
        for (i=0;i<8;i++)
          update(bits[i]);

        for (i = 0; i < 4; i++) {
            for (j = 0; j < 4; j++) {
                digestBits[i*4+j] = and(shr(state[i], (j * 8)) , 0xff);
            }
        }
    }
function hexa(n) {
 var hexa_h = "0123456789abcdef";
 var hexa_c="";
 var hexa_m=n;
 for (hexa_i=0;hexa_i<8;hexa_i++) {
   hexa_c=hexa_h.charAt(Math.abs(hexa_m)%16)+hexa_c;
   hexa_m=Math.floor(hexa_m/16);
 }
 return hexa_c;
}
var ascii="01234567890123456789012345678901" +
          " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ"+
          "[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

function MD5(nachricht)
{
 var l,s,k,ka,kb,kc,kd;

 init();
 for (k=0;k<nachricht.length;k++) {
   l=nachricht.charAt(k);
   update(ascii.lastIndexOf(l));
 }
 finish();
 ka=kb=kc=kd=0;
 for (i=0;i<4;i++) ka+=shl(digestBits[15-i], (i*8));
 for (i=4;i<8;i++) kb+=shl(digestBits[15-i], ((i-4)*8));
 for (i=8;i<12;i++) kc+=shl(digestBits[15-i], ((i-8)*8));
 for (i=12;i<16;i++) kd+=shl(digestBits[15-i], ((i-12)*8));
 s=hexa(kd)+hexa(kc)+hexa(kb)+hexa(ka);
 return s;
}



//-- TableSort --
var JB_Table = function(tab) {
var up =  ' ' + String.fromCharCode(9650);
var down = ' ' + String.fromCharCode(9660);
// var up = String.fromCharCode(8593);
// var down = String.fromCharCode(8595);
// var up = String.fromCharCode(11014);
// var down = String.fromCharCode(11015);
var no = String.fromCharCode(160,160,160,160);
var dieses = this;
var defsort = 0;
var startsort_u = -1,startsort_d = -1;
var first = true;
var ssort;
var tbdy = tab.getElementsByTagName("tbody")[0];
var tz = tbdy.rows;
var nzeilen = tz.length;
if (nzeilen==0) return;
var nspalten = tz[0].cells.length;
var Titel = tab.getElementsByTagName("thead")[0].getElementsByTagName("tr")[0].getElementsByTagName("th");
var Arr = new Array(nzeilen);
var ct = 0;
var sdir = new Array(nspalten);
var stype = new Array(nspalten);
for(var i=0;i<nspalten;i++) {
  stype[i] = "n";
  sdir[i] = "u";}
var Init_Sort = function(t,nr) {
  t.style.cursor = "pointer";
  t.onclick = function() { dieses.sort(nr); };
  sortsymbol.init(t,no);
  //t.title = 'Die Tabelle nach "'+t.firstChild.data+'" sortieren.';
  if(t.className.indexOf("vorsortiert-")>-1) {
    sortsymbol.set(t,down);
    ssort = nr;
  }
  else if(t.className.indexOf("vorsortiert")>-1) {
    sortsymbol.set(t,up);
    ssort = nr;
  }
  if(t.className.indexOf("sortiere-")>-1) startsort_d=nr;
  else if(t.className.indexOf("sortiere")>-1) startsort_u=nr;
} // Init_Sort
var sortsymbol = {
  init: function(t,s) {
    var sp = t.getElementsByTagName("span");
    for(var i=0;i<sp.length;i++) {
      if(!sp[i].hasChildNodes()) {
        t.sym = sp[i].appendChild(document.createTextNode(s));
        break;
      }
    }
    if(typeof(t.sym)=="undefined") t.sym = t.appendChild(document.createTextNode(s));
  },
  set: function(t,s) {
    t.sym.data = s;
  },
  get: function(t) {
    return t.sym.data;
  }
} // sortsymbol
var VglFkt_s = function(a,b) {
  var as = a[ssort], bs = b[ssort];
  var ret=(as>bs)?1:(as<bs)?-1:0;
  if(!ret && ssort!=defsort) {
    if (stype[defsort]=="s") { as = a[defsort]; bs = b[defsort]; ret = (as>bs)?1:(as<bs)?-1:0; }
    else ret = parseFloat(a[defsort])-parseFloat(b[defsort])
  }
  return ret;
} // VglFkt_s
var VglFkt_n = function(a,b) {
  var ret = parseFloat(a[ssort])-parseFloat(b[ssort]);
  if(!ret && ssort!=defsort) {
    if (stype[defsort]=="s") { var as = a[defsort],bs = b[defsort]; ret = (as>bs)?1:(as<bs)?-1:0; }
    else ret = parseFloat(a[defsort])-parseFloat(b[defsort]);
  }
  return ret;
} // VglFkt_n
var convert = function(val,s) {
  var dmy;
  var trmdat = function() {
    if(dmy[0]<10) dmy[0] = "0" + dmy[0];
    if(dmy[1]<10) dmy[1] = "0" + dmy[1];
    if(dmy[2]<10) dmy[2] = "200" + dmy[2];
    else if(dmy[2]<20) dmy[2] = "20" + dmy[2];
    else if(dmy[2]<99) dmy[2] = "19" + dmy[2];
    else if(dmy[2]>9999) dmy[2] = "9999";
  }
  if(val.length==0) val = "0";
  if(!isNaN(val) && val.search(/[0-9]/)!=-1) return val;
  var n = val.replace(",",".");
  if(!isNaN(n) && n.search(/[0-9]/)!=-1) return n;
  n = n.replace(/\s|&nbsp;|&#160;|\u00A0/g,"");
  if(!isNaN(n) && n.search(/[0-9]/)!=-1) return n;
  if(!val.search(/^\s*\d+\s*\.\s*\d+\s*\.\s*\d+\s+\d+:\d\d\:\d\d\s*$/)) {
    var dp = val.search(":");
    dmy = val.substring(0,dp-2).split(".");
    dmy[3] = val.substring(dp-2,dp);
    dmy[4] = val.substring(dp+1,dp+3);
    dmy[5] = val.substring(dp+4,dp+6);
    for(var i=0;i<6;i++) dmy[i] = parseInt(dmy[i],10);
    trmdat();
    for(var i=3;i<6;i++) if(dmy[i]<10) dmy[i] = "0" + dmy[i];
    return (""+dmy[2]+dmy[1]+dmy[0]+"."+dmy[3]+dmy[4]+dmy[5]).replace(/ /g,"");
  }
  if(!val.search(/^\s*\d+\s*\.\s*\d+\s*\.\s*\d+\s+\d+:\d\d\s*$/)) {
    var dp = val.search(":");
    dmy = val.substring(0,dp-2).split(".");
    dmy[3] = val.substring(dp-2,dp);
    dmy[4] = val.substring(dp+1,dp+3);
    for(var i=0;i<5;i++) dmy[i] = parseInt(dmy[i],10);
    trmdat();
    for(var i=3;i<5;i++) if(dmy[i]<10) dmy[i] = "0"+dmy[i];
    return (""+dmy[2]+dmy[1]+dmy[0]+"."+dmy[3]+dmy[4]).replace(/ /g,"");
  }
  if(!val.search(/^\s*\d+:\d\d\:\d\d\s*$/)) {
    dmy = val.split(":");
    for(var i=0;i<3;i++) dmy[i] = parseInt(dmy[i],10);
    for(var i=0;i<3;i++) if(dmy[i]<10) dmy[i] = "0"+dmy[i];
    return (""+dmy[0]+dmy[1]+"."+dmy[2]).replace(/ /g,"");
  }
  if(!val.search(/^\s*\d+:\d\d\s*$/)) {
    dmy = val.split(":");
    for(var i=0;i<2;i++) dmy[i] = parseInt(dmy[i],10);
    for(var i=0;i<2;i++) if(dmy[i]<10) dmy[i] = "0"+dmy[i];
    return (""+dmy[0]+dmy[1]).replace(/ /g,"");
  }
  if(!val.search(/^\s*\d+\s*\.\s*\d+\s*\.\s*\d+/)) {
    dmy = val.split(".");
    for(var i=0;i<3;i++) dmy[i] = parseInt(dmy[i],10);
    trmdat();
    return (""+dmy[2]+dmy[1]+dmy[0]).replace(/ /g,"");
  }
  stype[s] = "s";
  return val.toLowerCase().replace(/\u00e4/g,"ae").replace(/\u00f6/g,"oe").replace(/\u00fc/g,"ue").replace(/\u00df/g,"ss");
} // convert
this.sort = function(sp) {
  if (first) {
    for(var z=0;z<nzeilen;z++) {
      var zelle = tz[z].getElementsByTagName("td"); // cells;
      Arr[z] = new Array(nspalten+1);
      Arr[z][nspalten] = tz[z];
      for(var s=0;s<nspalten;s++) {
        if (zelle[s].getAttribute("data-sort_key"))
          var zi = convert(zelle[s].getAttribute("data-sort_key"),s);
        else if (zelle[s].getAttribute("sort_key"))
          var zi = convert(zelle[s].getAttribute("sort_key"),s);
        else
          var zi = convert(JB_elementText(zelle[s]),s);
        Arr[z][s] = zi ;
//       zelle[s].innerHTML += "<br>"+zi;
      }
    }
    first = false;
  }
  if(sp==ssort) {
    Arr.reverse() ;
    if ( sortsymbol.get(Titel[ssort])==down )
      sortsymbol.set(Titel[ssort],up);
    else
      sortsymbol.set(Titel[ssort],down);
  }
  else {
    if ( ssort>=0 && ssort<nspalten ) sortsymbol.set(Titel[ssort],no);
      ssort = sp;
    if(stype[ssort]=="s") Arr.sort(VglFkt_s);
    else                  Arr.sort(VglFkt_n);
    if(sdir[ssort]=="u") {
      sortsymbol.set(Titel[ssort],up);}
    else {
      Arr.reverse() ;
      sortsymbol.set(Titel[ssort],down);}}
  for(var z=0;z<nzeilen;z++)
    tbdy.appendChild(Arr[z][nspalten]);
  if(typeof(JB_aftersort)=="function") JB_aftersort(tab,tbdy,tz,nzeilen,nspalten,ssort);} // sort
//if(!tab.title.length) tab.title="Ein Klick auf die Spalten\u00fcberschrift sortiert die Tabelle.";
for(var i=Titel.length-1;i>-1;i--) {
  var t=Titel[i];
  if(t.className.indexOf("sortier")>-1) {
    ct++;
    Init_Sort(t,i);
    defsort = i ;
    if(t.className.indexOf("sortierbar-")>-1) sdir[i] = "d";}}
if(ct==0) {
  for(var i=0;i<Titel.length;i++)
    Init_Sort(Titel[i],i);
  defsort = 0;}
if(startsort_u>=0) this.sort(startsort_u);
if(startsort_d>=0) { this.sort(startsort_d); this.sort(startsort_d); }
if(typeof(JB_aftersortinit)=="function") JB_aftersortinit(tab,tbdy,tz,nzeilen,nspalten,-1);
} // JB_Table
var JB_addEvent = function (obj, type, fn) {
if(type.search("on")==0) type = type.substr(2);
if (obj.addEventListener) {
  obj.addEventListener(type, fn, false);}
else if (obj.attachEvent) {
  obj.attachEvent('on' + type, function () {
    return fn.call(obj, window.event);
  });}} // addEvent
var JB_getElementsByClass_TagName = function(tagname,classname) {
var tag = document.getElementsByTagName(tagname);
var reg = new RegExp("(^|\\s)" + classname + "(\\s|$)","gi");
var Elements = [];
for(var i=0;i<tag.length;i++) {
  if(tag[i].className.search(reg)>-1) Elements.push(tag[i]);}
return Elements;}
var JB_elementText = function(elem) {
var eT = function(ele) {
  var uele = ele.firstChild;
  while(uele) {
    if(uele.hasChildNodes()) eT(uele);
    if(uele.nodeType == 1) Text += " ";
    else if(uele.nodeType == 3) Text += uele.data;
    uele = uele.nextSibling;}}
var Text = "";
eT(elem);
return Text.replace(/\s+/g," ");}
JB_addEvent(window,"onload",function(e) {
if (!document.getElementsByTagName) return;
if (!document.getElementsByTagName('body')[0].appendChild) return;
var Sort_Table = JB_getElementsByClass_TagName("table","sortierbar");
for(var i=0;i<Sort_Table.length;i++) new JB_Table(Sort_Table[i]);
});

</script>