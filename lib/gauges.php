<?php

  // -- THIS IS AN ALMOST UNMODIFIED COPY OF EBBES --


  /**
   * cntProgressbar 2014-10-21
   * @author Patrick Höf
   *
   * HTML element based
   * use like this:
   * $myProgressbar = new class_progressbar();
   * echo $myProgressbar->HTMLexec(50);
   *
   *
   * e.g. disable max alarm: $myProgressbar->setAlarmMaxValue($myProgressbar->getMaxValue()); // -- so there's no alarm for maximum --
   */
  class class_progressbar
  {
    private $maxValue = 100;
    private $dimX = '300'; // px
    private $dimY = '20'; // px
    private $colFrom = 'green';   // -- green --
    private $colTo = 'red';     // -- red --
    private $captionStyle = 'color: white';
    private $captionPraefix = NULLSTR;
    private $captionSuffix = PERCENT;
    private $label = NULLSTR; // cntProgressbar
    private $labelStyle = 'color: black';
    private $labelAlign = 'left';
    private $alarming = true;
    private $alarmMinValue = 10;     // -- e.g. 10 --
    private $alarmMaxValue = 90;   // -- e.g. 90 --
    private $alarmMinColor = 'blue';
    private $alarmMaxColor = 'red';
    private $zoom = 1;
    // -- prepared to be external --
    private $bgColFrom = 'white';
    private $bgColTo = 'silver';
    private $valueFontSize = 'medium'; // -- new 2020 --

    private $htmlContent = NULLSTR;

    private $htmlID;

    function __construct($aMaxValue = 100)
    {
      // -- external --
      // -- current value or value or progress is set when executing to HTML, so it can be called more than once --
      $this->maxValue = $aMaxValue;
    }
    function getHTMLID()
    {
      return $this->htmlID;
    }
    function setHTMLID($aVal)
    {
      $this->htmlID = $aVal;
    }
    function getColFrom()
    {
      return $this->colFrom;
    }
    function setColFrom($aVal)
    {
      $this->colFrom = $aVal;
    }
    function getColTo()
    {
      return $this->colTo;
    }
    function setColTo($aVal)
    {
      $this->colTo = $aVal;
    }
    function getMaxValue()
    {
      return $this->maxValue;
    }
    function setMaxValue($aVal)
    {
      $this->maxValue = $aVal;
    }

    function getDimX()
    {
      return $this->dimX;
    }
    function setDimX($aVal)
    {
      $this->dimX = $aVal;
    }
    function getDimY()
    {
      return $this->dimY;
    }
    function setDimY($aVal)
    {
      $this->dimY = $aVal;
    }
    // -- ------------ --
    // -- beg of label --
    function getLabel()
    {
      return $this->label;
    }
    function setLabel($aVal)
    {
      $this->label = $aVal;
    }
    function getLabelStyle()
    {
      return $this->labelStyle;
    }
    function setLabelStyle($aVal)
    {
      $this->labelStyle = $aVal;
    }
    function getLabelAlign()
    {
      return $this->labelAlign;
    }
    function setLabelAlign($aVal)
    {
      $this->labelAlign = $aVal;
    }
    // -- end of label --
    // -- ------------ --
    // -- -------------- --
    // -- beg of caption --

    function getCaptionStyle()
    {
      return $this->captionStyle;
    }
    function setCaptionStyle($aVal)
    {
      $this->captionStyle = $aVal;
    }
    function getCaptionPraefix()
    {
      return $this->captionPraefix;
    }
    function setCaptionPraefix($aVal)
    {
      $this->captionPraefix = $aVal;
    }
    function getCaptionSuffix()
    {
      return $this->captionSuffix;
    }
    function setCaptionSuffix($aVal)
    {
      $this->captionSuffix = $aVal;
    }
    // -- end of caption --
    // -- -------------- --
    // -- ------------ --
    // -- beg of alarm --
    function getAlarming()
    {
      return $this->alarming;
    }
    function setAlarming($aVal)
    {
      $this->alarming = $aVal;
    }
    function getAlarmMinValue()
    {
      return $this->alarmMinValue;
    }
    function setAlarmMinValue($aVal)
    {
      $this->alarmMinValue = $aVal;
    }
    function getAlarmMaxValue()
    {
      return $this->alarmMaxValue;
    }
    function setAlarmMaxValue($aVal)
    {
      $this->alarmMaxValue = $aVal;
    }
    function getAlarmMinColor()
    {
      return $this->alarmMinColor;
    }
    function setAlarmMinColor($aVal)
    {
      $this->alarmMinColor = $aVal;
    }
    function getAlarmMaxColor()
    {
      return $this->alarmMaxColor;
    }
    function setAlarmMaxColor($aVal)
    {
      $this->alarmMaxColor = $aVal;
    }
    // -- end of alarm --
    // -- ------------ --
    function getZoom()
    {
      return $this->zoom;
    }
    function setZoom($aVal)
    {
      $this->zoom = $aVal;
    }

    function getvalueFontSize()
    {
      return $this->valueFontSize;
    }
    function setvalueFontSize($aVal)
    {
      $this->valueFontSize = $aVal;
    }

    private function _HTMLbuild($aCurValue)
    {
      $this->htmlContent = NULLSTR;
      $this->htmlID = uniqid('cntProgBar_', true);    // 'cntProgBar' . mt_rand(100, 999);
      if ($aCurValue > $this->maxValue)
        $aCurValue = $this->maxValue;
      if ($this->maxValue == 0)
        $this->maxValue = 1;
      $valX = ($aCurValue * $this->dimX) / $this->maxValue;
      $background = '
          background: radial-gradient(center, '.$this->bgColFrom.', '.$this->bgColTo.');
          background: -moz-radial-gradient(center, '.$this->bgColFrom.', '.$this->bgColTo.');
          background: -webkit-radial-gradient(center, '.$this->bgColFrom.', '.$this->bgColTo.');
      ';
      $grad = cntGradientFromTo(substr($this->colFrom, 1), substr($this->colTo, 1), $this->maxValue);
      $curValFake = (intval($aCurValue -1) < 0) ? 0 : intval($aCurValue -1);
      $foreground = '
          background: linear-gradient(to right, '.$this->colFrom.', #'.$grad[$curValFake].');
          background: -moz-linear-gradient(to right, '.$this->colFrom.', #'.$grad[$curValFake].');
          background: -webkit-linear-gradient(to right, '.$this->colFrom.', #'.$grad[$curValFake].');
      ';
      // -- !!! keep the order !!! --
      $alarm = NULLSTR;
      if ($this->alarming)
      {
        if ($aCurValue > $this->alarmMaxValue) // -- e.g. greater than 90% --
        {
          $alarm = 'box-shadow: 0px 0px 6px 0px '.$this->alarmMaxColor.';';
          //$alarm = 'box-shadow: inset 0px 0px 3px 0px '.$this->alarmMaxColor.';';
        }
        if ($aCurValue < $this->alarmMinValue)  // -- e.g. less 10% --
        {
          $alarm = 'box-shadow: 0px 0px 6px 0px '.$this->alarmMinColor.';';
          //$alarm = 'box-shadow: inset 0px 0px 3px 0px '.$this->alarmMaxColor.';';
        }
      }
      if ($this->zoom == 1)
      {
        $zoom = NULLSTR;
      }
      else
      {
         $zoom = '
           transform: scale('.$this->zoom.');
          -moz-transform: scale('.$this->zoom.');
          -webkit-transform: scale('.$this->zoom.');
        ';
      }
      $type = 'div'; // -- 'span' does not work! --
      $this->htmlContent .= '<'.$type.' style="'.$zoom.'">';
        if (strlen(trim($this->label)) > 0)
          $this->htmlContent .= '<div style="text-align:'.$this->labelAlign.';"><label style="'.$this->labelStyle.'" for="'.$this->htmlID.'">'.$this->label.'</label></div>';
        $this->htmlContent .= '
            <'.$type.' id="'.$this->htmlID.'" style="width:'.$this->dimX.'px; height:'.$this->dimY.'px;'.$background.' padding: 0.2em 0.5em 0.2em 0.2em; border-radius: 2px; '.$alarm.'">
            <'.$type.' style="'.$foreground.' width:'.$valX.'px; height:'.$this->dimY.'px; padding-right: 0.2em; text-align: right; border-radius: 2px; box-shadow: 1px 1px 1px #333; text-shadow: 1px 1px 1px black; '.$this->captionStyle.';">
              <span style="font-size: '.$this->valueFontSize.';">'.$this->captionPraefix.$aCurValue.$this->captionSuffix.'</span>
            </'.$type.'>
          </'.$type.'>
        ';
      $this->htmlContent .= '</'.$type.'>';
    }
    /**
     * "write" progressbar into HTML
     * @param integer $aCurValue
     * @return String (html)
     */
    function HTMLexec($aCurValue)
    {
      // -- build new with current value --
      $this->_HTMLbuild($aCurValue);
      // -- return to caller --
      return $this->htmlContent;
    }
  }

