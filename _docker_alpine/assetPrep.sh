#!/bin/bash

echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
echo 'Prepare asset files for Docker image build'
echo 'Written by P.Höf'
echo ''
echo 'History'
echo '2022-11-19 - Fork of makeRelease.sh'
echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
echo ''


# -------------------------
# init
# -------------------------

fMasterPath='../'
fTargetPathSource='asset/'



# --------------------------------------
# ACTION: Cleanup the target path before
# --------------------------------------

echo -n "Cleanup target source path '$fTargetPathSource'..."

rm -R -f $fTargetPathSource/*

echo 'OK'



# --------------------------------------------------
# ACTION: Copy source code files into asset location
# --------------------------------------------------

echo -n "Copying piMoo source files into asset location '$fTargetPathSource'..."

if [ ! -d $fTargetPathSource ]; then
  mkdir $fTargetPathSource
fi

rsync -r $fMasterPath* $fTargetPathSource \
         --exclude=_* \
         --exclude=keep/mediaBase.xml --exclude=keep/partyReport --exclude=keep/*.xml --exclude=keep/*.bak \
         --exclude=tmpS/*.txt --exclude=tmpS/*.xml \
         --exclude=tmp/cache/* --exclude=tmp/nicknames/* --exclude=tmp/player* --exclude=tmp/lastResult_* \
         --exclude=tmp/configShadow*
#          --exclude=keep/config.php

echo 'OK'



# ---------------------------------------------------------------
# ACTION: purge the code and patch config regarding $cfg_idDocker
# ---------------------------------------------------------------

echo -n "purging the code..."

#temporarily without:  ./assetPurger.php --silent -o $fTargetPathSource
#temporarily using sed:
sed -i 's/\$cfg_isDocker = false;/\$cfg_isDocker = true;/' ${fTargetPathSource}keep/config.php


echo 'OK'



# --------------------------------------------------------------------------------------
# ACTION: creating host pipe path. Parameter p stands for 'parents', expeted was -R :o)
# --------------------------------------------------------------------------------------

echo -n "creating host pipe path..."

mkdir -p ${fTargetPathSource}keep/PIPE/
timestamp=`date +%F+%T` # "2022-12-18+12:15:53"
echo "$timestamp" > ${fTargetPathSource}keep/PIPE/shutdown

echo 'OK'



exit 0
