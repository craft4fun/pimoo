#!/bin/bash

#https://docs.docker.com/config/containers/multi-service_container/

# -- build mediabase if not already existing (also possible via webinterface later) --
if [ ! -f "${WEBROOT}mediaBase.xml" ];
then
    echo 'No mediabase found, creating new one!'
    if [ -f '/music/feedback.xml' ];
    then
        echo 'Found feedback.xml - cool - syncing it with mediabase!'
        cp /music/feedback.xml ${WEBROOT}keep/feedback.xml
        chmod 777 ${WEBROOT}keep/feedback.xml # -- fix Permission denied in /var/www/html/api_feedback.php on --
        #chown www-data ${WEBROOT}keep/feedback.xml # -- actually this should be enough, to be checked! --
    fi

    cd ${WEBROOT}admin/
    /usr/bin/php82 ${WEBROOT}admin/cli_importerXML.php
    chmod 777 ${WEBROOT}keep/mediaBase.xml # -- make newly created mediaBase.xml mutable for apache2 afterwards --
    #chown www-data ${WEBROOT}keep/mediaBase.xml # -- actually this should be enough, to be checked! --

    chmod -R 777 ${WEBROOT}keep
    chmod -R 777 ${WEBROOT}tmp
    chmod -R 777 ${WEBROOT}tmpS
fi

# -- start directly when container is launched --
# -- Debian style --
# cd /etc/init.d/
# /etc/init.d/piMood start
# -- Alpine style --
#rc-service piMood start # seems to be already started automatically before


# -- Raspberry Pi support --
# -- read by previously in Dockerfile set env var --
# TODO triggerhappy is not available on Alpine
# if [ "${SYSTEM}" == "Raspi" ];
# then
#     echo 'Raspberry Pi detected, starting triggerhappy daemon...'
#     if [ -z "${TRIGGERHAPPY}" ];
#     then
#         cp /etc/triggerhappy/triggers.d/piMoo-mouse-events /etc/triggerhappy/triggers.d/piMoo-mouse-events.conf
#         rc-service triggerhappy start
#     elif [ "${TRIGGERHAPPY}" == "keyboard" ];
#     then
#         cp /etc/triggerhappy/triggers.d/piMoo-keyboard-events /etc/triggerhappy/triggers.d/piMoo-keyboard-events.conf
#         rc-service triggerhappy start
#     fi
# fi

# -- set backend admin password if provided --
if [ -n "${ADM_PASSWD}" ];
then
    echo 'Setting provided password as backend protection'
    md5=$(echo -n "${ADM_PASSWD}" | md5sum | awk '{print $1}')
    sed -i "s/06074627064edaeb9f6a7cc31294fdde/$md5/g" ${WEBROOT}keep/configDockerOverlay.php
fi


# -- start main process "webinterface" of this container in a blocking manner instead of background
httpd -DFOREGROUND

# -- required ? to be sure! --
cd ${WEBROOT}

exit 0
