#!/bin/bash

# -- build content --
./assetPrep.sh


# -- detect architecture --
echo -n "DETECTING RASPBERRY PI OS..."
if [ -f '/etc/rpi-issue' ];
then
  TAG='arm64-alpine'
  echo "YES ($TAG)"
  SYSTEM='Raspi'
else
  TAG='amd64-alpine'
  echo "NO ($TAG)"
  SYSTEM='Alpine'
fi

# -- build --
echo -n "BUILDING IMAGE..."
docker build -t craft4fun/pimoo:${TAG} --build-arg SYSTEM=${SYSTEM} .
echo 'OK'


# -- cleanup content --
echo -n "CLEANING ASSET CONTENT..."
rm -R asset/*
echo 'OK'


echo "PUSHING TO DOCKER HUB..."
docker push craft4fun/pimoo:${TAG}
echo 'OK'

