#!/bin/bash

# This script (shutdownDetection.sh) is required to shutdown the host system from inside the piMoo docker container.
# Requires "apt install inotify-tools" on the host machine.
# Copy it to host systems /root/ path, make it executable with chmod +x and trigger it like this after reboot:
# crontab -e
# @reboot /root/shutdownDetection.sh > /root/shutdownDetection.log 2>&1

if [ ! -x "$(command -v inotifywait)" ]; then
  echo 'inotify not found, please install with: apt install inotify-tools' >&2
  exit 1
fi

while true; do
  inotifywait -e modify /var/lib/docker/volumes/vol-pimoo/_data/PIPE/shutdown && \
  docker container stop pimoo && \
  /sbin/shutdown -h now;
  sleep 2
done
