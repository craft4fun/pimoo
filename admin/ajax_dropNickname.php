<?php
  // -- special piMoo stuff --
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once ('../lib/utils.php');


  // -- normaly $_SESSION['str_nickname'] would be enough, but this way I can delete other nicks by admin area

  // -- is admin? --
  if (!isset($_SESSION['bool_admin']) || !$_SESSION['bool_admin'] == true)
  {
    echo LNG_ERR_ONLYADMIN;
  }
  else
  {
    if (isset($_GET['nickname']))
    {
      $myNickname = new class_nickname(ONEDIRUP);
      if ($myNickname->dropForeign($_GET['nickname']))
      {
        // -- iMoo --
        $iMooPl = ONEDIRUP . $cfg_tmpPathStatic . SLASH . 'iMoo' . SLASH . $_GET['nickname'] . DOT . 'json';
        if (file_exists($iMooPl))
        {
          unlink($iMooPl);
        }
        echo LNG_SUC;
      }
      unset($myNickname);
    }
  }

