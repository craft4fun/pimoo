<?php
  require ('../defines.inc.php');
  require ('../keep/config.php');
  require ('../lib/utils.php');
  require ('../api_layout.php');
  $imprison = (isset($_SESSION['bool_imprison']) && $_SESSION['bool_imprison'] == true) ? true : false;
  echo makeHTML_begin(true, ONEDIRUP);
  includeStyleSheet();
  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  if ($cfg_mediaBaseType != MEDIABASETYPE_XML)
  {
    die('only in MEDIABASETYPE_XML mode');
  }


  require ('../lib/getid3/getid3.php');

  // -- head --
  echo '<div class="commonHead">';
    echo '<div class="commonHeadText">mediaBase importer (XML)</div>';

    if (cntIsConsole())
    {
      echo '<a style="float: right;" href="../frontend_noJS.php">back to '.PROJECT_NAME.'</a>';
    }
    else
    {
      if (!$imprison)
        echo '<a class="linkNaviNice" style="float: right;" href="../frontend.php">back to '.PROJECT_NAME.'</a>';
    }
    echo '<a class="linkNaviNice" href="backend.php">back to '.PROJECT_NAME.' (backend)</a>';
  echo '</div>';


  /**
   * importer for available music on disc
   * @author Patrick Höf
   * @todo if no ID3v1 available, take ID3v2
   *
   *
   */



  // -- ------------------------- --
  // -- beg of import (or update) --
  // -- ------------------------- --
  if (isset($_POST['btn_import']))
  {
    // -- --------------------------- --
    // -- safety backup in every case --
    if (file_exists(ONEDIRUP . $cfg_mediabaseFile))
    {
      if (!copy(ONEDIRUP . $cfg_mediabaseFile, ONEDIRUP . $cfg_mediabaseFile . UNDSCR . date('Y-m-d-G-i-s') . '.bak'))
        echo '<div style="color: red;">Could not made a backup before. Check rights for your apache (e.g. wwwrun or www-data)!</div>';
    }


    // -- ------------------------------------------ --
    // -- decission full import or time range update --
    $updateDays = NULLINT;
    $update = false;
    if (isset($_POST['sel_timeRange']) && $_POST['sel_timeRange'] != NULLINT)
    {
      $updateDays = $_POST['sel_timeRange'];
      $update = true;
    }

    // -- if no previously created mediabase file exsists, a new one has to be created in every case --
    $mediaBaseAlreadyExists = (file_exists(ONEDIRUP . $cfg_mediabaseFile)) ? true : false;

    // -- ----------------- --
    // -- init progress bar --
    include_once (ONEDIRUP . 'lib/progressbar.class.php');
    $count = cntFilecount($cfg_absPathToSongs);
    $progBar = new progressbar(0, $count, 300, 30); // -- init --
    echo '<div style="position: relative; margin-left: 2em; margin-top: 2em;">';
      echo 'found song files: '.$count;
      echo '<div style="font-weigth: bold; margin-top: 1.5em;">progress</div>';
      $progBar->print_code(); // -- if no progressbar is needed, do it without the following line --
    echo '</div>';

    if (!$update || !$mediaBaseAlreadyExists)
    {
      $mediaBase = new XMLWriter;
      $mediaBase->openURI(ONEDIRUP . $cfg_mediabaseFile);
      $mediaBase->startDocument('1.0', 'UTF-8');
      $mediaBase->startElement('mediaBase');
      $myFeedback = new class_feedback(ONEDIRUP);
      echo '<div style="position: relative; margin-left: 2em; margin-top: 3em;"><pre>';
        // -- core --
        recursive_writeMP3TagsToMediabase($cfg_absPathToSongs, true); // -- html mode --
      echo '</pre></div>';
      unset($myFeedback);
      // -- close mediabase xml document --
      $mediaBase->endElement();
      $mediaBase->endDocument();

      // -- kill the cache directory --
      // -- because it can get confusing, if old - not available songs are in --
      cntDelTree(ONEDIRUP . $cfg_cachePath);
    }
    else
    {
      echo sprintf('doing update for songs which comes in the last %d day(s)', $updateDays) . LFH . LFH;

      $mediaBaseUpdateStr = file_get_contents(ONEDIRUP.$cfg_mediabaseFile);
      $mediaBaseUpdate = simplexml_load_file(ONEDIRUP.$cfg_mediabaseFile);
      $myFeedback = new class_feedback(ONEDIRUP);
      // -- create timestamp to go back X days --
      $updateDaysUnix = mktime(date('H'), date('i'), date('s'), date('n'), date('j') - $updateDays);
      // -- core --
      recursive_updateMP3TagsToMediabase($cfg_absPathToSongs, $updateDaysUnix, true); // -- html mode --
      // -- ... write to XML file now --
      $handle = fopen(ONEDIRUP . $cfg_mediabaseFile, 'w');
      if (!fwrite($handle, $mediaBaseUpdate->asXML()))
      {
        echo 'failure while saving mediabase file' . LFH;
      }
      fclose($handle);
      unset($myFeedback);
      unset($mediaBaseUpdate);
      echo '</pre></div>';
    }
    // -- progress bar ready --
    $progBar->complete();
    unset($progBar);
  }
  // -- ------------------------- --
  // -- end of import (or update) --
  // -- ------------------------- --

  echo makeHTML_end();


  // -- kill the cache directory --
  // -- because it can get confusing, if old - not available songs are in --
  if (file_exists(ONEDIRUP.$cfg_cachePath))
    cntDelTree(ONEDIRUP.$cfg_cachePath);

