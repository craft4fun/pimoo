<?php

  /*
   * This scripts needs following lines added to suders with visudo
   *
   * == Debian (Raspi) ==
    www-data ALL=(ALL) NOPASSWD: /sbin/reboot
   *
   */

  // -- special piMoo stuff --
  require_once ('../defines.inc.php');
  require_once (ONEDIRUP.'keep/config.php');
  require_once (ONEDIRUP.'api_player.php');


  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    echo LNG_ERR_ONLYADMIN;
  }
  else
  {
    // -- shutdown player before, that's clean --
    $myPlayer = new class_player(ONEDIRUP);
    if ($myPlayer->getPlayerState() == PS_PLAYING)
      $myPlayer->cmdTurnOff();
    unset($myPlayer);

    // -- finally try to reboot the system --
    $msg = 'try to reboot the system...'.LFH;
    $msg .= (string)system ('sudo /sbin/reboot');
    echo $msg;
  }
