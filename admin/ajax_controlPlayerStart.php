<?php

  /*
   * This scripts needs following lines added to suders with visudo
   *
   * == Debian (Raspi) ==
    //old: www-data ALL=(ALL) NOPASSWD: /etc/init.d/piMoo.sh
    www-data ALL=(ALL) NOPASSWD: /var/www/html/piMoo/cli_player.php
    www-data ALL=(ALL) NOPASSWD: /var/www/html/piMoo/cli_common.php
   *
   */

  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  include_once ('../api_player.php');


  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $msg = LNG_ERR_ONLYADMIN;
  }
  else
  {
    $msg = NULLSTR;

    $myPlayer = new class_player(ONEDIRUP);

    // -- turn on HiFi via hardware - relais --
    $msg = $myPlayer->GPIO_ON();

    $msg .= 'try to start the player...<br>';

    // -- turn on the cli_player.php and cli_common.php --
    $msg .= $myPlayer->cmdTurnOn();
    unset($myPlayer);

  }

  echo $msg;
