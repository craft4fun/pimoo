<?php
  // -- special piMoo stuff --
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');

  if ($cfg_configShadow !== false)
  {
    $check = true;

    if (file_exists(ONEDIRUP.$cfg_configShadow))
    {
      $check = unlink(ONEDIRUP.$cfg_configShadow);
    }

    // -- giving feedback --
    switch ($check)
    {
      case true:
        {
          $msg = '<div style="font-weight: bold;">'.LNG_SUC.'</div>Page will be reloaded in 2s...';
          break;
        }
      default:
        {
          $msg =  '<span style="font-weight: bold;">'.LNG_ERR_UNKNOWN.'</span>';
        }
    }
  }

  echo $msg;