<?php
  // -- special piMoo stuff --
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once ('../api_player.php');
  require_once ('../api_layout.php');


  if (!isset($_GET['issue']))
  {
    echo LNG_ERR_PARAM_MISSING;
  }
  else
  {
    $playerState = class_player::static_getPlayerState(ONEDIRUP);

    if ($_GET['issue'] == ISSUE_BTNSTART)
    {
      // -- !!! this is a clone of a part of backend.php !!! --
      $btnStartState = ($playerState == PS_PLAYING || $playerState == PS_PAUSED) ? BLANK.'opacity: 0.25;'.BLANK : BLANK.'opacity: 1;'.BLANK;
      $btnStartState2 = ($playerState == PS_PLAYING || $playerState == PS_PAUSED) ? BLANK.'disabled="disabled"'.BLANK : NULLSTR;
      $result = '<input type="button" class="btnGreen btnAdminAdd" '.$btnStartState2.' style="'.$btnStartState.'" value="start '.PROJECT_NAME .' player" onclick="controlPlayerStart()" />';
    }


    if ($_GET['issue'] == ISSUE_BTNSTOP)
    {
      // -- !!! this is a clone of a part of backend.php !!! --
      $btnStopState = ($playerState != PS_PLAYING && $playerState != PS_PAUSED) ? BLANK.'opacity: 0.25;'.BLANK : BLANK.'opacity: 1;'.BLANK;
      $btnStopState2 = ($playerState != PS_PLAYING && $playerState != PS_PAUSED) ? BLANK.'disabled="disabled"'.BLANK : NULLSTR;
      $result = '<input type="button" class="btnRed btnAdminAdd" '.$btnStopState2.' style="'.$btnStopState.'" value="stop '.PROJECT_NAME.' player"  onclick="controlPlayerStop()" />';
    }

    // -- adminControlBar --
    if ($_GET['issue'] == ISSUE_ADMINCONTROLBAR)
    {
      $result = makeAdminControlBar(ONEDIRUP);
    }


    if ($_GET['issue'] == ISSUE_PLAYERSTATE)
    {
      $result = $playerState;
    }

    echo $result;
  }

