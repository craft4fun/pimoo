<?php
  // -- special piMoo stuff --
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once ('../lib/utils.php');

  if (!isset($_GET['value']))
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    // -- load current defaults and temporarily set config ... --
    // -- ... and map from 'yes' to 'true' or 'no' to 'false' --
    $allowMultiNickname = ($cfg_allowMultiNickname) ? 'true' : 'false';
    $allowMultiblePlaylistEntries = ($cfg_allowMultiblePlaylistEntries) ? 'true' : 'false';
    $partyMode = ($cfg_partyMode) ? 'true' : 'false';
    $genreWhitelist = util_coverStringsInApostrophes($_GET['value']);
    $mqtt = ($cfg_mqtt) ? 'true' : 'false';
    $mqttaddr = "'".$cfg_mqtt_broker_unified."'";
    $caching = ($cfg_cache) ? 'true' : 'false';

    // -- create shadow config with desired changes --
    createShadowConfig(ONEDIRUP,
                      $allowMultiNickname,
                      $allowMultiblePlaylistEntries,
                      $partyMode,
                      $genreWhitelist,
                      $mqtt,
                      $mqttaddr,
                      $caching);

    // -- giving feedback --
    $msg = 'Genre Whitelist'.ARROW.$genreWhitelist;
  }

  echo $msg;