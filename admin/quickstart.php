<?php
  // -- enter the piMoo with this is giving the possibility to start the piMoo immediatelly --
  // -- and optionally skip the "gimme nickname" procedure --
  // -- especcially makes sence if the piMoo is saved in a "Desktop" link on the smartphone --

  // -- Note: this hidden script is not linked in the frontend, so no normal user can't find it easily. --
  // -- However, someone could enter the adress in his browser and open it directly without giving the --
  // -- the admin password!. If you want to have your piMoo backend 100% protected, you have to setup a .htaccess file --

  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  include_once ('../lib/utils.php');
  include_once ('../api_player.php');

  // -- this keeps alive even if this page is leaved - until the admin has logged out --
  $_SESSION['bool_admin'] = true;

  // -- optionally set a nickname --
  if (isset($_GET['nick']))
  {
    $nickname = $_GET['nick'];
    $myNickname = new class_nickname(ONEDIRUP);
    if (!$myNickname->push($nickname))
    {
      include_once ('../api_layout.php');
      echo makeHTML_begin();
      includeStyleSheet();
      includeJavascript();
      echo makeHTMLHeader_end();
      echo makeHTMLBody_begin();
        // -- head --
        echo '<div class="commonHead">';
          echo '<span><a href="../index.php">back to '.PROJECT_NAME.'</a></span>';
        echo '</div>';
        // -- the CORE message --
        echo cntErrMsg(LNG_TAKE_ANOTHER_NICK);
      echo makeHTML_end();
    }
    unset($myNickname);
  }



  $msg = NULLSTR;

  // -- --------------------- --
  // -- start the piMoo player --
  $myPlayer = new class_player(ONEDIRUP);

  // -- turn on HiFi via hardware - relais --
  $msg .= $myPlayer->GPIO_ON();

  $msg .= 'try to start the player...<br>';

  // -- turn on the cli_player.php and cli_common.php --
  $msg .= $myPlayer->cmdTurnOn();
  unset($myPlayer);

  eventLog('quickstart.php: '.$msg, ONEDIRUP);

  // -- ---------------------------------------- --
  // -- if playlist is empty, take a random song --
  //include ('../api_playlist.php');
  // TODO paddy $nextSong = $myPlaylist->getNextSong();

  // for now nothing to do with $msg

  // -- ----------------------------- --
  // -- finally redirect to frontpage --
  header('location: ../index.php');
