#!/usr/bin/php
<?php

/**
 * importer for available music on disc based on mySQL
 *
 * strategy 3: Store all information in a database
 *
 * @author Patrick Höf
 * @todo if no ID3v1 available, take ID3v2
 *
 *
   * Preparations for using the database strategy
   * - Setup mySQL or MariaDB (if not already done)
   * - Create database named "piMoo"
   * - Import template "keep/mediaBase.sql"
   * - Create user "piMoo" with user "piMoo" or do something similar and make the relating changes in "keep/config.php",
   *   with this minimum permissions: SELECT,INSERT,UPDATE,DELETE
   * - have fun!
 *
 */


  require ('../defines.inc.php');
  require ('../keep/config.php');
  require ('../lib/utils.php');
  require ('../lib/getid3/getid3.php');
  require ('../api_feedback.php');

  $knownArgs = array('./cli_importerMYSQL.php', // -- this is always the first argument --
      '--help', '-h',
      '--update', '-u',
  );

  $updateDays = 7; // -- by default one week --
  $i = 0;
  $help = false;
  $update = false;
  $xo = 0;
  if (isset($_SERVER['argv']))
  {
    foreach ($_SERVER['argv'] as $arg)
    {
      if ($arg == '--help' || $arg == '-h') $help = true;
      if ($arg == '--update' || $arg == '-u')
      {
        $update = true;
        $xo = $i;
      }
      $i++;
    }
    if ($update)
    {
      if (isset($_SERVER['argv'][$xo + 1]))
        $updateDays = $_SERVER['argv'][$xo + 1];
      if (!($updateDays > 0 && $updateDays < 366))
        die("Parameter for --update must be greater than 0 and less then 366\n");
    }
  }

  if ($help)
  {
    echo "\n";
    echo "--update -> Update mysql bedia-database with latest songs.\n";
    die("\n");
  }


  if (!$update)
  {
    // -- safety warning --
    echo "Sure to clear the piMoo database and import a new one (again)?: (I need \"".YES."\")\n";
  }
  else
  {
    echo "Update the piMoo database with new music files?: (I need \"".YES."\")\n";
  }

  // -- read in answer --
  $input = trim(fgets(STDIN));
  if (!(strtolower($input) == YES))
  {
    die('FINISHED - nothing done' . LFC);
  }


  // function _sanitizeInputStr($input, $careUniqueVarChar = false)
  // {
  //   // 1
  //   $input = utils_escapeDBCriticalChars($input); # -- special issue regarding machine width inch " --
  //   // 2
  //   $input = mysqli_real_escape_string($this->mySQL, $input);  # -- Addslashes is generally not good enough when dealing with multibyte encoded strings --

  //   if ($careUniqueVarChar)
  //   {
  //     if (empty($input))
  //     {
  //       return NULL;
  //     }
  //   }

  //   return $input;
  // }


  function _getDetails($aFile)
  {
    global $mySQL;
    global $cfg_musicFileType;
    global $myFeedback;

    // -- Initialize getID3 engine --
    $getID3 = new getID3;
    $ThisFileInfo = $getID3->analyze($aFile);
    getid3_lib::CopyTagsToComments($ThisFileInfo);

    global $cfg_absPathToSongs;
    global $feedback;

    $curFile = str_replace($cfg_absPathToSongs, NULLSTR, $aFile);


    // -- check if interpret exists. If so take the id ... --
    $interpret = NULLSTR;
    if (isset($ThisFileInfo['comments_html']['artist'][0]))
    {
      //$interpret = htmlspecialchars_decode ( utf8encode( $ThisFileInfo['comments_html']['artist'][0] ) );
      $interpret = $ThisFileInfo['comments_html']['artist'][0];
    }
    // -- sanitize --
    $interpret = utils_escapeDBCriticalChars($interpret);


    $interpretID = NULLINT;
    $query = "SELECT `id` FROM `table_interpret` WHERE `interpret` = '$interpret'";
    $queryResult = $mySQL->query($query);
    if ($queryResult)
    {
      $obj = $queryResult->fetch_object();
      if (isset($obj->id))
      {
        $interpretID = $obj->id;
      }
      else
      {
        // -- ... otherwise create a database entry and take the new id --
        $query = "INSERT INTO `table_interpret` (`id`, `interpret`) VALUES (NULL, '$interpret');";
        $mySQL->query($query);
        $interpretID = $mySQL->insert_id;
      }
      $queryResult->close(); // -- free result set --
    }



    // -- check if album exists. If so take the id ... --
    $album = NULLSTR;
    if (isset($ThisFileInfo['id3v2']['album']))
    {
      $album = $ThisFileInfo['id3v2']['album'];
    }
    elseif (isset($ThisFileInfo['id3v1']['album']))
    {
      $album = $ThisFileInfo['id3v1']['album'];
    }
    // -- sanitize --
    $album = utils_escapeDBCriticalChars($album);
    // echo "sanitized album: $album";


    $albumID = NULLINT;
    $query = "SELECT `id` FROM `table_album` WHERE `album` = '$album'";
    $queryResult = $mySQL->query($query);
    if ($queryResult)
    {
      $obj = $queryResult->fetch_object();
      if (isset($obj->id))
      {
        $albumID = $obj->id;
      }
      else
      {
        // -- ... otherwise create a database entry and take the new id --
        $query = "INSERT INTO `table_album` (`id`, `album`) VALUES (NULL, '$album');";
        $mySQL->query($query);
        $albumID = $mySQL->insert_id;
      }
      $queryResult->close(); // -- free result set --
    }



    // -- check if genre exists. If so take the id ... --
    $genre = NULLSTR;
    if (isset($ThisFileInfo['comments_html']['genre'][0]))
    {
      $genre = $ThisFileInfo['comments_html']['genre'][0];
    }

    $genreID = NULLINT;
    $query = "SELECT `id` FROM `table_genre` WHERE `genre` = '$genre'";
    $queryResult = $mySQL->query($query);
    if ($queryResult)
    {
      $obj = $queryResult->fetch_object();
      if (isset($obj->id))
      {
        $genreID = $obj->id;
      }
      else
      {
        // -- ... otherwise create a database entry and take the new id --
        $query = "INSERT INTO `table_genre` (`id`, `genre`) VALUES (NULL, '$genre');";
        $mySQL->query($query);
        $genreID = $mySQL->insert_id;
      }
      $queryResult->close(); // -- free result set --
    }



    // -- --------------------- --
    // -- beg of "by" nicknames --
    $myFeedback->findSong(basename($aFile));

    /* -- this worked, but I don't need it this complicated, I take the nickname as a string --
    // -- check if user exists. If so take the id ... --
    $userRating = $myFeedback->getRatingBy();
    if ($userRating == trim(NULLSTR)) {
      $userRatingID = 'NULL'; } else {
      $userRatingID = 1;
      $query = "SELECT `id` FROM `table_user` WHERE `nickname` = '$userRating'";
      $queryResult = $mySQL->query($query);
      if ($queryResult) {
        $obj = $queryResult->fetch_object();
        if (isset($obj->id)) {
          $userRatingID = $obj->id; } else {
          // -- ... otherwise create a database entry and take the new id --
          $query = "INSERT INTO `table_user` (`id`, `nickname`) VALUES (NULL, '$userRating');";
          $mySQL->query($query); }
        $queryResult->close(); // -- free result set --
      }}*/

    // -- rating --
    $ratingBy = $myFeedback->getRatingBy();
    $rating = $myFeedback->getRating();

    // -- comment --
    $commentBy = $myFeedback->getCommentBy();
    $comment = $myFeedback->getComment();


    $myFeedback->findSongReset();
    // -- end of "by" nicknames --
    // -- --------------------- --



    // -- the absolute song path --
    $absolute = htmlspecialchars_decode($curFile);

    // -- title --
    if (isset($ThisFileInfo['comments_html']['title'][0]))
    {
      $title = htmlspecialchars_decode(utf8encode($ThisFileInfo['comments_html']['title'][0]));
    }
    else
    {
      $title = basename($aFile, $cfg_musicFileType);
    }

    // -- year --
    $year = NULLSTR;
    @$year = $ThisFileInfo['id3v2']['year'];
    if (empty($year))
    {
      @$year = $ThisFileInfo['id3v1']['year'];
    }

    // -- play time --
    $playtime = NULLSTR;
    if (isset($ThisFileInfo['playtime_string']))
    {
      $playtime = $ThisFileInfo['playtime_string'];
    }

    // -- file date --
    $filedate = NULLSTR;
    if (isset($ThisFileInfo['filenamepath']))
    {
      $filedate = date('Y-m-d', filemtime($ThisFileInfo['filenamepath']));
    }


    // -- finally the base entry --
    $query = "INSERT INTO `table_base`
             (`id`, `absolute`, `interpretID`, `title`, `albumID`, `year`, `playtime`, `genreID`, `filedate`, `rating`, `ratingBy`, `comment`, `commentBy`)
             VALUES
             (NULL, '$absolute', '$interpretID', '$title', '$albumID', '$year', '$playtime', '$genreID', '$filedate', '$rating', '$ratingBy', '$comment', '$commentBy');";
    $mySQL->query($query);

    unset($getID3);
  }




  // -- find all files --
  function dir_recursive($directory)
  {
    //global $mediaBase;
    global $cfg_musicFileType;

    $handle =  opendir($directory);
    while ($file = readdir($handle))
    {
      if ($file != DOT && $file != DDOT && substr($file, 0, 1) != DOT)
      {
        if (is_dir($directory . $file))
        {
          // -- Erneuter Funktionsaufruf, um das aktuelle Verzeichnis auszulesen --
          dir_recursive($directory . $file . SLASH);
        }
        else
        {
          if ($cfg_musicFileType == strtolower(cntGetFileExt($file)))
          {
            _getDetails($directory . $file);
            echo $file . LFC;
          }
        }
      }
    }
    closedir($handle);
  }


  // -- find all files in time range --
  function _dir_recursive_update($path, $updateDaysUnix)
  {
    global $cfg_musicFileType;
    global $mySQL;

    $handle =  opendir($path);
    while ($file = readdir($handle))
    {
      if ($file != DOT && $file != DDOT && substr($file, 0, 1) != DOT)
      {
        if (is_dir($path . $file))
        {
          // -- Erneuter Funktionsaufruf, um das aktuelle Verzeichnis auszulesen --
          _dir_recursive_update($path . $file . SLASH, $updateDaysUnix);
        }
        else
        {
          if ($cfg_musicFileType == strtolower(cntGetFileExt($file)))
          {

            if (filectime($path . $file) >= $updateDaysUnix)
            {
              //echo 'found and proofing: ' . $path . $file . LFC;
              echo 'found - check if not exists: ' . $file . LFC;

              $query = "SELECT `id` FROM `table_base` WHERE `absolute` LIKE '%$file'";
              $queryResult = $mySQL->query($query);
              if ($queryResult)
              {
                $obj = $queryResult->fetch_object();
                if (!isset($obj->id))
                {
                  echo 'not existing - adding now: ' . $file . LFC;
                  _getDetails($path . $file);
                }
              }
            }
          }
        }
      }
    }
    closedir($handle);
  }




  function mySQLConnect()
  {
    global $cfg_mySQL_host;
    global $cfg_mySQL_port;
    global $cfg_mySQL_user;
    global $cfg_mySQL_passwd;
    global $cfg_mySQL_dataBase;

    // -- ----------------- --
    // -- beg of connection --
    // -- and check if database exists at all --
    $tmpSQL = new mysqli($cfg_mySQL_host, $cfg_mySQL_user, $cfg_mySQL_passwd, $cfg_mySQL_dataBase, $cfg_mySQL_port);
    if (mysqli_connect_errno())
    {
      printf('Connect failed: ' . "%s\n", mysqli_connect_error());
      // prepared
      // $query = "CREATE DATABASE piMoo";
      // echo 'try to create database "piMoo"...: ' . $tmpSQL->query($query) . LFC;
    }

    // -- on some environments this is required - nobody knows why --
    mysqli_set_charset($tmpSQL, 'utf8');
    // -- in no case this (tested): --
    // mysqli_set_charset($this->mySQL, 'unicode');
    // -- end of connection --
    // -- ----------------- --

    return $tmpSQL;
  }


  /**
   * check database and clean tables
   * @param object $aMySQL
   * @param boolean $checkOnly
   * @return boolean
   */
  function prepareTables($aMySQL, $checkOnly = false)
  {
    $result = false;

    $check = $aMySQL->query("SHOW TABLES LIKE 'table_base'");
    if (mysqli_num_rows($check) > 0)
    {
      if (!$checkOnly)
      {
        $query = "TRUNCATE TABLE `table_base`";
        echo 'clear table_base: ' . $aMySQL->query($query) . LFC;

        $query = "TRUNCATE TABLE `table_interpret`";
        echo 'clear table_interpret: ' . $aMySQL->query($query) . LFC;

        $query = "TRUNCATE TABLE `table_album`";
        echo 'clear table_album: ' . $aMySQL->query($query) . LFC;

        $query = "TRUNCATE TABLE `table_genre`";
        echo 'clear table_genre: ' . $aMySQL->query($query) . LFC;
      }

      $result = true;
    }
    else
    {
      echo "It seems you are using a fresh database. Please setup a mysql or mariadb environment and import the 'keep/mediabase.sql' template first\n";

      //       $query = "CREATE TABLE `table_base` (
      //                   `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
      //                   `absolute` text NOT NULL ,
      //                   `interpretID` INT(11) NULL ,
      //                   `title` text NOT NULL ,
      //                   `albumID` INT(11) NULL ,
      //                   `year` varchar(4) NULL ,
      //                   `playtime` varchar(8) NULL ,
      //                   `genreID` INT(11) NULL ,
      //                   `filedate` date NULL ,
      //                   `rating` varchar(3) NULL ,
      //                   `ratingByID` INT(11) NULL ,
      //                   `comment` text NULL ,
      //                   `commentByID` INT(11) NULL ,
      //         ) ENGINE = innoDB ;
      //                 ";
      //       echo 'prepare table_base: ' . $aMySQL->query($query) . LFC;
    }
    unset($check);

    return $result;
  }



  $myFeedback = new class_feedback(ONEDIRUP);
  $mySQL = mySQLConnect();

  // -- ----------------------------- --
  // -- create new and full mediabase --
  if (!$update)
  {
    if (prepareTables($mySQL))
    {
      // -- core --
      dir_recursive($cfg_absPathToSongs);
    }
  }
  else
  {
    echo '*******************************************************' . LFC;
    echo sprintf('doing update for songs which comes in the last %d day(s)', $updateDays) . LFC;
    echo '*******************************************************' . LFC;

    if (prepareTables($mySQL, true))
    {
      $myFeedback = new class_feedback(ONEDIRUP);

      // -- create timestamp to go back X days --
      $updateDaysUnix = mktime(date('H'), date('i'), date('s'), date('n'), date('j') - $updateDays);

      // -- core --
      _dir_recursive_update($cfg_absPathToSongs, $updateDaysUnix);
    }
  }

  unset($myFeedback);


  // -- kill the cache directory --
  // -- because it can get confusing, if old - not available songs are in --
  if (file_exists(ONEDIRUP . $cfg_cachePath))
    cntDelTree(ONEDIRUP . $cfg_cachePath);

