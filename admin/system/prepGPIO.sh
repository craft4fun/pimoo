#!/bin/sh

# 2014-01-13: not required this time, because wiringPi is used




# this must be called at system startup e.g. by rc.local


GN='17';

echo "$GN" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio$GN/direction
echo "0" > /sys/class/gpio/gpio$GN/value

chmod 666 /sys/class/gpio/gpio$GN/direction
chmod 666 /sys/class/gpio/gpio$GN/value


