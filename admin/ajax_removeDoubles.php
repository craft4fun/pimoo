<?php
  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  include_once ('../lib/utils.php');
  include_once ('../api_playlist.php');


  // -- is admin? --
  if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
  {
    $myPlaylist = new class_playlist('../');
    echo $myPlaylist->removeDoubles();
    unset($myPlaylist);
  }
  else
    echo LNG_ERR_ONLYADMIN;
