#!/usr/bin/php
<?php
  require '../defines.inc.php';
  require '../keep/config.php';
  require '../lib/utils.php';
  require '../lib/getid3/getid3.php';
  require '../api_feedback.php';
  require '../api_layout.php';
  require '../lib/class_cache.php';
  //require '../api_mediabase.php';


  function _buildCache($aCacheXML)
  {
    global $cfg_cachePath;
    global $cfg_cacheHistory;
    global $cfg_maxSearchResultsPerPage;

    echo 'building cache for:' . LFC;
    $myMediaBase = new class_mediaBase(ONEDIRUP);


    if ($aCacheXML)
    {
      foreach ($aCacheXML->i as $item)
      {
        $myCache = new class_outputCache();
        $myCache->setCachePath(ONEDIRUP . $cfg_cachePath);

        $search = (string)($item->s);
        $offset = intval($item->o);
        $limit = $cfg_maxSearchResultsPerPage; //intval($item->m);
        $damerau = (isset($item->d)) ? intval($item->d) : 0;

        if ($item->t == CACHE_ITEMS_BY_SEARCH)
        {
          $tmp = $myMediaBase->getSearchResult($search, $offset, $limit, $item->d);
          $output = makeResultItems($tmp);

          $output .= '<div style="clear: both;">';
            if ($limit > 0)
            {
              if (count($tmp) > 0 && count($tmp) >= $limit)
                $output .= '<div class="resultNavi" style="float: right;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByStr(\''.$search.'\', '.intval($offset + $limit).');">'.LNG_NEXT_RESULTS.'</a></div></div>';
              if ($offset > 0)
                $output .= '<div class="resultNavi" style="float: left;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByStr(\''.$search.'\', '.intval($offset - $limit).');">'.LNG_PREVIOUS_RESULTS.'</a></div></div>';
              else
                $output .= '<div class="resultNavi" style="float: left;"></div>';
            }
          $output .= '</div>';


          $cacheString = CACHE_ITEMS_BY_SEARCH . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $limit . CACHE_RSLT_VIEWFORM . $item->v . CACHE_RSLT_LANG . $item->l . CACHE_RSLT_DAMLEV . $damerau;
          $myCache->setCacheFile($cacheString);
          echo $cacheString . LFC;
        }

        if ($item->t == CACHE_ITEMS_BY_USER)
        {
          $tmp = $myMediaBase->getSearchResultByUser($item->s, $offset, $limit);
          $output = makeResultItems($tmp);

          $output .= '<div style="clear: both;">';
          if ($limit > 0)
          {
            if (count($tmp) > 0 && count($tmp) >= $limit)
              $output .= '<div class="resultNavi" style="float: right;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$search.'\', '.intval($offset + $limit).');">'.LNG_NEXT_RESULTS.'</a></div></div>';
            if ($offset > 0)
              $output .= '<div class="resultNavi" style="float: left;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$search.'\', '.intval($offset - $limit).');">'.LNG_PREVIOUS_RESULTS.'</a></div></div>';
            else
              $output .= '<div class="resultNavi" style="float: left;"></div>';
          }
          $output .= '</div>';

          $cacheString = CACHE_ITEMS_BY_USER . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $limit . CACHE_RSLT_VIEWFORM . $item->v . CACHE_RSLT_LANG . $item->l;
          $myCache->setCacheFile($cacheString);
          echo $cacheString . LFC;
        }

        if ($item->t == CACHE_ITEMS_BY_DATE)
        {
          $days = $search;

          // -- calculate the days --
          $days2 = intval(substr($days, 1)); // -- N60 -> 60 --
          $daysAsSeconds = $days2 * 86400; // 24 * 60 * 60;
          $diff = time() - $daysAsSeconds;  // -- today minus x days --
          $past = date('Ymd', $diff);
          $today = date('Ymd', time());

          $tmp = $myMediaBase->getSongsByDate($past, $today, $offset, $limit);
          $output = makeResultItems($tmp);

          $cacheString = CACHE_ITEMS_BY_DATE . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $limit . CACHE_RSLT_VIEWFORM . $item->v . CACHE_RSLT_LANG . $item->l;
          $myCache->setCacheFile($cacheString);
          echo $cacheString . LFC;
        }

        $myCache->putCacheContent($output);
        unset($myCache);

        //unset($output); // faster without
      }
    }
    unset($myMediaBase);
  }

  // -- core --
  $cacheXML = simplexml_load_file(ONEDIRUP . $cfg_cacheHistory);
  _buildCache($cacheXML);
  unset($cacheXML);


  // -- force build cache for hitlist --
  require_once '../hitlist.php';
  $myHitlist = new class_everlastingHitlist(ONEDIRUP);
  $myHitlist->doOutput(true);
  echo CACHE_HITLIST . LFC;


