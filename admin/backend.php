<?php
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once ('../api_layout.php');
  require_once ('../lib/utils.php');
  require_once ('../api_mediabase.php');
  require_once ('../api_playlist.php');
  //require_once ('../api_feedback.php');
  require_once ('../api_player.php');

  // moved down: $_SESSION['bool_admin'] = true;

  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;
  $console = cntIsConsole();
  $imprison = (isset($_REQUEST['imprison'])) ? true : false;

  echo makeHTML_begin(true, ONEDIRUP);

  if (!$console)
  {
    includeStyleSheet(true); // -- force bright mode --
    includeJavascript(ONEDIRUP);
  }

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin(); // 'style="color: silver;"'


  // -- ------------------------------------------------------------ --
  // -- Admin protection by configured password during installation. --
  // -- This comes in addition to a manually set .htaccess password. --
  // -- ------------------------------------------------------------ --

  // -- is admin by configured password? --
  if (    $cfg_admin_passwd !== false // -- if no access restriction is desired --
       && (!isset($_SESSION['bool_admin']) || $_SESSION['bool_admin'] !== true)
    )
  {
    echo '<div style="margin-top: 1em; margin-left: 2em;"><a class="linkNaviNice" href="../index.php">'.LNG_BACK_TO_ . PROJECT_NAME.'</a></div>';

    echo '<div style="margin-top: 2em; padding: 1em;">';
    echo '<label for="backend_login" class="adminBoxLabel">login</label>';
      echo '<div id="backend_login" class="adminBoxInner">';
        if ($cfg_isDocker !== true)
          echo LNG_PROVIDE_PASSWORD_INSTALL;
        if ($cfg_isDocker === true)
          echo '<span style="color:'.COL_DOCKER.';">'.LNG_PROVIDE_PASSWORD_DOCKER.'</span>';

        echo '<div style="font-size: smaller; color: gray;">
                (Default is the name of this project)
              </div>';

        echo '<form name="FORM_BACKEND_LOGIN" method="POST" action="action_login.php">';
          echo '<input type="password" name="edt_password" value="" autofocus style="margin-top: 2em;" />';
          echo '<input type="submit" name="btn_password" value="GO" style="margin-left: 0.5em;" />';
        echo '</form>';
      echo '</div>';
    echo '</div>';
  }
  else
  {
    // -- this keeps alive even if this page is leaved - until the admin has logged out --
    $_SESSION['bool_admin'] = true;


    if (!$console)
    {
      if ($imprison)
      {
        echo makeStandards(false, ONEDIRUP); // -- it's intended to have the admin bar NOT in admin area because it's double --
      }
      else
      {
        echo makeStandards(true, ONEDIRUP);
      }
    }


    // -- head --
    echo '<div class="commonHead">';

      echo '<div class="commonHeadText">' . PROJECT_NAME . ' ('.LNG_BACKEND.')</div>';

      /* not required anymore, is now in adminControlBar
      // -- quick exit in admin mode for me :o) --
      if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
        echo '<span style="float: right; margin-top: 1.25em; font-size: smaller; ">'.cntMBL('<a onclick="return confirm(\''.LNG_SURE.'\')" style="color: red;" href="javascript:logout(\''.$_SESSION['str_nickname'].'\', \'../\')">'.LNG_LOGOUT.'</a>').'</span>';
      */

      if (!$imprison && !$isMobile)
      {
        echo '<span style="float: right; margin-top: 1.25em;">';
          echo cntMBL('<a href="../frontend_admin.php"><nobr><span style="color: red;">'.LNG_ADMIN_SPECIAL.'</span></nobr></a>');
          echo '<input type="button" id="openAdminAreaButton" style="margin-left: 0.5rem;" />';
        echo '</span>';
      }

      if (!$imprison)
      {
        echo '<div style="margin-top: 1em;"><a class="linkNaviNice" href="../index.php">'.LNG_BACK_TO_ . PROJECT_NAME.'</a></div>';
      }

      if ($imprison)
      {
        echo '<div class="watermark" style="margin-top: 1em;" title="'.LNG_ADMIN_BREAK.'">'.LNG_ADMIN_IMPRISONED.'</div>';
      }

    echo '</div>';

  ?>
  <div style="padding: 0.5em;">

    <div class="adminBoxOuter">
      <?php
        $playerState = class_player::static_getPlayerState(ONEDIRUP);
        $btnStartState = ($playerState == PS_PLAYING || $playerState == PS_PAUSED) ? BLANK . 'opacity: 0.25;' . BLANK : BLANK . 'opacity: 1;' . BLANK;
        $btnStartState2 = ($playerState == PS_PLAYING || $playerState == PS_PAUSED) ? BLANK . 'disabled="disabled"' . BLANK : NULLSTR;
        $btnStopState =  ($playerState != PS_PLAYING && $playerState != PS_PAUSED) ? BLANK . 'opacity: 0.25;' . BLANK : ' opacity: 1; ';
        $btnStopState2 = ($playerState != PS_PLAYING && $playerState != PS_PAUSED) ? BLANK . 'disabled="disabled"' . BLANK : NULLSTR;
        // -- !!! do not forget to change the clone in ajax_getPlayerState.php if modified here --
      ?>
      <label for="playlist" class="adminBoxLabel">player control (<span style="color: red;" id="target_playerState"><?php echo $playerState; ?></span>)</label>
      <div id="playlist" class="adminBoxInner">
        <div class="columnGridFlex">
          <span id="target_btnPlayerStart">
            <input type="button" class="btnGreen btnAdminAdd" <?php echo $btnStartState2; ?> style="<?php echo $btnStartState; ?>" value="start <?php echo PROJECT_NAME; ?> player" onclick="controlPlayerStart()" />
          </span>
          <span id="target_btnPlayerStop">
            <input type="button" class="btnRed btnAdminAdd"   <?php echo $btnStopState2; ?> style="<?php echo $btnStopState; ?>"  value="stop <?php echo PROJECT_NAME; ?> player"  onclick="controlPlayerStop()" />
          </span>
        </div>
      </div>
    </div>





    <div class="adminBoxOuter">
      <label for="playlist" class="adminBoxLabel"><?php echo LNG_PLAYLIST; ?></label>
      <div id="playlist" class="adminBoxInner">
        <?php
          $myPlaylist = new class_playlist(ONEDIRUP);
        ?>
        <form name="FORM_PLAYLIST" method="POST" action="action_backend.php" enctype="multipart/form-data" >

          <div class="columnGridFlex">
            <table>
              <tr>
                <td>songs in <a class="linkNice" href="<?php echo ONEDIRUP . $cfg_playlistFile; ?>" target="_blank"><?php echo LNG_QUEUE; ?></a>:</td>
                <td><span style="color: red;"><?php echo $myPlaylist->getPlaylistCount(); ?></span></td>
              </tr>
            </table>
            <div>
              <input class="uploadFile" type="file" name="playlistUploadFile" />
              <input type="submit" name="btn_goUploadPlaylist" class="btnGray btnAdminAdd" value="upload to playlist" />
            </div>
          </div>

        <?php
          if ($console)
          {
            echo '<input type="submit" name="btn_clearPlaylist" value="'.LNG_CLEAR_QUEUE.'" />';
            echo '<input type="submit" name="btn_removeDoubles" value="'.LNG_REMOVE_DOUBLES.'" />';
            echo '<input type="submit" name="btn_removeCurNotAvail" value="'.LNG_REMOVE_MISSING.'" />';
          }
          if (!$console)
          {
            echo '<div class="columnGridFlex" style="margin-top: 3em;">';
              echo '<div><input type="button" class="btnGray btnAdminAdd" onclick="clearQueue()" value="'.LNG_CLEAR_QUEUE.'" /></div>';
              echo '<div><input type="button" class="btnGray btnAdminAdd" onclick="removeDoubles()" value="'.LNG_REMOVE_DOUBLES.'" /></div>';
              echo '<div><input type="button" class="btnGray btnAdminAdd" onclick="removeCurNotAvail()" value="'.LNG_REMOVE_MISSING.'" /></div>';
            echo '</div>';
          }
        ?>
        </form>
        <?php
          unset($myPlaylist);
        ?>
      </div>
    </div>


    <div class="adminBoxOuter">
      <label for="partyReport" class="adminBoxLabel"><?php echo LNG_PARTY_REPORT; ?></label>
      <div id="partyReport" class="adminBoxInner">
        <div>
        <?php
          // -- ----------------- --
          // -- show hitlist file --
          // -- ----------------- --
          if (file_exists(ONEDIRUP.'keep/partyReport/hitlist.txt'))
          {
            echo '<div style="float: right;">
                    <a class="linkNice" href="../keep/partyReport/hitlist.txt" target="_blank">hitlist</a>
                  </div>';
          }

          // -- ------------------------------------------------- --
          // -- list all files in "../keep/partyReport/" as links --
          // -- ------------------------------------------------- --
          $files = scandir('../keep/partyReport/');
          $files = array_reverse($files); // -- change order ascending --
          $maxPrev = 10;
          $i = 0;
          foreach($files as $fl)
          {
            if (substr($fl, 0, 1) != DOT && $fl != 'hitlist.txt') // -- show no hidden file or dir like .git and not the hitlist --
            {
              if ($i < $maxPrev)
              {
                echo '<a class="linkNice" href="'.ONEDIRUP.'keep/partyReport/'.$fl.'" target="_blank">'.$fl.'</a><br>';
              }
            }
            $i++;
          }
          echo '<div id="adminPartyReportBar" onclick="openAdminPartyReport()"
                    style="text-align: center; margin-top: 1em; cursor: default; background-color: white;">
                    click here to load more!
                </div>';
          echo '<div id="adminPartyReport" style="display: none;">';
          $i = 0; // -- reset --
          foreach($files as $fl)
          {
            if (substr($fl, 0, 1) != DOT && $fl != 'hitlist.txt') // -- show no hidden file or dir like .git and not the hitlist --
            {
              if ($i >= $maxPrev)
              {
                echo '<a class="linkNice" href="'.ONEDIRUP.'keep/partyReport/'.$fl.'" target="_blank">'.$fl.'</a><br>';
              }
            }
            $i++;
          }
          echo '</div>';
        ?>
        </div>
      </div>
    </div>



    <?php
      if ($cfg_configShadow !== false)
      {
    ?>

    <div class="adminBoxOuter">
      <label for="configShadow" class="adminBoxLabel"><?php echo LNG_SHADOW_CONFIG; ?></label>
      <div id="configShadow" class="adminBoxInner">

        <div style="margin-bottom: 3em;">
          <div style="float: right; margin-left: 1em;">
            <input class="btnGray btnAdminAdd" type="button" value="reset !" onclick="resetShadowConfig()" />
          </div>
          <span style="color: brown;">Temporarily overwrite the default configuration (keep/config.php)</span>
        </div>

        <?php
          $isMQTT = ($cfg_mqtt) ? 'selected' : NULLSTR;
          echo '<div style="margin: 2em 0em 1em 0em;">';
            echo '<label for="sc_mqtt" class="textShadowBlur">MQTT</label>';
            echo '<select id="sc_mqtt" onchange="setMQTT(this)" class="adminOption">
                    <option value="'.NO.'">'.LNG_NO.'</option>
                    <option '.$isMQTT.' value="'.YES.'">'.LNG_YES.'</option>
                  </select>';
            echo '<div style="font-size: smaller; color: gray;">
                    Playlist change notification in realtime on frontend side (requires a backend service like pimoo-pcn and a mqtt broker like mosquitto).
                  </div>';
          echo '</div>';

          echo '<div style="margin: 2em 0em 1em 0em;">';
            echo '<label for="sc_mqtt_addr" class="textShadowBlur">MQTT broker address</label>';
            echo '<input id="sc_mqtt_addr" onchange="setMQTTaddr(this)" class="adminOption" value="'.$cfg_mqtt_broker_unified.'" />';
            echo '<div style="font-size: smaller; color: gray;">
                    Depending on the environment (native, docker, or remote broker) the individual address of the broker must be entered here.
                    <div style="color: orange;">
                      Change requires restart of backend service pimoo-pcn Daemon or Docker!
                    </div>
                  </div>';
          echo '</div>';

          $isCaching = ($cfg_cache) ? 'selected' : NULLSTR;
          echo '<div style="margin: 2em 0em 1em 0em;">';
            echo '<label for="sc_caching" class="textShadowBlur">Caching</label>';
            echo '<select id="sc_caching" onchange="setCaching(this)" class="adminOption">
                    <option value="'.NO.'">'.LNG_NO.'</option>
                    <option '.$isCaching.' value="'.YES.'">'.LNG_YES.'</option>
                  </select>';
          echo '</div>';

          $isSetMultiNick = ($cfg_allowMultiNickname) ? 'selected' : NULLSTR;
          echo '<div style="margin: 2em 0em 1em 0em;">';
            echo '<label for="sc_multinick" class="textShadowBlur">'.LNG_MULTIBLE_NICKNAME.'</label>';
            echo '<select id="sc_multinick" onchange="setMultiNick(this)" class="adminOption">
                    <option value="'.NO.'">'.LNG_NO.'</option>
                    <option '.$isSetMultiNick.' value="'.YES.'">'.LNG_YES.'</option>
                  </select>';
          echo '</div>';

          $isAllowMultiEntries = ($cfg_allowMultiblePlaylistEntries) ? 'selected' : NULLSTR;
          echo '<div style="margin: 2em 0em 1em 0em;">';
            echo '<label for="sc_multientries" class="textShadowBlur">'.LNG_MULTIBLE_ENTRIES.'</label>';
            echo '<select id="sc_multientries" onchange="setMultiEntries(this)" class="adminOption">
                    <option value="'.NO.'">'.LNG_NO.'</option>
                    <option '.$isAllowMultiEntries.' value="'.YES.'">'.LNG_YES.'</option>
                  </select>';
            echo '<div style="font-size: smaller; color: gray;">
                    Does not affect admins
                  </div>';
          echo '</div>';

          $isPartymode = ($cfg_partyMode) ? 'selected' : NULLSTR;
          echo '<div style="margin: 2em 0em 1em 0em;">';
            echo '<select id="sc_partymode" onchange="setPartyMode(this)" class="adminOption">
                    <option value="'.NO.'">'.LNG_NO.'</option>
                    <option '.$isPartymode.' value="'.YES.'">'.LNG_YES.'</option>
                  </select>';
            echo '<label for="sc_partymode" class="textShadowBlur">'.LNG_PARTY_MODE.'</label>';
            echo '<div class="toBlink" style="font-size: smaller; color: gray;">
                    Takes effect after player restart (red &amp; green button)!
                  </div>';
          echo '</div>';

          $genreWhitelist = implode(COMMA, $cfg_genreWhitelist);
          echo '<div style="margin: 2em 0em 1em 0em;">';
            echo '<input id="sc_genreWhitelist" onchange="setGenreWhitelist(this)" class="adminOption" value="'.$genreWhitelist.'" />';
            echo '<label for="sc_genreWhitelist" class="textShadowBlur">'.LNG_GENRE_WHITELIST.'</label>';

            echo '<div style="font-size: smaller; color: gray;">
                    Case sensitive!<div class="toBlink">Takes effect after player restart (red &amp; green button)!</div>
                  </div>';

            echo '<div style="min-height: 4rem; font-size: smaller; color: tomato; margin-top: 3rem;">
                    <div style="float: left; max-width: 60%;">
                      Typical genres you might want to add during party:
                    </div>
                    <select onchange="addToOverlayWhitelist(this.value)" style="float: right; color: tomato;">

                      <option value="'.DASH.'">'.DASH.'</option>
                      <option value="'.NULLINT.'">'.DASH.BLANK.LNG_CLEAR.BLANK.DASH.'</option>
                      <option value="'.ONEINT.'">'.DASH.BLANK.LNG_APPLY.BLANK.DASH.'</option>';

                      foreach ($cfg_genreWhitelistTemplate as $genre)
                      {
                        echo '<option value="'.$genre.'">'.$genre.'</option>';
                      }

               echo '</select>
                  </div>';

        echo '</div>';


        ?>
      </div>
    </div>

    <?php
      } // -- end of: if ($cfg_configShadow !== false) --
    ?>


    <?php
      $dockerHint = ($cfg_isDocker) ? '...inside Docker' : NULLSTR;
    ?>
    <div class="adminBoxOuter">
      <label for="system" class="adminBoxLabel">
        system (<span style="color: red;"><?php echo $cfg_system.$dockerHint; ?></span>)
        <span style="float: right; margin-right: 1em;">alsactrl (<span style="color: red;"><?php echo $cfg_alsacontrol; ?></span>)</span>
      </label>
      <div id="system" class="adminBoxInner">

        <?php
          $dockerCheck = NULLSTR;
          $dockerCheckCSS = NULLSTR;
          if ($cfg_isDocker === true)
          {
            $dockerCheck = 'disabled title="disabled in Docker environment"';
            $dockerCheckCSS = 'dockerDisabled';
          }
        ?>
        <span style="float: right; margin-left: 1em;">
          <input class="btnRed btnAdminAdd <?php /*echo $dockerCheckCSS;*/ ?>" type="button" value="!!! shutdown !!!" onclick="systemShutdown()" <?php /*echo $dockerCheck;*/ ?> />
        </span>
        <span style="float: right;">
          <input class="btnYellow btnAdminAdd <?php echo $dockerCheckCSS; ?>" type="button" value="reboot"           onclick="systemReboot()"   <?php echo $dockerCheck; ?> />
        </span>

        <div style="margin-bottom: 1em;"><a class="linkNice" href="showConfig.php">show default config</a></div>
        <?php // style="float: right;"
          if (file_exists(ONEDIRUP.$cfg_logFile))
            echo '<a class="linkNice" href="'.ONEDIRUP.$cfg_logFile.'" target="_blank">event log</a>';
        ?>

        <div style="margin-top: 2em;">
          <?php
            require_once ONEDIRUP . 'lib/gauges.php';
            $myProgBar = new class_progressbar();
            $myProgBar->setLabelStyle('font-size: smaller;');
            $myProgBar->setAlarmMinValue(0);
            //$myProgBar->setZoom(0.9);
            $myProgBar->setDimX(480); // 640
            $myProgBar->setDimY(32);
            $myProgBar->setvalueFontSize('xx-large');

            // -- drive --
            $myProgBar->setLabel('Drive usage for: ' . $cfg_absPathToSongs);
            echo $myProgBar->HTMLexec(cntGetDriveUsageInPercent($cfg_absPathToSongs));
            // -- RAM --
            $myProgBar->setLabel('RAM usage (snapshot)');
            echo $myProgBar->HTMLexec(round(cntGetServerMemoryUsage()));
            // -- CPU usage --
            //$myProgBar->setLabel('CPU usage (snapshot)');
            //echo $myProgBar->HTMLexec(round(cntGetServerCPU_usage()));
            // -- CPU temperature --
            $myProgBar->setLabel('CPU temperature (snapshot)');
            $myProgBar->setMaxValue(RASPI_CPU_MAX_TEMP);
            $myProgBar->setCaptionSuffix('°C');
            $myProgBar->setAlarmMaxValue(50);
            echo $myProgBar->HTMLexec(round(cntGetCPU_temperature()));

            unset($myProgBar);
          ?>
        </div>

      </div>
    </div>



    <div class="adminBoxOuter">
      <label for="mediabase" class="adminBoxLabel"><?php echo LNG_MEDIABASE; ?> (<span style="color: red;"><?php echo $cfg_mediaBaseType; ?></span>)</label>
      <div id="mediabase" class="adminBoxInner">

      <?php
        // -- ---------- --
        // -- music pool --
        $check = (is_dir($cfg_absPathToSongs)) ? NULLSTR : LNG_ERR_PATH;
        echo '<div>'.LNG_MUSIC_POOL.': <span style="color: red;">' . $cfg_absPathToSongs . BLANK . $check.'</span></div>';

        // -- ---------- --
        // -- media base --
        $myMediabase = new class_mediaBase(ONEDIRUP);
        $totalSongCount = $myMediabase->getTotalSongCount();
        if ($cfg_mediaBaseType == MEDIABASETYPE_XML)
        {
      ?>

        <div><?php echo LNG_TOT_SONGS_AVAIL; ?>: <span style="color: red; font-weight: bold; margin-top: 0.5em;"><?php echo $totalSongCount; ?></span></div>

        <div style="margin-top: 3em; padding-bottom: 1em;">
          <form name="FORM_IMPORTER" method="POST" action="action_importer.php">
          <input type="submit" class="btnBlue btnAdminAdd" name="btn_import" value="(re) import mediabase" onclick="return confirm('<?php echo LNG_SURE; ?>')" />
            <div style="float: right;">
              <select name="sel_timeRange" class="adminOption">
              <?php
                // -- offer update (re-import) only if there are already songs --
                if (!empty($totalSongCount) && $totalSongCount != QUESTMARK)
                {
              ?>
                <option value="1">one day</option>
                <option value="7">one week</option>
                <option value="31">one month</option>
              <?php
                }
              ?>
                <option value="0">new!</option>
              </select>
            </div>
            <div style="float: right; margin-top: 0.25em; font-size: smaller; padding: 0 0.25em; word-wrap: break-word; width: 12.5em;">
              limit time range or create new
            </div>
          </form>
        </div>
        <?php
          }

          if ($cfg_mediaBaseType == MEDIABASETYPE_MYSQL)
          {
            echo '<div>'.LNG_TOT_SONGS_AVAIL.': <span style="color: red; font-weight: bold;">'.$totalSongCount.'</span></div>';
            echo '<div style="margin-top:0.5em;">Using database on host: <span style="color: red;"><span class="linkNaviNice"><a target="_blank" href="http://'.$cfg_mySQL_host.'/phpmyadmin">'.$cfg_mySQL_host.'</a></span></span>, Port: '.$cfg_mySQL_port.'</div>';
          }
          unset($myMediabase);
        ?>
      </div>
    </div>



    <div class="adminBoxOuter">
      <label for="feedback" class="adminBoxLabel">feedback</label>
      <div id="feedback" class="adminBoxInner" style="min-height: 5em;">
        <?php
          $myFeedback = new class_feedback(ONEDIRUP);
        ?>
        <a class="linkNice" href="<?php echo ONEDIRUP . $cfg_feedbackFile; ?>" target="_blank"><?php echo LNG_VALUED_SONGS; ?>:</a>
        <span style="color: red;"><?php echo $myFeedback->getAllValued(); ?></span>
        <?php
          unset($myFeedback);
        ?>
        <div style="float: right;">
          <form name="FORM_FEEDBACK" method="POST" action="action_backend.php" enctype="multipart/form-data" >
            <input type="submit" name="btn_goCleanFeedbackXML" value="clean feedback.xml" class="btnRed btnAdminAdd" onclick="return confirm('<?php echo LNG_SURE; ?>')" />
          </form>
          <div style="margin-top: 0.25em; font-size: smaller; padding: 0 0.25em; word-wrap: break-word; width: 12.5em;">
            <?php
              if ($cfg_mediaBaseType == MEDIABASETYPE_MYSQL)
                echo '<div style="color: red;">WARNING: Not stable in mysql mode!!!</div>';
            ?>
            Clean feedback depending on current mediabase!
          </div>
        </div>
      </div>
    </div>


    <div class="adminBoxOuter">
      <label for="nicknames" class="adminBoxLabel"><?php echo LNG_TAKEN_NICKNAMES; ?> (who's logged in)</label>
      <div id="nicknames" class="adminBoxInner">
        <div>
        <?php
          $nicknames = ONEDIRUP.$cfg_tmpPath.DIR_NICKNAMES;
          if (is_dir($nicknames))
          {
            //echo '<a style="float: right;" href="../'.$cfg_nicknameFile.'" target="_blank">error log</a>';
            echo '<pre>';
              $files = scandir($nicknames);
              //$files = array_reverse($files); // -- change order ascending --
              foreach($files as $nicks)
              {
                if (substr($nicks, 0, 1) != DOT) // -- show no hidden file or dir like .git --
                {
                  if (isset($_SESSION['str_nickname']) && $nicks != $_SESSION['str_nickname'])
                    echo '<div id="nickID'.$nicks.'">'.cntMBL('<a class="linkNice" href="javascript:dropNickname(\''.$nicks.'\')" onclick="return confirm(\'drop '.$nicks.' ?\')">'.$nicks.'</a>').ARROW.file_get_contents(ONEDIRUP.$cfg_tmpPath.DIR_NICKNAMES. $nicks).'</div>';
                  else
                    echo '<div id="nickID'.$nicks.'">'.cntMBL('<a style="color: red;" class="linkNice">'.$nicks.'</a>').ARROW.file_get_contents(ONEDIRUP.$cfg_tmpPath.DIR_NICKNAMES.$nicks).'</div>';
                }
              }
            echo '</pre>';
          }
        ?>
        </div>
      </div>
    </div>



    <div class="adminBoxOuter">
      <label for="network" class="adminBoxLabel">network</label>
      <div id="network" class="adminBoxInner">

        <div style="float: right;">
          <label for="client" style="font-style: italic;">Client</label>
          <div id="client">
            <?php
              $ip = $_SERVER['REMOTE_ADDR'];
              $host = gethostbyaddr($ip);
              echo 'IP address: <span style="color: red;">'.$ip.'</span><br/>';
              echo 'Hostname: <span style="color: red;">'.$host.'</span>';
            ?>
          </div>
        </div>

        <label for="server" style="font-style: italic;">Server</label>
        <div id="client">
          <?php
            $ip = $_SERVER['SERVER_ADDR'];
            $host = gethostbyaddr($ip);
            echo 'IP address: <span style="color: red;">'.$ip.'</span><br/>';
            echo 'Hostname: <span style="color: red;">'.$host.'</span>';
          ?>
        </div>

      </div>
    </div>



    <div class="adminBoxOuter">
      <label for="cache" class="adminBoxLabel">caching <span style="color: red;"><?php echo ($cfg_cache) ? ON : OFF; ?></span></label>
      <div id="cache" class="adminBoxInner">
        <form name="FORM_CACHE" method="POST" action="action_backend.php">
          <div class="columnGridFlex">
            <input type="submit" name="btn_clearCache" value="clear cache" class="btnGray btnAdminAdd" onclick="return confirm('<?php echo LNG_SURE; ?>')" />
            <?php
              if ($cfg_cache)
                echo '<div><input type="submit" name="btn_buildCache" value="build cache" class="btnGray btnAdminAdd" /></div>';
              else
                echo '<div><input type="button" name="btn_buildCache" value="build cache" class="btnGray btnAdminAdd" style="text-decoration: line-through;" disabled="disabled" /></div>';
            ?>
          </div>
        </form>
      </div>
    </div>



    <div class="adminBoxOuter">
      <label for="development" class="adminBoxLabel"><?php echo LNG_DEVELOPMENT; ?></label>
      <div id="development" class="adminBoxInner">
        <div>
          <!-- disable automatic playlist refresh -->
          <div style="margin: 2em 0em 1em 0em;">
            <label for="noPlaylistRefresh" class="textShadowBlur"><?php echo LNG_NO_PLAYLLIST_REFRESH; ?></label>
            <select id="sel_noPlaylistRefresh" onchange="setNoPlaylistRefresh(this.value)" class="adminOption">
              <option value="<?php echo NO; ?>"><?php echo LNG_NO; ?></option>
              <option value="<?php echo YES; ?>"><?php echo LNG_YES; ?></option>
            </select>
            <div style="color: gray; margin-top: 0.5em;">Disable automatic playlist refresh for debugging.</div>
          </div>

          <form name="FORM_DEVELOPMENT" method="POST" action="action_backend.php" enctype="multipart/form-data" >
            <input type="submit" name="btn_devel" value="devel" class="btnBlue btnAdminAdd" />
          </form>


        </div>
      </div>
    </div>

  </div>
<?php
  }
  echo makeBottom(ONEDIRUP);
  echo makeHTML_end();
?>
<script>
  function initNoPlaylistRefresh()
  {
    let sel_noPlaylistRefresh = $("sel_noPlaylistRefresh");
    let bool_noPlaylistRefresh = localStorage.getItem("bool_noPlaylistRefresh");
    if (bool_noPlaylistRefresh == '<?php echo NO; ?>' || bool_noPlaylistRefresh == '<?php echo YES; ?>')
    {
      sel_noPlaylistRefresh.value = bool_noPlaylistRefresh;
    }
  }
  dojo.ready(initNoPlaylistRefresh);
</script>

