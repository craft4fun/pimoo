<?php
  /**
   * clean feeedback.xml (creates a new cleaned one) depending on mediabase.xml
   *
   */

  class class_feedbackCleaner
  {
    private static function _stat_createNew($aFile)
    {
      $newfile = fopen($aFile, 'w');
      fwrite($newfile, '<?xml version="1.0" encoding="UTF-8"?>'.LFC.'<feedback></feedback>');
      fclose($newfile);
      unset($newfile);
    }

    private static function _stat_makeHumanXML($aXMLFile)
    {
       $result = false;
       $dom = new DomDocument();
       $dom->formatOutput = true;
       $dom->preserveWhiteSpace = false;
       $dom->load($aXMLFile);
       if ($dom->save($aXMLFile))
         $result = true;
       return $result;
    }





    static function stat_cleanFeedback()
    {
      //include_once (ONEDIRUP.'lib/getid3/getid3.php');

      global $cfg_feedbackFile;
      global $cfg_mediabaseFile;

      $feedbackFileNew = dirname($cfg_feedbackFile).SLASH.'cleanedFeedback.xml';

      $myFeedback = new class_feedback(ONEDIRUP);
      $myMediabase = new class_mediaBase(ONEDIRUP);


      // -- backup the original feedback before touching something --
      if (!copy(ONEDIRUP . $cfg_feedbackFile, ONEDIRUP . $cfg_feedbackFile . UNDSCR . date('Y-m-d_G-i-s') . '.bak'))
        echo 'could not made a backup of feedback.xml before! Why that?<br>';

      include_once (ONEDIRUP.'lib/progressbar.class.php');
      $count = $myFeedback->getCount();
      $progBar = new progressbar(0, $count, 300, 30); // -- init --
      echo '<div style="position: relative; margin-left: 2em; margin-top: 2em; margin-bottom: 1em;">';
        //echo 'found "feedbacks" in '.VALUEOPERATOR_INI.': ' . $count;
        echo '<div style="font-weigth: bold; margin-top: 15px;">progress</div>';
        $progBar->print_code(); // -- if no progressbar is needed, do it without the following line --
      echo '</div>';

      echo '<div style="margin: 1em;">';

        $cleanedFeedback = new XMLWriter;

        if (!file_exists(ONEDIRUP . $feedbackFileNew))
          self::_stat_createNew(ONEDIRUP . $feedbackFileNew);

        $cleanedFeedback->openURI(ONEDIRUP . $feedbackFileNew);
        $cleanedFeedback->startDocument('1.0', 'UTF-8');
        $cleanedFeedback->startElement('feedback');

        echo 'Songs currently available in feedback: <span style="color: red">'.$count.'</span>'.LFH.LFH;
        echo 'All songs which cannot be found in mediabase will be listed here:'.LFH.LFH;

        $attr = XML_BY;
        $c = NULLINT;

        // -- --------- --
        // -- core loop --
        foreach ($myFeedback->xml->i as $song)
        {
          //$check = $myMediabase->findSong($song->a);
          $check = $myMediabase->getSongInfo($song->a, true);
          if (false === $check)
          {
            echo $song->a . LFH; // 'NOT FOUND: ' .
            $c++;
          }
          else
          {
            $cleanedFeedback->startElement(XML_STRDITEM);

              // -- the absolute song reference --
              $cleanedFeedback->startElement('a');
                $cleanedFeedback->text( htmlspecialchars_decode( $song->a ) ); // best so far!
              $cleanedFeedback->endElement();

              // -- rating --
              $cleanedFeedback->startElement('r');

                $cleanedFeedback->startAttribute(XML_BY);
                  $cleanedFeedback->text($song->r->attributes()->$attr);
                $cleanedFeedback->endAttribute();

                $cleanedFeedback->text($song->r);
              $cleanedFeedback->endElement();

              // -- comment --
              $cleanedFeedback->startElement('c');

                $cleanedFeedback->startAttribute(XML_BY);
                  $cleanedFeedback->text($song->c->attributes()->$attr);
                $cleanedFeedback->endAttribute();

                $cleanedFeedback->text($song->c);
              $cleanedFeedback->endElement();


            $cleanedFeedback->endElement();
          }

          $progBar->step();
        }

        $cleanedFeedback->endElement();
        $cleanedFeedback->endDocument();


//         // -- ... write to XML file now --
//         $handleNew = fopen(ONEDIRUP . $feedbackFileNew, 'w');
//         if (!fwrite($handleNew, $cleanedFeedback->asXML()))
//           echo 'failure while saving new feedback file' . LFC;
//         fclose($handleNew);


        self::_stat_makeHumanXML(ONEDIRUP.$feedbackFileNew);

        echo LFH.'Songs still available in feedback: <span style="color: red">'.($count-$c).'</span>'.LFH;


        if (!copy(ONEDIRUP . $feedbackFileNew, ONEDIRUP . $cfg_feedbackFile))
          echo 'Could not copy cleaned file to original one! Why that?<br>';
        else
        {
          if (!unlink(ONEDIRUP . $feedbackFileNew))
            echo 'Could not unlink cleaned temp file! Why that?<br>';
        }

        unset($progBar);

      echo '</div>';
    }
  }

