<?php
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  include_once ('../lib/utils.php');
  include_once ('../api_layout.php');
  $imprison = (isset($_SESSION['bool_imprison']) && $_SESSION['bool_imprison'] == true) ? true : false;
  echo makeHTML_begin(true, ONEDIRUP);
  includeStyleSheet();
  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();


  require_once ('../api_playlist.php');
  require_once ('../api_feedback.php');

  // moved inside the caller function require_once ('valueOperator.php');

  require_once ('../api_mediabase.php');
  require_once ('../lib/class_cache.php');


  // -- head --
  echo '<div class="commonHead">';
    echo '<div class="commonHeadText">'.PROJECT_NAME.' ('.LNG_BACKEND.') action</div>';
    if (!$imprison)
      echo '<a class="linkNaviNice" style="float: right;" href="../frontend.php">back to '.PROJECT_NAME.'</a>';
    echo '<a class="linkNaviNice" href="backend.php">back to '.PROJECT_NAME.' (backend)</a>';
  echo '</div>';


  if (!(isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true))
  {
    $report = LNG_ERR_ONLYADMIN;
  }
  else
  {
    $report = LFH;


    // -- clean feeedback.xml (creates a new cleaned one) depending on mediabase.xml --
    if (isset($_POST['btn_goCleanFeedbackXML']))
    {
      include_once ('feedbackCleaner.php');
      $report .= class_feedbackCleaner::stat_cleanFeedback() . LFH;
    }



    // -- clear the current playlist --
    if (isset($_POST['btn_clearPlaylist']))
    {
      $myPlaylist = new class_playlist(ONEDIRUP);
      $report .= $myPlaylist->clearPlaylist();
      unset($myPlaylist);
    }

    // -- dedupe playlist --
    if (isset($_POST['btn_removeDoubles']))
    {
      $myPlaylist = new class_playlist(ONEDIRUP);
      $report .= $myPlaylist->removeDoubles();
      unset($myPlaylist);
    }

    // -- remove songs from playlist which are currently not available in the filesystem --
    if (isset($_POST['btn_removeCurNotAvail']))
    {
      $myPlaylist = new class_playlist(ONEDIRUP);
      $report .= $myPlaylist->removeCurNotAvailInFilesys();
      unset($myPlaylist);
    }



    // -- upload and add a playlist --
    $uploadDir = ONEDIRUP . dirname($cfg_playlistFile) . SLASH;
    if (isset($_POST['btn_goUploadPlaylist']))
    {
      $uploadFile = $uploadDir . basename($cfg_playlistFile);
      if (move_uploaded_file($_FILES['playlistUploadFile']['tmp_name'], $uploadFile . 'tmp'))
      {
        $report .= 'upload succeeded' . LFH;
        $myPlaylist = new class_playlist(ONEDIRUP);

        $uploadedPlaylist = simplexml_load_file($uploadFile . 'tmp');
        if ($uploadedPlaylist)
        {
          foreach ($uploadedPlaylist->item as $add)
          {
            $myPlaylist->setWishedBy($add->w);
            $myPlaylist->addSong($add->a, $add->i, $add->t, $add->p, $add->l, true);
            eventLog($myPlaylist->getLastMsg() . COLON . BLANK .  $add->t, ONEDIRUP);
            $report .= $myPlaylist->getLastMsg() . COLON . BLANK .  $add->t . LFH;
          }

        }

        unset($myPlaylist);
        unset($uploadedPlaylist);
        unlink($uploadFile . 'tmp');
      }
      else
        $report .= 'upload failed' . LFH;

    }


    // -- cleaning up cache --
    if (isset($_POST['btn_clearCache']))
    {
      cntDelTree(ONEDIRUP . $cfg_cachePath);
      $report .= 'cleared cache' . LFH;
    }

    // -- beg of build cache --
    // -- only for the statics, search results and feedback results are cached in common way only for a few minutes --
    if (isset($_POST['btn_buildCache']))
    {
      function _buildCache($aCacheXML, $aProgBar)
      {
        global $cfg_cachePath;
        global $cfg_cacheHistory;
        global $cfg_maxSearchResultsPerPage;

        $result = NULLSTR;
        $myMediaBase = new class_mediaBase(ONEDIRUP);
        if ($aCacheXML)
        {
          foreach ($aCacheXML->i as $item)
          {
            $myCache = new class_outputCache();
            $myCache->setCachePath(ONEDIRUP . $cfg_cachePath);

            $search = (string)($item->s);
            $offset = intval($item->o);
            $limit = $cfg_maxSearchResultsPerPage; //intval($item->m);
            $damerau = (isset($item->d)) ? intval($item->d) : 0;

            if ($item->t == CACHE_ITEMS_BY_SEARCH)
            {
              $tmp = $myMediaBase->getSearchResult($search, $offset, $limit, $item->d);
              $output = makeResultItems($tmp);

              $output .= '<div style="clear: both;">';
                if ($limit > 0)
                {
                  if (count($tmp) > 0 && count($tmp) >= $limit)
                    $output .= '<div class="resultNavi" style="float: right;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByStr(\''.$search.'\', '.intval($offset + $limit).');">'.LNG_NEXT_RESULTS.'</a></div></div>';
                  if ($offset > 0)
                    $output .= '<div class="resultNavi" style="float: left;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByStr(\''.$search.'\', '.intval($offset - $limit).');">'.LNG_PREVIOUS_RESULTS.'</a></div></div>';
                  else
                    $output .= '<div class="resultNavi" style="float: left;"></div>';
                }
              $output .= '</div>';


              $cacheString = CACHE_ITEMS_BY_SEARCH . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $limit . CACHE_RSLT_VIEWFORM . $item->v . CACHE_RSLT_LANG . $item->l . CACHE_RSLT_DAMLEV . $damerau;
              $myCache->setCacheFile($cacheString);
              $result .= $cacheString . LFH;
            }

            if ($item->t == CACHE_ITEMS_BY_USER)
            {
              $tmp = $myMediaBase->getSearchResultByUser($item->s, $offset, $limit);
              $output = makeResultItems($tmp);

              $output .= '<div style="clear: both;">';
              if ($limit > 0)
              {
                if (count($tmp) > 0 && count($tmp) >= $limit)
                  $output .= '<div class="resultNavi" style="float: right;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$search.'\', '.intval($offset + $limit).');">'.LNG_NEXT_RESULTS.'</a></div></div>';
                if ($offset > 0)
                  $output .= '<div class="resultNavi" style="float: left;"><div class="grpLstItmL"><a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callSearchByUser(\''.$search.'\', '.intval($offset - $limit).');">'.LNG_PREVIOUS_RESULTS.'</a></div></div>';
                else
                  $output .= '<div class="resultNavi" style="float: left;"></div>';
              }
              $output .= '</div>';

              $cacheString = CACHE_ITEMS_BY_USER . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $limit . CACHE_RSLT_VIEWFORM . $item->v . CACHE_RSLT_LANG . $item->l;
              $myCache->setCacheFile($cacheString);
              $result .= $cacheString . LFH;
            }

            if ($item->t == CACHE_ITEMS_BY_DATE)
            {
              $days = $search;

              // -- calculate the days --
              $days2 = intval(substr($days, 1)); // -- N60 -> 60 --
              $daysAsSeconds = $days2 * 86400; // 24 * 60 * 60;
              $diff = time() - $daysAsSeconds;  // -- today minus x days --
              $past = date('Ymd', $diff);
              $today = date('Ymd', time());

              $tmp = $myMediaBase->getSongsByDate($past, $today, $offset, $limit);
              $output = makeResultItems($tmp);

              $cacheString = CACHE_ITEMS_BY_DATE . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $limit . CACHE_RSLT_VIEWFORM . $item->v . CACHE_RSLT_LANG . $item->l;
              $myCache->setCacheFile($cacheString);
              $result .= $cacheString . LFH;
            }

            $myCache->putCacheContent($output);
            unset($myCache);

            //unset($output); // faster without
            $aProgBar->step();
          }
        }

        unset($myMediaBase);
        return $result;
      }

      include_once (ONEDIRUP . 'lib/progressbar.class.php');

      $cacheXML = simplexml_load_file(ONEDIRUP . $cfg_cacheHistory);
      $count = count($cacheXML->i) + 1; // -- one for hitlist! --
      $progBar = new progressbar(0, $count, 300, 30); // -- init --

      echo '<div style="font-weight: bold;">built cache for: </div>';

      echo '<div style="position: relative; margin-left: 2em; margin-top: 2em;">';
        echo 'caches to build: ' . $count;
        $progBar->print_code(); // -- if no progressbar is needed, do it without the following line --
      echo '</div>';

      // -- force build cache for hitlist --
      require_once '../hitlist.php';
      $myHitlist = new class_everlastingHitlist(ONEDIRUP);
      $myHitlist->doOutput(true);
      $report .= CACHE_HITLIST . LFH.LFH;


      // -- core --
      $report .= _buildCache($cacheXML, $progBar);

      $progBar->complete(); // -- ready --
      unset($progBar);
      unset($cacheXML);
    }
    // -- end of build cache --



    // -- do some development stuff --
    if (isset($_POST['btn_devel']))
    {
      $report .= 'get random song'.LFH;
      $myMediaBase = new class_mediaBase(ONEDIRUP);
      $song = $myMediaBase->getRandomSong();
      var_dump($song);
    }

  }


?>
<div style="position: relative; margin-left: 2em; margin-top: 3em;">
  <pre>
    <?php
      echo $report;
    ?>
  </pre>
</div>

<?php
  echo makeHTML_end();
