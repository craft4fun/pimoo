<?php
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once ('../api_layout.php');

  require_once ('../lib/utils.php');


  $_SESSION['bool_admin'] = true;


  echo makeHTML_begin(true, ONEDIRUP);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();


  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">' . PROJECT_NAME . ' (backend)</div>';

    echo '<span style="float: right;"><a class="linkNaviNice" href="../index.php">back to '.PROJECT_NAME.'</a></span>';
    echo '<span><a class="linkNaviNice" href="backend.php">back to '.PROJECT_NAME.' (backend)</a></span>';

  echo '</div>';

  $fontSize = (cntIsMobile()) ? 'font-size: 24px;' : NULLSTR;
?>
<div style="padding: 1em; <?php echo $fontSize; ?>">
  <pre>
    <?php
      //echo file_get_contents('http://localhost/piMoo/keep/config.php'); // -- no output of course --
      //echo file_get_contents(ONEDIRUP.DIR_KEEP.'onfig.php'); // '../keep/config.php'

      copy (ONEDIRUP.DIR_KEEP.'config.php', ONEDIRUP.$cfg_tmpPath.'config');
      // -- cut disturbing php open tag by offset = 5  --
      echo file_get_contents(ONEDIRUP.$cfg_tmpPath.'config', false, NULL, 5);
    ?>
  </pre>
</div>