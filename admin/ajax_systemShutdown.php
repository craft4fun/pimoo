<?php

  /**
   * This scripts needs following lines added to suders with visudo in native environment
   * or a host pipe in Docker environment.
   *
   * == Debian (Raspi) ==
   * www-data ALL=(ALL) NOPASSWD: /sbin/reboot
   *
   * == Docker ==
   * host-pipe with inotify
   *
  */

  // -- special piMoo stuff --
  require_once ('../defines.inc.php');
  require_once (ONEDIRUP.'keep/config.php');
  require_once (ONEDIRUP.'lib/utils.php');
  require_once (ONEDIRUP.'api_player.php');

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    echo LNG_ERR_ONLYADMIN;
  }
  else
  {
    $msg = NULLSTR;

    // -- stop player before, that's clean --
    $myPlayer = new class_player(ONEDIRUP);
    if ($myPlayer->getPlayerState() == PS_PLAYING)
      $msg .= $myPlayer->cmdTurnOff();

    // -- try to shutdown finally --
    $msg .= util_systemShutdown(ONEDIRUP);

    echo $msg;
  }
