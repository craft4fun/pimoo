#!/usr/bin/php
<?php

  /**
   * importer for available music on disc based on XML
   *
   * strategy 1: have an entry with all containing information in one dataset
   *
   * @author Patrick Höf
   * TODO paddy if no ID3v1 available, take ID3v2
   *
   *
   */

  require ('../defines.inc.php');
  require ('../keep/config.php');
  require ('../lib/utils.php');
  require ('../lib/getid3/getid3.php');
  require ('../api_feedback.php');


  $knownArgs = array('./cli_importerXML.php', // -- this is always the first argument --
    '--help', '-h',
    '--update', '-u',
  );

  $updateDays = 7; // -- by default one week --
  $i = 0;
  $help = false;
  $update = false;
  $xo = 0;
  if (isset($_SERVER['argv']))
  {
    foreach ($_SERVER['argv'] as $arg)
    {
      if ($arg == '--help' || $arg == '-h') $help = true;
      if ($arg == '--update' || $arg == '-u')
      {
        $update = true;
        $xo = $i;
      }
      $i++;
    }
    if ($update)
    {
      if (isset($_SERVER['argv'][$xo + 1]))
        $updateDays = $_SERVER['argv'][$xo + 1];
      if (!($updateDays > 0 && $updateDays < 366))
        die("Parameter for --update must be greater than 0 and less then 366\n");
    }
  }

  if ($help)
  {
    echo "\n";
    echo "--update -> Update mediabase with latest songs.\n";
    die("\n");
  }


  // -- TODO paddy tricky: the check for valid argument --
  //   if (
  //            ( $i == 1 && !in_array($arg, $knownArgs) )                     // -- for the first --
  //         || ( $i >= 1 && !in_array($arg, $knownArgs) && !($i == $xo + 1) ) // -- all the rest --
  //       )
  //   die ("ABORTED - unknown argument: '$arg'\n");



  // -- make backup before --
  if (file_exists(ONEDIRUP . $cfg_mediabaseFile))
  {
    if (!copy(ONEDIRUP . $cfg_mediabaseFile, ONEDIRUP . $cfg_mediabaseFile . UNDSCR . date('Y-m-d-G-i-s') . '.bak'))
    {
      echo LFC . 'could not made a backup before. Check your rights!' . LFC . LFC;
    }
  }


  // -- ----------------------------- --
  // -- create new and full mediabase --
  if (!$update)
  {
    $mediaBase = new XMLWriter;
    $mediaBase->openURI(ONEDIRUP . $cfg_mediabaseFile);
    $mediaBase->startDocument('1.0', 'UTF-8');
    $mediaBase->startElement('mediaBase');

    $myFeedback = new class_feedback(ONEDIRUP);

    // -- core --
    $mp3FileCount = cntFilecount($cfg_absPathToSongs);
    require_once ONEDIRUP.'lib/class_cli_progressbar.php';
    $cliProgBar = new cli_ProgressBar($mp3FileCount);
    recursive_writeMP3TagsToMediabase($cfg_absPathToSongs, false); // -- cli mode --
    $cliProgBar->finish();

    unset($myFeedback);

    $mediaBase->endElement(); // -- mediaBase --
    $mediaBase->endDocument();
  }

  // -- ------------------------------------------ --
  // -- update mediabase with the latest song only --
  else
  {
    echo '*******************************************************' . LFC;
    echo sprintf('doing update for songs which comes in the last %d day(s)', $updateDays) . LFC;
    echo '*******************************************************' . LFC;

    $mediaBaseUpdateStr = file_get_contents(ONEDIRUP.$cfg_mediabaseFile);
    $mediaBaseUpdate = simplexml_load_file(ONEDIRUP.$cfg_mediabaseFile);
    $myFeedback = new class_feedback(ONEDIRUP);
    // -- create timestamp to go back X days --
    $updateDaysUnix = mktime(date('H'), date('i'), date('s'), date('n'), date('j') - $updateDays);

    // -- core --
    $mp3FileCount = cntFilecount($cfg_absPathToSongs);
    require_once ONEDIRUP.'lib/class_cli_progressbar.php';
    $cliProgBar = new cli_ProgressBar($mp3FileCount);
    recursive_updateMP3TagsToMediabase($cfg_absPathToSongs, $updateDaysUnix, false); // -- cli mode --
    $cliProgBar->finish();

    // -- ... write to XML file now --
    $handle = fopen(ONEDIRUP.$cfg_mediabaseFile, 'w');
    if (!fwrite($handle, $mediaBaseUpdate->asXML()))
      echo 'failure while saving mediabase file' . LFC;
    fclose($handle);

    unset($myFeedback);
    unset($mediaBaseUpdate);
  }


  // -- kill the cache directory --
  // -- because it can get confusing, if old - not available songs are in --
  if (file_exists(ONEDIRUP.$cfg_cachePath))
  {
    cntDelTree(ONEDIRUP.$cfg_cachePath);
  }

