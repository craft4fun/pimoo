<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  require_once ('lib/utils.php');

  require_once ('api_mediabase.php');
  require_once ('api_playlist.php');
  //require_once ('api_feedback.php');

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();


  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">'.LNG_DOCUMENTATION.'</div>';

    echo '<span><a class="linkNaviNice" href="myMooEvenMore.php">'.LNG_BACK_TO_ . LNG_EVEN_MORE.'</a></span>';
    echo '<span style="float: right;"><a class="linkNaviNice" href="index.php">' . LNG_BACK_TO_ .  PROJECT_NAME.'</a></span>';

  echo '</div>';


?>
<div style="padding: 0.5em;">

  <div class="adminBoxOuter">
    <label for="searchOrder" class="adminBoxLabel"><?php echo LNG_SEARCH_ORDER; ?></label>
    <div id="searchOrder" class="docuBoxInner">
      interpret - title - album  - genre - year - rating - ratingByWhom - comment - commentByWhom
      <br>
      <br>
      example: <span style="color: red;">*take*paddy*</span> will reveal <span style="color: red;">Phil Collins - Take me home</span>
      <br>
      or: <span style="color: red;">Phil Collins~5~</span> will reveal <span style="color: red;">Phil Collins - Take me home</span>
    </div>
  </div>

  <div class="adminBoxOuter">
    <label for="searchParams" class="adminBoxLabel"><?php echo LNG_SEARCH_OPTIONS; ?></label>
    <div id="searchParams" class="docuBoxInner">
      if the first three characters machtes one of these three patterns: ~i~, ~t~ or ~a~ the search will be limited to this certain field.
      (~i~ interpret, ~t~ title and ~a~ album)
      <br>
      <br>
      example: <span style="color: red;">~a~metallica</span> will reveal all Songs of the album "metallia" (aka "black album").
      <br>
      <br>
      or: <span style="color: red;">~a~metallica*~4~</span> will reveal all Songs of the album "metallia" which are rated "great".
    </div>
  </div>


  <div class="adminBoxOuter">
    <label for="cutBlank" class="adminBoxLabel"><?php echo LNG_CUT_BLANK; ?></label>
    <div id="cutBlank" class="docuBoxInner">

      <?php echo LNG_CUT_BLANK_HELP; ?>

    </div>
  </div>



  <div class="adminBoxOuter">
    <label for="tablePlusAlbum" class="adminBoxLabel" style="font-style: italic;"><?php echo LNG_SEARCH_RESULT_AS_TABLE; ?></label>
    <div id="tablePlusAlbum" class="docuBoxInner">

      <?php echo LNG_SEARCH_RESULT_AS_TABLE_HELP; ?>

    </div>
  </div>


  <div class="adminBoxOuter">
    <label for="mouseControl" class="adminBoxLabel"><?php echo LNG_MOUSE_CONTROL; ?></label>
    <div id="mouseControl" class="docuBoxInner">
      <table style="text-align: left;">
        <thead>
          <tr>
            <th>Trigger</th>
            <th>Function</th>
            <th>Alternative</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Left button</td>
            <td>start piMoo</td>
            <td>-</td>
          </tr>
          <tr>
            <td>Middle button</td>
            <td>next song</td>
            <td>shutdown (if stopped)</td>
          </tr>
          <tr>
            <td>Right button</td>
            <td>stop piMoo</td>
            <td>-</td>
          </tr>
          <tr>
            <td>Wheel up</td>
            <td>Volume up</td>
            <td>-</td>
          </tr>
          <tr>
            <td>Wheel down</td>
            <td>Volume down</td>
            <td>-</td>
          </tr>
          <tr>
            <td>Move mouse up, down, left or right</td>
            <td>Load one of four favourite playlists</td>
            <td>coming soon...</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>


  <div class="adminBoxOuter">
    <label for="keyboardControl" class="adminBoxLabel"><?php echo LNG_KEYBOARD_CONTROL; ?></label>
    <div id="keyboardControl" class="docuBoxInner">
      <table style="text-align: left;">
        <thead>
          <tr>
            <th>Trigger</th>
            <th>Function</th>
            <th>Alternative</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>F5</td>
            <td>start piMoo</td>
            <td>-</td>
          </tr>
          <tr>
            <td>F6</td>
            <td>next song</td>
            <td>shutdown (if stopped and configured?)</td>
          </tr>
          <tr>
            <td>F7</td>
            <td>stop piMoo</td>
            <td>-</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div style="padding: 0.5em; margin-top: 3rem; color: orange;">
    All functions and features are well testet when <?php echo PROJECT_NAME; ?> works in mediabase variant "xml". Using a mariadb database could be buggy or differ in features
  </div>

</div>

<?php
  echo makeBottom();
  echo makeHTML_end();
