<style>

BODY {
  <?php
    // $bgFile = ($cfg_isDocker) ? 'res/piMoo_bg_docker.png' : 'res/piMoo_bg.png';

    // $bgAlpineSuffix = ($cfg_system == SYSTEM_ALPINE) ? '-alpine' : NULLSTR;
    // $bgFile = ($cfg_isDocker) ? "res/piMoo_bg_docker$bgAlpineSuffix.png" : 'res/piMoo_bg_dimmed.png';

    $bgFile = ($cfg_isDocker) ? 'res/piMoo_bg_docker.png' : 'res/piMoo_bg_dimmed.png';

    if (file_exists($bgFile) && !$isMobile)
    {
      echo 'background-image:url("'.$bgFile.'");';
    }
  ?>
}

SELECT {
  color: black;
}


/* this is partly dynamically overwritten while song is playing over time */
#omniPresentSong {
  /*color: red;
  text-shadow: 2px 2px 10px red, 1px 1px #000;*/
}

#adminControlBar {
  background-color: #769cb1;
  background: radial-gradient(#FFFFFF, #769cb1); /*a nice blue: 769cb1*/
  background: -moz-radial-gradient(#FFFFFF, #769cb1);
  background: -webkit-radial-gradient(#FFFFFF, #769cb1);
}



/* the basic template for all messages */
#target_feedback, #StdMsg, #okMsg, #errorMsg {
  border-color: blue;
  border-bottom-width: 2px;
  border-style: inset;
  box-shadow: 3px 3px 3px blue,
    -3px 3px 3px blue,
    3px -3px 3px navy,
    -3px -3px 3px silver;

  background-color: silver;
  background: radial-gradient(#FFFFFF, #AAAAAA);
  background: -moz-radial-gradient( #FFFFFF, #AAAAAA);
  background: -webkit-radial-gradient(#FFFFFF, #AAAAAA);
}


#frame_navi {
  background-color: rgba(100, 100, 100, 0.7);
  background: radial-gradient(rgba(100, 100, 100, 0.7), silver);
  background: -moz-radial-gradient(rgba(100, 100, 100, 0.7), silver);
  background: -webkit-radial-gradient(rgba(100, 100, 100, 0.7), silver);
}


#dlg_pleaseWait {
  background: white;
  <?php
    if ($isMobile)
    {
      echo 'color: navy;';
      echo 'text-shadow: 1px 1px 3px #0036ff, 1px 1px #000;';
    }
    else
    {
      echo 'color: white;';
      echo 'text-shadow: 1px 1px 3px #0036ff, 1px 1px #000;';
    }
  ?>
  box-shadow: 3px 3px 3px #fefefe,
    -3px 3px 3px #fefefe,
    3px -3px 3px #fefefe,
    -3px -3px 3px #fefefe;
}


.menuButton {
  /*prep*/
}
.menuButton:hover {
  background-color: gray;
}


#edt_songSearch {
  color: white;
  border-style: solid;
  border-width: 1px;
  border-color: white;
}


#onAirTitle {
  color: #00ff0f;
  text-shadow: 3px 3px 10px #00ff0f, 1px 1px silver;
}
#onAirWaiting {
  color: #b81e14;
  text-shadow: 2px 2px 7px red;
}


.modernCenterBackground {
  <?php
    if ($isMobile)
    {
      echo 'color: navy;';
      echo 'text-shadow: 1px 1px 3px #0036ff, 1px 1px #000;';
    }
    else
    {
      echo 'color: white;';
      echo 'text-shadow: 1px 1px 3px #0036ff, 1px 1px #000;';
    }
  ?>
  background: white;
  box-shadow: 3px 3px 3px #fefefe,
    -3px 3px 3px #fefefe,
    3px -3px 3px #fefefe,
    -3px -3px 3px #fefefe;

}


<?php
  // -- ------------------------ --
  // -- begin of mediaBase group --
  // -- ------------------------ --
?>

.grpLstContL {
  background-color: gray;
  background: radial-gradient(darkgray, gray);
  background: -moz-radial-gradient(darkgray, gray);
  background: -webkit-radial-gradient(darkgray, gray);

  box-shadow: 2px 2px 2px #769cb1,
    -2px 2px 2px #769cb1,
    2px -2px 2px #769cb1,
    -2px -2px 2px #769cb1;
}
.grpLstContL:HOVER {
  background-color: #769cb1;
  background: radial-gradient(#FFFFFF, #769cb1);
  background: -moz-radial-gradient(#FFFFFF, #769cb1);
  background: -webkit-radial-gradient(#FFFFFF, #769cb1);
}

.grpLstItmL {
  text-decoration: none;
  color: black;
  cursor: default;
}

.grpLstContR {
  background-color: #fff79e;
  background: radial-gradient(#fff79e, silver);
  background: -moz-radial-gradient(#fff79e, silver);
  background: -webkit-radial-gradient(#fff79e, silver);

  box-shadow: 3px 3px 3px gray,
    -3px 3px 3px silver,
    3px -3px 3px silver,
    -3px -3px 3px silver;
}

.grpLstContR:HOVER {
  background-color: silver;
  background: radial-gradient(silver, #fff79e);
  background: -moz-radial-gradient(silver, #fff79e);
  background: -webkit-radial-gradient(silver, #fff79e);

  box-shadow: 0.5em 0.5em 0.5em #fff79e,
    -0.5em 0.5em 0.5em #fff79e,
    0.5em -0.5em 0.5em #fff79e,
    -0.5em -0.5em 0.5em #fff79e;
}


.grpLstItmR {
  text-decoration: none;
  color: black;
  cursor: default;
}





.tableContent {
  background-color: silver;
}

.tableContentRight {
  background-color: #c0c0d0;
}

/* Sortable tables */
table.sortable thead {
  background-color: #eee;
  color: #666666;
}


<?php
  // -- ---------------------- --
  // -- end of mediaBase group --
  // -- ---------------------- --



  // -- ----------------- --
  // -- begin of playlist --
  // -- ----------------- --
?>

.playListRowL {
  box-shadow: 2px 2px 2px #769cb1,
    -2px 2px 2px #769cb1,
    2px -2px 2px #769cb1,
    -2px -2px 2px #769cb1;

  background-color: rgba(100, 100, 100, 0.7);
  background: radial-gradient(silver, rgba(100, 100, 100, 0.7));
  background: -moz-radial-gradient(silver, rgba(100, 100, 100, 0.7));
  background: -webkit-radial-gradient(silver, rgba(100, 100, 100, 0.7));
}

.playListItemL {
  color: black;
}

.playListRowR {
  float: right;
  margin: 10px 0px 0px 0px;
  padding: 5px;

  background-color: #fff79e;
  background: radial-gradient(#fff79e, silver);
  background: -moz-radial-gradient(#fff79e, silver);
  background: -webkit-radial-gradient(#fff79e, silver);

  min-width: 2em;
  min-height: 1.5em;

  transition: all 0.5s;

  box-shadow: 3px 3px 3px silver,
    -3px 3px 3px silver,
    3px -3px 3px silver,
    -3px -3px 3px silver;

  border-radius: 5px;
}
.playListRowR:HOVER {
  box-shadow: 6px 6px 6px #fff79e,
    -6px 6px 6px silver,
    6px -6px 6px silver,
    -6px -6px 6px #fff79e;
}

.playListItemR {
  color: black;
}


.listItemInterpret{
  color: black;
}

.listItemTitle{
  color: blue;
}

.listItemRemainTime {
  color: white;
}

.listItemWishedBy{
  color: #ffff80;
}




.plInfoRow{
  font-style: italic;
  cursor: default;
  text-align: center;
}



.comment {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 30px;';
    else
      echo 'padding: 15px 0px 0px 0px;';
  ?>
  cursor: default;
  background-image: url("res/comment<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.rating {
  margin-left: 1em;
  margin-bottom: 1em;
  background-color: transparent;
  border-radius: 3px;

  border-style: solid;
  border-width: 1px;
  border-left-color: gray;
  border-top-color: gray;
  border-right-color: black;
  border-bottom-color: black;


  <?php
    if ($isMobile)
      echo 'padding: 10px;';
    else
      echo 'padding: 5px;';
  ?>
  }



.arrowDown {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowDown<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.arrowUp {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowUp<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.arrowUpTop {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowUpTop<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}



<?php
  // -- --------------- --
  // -- end of playlist --
  // -- --------------- --
?>



.watermarkCover {
  width: 90%;
  text-align: center;
  position: absolute;
  line-height: 99%;
  pointer-events: none;
}
.watermarkBgText{
  opacity: 0.5;
  color: white;
  text-shadow: 1px 1px 2px black, 0 0 25px red, 0 0 5px orange;
}



.textShadowBlur {
  text-shadow: 0px 0px 1px #808080;
}

.boxShadow {
  box-shadow: 3px 3px 3px grey,
     3px 3px 3px grey,
     3px 3px 3px grey,
     3px 3px 3px grey;
}

.explainText {
  color: gray;
  font-size: smaller;
  margin: 0.5em;
}


.watermark {
  background-color: #666666;
  text-shadow: rgba(255,255,255,0.5) 0px 3px 3px;
}

/* an optical nice link */
.linkNice {
  color: grey;
  text-shadow: 2px 2px 10px #00bf0b, 1px 1px #000;
}

/* an optical nice link for navigation */
.linkNaviNice {
  color: grey;
  text-shadow: 2px 2px 10px #3e3ec9, 1px 1px #000;
}

.selectNice {
  color: grey;
  text-shadow: 1px 1px 5px #00bf0b, 1px 1px #000;
}

.textNice {
  color: grey;
  text-shadow: 1px 1px 5px #00bf0b, 1px 1px #000;
}


.btnBlue, .btnRed, .btnGreen, .btnGray, .btnYellow {
  color: white;
  border-style: solid;
  border-width: 1px;
  border-radius: 5px;
}
.btnBlue {
  background-color: blue;
  background: radial-gradient(blue, aqua);
  background: -moz-radial-gradient(blue, aqua);
  background: -webkit-radial-gradient(blue, aqua);
}
.btnBlue:hover {
  background: radial-gradient(aqua, blue);
  background: -moz-radial-gradient(aqua, blue);
  background: -webkit-radial-gradient(aqua, blue);
}
.btnRed {
  background-color: red;
  background: radial-gradient(red, orange);
  background: -moz-radial-gradient(red, orange);
  background: -webkit-radial-gradient(red, orange);
}
.btnRed:hover {
  background: radial-gradient(orange, red);
  background: -moz-radial-gradient(orange, red);
  background: -webkit-radial-gradient(orange, red);
}
.btnGreen {
  background-color: green;
  background: radial-gradient(green, lime);
  background: -moz-radial-gradient(green, lime);
  background: -webkit-radial-gradient(green, lime);
}
.btnGreen:hover {
  background: radial-gradient(lime, green);
  background: -moz-radial-gradient(lime, green);
  background: -webkit-radial-gradient(lime, green);
}
.btnGray {
  background-color: gray;
  background: radial-gradient(gray, silver);
  background: -moz-radial-gradient(gray, silver);
  background: -webkit-radial-gradient(gray, silver);
}
.btnGray:hover {
  background: radial-gradient(silver, gray);
  background: -moz-radial-gradient(silver, gray);
  background: -webkit-radial-gradient(silver, gray);
}

.btnAdminAdd {
  font-size: 1.2em;
  height: 2.2em;
  padding: 0.2em 0.5em;
}

.myMooOption, .selected {
  background-color: white;
}


.docuBoxInner {
  background-color: white;
}


</style>