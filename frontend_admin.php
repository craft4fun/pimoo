<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="author" content="Patrick Höf">

    <?php
      require_once ('defines.inc.php');
      $darkMode = (isset($_COOKIE['bool_darkMode']) && $_COOKIE['bool_darkMode'] == YES) ? true : false;
    ?>

    <style type="text/css">
      html, body {
        margin: 0px;
        height: 100%;
      }
      .prison {
        box-sizing: border-box;
        border-style: none;
      }
      #frontend {
        width: 60%;

        padding-right: 2px;
      }
      #backend {
        width: 40%;
        float: right;

        padding-left: 2px;

        border-left-style: double;
        border-left-color: orange;
        border-left-width: 2px;

        /* background-color: white;
        background: linear-gradient(white, silver);
        background: -moz-linear-gradient(white, silver);
        background: -webkit-linear-gradient(white, silver); */

        <?php
          if (!$darkMode)
          {
        ?>
          background-color: white;
          background: linear-gradient(silver, white, #273746);
          background: -webkit-linear-gradient(silver, white, #273746);
          background: -moz-linear-gradient(silver, white, #273746);
        <?php
          }
          else
          {
        ?>
          background-color: gray;
          background: linear-gradient(gray, #303030);
          background: -webkit-linear-gradient(gray, #303030);
          background: -moz-linear-gradient(gray, #303030);
        <?php
          }
        ?>
      }
    </style>
    <script type="text/javascript">

      function _setBodyHeight()
      {
        var frontend = document.getElementById('frontend');
        var backend = document.getElementById('backend');
        frontend.style.height = window.innerHeight + 'px';
        backend.style.height = window.innerHeight + 'px';
      }
      window.addEventListener("resize", _setBodyHeight);

    </script>
  </head>
  <body onload="_setBodyHeight()">
    <iframe src="frontend.php?imprison"      id="frontend" class="prison"></iframe>
    <iframe src="admin/backend.php?imprison" id="backend"  class="prison"></iframe>
  </body>
</html>