<?php
  // -- messages for errors --
  define('LNG_ERR_UNKNOWN','unbekannter oder unerwarteter Fehler');
  define('LNG_ERR_UNKNOWN_FUNNY','Ups...Irgendwas ist falsch gelaufen');
  define('LNG_ERR_PARAM_MISSING','ein benötigter Parameter fehlt');
  define('LNG_ERR_WRITEFILE','kann Datei nicht schreiben');
  define('LNG_ERR_ONLYADMIN','Nur dem Administrator ist es erlaubt dies zu tun');
  define('LNG_ERR_NOSONGINFO','Es ist keine Songfinformation verfügbar, offensichtlich konnte der Song nicht in der Mediendatenbank gefunden werden');
  define('LNG_ERR_NOSONGINFILESYSTEM','Der Song ist im Dateisystem nicht verfügbar');
  define('LNG_ERR_PATH','!!! PFAD EXISTIERT NICHT !!!');
  define('LNG_ERR_NOPAGE','klotz (Seite) existiert nicht');
  define('LNG_ERR_HUMANFORMAT','Fehler beim formatieren in ein menschlich lesbares Format');
  define('LNG_ERR_NOAUDIOELEM','dein Browser unterstützt das Audio Element nicht '.EMO_SAD);
  define('LNG_ERR_NO_RATING_SELECTED','keine Wertung ausgewählt');
  // -- messages for success --
  define('LNG_SUC','OK');
  define('LNG_SUC_ADDED','der Playlist hinzugefügt');
  define('LNG_SUC_KILLED','Song(s) wurde von der Playlist gelöscht');
  define('LNG_SUC_MOVEDUP','Song wurde erfolgreich hochbewegt');
  define('LNG_SUC_MOVEDDOWN','Song wurde erfolgreich runterbewegt');
  define('LNG_SUC_MOVEDTOP','Song wurde erfolgreich ganz nach oben bewegt');
  define('LNG_SUC_COMMENTED','Song wurde erfolgreich kommentiert');
  define('LNG_SUC_RATED','Song wurde erfolgreich bewertet');
  // -- messages generally --
  define('LNG_PLEASE_WAIT','bitte warten...');
  define('LNG_ALREADY_IN','Song ist bereits in der Playlist');
  define('LNG_NO_MORE','mehr Songs sind nicht erlaubt, die Playlist ist voll');
  define('LNG_GIMMENICKNAME','willkommen,<br>bitte gib mir einen Spitznamen');
  define('LNG_TAKEN_NICKNAMES','die sind schon da');
  define('LNG_LOGOUT','bring mich hier weg');
  define('LNG_LOGOUT_SHORT','Tschöö');
  define('LNG_PARTYPOOPER','komm schon, sei kein Spielverderber und klicke hier');
  define('LNG_CODEWORD_OF_THE_DAY_HINT','klicke hier und gib mir das Geheimwort des Tages!<div style="margin-top: 1em; font-size: smaller;">(vielleicht kann der Party-Flyer helfen, oder frag einfach den Gastgeber)</div>'); //
  define('LNG_GOODBYE','Tschüss <span style="font-weight: bold;">'.EMO_SAD.'</span>');
  define('LNG_SSB_DISABLEDBYADMIN','die Suchleiste ist vom Administrator ausgeschaltet worden.');
  define('LNG_LOADED_FROM_CACHE','aus dem Cache geladen');
  define('LNG_HITLIST_EMPTY','die Bestenliste ist leer');
  define('LNG_NOLYRIC_AVAILABLE','kein Songtext verfügbar '.EMO_SAD);
  define('LNG_NICK_NOT_VALID','Spitzname ist nicht gültig');
  define('LNG_NICK_ALREADY_USED','Spitzname wird bereits benutzt');
  define('LNG_TAKE_ANOTHER_NICK','bitte nimm einen anderen Spitznamen');
  define('LNG_WRONG_ANSWER','das ist nicht richtig');
  define('LNG_WELCOME','willkommen');
  // -- questions --
  define('LNG_SURE','Sicher?');
  define('LNG_CODEWORD_OF_THE_DAY','Geheimwort des Tages?');
  define('LNG_WISHED_NICKNAME','dein gewünschter Spitzname');
  define('LNG_YOUR_COMMENT','dein Kommentar');
  define('LNG_LEAVE_PAGE_WHILE_PLAYING','Spielt gerade ab, trotzdem verlassen?');
  // -- standards --
  define('LNG_ALL','alle');
  define('LNG_NO','nein');
  define('LNG_YES','ja');
  define('LNG_HELLO','hallo');
  define('MSG_THANKS','Danke');
  define('LNG_MORE','mehr');
  define('LNG_EVEN_MORE','noch mehr');
  define('LNG_BACK','zurück');
  define('LNG_BACK_TO_','zurück zu ');
  define('LNG_PREVIOUS_RESULTS','vorhergehende Ergebnisse');
  define('LNG_NEXT_RESULTS','nächste Ergebnisse');
  define('LNG_MOVE_TOP','hochrutschen!');
  define('LNG_CHANGE_NICKNAME','Spitznamen ändern');
  define('LNG_LANGUAGE','Sprache');
  define('LNG_TOLERANT_SEARCH','tolerante Suche');
  define('LNG_CASE_SENSTIVE','Groß- Kleinschreibung beachten');
  define('LNG_CUT_BLANK','Leerzeichen für die Suche abschneiden');
  define('LNG_CUT_BLANK_HELP','Ohne Leerwertkorrektur auf Suchzeichenfolgen kann man z.B. "Sia" und nicht "Asia" mit dem Suchmuster " Sia" finden');
  define('LNG_RANDOM_SEARCH_RESULTS','Anzahl zufälliger Suchergebnisse');
  define('LNG_DAYS_SONG_NEWCOMMER','Tage wie lange ein Song ein Neueinsteiger ist');
  define('LNG_SEARCH_RESULT_AS_TABLE','Suchergebnisse als Tabelle');
  define('LNG_SEARCH_RESULT_AS_TABLE_HELP','"Suchergebnisse als Tabelle anzeigen" mit Albumspalte <span style="font-weight: bold;">(ja)</span> kann beim Laden etwas länger dauern');
  define('LNG_SHOW_SONG_OMNIPRESENT','zeige den aktuellen Song-Titel immerwährend');
  define('LNG_SHOW_ALBUM_IN_PLAYLIST','zeige Album in playlist wenn verfügbar');
  define('LNG_DARKMODE','dunkler Modus');
  define('LNG_DISPLAY_ZOOM','Bildschirmzoom');
  define('LNG_NOCOMMENT_DO','kommentiere mich..');
  define('LNG_NORATING_VOTE','bewerte mich..');
  define('LNG_NEWCOMMER','Neueinsteiger');
  define('LNG_BAD','schlecht');
  define('LNG_TOLERABLY','tolerierbar');
  define('LNG_GOOD','gut');
  define('LNG_GREAT','großartig');
  define('LNG_EXCELLENT','überragend');
  define('LNG_OFF','aus');
  define('LNG_LOW','niedrig');
  define('LNG_MEDIUM','mittel');
  define('LNG_HIGH','hoch');
  define('LNG_SURE_NEXT_SONG_IMMEDIATELLY','Den nächsten Song sofort spielen?');
  define('LNG_NOW_NEXT_SONG','...nächster Song...');
  define('LNG_DELETE','löschen');
  define('LNG_SURE_DELETE_SONG_FROM_PLAYLIST','-%s- von der Playlist löschen?');
  define('LNG_COULD_FREEZE_YOUR_DEVICE','wirklich?\nKönnte eine lange Zeit dauern und dein Endgerät einfrieren lassen...');
  define('LNG_ONAIR', '~ on air ~');
  define('LNG_WAITING', '~ wartet ~');
  define('LNG_PLAYLIST','playlist');
  define('LNG_QUEUE','Warteschlange');
  define('LNG_CLEAR_QUEUE','Warteschlange löschen');
  define('LNG_REMOVE_DOUBLES','Doppelte entfernen');
  define('LNG_REMOVE_MISSING','Fehlende entfernen');
  define('LNG_HITLIST_N_PARTYREPORT','Bestenliste und Partybericht');
  define('LNG_EVERLASTING_HITLIST','immerwährende Bestenliste');
  define('LNG_FAST','schnell');
  define('LNG_VALUED_SONGS','bewertete Songs');
  define('LNG_PARTY_REPORT','Partybericht');
  define('LNG_HISTORY','Historie');
  define('LNG_DOCUMENTATION','Dokumentation');
  define('LNG_SEARCH_ORDER','Suchreihenfolge');
  define('LNG_SEARCH_OPTIONS','Suchoptionen');
  define('LNG_SEARCH_USER','Nach Benutzer suchen');
  define('LNG_INTERPRET','Interpret');
  define('LNG_TITLE','Titel');
  define('LNG_DURATION','Länge');
  define('LNG_WISHED_BY','Wunsch von');
  define('LNG_ALBUM','Album');
  define('LNG_GENRE','Genre');
  define('LNG_YEAR','Jahr');
  define('LNG_FILE_DATE','Datei Datum');
  //define('LNG_BIT_RATE','Bitrate');
  define('LNG_RATING','Bewertung');
  define('LNG_COMMENT','Kommentar');
  define('LNG_PATH_FILE','pfad/datei');
  define('LNG_FIRST_HIT','erster Treffer');
  define('LNG_OLD_FEEDBACK_MEDIABASE','dies is eine ältere Bewertung, gespeichert in der Mediendatenbank');
  define('LNG_TITLE_UPLOAD_OWN_SONG','Eigenes Lied hochladen');
  define('LNG_BTN_UPLOAD_OWN_SONG','Hochladen und der Mediendatenbank hinzufügen');
  define('LNG_ONE_MORE_UPLOAD','noch ein upload');
  define('LNG_SONGS_FOUND_','Gefundene Song(s): <span style="color:red;">%d</span>');
  define('LNG_SONGS_FOUND_MORE_THAN_','Mehr als <span style="color:red;">%d</span> Song(s) gefunden');
  define('LNG_PLAYLIST_SONGS_IN_','<span style="color: #fc3b00;">%d</span>&nbsp;song(s) in der Warteschlange');
  define('LNG_PLAYLIST_NO_SONGS_IN_HELP','Die Playlist ist leer - Füge einen Song hinzu!');
  define('LNG_ADD_TO_PLAYLIST','zur Playlist hinzufügen');
  define('LNG_ADD_ALL_TO_PLAYLIST','alle auf einmal hinzufügen');
  define('LNG_STASH','Versteck');
  define('LNG_STASH_PUSH_TO','Song versteckt');
  define('LNG_STASH_NO_SONG','Du hast keinen Song im Versteck');
  define('LNG_STASH_POP_FROM','Song aus Versteck holen');
  define('LNG_STASH_SHOW','Versteckinhalt anzeigen');
  define('LNG_STASH_POPPED_FROM','Song aus Versteck geholt');
  define('LNG_STASH_EMPTY','Du hast keinen Song in Deinem Versteck');
  define('LNG_STASH_POP_FAILED','Fehler beim Holen des Songs aus dem Versteck');
  define('LNG_PLAYER_MUST_BE_RUNNING','nur wenn der player läuft');
  define('LNG_SKIP_ONLY_ON_PLAY','nur wenn die Musik läuft');
  define('LNG_SKIP_TIME_TO_SHORT','nicht genug Zeit zum Springen');
  // -- backend --
  define('LNG_BACKEND','Verwaltung');
  define('LNG_MEDIABASE','Mediendatenbank');
  define('LNG_MUSIC_POOL','Musikpool');
  define('LNG_TOT_SONGS_AVAIL','Summe aller verfügbaren Songs');
  define('LNG_EXPLAIN_iMoo','piMoo-Reverse dreht die Funktionalität der Musikausgabe um. Die Musik wird z.B. auf dem Smartphone ausgebenen.');
  define('LNG_EXPLAIN_piMooApp','Genau wie die piMooApp, kann aber direkt im Browser gestartet werden.');
  define('LNG_IN_NEW_TAB','In neuem Tab');
  define('LNG_MOUSE_CONTROL','externe Maussteuerung');
  define('LNG_KEYBOARD_CONTROL','externe Tastatursteuerung');
  define('LNG_ADMIN_SPECIAL','Spezial Admin Seite');
  define('LNG_ADMIN_IMPRISONED','eingesperrt');
  define('LNG_ADMIN_BREAK','Verlassen nur mit \'Logout\' oder \''.LNG_LOGOUT_SHORT.'\'!');
  define('LNG_DEVELOPMENT','Entwicklung');
  define('LNG_NO_PLAYLLIST_REFRESH','Keine Playlist Aufrischung');
  define('LNG_SHADOW_CONFIG','Schattenkonfiguration');
  define('LNG_MULTIBLE_NICKNAME','Mehrere Spitznamen erlauben');
  define('LNG_MULTIBLE_ENTRIES','Mehrere gleiches Einträge erlauben');
  define('LNG_PARTY_MODE','Partymodus (auto. neuer Song wenn Playlist leer)');
  define('LNG_GENRE_WHITELIST','Genre whitelist');
  define('LNG_TRYING_TO_STOP_PLAYER','versuche den player zu stoppen...');
  define('LNG_PROVIDE_PASSWORD_INSTALL','Du musst das Passwort angeben, welches bei der Installation gesetzt wurde!');
  define('LNG_PROVIDE_PASSWORD_DOCKER','Du musst das Passwort angeben, welches beim Erzeugen des Docker Containers gesetzt wurde!');
  define('LNG_APPLY','übernehmen');
  define('LNG_CLEAR','leeren');
