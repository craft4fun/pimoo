<?php
  // -- special piMoo stuff --
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_playlist.php');


  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  //-- get nickame --
  if (isset($_SESSION['str_nickname']))
    $wishedBy = $_SESSION['str_nickname'];
  else
    $wishedBy = $_SERVER['REMOTE_ADDR'];


  if (!isset($_GET['a']))
  {
    echo LNG_ERR_PARAM_MISSING;
  }
  else
  {
    $in = (isset($_GET['i'])) ? $_GET['i'] : QUESTMARK;
    $ti = (isset($_GET['t'])) ? $_GET['t'] : QUESTMARK;
    $pt = (isset($_GET['p'])) ? $_GET['p'] : '-1';
    $al = (isset($_GET['l'])) ? $_GET['l'] : NULLSTR;

    $myPlaylist = new class_playlist();
    $myPlaylist->setWishedBy($wishedBy);

    $msg = NULLSTR;

    $check = $myPlaylist->addSong(base64_decode($_GET['a']),
                                  base64_decode($in),
                                  base64_decode($ti),
                                  $pt,
                                  base64_decode($al),
                                  $admin);
    eventLog($myPlaylist->getLastMsg().COLON.BLANK.base64_decode($ti)); // -- no longer required: strip_tags() --
    if ($check === true)
      $msg .= '<div class="listItemWishedBy">'.$myPlaylist->getLastMsg().'</div>';
    else
      $msg .= cntErrMsg($myPlaylist->getLastMsg().BLANK.'"'.base64_decode($ti).'"');


    $msg .= '<div class="listItemTitle" style="text-align: center; font-size: smaller; margin-bottom: 0.5em;">(' . base64_decode($ti) . ')</div>';

    if ($admin && $myPlaylist->getPlaylistCount() > 1)
      $msg .= cntMBL('<a style="color: red;"  href="javascript:songMoveTopAfterAdding()">'.LNG_MOVE_TOP.'</a>');

    unset($myPlaylist);

    echo $msg;
  }

