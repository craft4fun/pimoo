#!/usr/bin/php
<?php
  require ('defines.inc.php');
  require ('keep/config.php');
  require ('lib/utils.php');

  // -- needed, if it is to be controlled by runlevel scripts --
  define ('PLAYERSCRIPT', 'cli_player.php');

  $path = getCLIpathDependOnConfig($cfg_isDocker, $cfg_system, __DIR__);
  // -- finally set path - anyhow --
  chdir($path);

  // -- called by external trigger -> mouse event --
  file_put_contents($cfg_playerStateFile, PS_PLAYING);

