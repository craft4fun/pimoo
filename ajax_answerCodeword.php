<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');


  if (!isset($_GET['codeword']))
  {
    //    $_SESSION['str_nickname'] = $_SERVER['REMOTE_ADDR'];
    echo cntErrMsg(LNG_ERR_UNKNOWN_FUNNY);
  }
  else
  {
    if ($_GET['codeword'] == $cfg_codeWordAnswer)
    {
      $_SESSION['bool_codewordOK'] = true;
      echo cntOkMsg('<span style="font-weight: bold;">' . LNG_WELCOME . '</span>');
    }
    else
    {
      $_SESSION['bool_codewordOK'] = false;
      echo cntErrMsg('<span style="font-weight: bold;">' . LNG_WRONG_ANSWER . '</span>');
    }
  }
