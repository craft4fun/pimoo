<?php
  // -- piMoo basic stuff --
  require ('defines.inc.php');
  require ('keep/config.php');
  require ('lib/utils.php');
  require ('lib/class_cache.php');

  // -- special piMoo stuff --
  require ('api_layout.php');

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  $isHulk = isset($_GET['hulk']) && $_GET['hulk'] != NULLSTR;

  if (isset($_REQUEST['imprison']))
  {
    $_SESSION['bool_imprison'] = true;
  }

  echo makeHTML_begin();

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  // -- ------------------------ --
  // -- begin of top most issues --

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck(true);

  // -- end of top most issues --
  // -- ---------------------- --

  // -- the piMoo cow logo --
  echo makeTopNavi();

  // -- the search bar --
  echo makeSearchBar($cfg_allowExternControl || $admin);

  // -- welcome logo "moo!" --
  echo makeWelcomeLogo(!$isHulk);

  $logoTime = ($admin) ? '0' : WELCOMETIMEOUT;

  echo makeAJAXframeHulk(true);
?>
<script>
  // -- must be done like this because the object ajaxInterval is needed (even though this is a strange JS behaviour) --
  // -- and it is done here to avoid trouble with very fast clicking users --
  ajaxInterval = window.setInterval("ajaxCurPlaylistCall();", <?php echo getPLRI(); ?>);

  function showWelcome()
  {
    elem = $("welcomeLogo");
    if (elem)
    {
      elem.style.opacity = "0";
      window.setTimeout("killWelcomeLogo(elem)", <?php echo $logoTime; ?>);
    }

    // -- toggle the navigation bar --
    if (localStorage["naviOpen"] == 0)
      hideNavi();

    // -- remember the last search to eventually modify an old search --
    var edt_songSearch = $("edt_songSearch");
    if (edt_songSearch)
    {
      if (localStorage["edt_songSearch"])
        edt_songSearch.value = localStorage["edt_songSearch"];
    }

<?php
    // -- show the navigation bar immediatelly --
    if ($isHulk)
      echo showGroupNavi();
?>

  } // -- end of showWelcome() --

  dojo.ready(showWelcome);

  // -- begin of the actually main javascript startup procedure --
  function killWelcomeLogo(aElem)
  {
    if (aElem)
      elem.style.display = "none";

<?php if (!$isHulk) { ?>

    DojoAjax("ajax_getCurrentPlaylist.php", "frame_hulk");

<?php } else { ?>

    // -- must be done like this because the object ajaxInterval is needed (even though this is a strange JS behaviour) --
    resetAjaxCurPlaylistCall();

<?php }

  // -- and at last show the navigation bar --
  if (!$isHulk)
    echo showGroupNavi();

  // -- set focus to seach field (in most cases very handy but not with mobile devices because the vitual keyboard opens immediately) --
  if (!$isMobile)
  {
?>
    edt_songSearch = $("edt_songSearch");
    if (edt_songSearch)
      edt_songSearch.focus();

<?php } ?>

  } // -- end of the actually the main javascript startup procedure --

  function reloadPlaylist()
  {
    if (localStorage.getItem("bool_noPlaylistRefresh") != "<?php echo YES; ?>")
    {
      DojoAjax("ajax_getCurrentPlaylist.php", "frame_hulk"); // -- the day may come where a anti-cache timestamp must be added --
      //NO: callHulk(\'frontend.php?hulk=ajax_getCurrentPlaylist.php\');
    }
  }

  // -- this function is called by all normal triggers --
  function ajaxCurPlaylistCall()
  {
    <?php
      if (!$cfg_mqtt)
      {
    ?>
      reloadPlaylist();
    <?php
      }
    ?>
  }

</script>
<?php
  echo '<div id="hitlist">';
    require_once 'hitlist.php';
    $myHitlist = new class_everlastingHitlist();
    echo $myHitlist->doOutput();
  echo '</div>';

  echo makeBottom();
  echo makeHTML_end();
