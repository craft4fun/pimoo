<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  require_once ('lib/utils.php');

  require_once ('api_mediabase.php');
  //require_once ('api_playlist.php');
  require_once ('api_feedback.php'); // needed ?


  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();


  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">'.LNG_TITLE_UPLOAD_OWN_SONG.'</div>';

    echo '<span><a class="linkNaviNice" href="myMooEvenMore.php">'.LNG_BACK_TO_ . LNG_EVEN_MORE.'</a></span>';
    echo '<span style="float: right;"><a class="linkNaviNice" href="index.php">' . LNG_BACK_TO_ .  PROJECT_NAME.'</a></span>';

  echo '</div>';
?>
<div style="padding: 1em;">

  <form name="FORM_UPLOAD" method="POST" action="actionUpload.php" enctype="multipart/form-data" >

    <div style="margin-top: 2em;">
      <span>
        <input type="file"   name="uploadSong"    class="uploadFile" />
        <input type="submit" name="btnUploadSong" class="btnGray" value="<?php echo LNG_BTN_UPLOAD_OWN_SONG; ?>" />
      </span>
    </div>

  </form>
</div>
<?php
  echo makeBottom();
  echo makeHTML_end();
