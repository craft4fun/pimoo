<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  include_once ('lib/utils.php');
  include_once ('api_mediabase.php');

  // -- is admin? --
  //$admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  $msg = NULLSTR;

  if (isset($_GET['song']))
  {
    // -- needed if called e.g. by valuedSong.php to take the first hit of song name without intern mediabase path --
    $firstHit = (isset($_GET['firstHit'])) ? true : false;
    $noPreview = (isset($_GET['noPreview'])) ? true : false;


    $song = base64_decode($_GET['song']);


    $myMediabase = new class_mediaBase();
    $info = $myMediabase->getSongInfo($song, $firstHit);

    // -- check if the song can be found in the mediabase --
    if ($info === false || !isset($info['i']) || !isset($info['t'])) // -- check a bit more --
    {
      $msg .= cntErrMsg(LNG_ERR_NOSONGINFO);
    }
    else
    {
      // -- ----------- --
      // -- cover thumb --
      // -- ----------- --

      // -- only logged in can see preview? --
      if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
      {

        if (cntIsMobile())
        {
          $thumgSize = 'width="150px" height="150px"';
          $mobileSuffix = 'M';
        }
        else
        {
          $thumgSize = 'width="50px" height="50px"';
          $mobileSuffix = NULLSTR;
        }


        // -- preview --
        if (!$noPreview)
        {
          $msg .= '
            <span style="float: right;">
              <a href="preview.php?song='.$_GET['song'].'&i='.base64_encode($info['i']).'&t='.base64_encode($info['t']).'&p='.base64_encode($info['p']).'&l='.base64_encode($info['l']).'">
                <img '.$thumgSize.' src="'.cntGetAlbumPicURL($song, $info['l']).'" title="preview" />
              </a>
            </span>';
        }

      }

      // -- if found by luckiness, the first hit (without intern mediabase path, only filename) --
      if ($firstHit)
        $msg .= '<div style="text-align: center; color: red;">'.LNG_FIRST_HIT.'</div>';


      // -- ------------------ --
      // -- begin of song info --
      // -- ------------------ --

      // -- escape the apostrophe for further javascript issues --
      $escapedInterpret = str_replace('\'', '\\\'', $info['i']);
      $escapedTitle = str_replace('\'', '\\\'', $info['t']);
      $escapedAlbum = str_replace('\'', '\\\'', $info['l']);

      $escapedRatingBy = str_replace('\'', '\\\'', $info['rby']);
      $escapedComment = str_replace('\'', '\\\'', $info['c']);
      $escapedCommentBy = str_replace('\'', '\\\'', $info['cby']);

      $msg .= '
        <table>
          <tr>
            <td>'.LNG_INTERPRET.'</td>
            <td><a class="linkNice" href="javascript:callSearchByStr(\''.SO_INTERPRET.$escapedInterpret.'\')">'.$info['i'].'</a></td>
          </tr>
          <tr>
            <td>'.LNG_TITLE.'</td>
            <td><a class="linkNice" href="javascript:callSearchByStr(\''.SO_TITLE.$escapedTitle.'\')">'.$info['t'].'</a></td>
          </tr>
          <tr>
            <td>'.LNG_DURATION.'</td>
            <td>'.$info['p'].'</td>
          </tr>
          <tr>
            <td>'.LNG_ALBUM.'</td>
            <td><a class="linkNice" href="javascript:callSearchByStr(\''.SO_ALBUM.$escapedAlbum.'\')">'.$info['l'].'</a></td>
          </tr>
          <tr>
            <td>'.LNG_YEAR.'</td>
            <td><a class="linkNice" href="javascript:callSearchByStr(\''.$info['y'].'\')">'.$info['y'].'</a></td>
          </tr>
          <tr>
            <td>'.LNG_GENRE.'</td>
            <td><a class="linkNice" href="javascript:callSearchByStr(\''.SO_GENRE.$info['g'].'\')">'.$info['g'].'</a></td>
          </tr>
          <tr style="font-size: smaller;">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>'.LNG_RATING.'</td>
            <td><a class="linkNice" href="javascript:callSearchByStr(\''.$info['r'].'\')">'.cntGetHumanRating($info['r']).'</a>';
            if (strlen($info['r']) > NULLINT)
              $msg .= '  (by: <a class="linkNice" href="javascript:callSearchByUser(\''.$escapedRatingBy.'\')">'.$info['rby'].'</a>)';
            $msg .= '</td>';
       $msg .= '
          </tr>
          <tr>
            <td>'.LNG_COMMENT.'</td>
            <td><a class="linkNice" href="javascript:callSearchByStr(\''.SO_GENRE.$escapedComment.'\')">'.$info['c'].'</a>';
            if (strlen($info['c']) > NULLINT)
              $msg .= '  (by: <a class="linkNice" href="javascript:callSearchByUser(\''.$escapedCommentBy.'\')">'.$info['cby'].'</a>)';
            $msg .= '</td>';
       $msg .= '
         </tr>
          <tr style="font-size: smaller;">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
           <tr>
            <td>'.LNG_FILE_DATE.'</td>
            <td>'.$info['d'].'</td>
          </tr>
          <tr style="font-size: smaller;">
            <td>'.LNG_PATH_FILE.'</td>
            <td>'.$info['a'].'</td>
          </tr>

        </table>
      ';
//<a href="feedbackHistory or so">'.LNG_HISTORY.'</a>
    }

    unset($myMediabase);
  }

  echo $msg;

/*wherefore?
  <tr>
  <td>'.LNG_BIT_RATE.'</td>
  <td>'.$info[7].'</td>
  </tr>
*/