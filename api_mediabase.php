<?php
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_feedback.php';

  require 'api_mediabaseInterface.php';

  // TODO paddy fix this trouble caused when called by ajax --
  if (!isset($cfg_mediaBaseType))
  {
    require_once 'api_mediabaseXML.php';
  }
  else
  {
    if ($cfg_mediaBaseType == MEDIABASETYPE_XML)
    {
      require_once 'api_mediabaseXML.php';
    }

    if ($cfg_mediaBaseType == MEDIABASETYPE_MYSQL)
    {
      require_once 'api_mediabaseMYSQL.php';
    }
  }
