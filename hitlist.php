<?php
  require_once 'lib/class_cache.php';

  class class_everlastingHitlist
  {
    function __construct($aDepth = NULLSTR)
    {
      global $cfg_hitlistLimit;
      global $cfg_musicFileType;
      global $cfg_cachePath;

      // -- check path deepness --
      $this->depth = $aDepth;

      // -- basics --
      $this->hitlistLimit = $cfg_hitlistLimit;
      $this->hitlistFilename = $this->depth . 'keep/partyReport/hitlist.txt';
      $this->musicFileType = $cfg_musicFileType;

      $this->cachePath = $this->depth.$cfg_cachePath;
      require_once ($this->depth.'lib/class_cache.php');
    }

    /**
     * creates and shows hitlist. In case of buildCache only cache is built but no output will be done
     * @param boolean $abuildCache
     * @return string
     */
    function doOutput($abuildCache = false)
    {
      $output = NULLSTR;

      // -- cache handling --
      $myCache = new class_outputCache(CACHELIFETIME_VERYLONG);
      $myCache->setCachePath($this->cachePath);
      $myCache->setCacheFile(CACHE_HITLIST);

      // -- take it from cache --
      if (!$abuildCache && $myCache->getIsCacheFileAlive())
      {
        if (CFG_DEBUG)
          $output .= '<div style="text-align: center; color: grey;"><nobr>... ' . LNG_LOADED_FROM_CACHE . ' ...</nobr></div>';
        $output .= file_get_contents($myCache->getCacheFile());
        unset($myCache);
      }
      // -- otherwise show it native and build cache --
      else
      {
        unset($myCache);

        $output .= '<div class="watermarkArea" style="padding: 1em;">';
          $output .= '<div class="watermarkCover"><p class="watermarkBgText">TOP '.$this->hitlistLimit.'</p></div>';

          if (!file_exists($this->hitlistFilename))
          {
            $output .= '<span style="color: red;">' . LNG_HITLIST_EMPTY . '</span>';
          }
          else
          {
            $myMediabase = new class_mediaBase($this->depth);

            $file = file($this->hitlistFilename);
            $grad = cntGradientFromTo('019e1a', 'd0218f', count($file));  // 00DF20 to BF0040
            unset($file);

            $hitlist = fopen($this->hitlistFilename, 'r');
            $i = 0;
            while(!feof($hitlist))
            {
              $song = fgets($hitlist, 1024);
              $song = str_replace(LFC, NULLSTR, $song);
              if (strlen($song) > 0)
              {
                $info = $myMediabase->getSongInfo($song);
                if ($info === false || !isset($info['i']) || !isset($info['t']))
                {
                  // -- not available in the current mediaBase --
                  $output .= '<div class="grpLstItmL textShadowBlur" style="text-decoration: line-through; color: #'.$grad[$i].';">'.$song.'</div>';
                }
                else
                {
                  // -- escape the apostrophe for further javascript issues --
                  $escapedInterpret = str_replace('\'', '\\\'', $info['i']);
                  $escapedTitle = str_replace('\'', '\\\'', $info['t']);

                  $output .= '<div>
                                <a class="grpLstItmL textShadowBlur" style="color: #'.$grad[$i].';" href="javascript:songAdd(\''.base64_encode($song).'\',\''.base64_encode($info['i']).'\',\''.base64_encode($info['t']).'\',\''.$info['p'].'\',\''.base64_encode($info['l']).'\',\''.$escapedTitle.'\')">
                                  <span style="float: right;">'.$escapedInterpret.'</span>'.$escapedTitle.'
                                </a>
                              </div>';
                }
              }
              $i++;
            }
            fclose($hitlist);
            unset($myMediabase);
          }
        $output .= '</div>'; // -- end of watermarkArea --

        // -- build cache for next time --
        $myCache = new class_outputCache();
        $myCache->setCachePath($this->cachePath);
        $myCache->setCacheFile(CACHE_HITLIST);
        $myCache->putCacheContent($output);
        unset($myCache);
      }

      if(!$abuildCache)
        return $output;
    }

  }
