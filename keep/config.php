<?php
  // -- This is the core config file for (almost) every possible option. --
  // -- (except accessTokens.php)
  // -- Some of them are pre-set during installation, like: $cfg_absPathToSongs, $cfg_system etc. --
  // -- Settings in section "common" can be overwritten after setup while running. --
  // -- To make this possible, an optional config-shadow-file is included, where all --
  // -- current changes are written to and used. See end of this file! --
  // -- But, there is no need to change anything special here, just use it with settings made during installation. --


  // -- ------------------ --
  // -- mediabase settings --
  // -- ------------------ --

  // -- /path/to/music/ example: /home/user/music/ --
  $cfg_absPathToSongsDefault = '/home/pi/music/';
  $cfg_absPathToSongs = $cfg_absPathToSongsDefault;

  // -- only one type at one time is supported --
  // -- default: mp3 --
  $cfg_musicFileType = 'mp3';
  // -- covers and lyrics --
  $cfg_textFileType = 'txt';
  $cfg_pictureFileType = 'jpg'; // later: if more needed, than as array e.g. png

  // -- time zone --
  // -- usually the timezone has to be set by each client who want to see the time in his certain place, --
  // -- but this is a party player and therefore we are bind to one single place where the party happens - no need to set it for each client. --
  // -- default: 'UTC' --
  $cfg_timeZone = 'Europe/Berlin'; // -- ['UTC'|'Europe/Berlin'|'Europe/London'|...] --


  // ------------------ --
  // -- common settings --
  // -- settings in this section can be overwritten in admin area temporarily --
  // ------------------ --


  // -- ------------------------------- --
  // -- beg of "todays party" code word --
  // -- could be published on the flyer --

  // -- false does disable the code word question --
  // -- default: false --
  $cfg_codeWordQuestion = false; // -- supported here: [false|'a question'] --
  $cfg_codeWordAnswer = 'an answer';

  // -- end of "todays party" code word --
  // -- ------------------------------- --

  // -- does not check if the wished nickname does already exists --
  // -- default: true (rather home use); false (better in party mode) --
  $cfg_allowMultiNickname = true;

  // -- allows more than one entry of a song in the playlist (does not impact admins) --
  // -- default: false --
  $cfg_allowMultiblePlaylistEntries = false;

  // -- generated a playlist with all played title each day --
  // -- it also creates a simple everlasting hitlist with the songfiles --
  // -- these files can normally be found under: keep/partyReport --
  // -- default: true --
  $cfg_partyReport = true;

  // -- party mode FALSE does NOT randomly pick songs and play them automatically but wait appox 15 min after last song --
  // -- if no further song is chosen the system either will be shutdown down or in case of raspbian the GPIOs turned off --
  // -- party mode false could be useful to fall a sleep or if the piMoo is idle for a few minutes to safe amplifier --
  // -- shutdown requires sudoers for sudo -u wwwrun /sbin/shutdown -h now) --
  // -- in party mode TRUE the system will not be shutdown but a randomly picked song will be played automatically if playlist is empty --
  // -- default: false --
  $cfg_partyMode = false;

  // -- limit the amount of songs in the playlist (does not impact admins) --
  // -- default: 10 --
  $cfg_maxPlaylistEntries = 10;

  // -- Allow only songs to be picked as random or returned by search if available in this array --
  // -- alternatively an empty array allowes all songs to be played --
  // -- Attention: The less genres set here, the longer the random search will take because it has to search until it found e.g. 10 relating songs!!! --
  // -- default: array() --
  $cfg_genreWhitelist = array(); // -- case sensitive! --

  // -- usually available genres: --
  $cfg_genreWhitelistTemplate = array('A Cappella','Ballad','Blues','BritPop','Classical','Dance','Disco','Gothic','Hard Rock','Heavy Metal','Indie','Instrumental','Kids',
                                      'Metal','National Folk','NDW','Oldies','Pop','Pop-Folk','Progressive Rock','Psychodelic Rock','Punk','Punk Rock',
                                      'Rock','R&B','Rock/Pop','Rock & Roll','Soundtrack','Techno','Tophits','Terror','Vocal','Other');


  // -- Songs to these genre are filtered out in search results and randomly picked songs --
  // -- examples: array('UltraStar','Spoken Word','Comedy') --
  // -- default: array() --
  $cfg_genreBlacklist = array('UltraStar','Spoken Word','Comedy'); // -- case sensitive! --


  // -- recommendaction by nane --
  // -- when running in party mode (randomly), try to pick a well rated song --
  // -- default: array() --
  $cfg_rateWhitelist = array(); // RATE3, RATE4, RATE5
  $cfg_rateBlacklist = array(); // RATE1, RATE2



  // ---------------- --
  // -- more settings --
  // ---------------- --


  // -- Allow stash functions --
  // -- default: true --
  $cfg_stash = true;

  // -- false does disable, otherwise give a path relative to the music path --
  // -- default: 'upload' --
  $cfg_songUpload = 'upload'; // -- supportet here: [false|'upload'|'anypath'] --

  // -- playlist refresh interval (less than 1 will be forced to 1) --
  // -- default: 10 --
  $cfg_playlistRefreshInterval = 10;

  // -- minimum search characters --
  // -- default: 3 --
  // -- less than 1 will be forced to 1, because 0 would make no sense --
  $cfg_minimumSearchChars = 3; // -- 3 is useful for ratings like ~3~; e.g. U2 could be found via u2* --

  // -- limit the search results for each site to avoid mobile devices break on too large HTML content --
  // -- "0" does deactivate the limit --
  // -- default: 25 --
  $cfg_maxSearchResultsPerPage = 25;
  // -- default: 2 --
  $cfg_nonMobileMultiplier = 2;

  // -- The limit of song in the everlasting hitlist --
  // -- less than 1 will be forced to 1 --
  // -- default: 10 --
  $cfg_hitlistLimit = 10;



  // -- ---------------- --
  // -- special settings --
  // -- ---------------- --

  // -- catch when defines not has been loaded previously --
  if (file_exists('defines.inc.php'))
    require_once('defines.inc.php');
      elseif (file_exists('../defines.inc.php'))
        require_once('../defines.inc.php');
          elseif (file_exists('/var/www/html/piMoo/defines.inc.php'))
            require_once('/var/www/html/piMoo/defines.inc.php');
              // -- docker --
              elseif (file_exists('/var/www/html/defines.inc.php'))
                require_once('/var/www/html/defines.inc.php');

  // -- output caching to improve the performance --
  // -- default: true --
  $cfg_cache = true;
  // -- standard cache lifetime for search results --
  // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
  // -- default: CACHELIFETIME_INFINITE --
  define('CFG_SEARCH_CACHELIFETIME',CACHELIFETIME_INFINITE); // CACHELIFETIME_NORM CACHELIFETIME_LONG CACHELIFETIME_VERYLONG CACHELIFETIME_ULTRALONG CACHELIFETIME_INFINITE
  $cfg_cachePath = 'tmp/cache/';
  $cfg_cacheHistory = 'tmpS/cacheHistory.xml';


  // -- mediabase type: MEDIABASETYPE_XML (xml) or MEDIABASETYPE_MYSQL (mySQL) --
  // -- default: MEDIABASETYPE_XML --
  $cfg_mediaBaseType = MEDIABASETYPE_XML;

  // -- only of interest if $cfg_mediaBaseType == MEDIABASETYPE_MYSQL --
  // -- database host, e.g. '127.0.0.1' or 'craft4fun.de' --
  // -- default: '127.0.0.1' --
  $cfg_mySQL_host = '127.0.0.1';
  $cfg_mySQL_port = 3306;
  $cfg_mySQL_dataBase = 'piMoo';
  $cfg_mySQL_user = 'piMoo';
  $cfg_mySQL_passwd = 'piMoo';


  // -- should normally not be changed (unless you have antoher Linux than Debian or Raspi) --
  $cfg_playerProc = 'mpg123'; // -- the binary which is a process later --
  $cfg_playerExec = '/usr/bin/'.$cfg_playerProc;

  // -- volume control up, down and fade --
  // -- values less than 0.1 will be forced to 0.1 (0.1 works well for HDMI) --
  // -- default = 3 --
  $cfg_volStep = 3;

  // -- alsa mixer control, supportet here: [ALSA_CRTL_PCM|ALSA_CRTL_HEADPHONE|ALSA_CRTL_MASTER] --
  // -- Note: You must manually edit also: /etc/triggerhappy/triggers.d/piMoo-mouse-events.conf --
  // -- default since Raspbian "Buster": ALSA_CRTL_HEADPHONE --
  // -- default since Raspbian "Bookworm": ALSA_CRTL_PCM --
  // -- For Debian native use MASTER or PCM --
  $cfg_alsacontrol = ALSA_CRTL_HEADPHONE;


  // -- should normally not be changed (there should be no reason) --
  $cfg_mediabaseFile = 'keep/mediaBase.xml';
  $cfg_feedbackFile = 'keep/feedback.xml';
  $cfg_partyReportPath = 'keep/partyReport/';
  $cfg_tmpPath = 'tmp/';
  $cfg_tmpPathStatic = 'tmpS/';
  $cfg_playerRemainTime = 'tmp/playerRemainTime';
  $cfg_playerStateFile = 'tmp/playerState';
  $cfg_playerParamFile = 'tmp/playerParam';
  // -- supportet here: [false|'tmp/configShadow.php'|'path/to/configShadow'] --
  $cfg_configShadow = 'tmp/configShadow'; // -- works even without php file extension --
  $cfg_playlistFile = 'tmpS/playlist.xml';
  // -- supportet here: [false|'keep/event.txt'|'path/to/log'] --  // -- false does disable the logging --   // -- actually a .log, but better to open inside browser --
  $cfg_logFile = 'tmpS/events.txt';
  $cfg_DOJO = 'lib/dojo1703/dojo/dojo.js'; // -- fallback: dojo1101 --


  // -- ------------------------------------------------ --
  // -- System choice (initially set while installation) --
  // -- ------------------------------------------------ --

  // -- supportet here: SYSTEM_DEBIAN, SYSTEM_RASPI (aka Raspberry PI OS) and SYSTEM_ALPINE --
  // -- default: SYSTEM_RASPI --
  $cfg_systemDefault = SYSTEM_RASPI;
  $cfg_system = $cfg_systemDefault;


  // -- --------------------------------------------------------------- --
  // -- Backend password protection (initially set while installation). --
  // -- This is default, in addition a .htaccess password protection can be setup manually --
  // -- Also used by Docker solution and piMoo api v1 legacy support --
  // -- (piMoo api v2 takes the admin access from accessTokens.php) --
  // -- if set to false, the access to the backend is open without restrictions --
  // -- default: "piMoo" (md5 hashed -> '06074627064edaeb9f6a7cc31294fdde') --
  $cfg_admin_passwdDefault = '06074627064edaeb9f6a7cc31294fdde';
  $cfg_admin_passwd = $cfg_admin_passwdDefault;



  // -- --------------------------------------- --
  // -- Flag if running inside docker container --
  // -- Also include Docker overlay configuration per each system variant ..
  // -- .. at bottom of this file --
  // -- the default is line is also detected during Docker build process and patched automatically in the asset copy! --
  // -- default: false --
  $cfg_isDocker = false;

  // -- Docker pipe-file for shutdown command in volume path, called INSIDE docker container! Must be an absolute path. --
  $cfg_dockerHostPipeShutdown = DEBIAN_WEBROOT_BASIC.'keep/PIPE/shutdown'; // -- /var/www/html/keep/PIPE/shutdown --



  // -- ---------------------------- --
  // -- hardware (Raspberry PI only) --
  // -- OFF => "off" or e.g.: "17" -> GPIO 17 which is PIN 11 --
  // -- Note: when changing the gpio, you have to change the install.sh file accordingly --
  $cfg_GPIO_HiFi = '17';
  // -- depends on which relais you want to use (Low- or High trigger) --
  $cfg_GPIO_high = '1'; // -- 1 or 0 --
  $cfg_GPIO_low = '0';  // -- vice versa --




  // -- if false no preview of a song. Also no piMoo at all (it's a license thing if piMoo serves to the internet) --
  // -- and no album picture too, of course  --
  // -- default: true --
  $cfg_allowStreaming = true;

  // -- allow extren control, so songs can be added to playlist --
  // -- default: true (should normally not be changed) --
  $cfg_allowExternControl = true;



  // -- ----------------------------------------------------- --
  // -- developer settings, do not change in normal use cases --
  // -- ----------------------------------------------------- --

  // -- debug output also in HTML --
  // -- default: false; --
  define('CFG_DEBUG',false);

  // -- saves the feedback.xml in a human readable form --
  // -- Note: this does slowdown the speed, but helps when changes has to be done manually --
  // -- default: true --
  define('CFG_HUMAN_READABLE_FEEDBACK', true);




  // -- ------------------------------- --
  // -- mqtt / playlist change notifier --
  // -- ------------------------------- --
  // -- default: localhost (should be local because public brokers requires individual namespaces somehow) --
  // $cfg_mqtt_broker_4backend = 'localhost';
  // $cfg_mqtt_broker_4frontend = $cfg_mqtt_broker_4backend; // -- something like 'pimoo.local' --
  $cfg_mqtt_broker_unified = 'localhost';
  $cfg_mqtt_port = 1883;
  define('CFG_MQTT_USER',PROJECT_NAME);
  define('CFG_MQTT_PASSWD','secret');
  define('CFG_MQTT_CLIENTID_PREFIX',PROJECT_NAME); // -- must be random. when used by client a random suffix is added --
  define('CFG_MQTT_TOPIC_PLAYLIST',CFG_MQTT_CLIENTID_PREFIX.'/playlist'); // -- e.g. piMoo/playlist --
  // -- client settings, controlled by server --
  $cfg_mqtt = false;






  // -- !!! DO NOT CHANGE THINGS FROM NOW ON !!! --
  // -- !!! DO NOT CHANGE THINGS FROM NOW ON !!! --
  // -- !!! DO NOT CHANGE THINGS FROM NOW ON !!! --
  // -- !!! DO NOT CHANGE THINGS FROM NOW ON !!! --
  // -- !!! DO NOT CHANGE THINGS FROM NOW ON !!! --

  $cfg_playerPath = dirname($cfg_playerExec) . SLASH;  // -- path can be figured out with: "which mpg123" --
  $cfg_playerRemainTime_abs = dirname($_SERVER['SCRIPT_FILENAME']).SLASH.$cfg_playerRemainTime;
  $cfg_playerParamFile_abs = dirname($_SERVER['SCRIPT_FILENAME']).SLASH.$cfg_playerParamFile;
  $cfg_playerStateFile_abs = dirname($_SERVER['SCRIPT_FILENAME']).SLASH.$cfg_playerStateFile;

  $cfg_playlistRefreshInterval = ($cfg_playlistRefreshInterval >= 1) ? $cfg_playlistRefreshInterval : 1;
  $cfg_volStep = ($cfg_volStep >= 0.1) ? $cfg_volStep : 0.1;

  $cfg_hitlistLimit = ($cfg_hitlistLimit < 1) ? 1 : $cfg_hitlistLimit;

  date_default_timezone_set($cfg_timeZone);

  // -- the enforce the session lifetime, this needs to be central --
  ini_set('session.gc_maxlifetime',SECONDS_PER_WEEK);
  ini_set('session.cookie_lifetime',SECONDS_PER_WEEK);
  if (!isset($_SESSION))
    session_start(); // @session_start();



  // -- if docker, then include docker configuration overlay --
  if ($cfg_isDocker === true)
  {
    require_once DOCKER_CONF_OVERLAY;
  }


  // -- include config shadow file if exists and wished --
  // -- MUST BE AFTER DOCKER OVERLAY --
  if ($cfg_configShadow !== false)
  {
    // -- if called by cli outside of project path like from daemons --
    $abs_configShadow = dirname(__DIR__) . SLASH . $cfg_configShadow;
    // echo "abs_configShadow: $abs_configShadow\n";

    if (file_exists($cfg_configShadow))
    {
      require_once $cfg_configShadow;
    }
    elseif (file_exists(ONEDIRUP.$cfg_configShadow))
    {
      require_once ONEDIRUP.$cfg_configShadow;
    }
    elseif (file_exists($abs_configShadow))
    {
      require_once $abs_configShadow;
    }
  }

