<?php
  // -- ------ --
  // -- API-V1 --
  // -- legacy support for piMooApp --



  // -- ------ --
  // -- API-V2 --
  // -- tokens are reserved for admins only, for common users the nickname is the "token" --


  // -- Whether it is allowed to use anonymous nickname registration with API-V2/_coreBeg.php --
  // -- Risk is, that an attacker can flood the system with nicknames! --
  // -- default: false --
  $trustedEnvironment = false;


  // -- enter your individual user/acess tokens here --
  $accessTokens = array(
    'developer'=>'delete-those-tokens-before-deployment',
    'everybody'=>'PraiseTheLord'
  );

