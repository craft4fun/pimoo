<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  $msg = LNG_ERR_UNKNOWN;

  if (!isset($_GET['value']))
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    // -- because needed in javascript environment in clientside a cookie is required --
    setcookie('str_displayZoom', $_GET['value'], strtotime('+365 days'));

    switch ($_GET['value']) // -- do not use the cookie here, it is not updated yet --
    {
      case LOW:
        {
          $msg = LNG_DISPLAY_ZOOM.ARROW.'<span style="font-weight: bold;">'.LOW.'</span>';
          break;
        }
      case MEDIUM:
        {
          $msg = LNG_DISPLAY_ZOOM.ARROW.'<span style="font-weight: bold;">'.MEDIUM.'</span>';
          break;
        }
      case HIGH:
        {
          $msg = LNG_DISPLAY_ZOOM.ARROW.'<span style="font-weight: bold;">'.HIGH.'</span>';
          break;
        }
    }
  }

  echo $msg;