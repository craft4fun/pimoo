<?php
  // -- this script forces the whole page to deliver for a mobile device ... --
  // -- ... and in this case it's the landing page for piMooHome - Demo --

  date_default_timezone_set('UTC'); // -- does not matter, no need to include all defines and config --
  if (!isset($_SESSION)) // -- session starting here is an exception as an emergency case --
    session_start();

  //$_SESSION['forceIsMobile'] = true;

  $_SESSION['isDemo'] = true;
  header('location: frontend.php');