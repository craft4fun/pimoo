<?php
  // -- special piMoo stuff --
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_playlist.php');
  require_once ('api_player.php');
  require_once ('lib/utils.php');
  require_once ('lib/class_cache.php');
  require_once ('api_layout.php');

  echo '<a href="frontend_noJS.php">'.LNG_BACK.'</a><p>';

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);



  // -- ----------------- --
  // -- begin of add song --
  if (isset($_POST['hid_count']))
  {
    $count = $_POST['hid_count'];
    for($i = 0; $i <= $count; $i++)
    {
      if (isset($_POST['btn_add'.$i]) && isset($_POST['hid_a'.$i]))
      {
        $in = (isset($_POST['hid_in'.$i])) ? $_POST['hid_in'.$i] : QUESTMARK;
        $in = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', NULLSTR, urldecode(html_entity_decode(strip_tags($in))))));

        $ti = (isset($_POST['hid_ti'.$i])) ? $_POST['hid_ti'.$i] : QUESTMARK;
        // -- dirty workaround to delete the htmlspecialchars which do not work in noJS environment --
        $ti = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', NULLSTR, urldecode(html_entity_decode(strip_tags($ti))))));

        $pt = (isset($_POST['hid_pt'.$i])) ? $_POST['hid_pt'.$i] : '-1';

        $al = (isset($_POST['hid_al'.$i])) ? $_POST['hid_al'.$i] : NULLSTR;
        $al = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', NULLSTR, urldecode(html_entity_decode(strip_tags($al))))));

        $myPlaylist = new class_playlist();
        $myPlaylist->setWishedBy($_SERVER['REMOTE_ADDR']);

        $myPlaylist->addSong($_POST['hid_a'.$i], $in, $ti, $pt, $al, $admin);
        eventLog($myPlaylist->getLastMsg() . COLON . BLANK .  $ti);
        echo $myPlaylist->getLastMsg() . COLON . BLANK .  $ti;

        unset($myPlaylist);

        break;
      }
    }
   }
   // -- end of add song --
   // -- --------------- --




  // -- is it allowed to add titles as guest ? --
  if ($cfg_allowExternControl OR $admin)
  {

    // -- ------------------------------------ --
    // -- beg of return songs by search string --
    if (isset($_POST['btn_songSearch']))
    {
      $output = NULLSTR;

      // -- remember for next search --
      setcookie('strSearch', $_POST['edt_search']);

      // -- core of this file --
      $search = strtolower($_POST['edt_search']);

      // -- cache handling --
      $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
      $myCache->setCachePath($cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_SEARCH . $search);

      // -- take it from cache --
      if ($myCache->getIsCacheFileAlive())
      {
        echo file_get_contents($myCache->getCacheFile());
        unset($myCache);
      }

      // -- otherwise show it native and build cache --
      else
      {
        unset($myCache);

        // -- ------------------------------- --
        // -- begin of the the very main core --
        // -- ------------------------------- --
        require_once ('api_mediabase.php');
        $myMediaBase = new class_mediaBase();
        $output .= makeResultItems_noJS( $myMediaBase->getSearchResult($search, 0, 0) ); // , $cfg_searchFeedback
        unset($myMediaBase);
        // -- ----------------------------- --
        // -- end of the the very main core --
        // -- ----------------------------- --


        // -- "execute" normal way --
        echo $output;
        // -- build cache for next time --
        $myCache = new class_outputCache();
        $myCache->setCachePath($cfg_cachePath);
        $myCache->setCacheFile(CACHE_ITEMS_BY_SEARCH . $search);
        $myCache->putCacheContent($output);
        unset($myCache);
        unset($output);
      }
    }
    // -- end of return songs by search string --
    // -- ------------------------------------ --


    // -- --------------------------- --
    // -- beg of return songs by user --
    if (isset($_POST['btn_userSearch']))
    {
      $output = NULLSTR;

      // -- remember for next search --
      setcookie('strSearch', $_POST['edt_search']);

      // -- core of this file --
      $search = strtolower( $_POST['edt_search'] );

      // -- cache handling --
      $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
      $myCache->setCachePath($cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_USER . $search);

      // -- take it from cache --
      if ($myCache->getIsCacheFileAlive())
      {
        echo file_get_contents($myCache->getCacheFile());
        unset($myCache);
      }

      // -- otherwise show it native and build cache --
      else
      {
        unset($myCache);

        // -- ------------------------------- --
        // -- begin of the the very main core --
        // -- ------------------------------- --
        require_once ('api_mediabase.php');
        $myMediaBase = new class_mediaBase();
        $output .= makeResultItems_noJS( $myMediaBase->getSearchResultByUser($search, 0, 0) ); // , $cfg_searchFeedback
        unset($myMediaBase);
        // -- ----------------------------- --
        // -- end of the the very main core --
        // -- ----------------------------- --


        // -- "execute" normal way --
        echo $output;
        // -- build cache for next time --
        $myCache = new class_outputCache();
        $myCache->setCachePath($cfg_cachePath);
        $myCache->setCacheFile(CACHE_ITEMS_BY_USER . $search);
        $myCache->putCacheContent($output);
        unset($myCache);
        unset($output);
      }
    }
    // -- end of return songs by search string --
    // -- ------------------------------------ --


    // -- --------------------------- --
    // -- beg of return songs by date --
    if (isset($_POST['btn_byDate']))
    {
      $output = NULLSTR;

      // -- core of this file --
      $days = $_POST['btn_byDate'];


      // -- cache handling --
      $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
      $myCache->setCachePath($cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_DATE . $days);

      // -- take it from cache --
      if ($myCache->getIsCacheFileAlive())
      {
        echo file_get_contents($myCache->getCacheFile());
        unset($myCache);
      }

      // -- otherwise show it native and build cache --
      else
      {
        unset($myCache);

        // -- ------------------------------- --
        // -- begin of the the very main core --
        // -- ------------------------------- --
        require_once ('api_mediabase.php');
        $myMediaBase = new class_mediaBase();

        // -- calculate the days --
        $days2 = intval(substr($days, 1)); // -- N60 -> 60 --
        $daysAsSeconds = $days2 * 86400; // 24 * 60 * 60;
        $diff = time() - $daysAsSeconds;  // -- today minus x days --
        $past = date('Ymd', $diff);
        //$past = $diff;
        $today = date('Ymd', time());
        //$today = time();

        $output .= makeResultItems_noJS( $myMediaBase->getSongsByDate($past, $today, 0, 0) );

        unset($myMediaBase);
        // -- ----------------------------- --
        // -- end of the the very main core --
        // -- ----------------------------- --


        // -- "execute" normal way --
        echo $output;
        // -- build cache for next time --
        $myCache = new class_outputCache();
        $myCache->setCachePath($cfg_cachePath);
        $myCache->setCacheFile(CACHE_ITEMS_BY_DATE . $days);
        $myCache->putCacheContent($output);
        unset($myCache);
        unset($output);
      }
    }
    // -- end of return songs by date --
    // -- --------------------------- --


    // -- ----------------------------- --
    // -- beg of return songs by random --
    if (isset($_POST['btn_byRandom']))
    {
      $output = NULLSTR;
       require_once ('api_mediabase.php');
       $myMediaBase = new class_mediaBase();
      $rand = intval(substr($_POST['btn_byRandom'], 1));
       echo makeResultItems_noJS( $myMediaBase->getRandomSongs($rand) );
       unset($myMediaBase);
    }
    // -- end of return songs by random --
    // -- ----------------------------- --



  }





  // -- ------------------ --
  // -- beg of next song --
  if (isset($_POST['btnNext']) && $admin)
  {
    $msg = 'trying pkill -USR1 mpg123<br>';
    //$msg .= (string)system ('pkill -USR1 mpg123');

    $myPlayer = new class_player();
    $myPlayer->cmdNext();
    unset($myPlayer);

    echo $msg;
  }
  // -- ---------------- --
  // -- end of next song --




  // -- ---------------- --
  // -- beg of kill song --
  if (isset($_POST['hid_count']))
  {
    $count = $_POST['hid_count'];
    for($i = 0; $i <= $count; $i++)
    {
      if (isset($_POST['btnKill'.$i]))
      {
        $myPlaylist = new class_playlist();
        $myPlaylist->removeSong($i, $admin);
        $msg = $myPlaylist->getLastMsg();
        unset($myPlaylist);

        echo '<p>' . $msg . '</p>';
        break;
      }
    }
   }
   // -- end of kill song --
   // -- ---------------- --



   // -- -------------------- --
   // -- beg of move top song --
   if (isset($_POST['hid_count']))
   {
    $count = $_POST['hid_count'];
     for($i = 0; $i <= $count; $i++)
     {
       if (isset($_POST['btnMoveTop'.$i]))
       {
         $myPlaylist = new class_playlist();
         $msg = $myPlaylist->moveTopSong($i, $admin);
         unset($myPlaylist);

         echo '<p>' . $msg . '</p>';
         break;
      }
    }
  }
  // -- end of move top song --
  // -- -------------------- --


