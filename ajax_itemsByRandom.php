<?php
  // -- special piMoo stuff --
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('lib/utils.php');
  require_once ('lib/class_cache.php');
  require_once ('api_layout.php');


  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  // -- is it allowed to add titles as guest ? --
  if ($cfg_allowExternControl || $admin)
  {
    // -- standard check, if the required parameter is given --
    if (!isset($_GET['val']))
    {
      echo cntErrMsg(LNG_ERR_PARAM_MISSING);
    }
    else
    {
      $val = $_GET['val'];

      // -- ------------------------------- --
      // -- begin of the the very main core --
      // -- ------------------------------- --

      // -- !!! this script has not cache handling, of course !!! --

      require_once ('api_mediabase.php');
      $myMediaBase = new class_mediaBase();
      $intermediateResult = $myMediaBase->getRandomSongs(intval(substr($val, ONEINT)));
      $output = makeResultItems($intermediateResult);


      // -- offer the possibilty to get the next random results --
      $output .= '
      <div class="resultNavi" style="float: right;">
        <div class="grpLstItmL" title="'.LNG_NEXT_RESULTS.'">
          <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="javascript:callRand(\''.$val.'\');">'.HTML_GREATER.'</a>
        </div>
      </div>';


      $output .= '<div style="clear: both;"></div>'; // -- in longer results the navigators are inside this div --
      // -- ------------------------------------------------- --
      // -- the following content comes clearly after results --
      // -- ------------------------------------------------- --


      // -- ---------------------------------------------- --
      // -- beg of offer possibilty to add all to playlist --
      if ($admin)
      {
        //-- get nickame --
        $wishedBy = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : $_SERVER['REMOTE_ADDR'];
        // -- result file reference --
        $resultFile = $cfg_cachePath . 'lastResult' . UNDSCR . 'random_' . $wishedBy;
        // -- use api_playlist --
        require_once ('api_playlist.php');
        $myPlaylist = new class_playlist;
        $myPlaylist::stat_lastResult_pushSongs($resultFile, $intermediateResult, $wishedBy);
        // -- output "button" --
        $output .= '<a class="linkNice grpLstContR" style="float: left; margin-left: 1em;" href="javascript:addResultToPlaylist(\''.$resultFile.'\')">'.LNG_ADD_ALL_TO_PLAYLIST.'</a>';
      }
      // -- end of offer possibilty to add all to playlist --
      // -- ---------------------------------------------- --



      // unset($tmp); -- removed 2022-01-01 --
      unset($myMediaBase);
      // -- ----------------------------- --
      // -- end of the the very main core --
      // -- ----------------------------- --

      // -- "execute" normal way --
      echo $output;
    }
  }
  // that is done in frontend
  //else
    //echo '<div style="margin: 5px;">the song navigation has been disabled by admin</div>';

