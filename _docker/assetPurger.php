#!/usr/bin/php
<?php

  // -- ------------------------- --
  // -- PURPOSE: Strip PHP files! --
  // -- ------------------------- --

  // -- call like this --
  // -- ./phPurger.php -o [path/to/piMoo_project/]


  // -- name: phPurger --
  // -- autor: Patrick Höf --
  // -- created on: 2012/01/18 --
  // -- last change: see history --
  // -- description: Release a whole project by simply running this script --
  // --              do some extra stuff at the end --
  // -- benefits: Avoids easy reverse engineering --
  // -- example call: ./phPurger.php --current --confuse --extra
  // -- or          : ./phPurger.php --current --confuseMore --extra
  // -- typical call: ./phPurger.php --confuse --extra

  // -- Version: 0.3 --

  // -- history --
  // -- 2020/06/25 Fork of iMuh project --
  // -- 2022-11-19 Adapt to assetPurger for Docker image build
  // -- 2022-12-10 Patch base config.php regarding $cfg_isDocker


  // -- known bugs --
  // -- 01 the deleteDoubleSlashComment function cannot handle a construction like:    bla blu'; // killMe="killer'.$i.'"
  // -- 02 because it only handles line per line --


  // -- todo --
  // -- 01 deleteDoubleSlashComment() work with all lines as one string to fix the known bug: function cannot handle a construction like:    bla blu'; // killMe="killer'.$i.'"
  // -- 02 deleteDoubleSlashComment() should detect being inside <?php, so it would not affect pure HTML like http:// --
  // -- 03 kill hash key comment - to be complete -- (may be obsolete, because a # could become deprecated)




  // -- ------------------------------------------- --
  // -- begin of pre settings -> make your settings --
  // -- ------------------------------------------- --

  // -- work path --
  define ('PURGESPACEPREDEFINED', '/path/to/project_code'); // to be sure

  define ('MINFUNCTIONLENGTH' , 4); // -- e.g. gTrl or longer isRun --
  define ('MINClASSLENGTH' , 4); // -- e.g. TXML --


  define('SLOWMOTION', 500000); // -- 500000µs -> 0.5s --


  // -- file type to handle --
  $fileTypes = array(
                      'php'
  );

  // -- phPurger will NOT touch these directories (relative to PURGESPACE) --
  $blacklistDir = array(
                          //geht net, bin ja da drin 'piMoo/aPaddy',
                          'lib/dojo1101',
                          'lib/dojo1703',
                          'lib/getid3',

  );


  // -- phPurger will NOT touch these unique files (MUST be with relative path) --
  $blacklistUniqueFile = array(
                          'cli_common.php',
                          'cli_player.php',
                          'admin/cli_importer.php',
                          'cli_ext_song_addRandom.php',
                          'keep/config.php',
                          'lib/utils_ext1.js.php',
                          'lib/progressbar.class.php',
                          'lib/class_cli_progressbar.php',

  );

  // -- phPurger will NOT touch these files (whereever they occurs) --
  $blacklistFile = array(
                          'exampleFile.php',
                          'index.php', // -- actually it should be above in blacklistDir "_private" --
                          'patchConfig.php', // -- actually it should be above in  blacklistDir "_private" --
  );



  // -- ------------------- --
  // -- end of pre settings --
  // -- ------------------- --



  // -- -------------------------------------------------------- --
  // -- do only touch from now on if you know what you are doing --
  // -- -------------------------------------------------------- --


  // -- optionally helpers --
  // -- kill recursively all files and sub folders --
  function delTree($path, $aDirToo = false)
  {
    $path = rtrim($path, '/').'/';
    $handle = opendir($path);
    while(false !== ($file = readdir($handle)))
    {
      if($file != '.' and $file != '..')
      {
        $fullpath = $path.$file;
        if(is_dir($fullpath))
          delTree($fullpath, $aDirToo);
        else
          unlink($fullpath);
      }
    }
    closedir($handle);
    if ($aDirToo)
      rmdir($path);
  }


  define ('YES', 'yes');


  // -- ----------------------------- --
  // -- begin of arguments and values --
  // -- ----------------------------- --

  $knownArgs = array(//'./phPurger.php', // -- this is always the first argument --
                     '--debug', '-d',
                     '--slow', '-s',
                     '--step',
                     '--silent',
                     '--help', '-h',
                     '--operate', '-o'
  );

  // -- init the standalone arguments --
  $debug = false;
  $slowMotion = false;
  $stepByStep = false;
  $silent = false;
  $help = false;

  // -- init the arguments followed by values --
  $operate = false;
  $i = 0;
  $xo = 0;      // -- found --operate --

  if (isset($_SERVER['argv']))
  {
    foreach ($_SERVER['argv'] as $arg)
    {
      // -- standalone arguments --
      if ($arg == '--debug'   || $arg == '-d')                           $debug = true; // -- MUST be the first here --
      if ($arg == '--slow'    || $arg == '-s')                           $slowMotion = true;
      if ($arg == '--step')                                              $stepByStep = true;
      if ($arg == '--silent'  || $arg == '--scanF' || $arg == '--scanC') $silent = true;
      if ($arg == '--help'    || $arg == '-h')                           $help = true;

      // -- arguments with following values --
      if ($arg == '--operate' || $arg == '-o')
      {
        $operate = true;
        $xo = $i;
      }

      // -- tricky: the check for valid argument --
      if (
               ( $i == 1 && !in_array($arg, $knownArgs) )                     // -- for the first --
            OR ( $i >= 1 && !in_array($arg, $knownArgs) && !($i == $xo + 1) ) // -- all the rest --
         )
        die ("ABORTED - unknown argument: '$arg'\n");



      $i++;
    }


    // -- handle the parameters --
    if ($operate)
    {
      if (isset($_SERVER['argv'][$xo + 1]) && $_SERVER['argv'][$xo + 1] != '')
        $operateOn = $_SERVER['argv'][$xo + 1];
      else
        die("ABORTED - Value for --operate / -o must not be empty\n");
    }
  }


  if ($help)
  {
    echo "\n";
    echo "--debug / -d    -> do some debug output.\n";
    echo "--slow / -s     -> run in slow motion mode.\n";
    echo "--step          -> run step by step with ENTER.\n";
    echo "--silent        -> disables default echo.\n";
    echo "--help / -h     -> this help.\n";
    echo "--operate / -o  -> followed by the full path to operate on - Therefore it MUST be the last argument.\n";
    echo "\nWithout any arguments phPurger will do standard execution the the predefined folder (PURGESPACEPREDEFINED).\n";
    die("\n");
  }

  // -- --------------------------- --
  // -- end of arguments and values --
  // -- --------------------------- --




  // -- !!! ATTENTION, that would mean HERE !!! --> define('PURGESPACE', __DIR__ . '/');

  // -- check if purge space path is available at all --
  if ($operate && $operateOn != '')
    define('PURGESPACE', $operateOn); //  . '/'
  else
    define('PURGESPACE', PURGESPACEPREDEFINED);

  if (!is_dir(PURGESPACE))
    die('ABORTED - "' . PURGESPACE . "\" is not available\n");



  // -- no safety warning for piMoo !!! --




  // -- script intern declarations --
  // quickDirty OK $VariableEndChar = array(' ', '=', '<', '>', '+', '-', '/', '*', '^', '.', ',', ':', ';', '(', ')', '[', ']', '{', '}', '?', '!', '%', '&', '#', '\'', '"', '~', '|', "\t"); //"\n" // -- a new line inside a variable is not allowed, so not required --
  $VariableEndChar = array(' ', '=', '<', '>', '+', '-', '/', '\\', '*', '^', '.', ',', ':', ';', '(', ')', '[', ']', '{', '}', '?', '!', '%', '&', '#', '\'', '"', '~', '|', "\t", "\n"); // -- but here, because it works with "bigLine". ok for Quick and dirty too --
  $PHPKeyWords = array('$this', '$self', '$_session', '$_server', '$globals', '$_env', '$_get', '$_post', '$_cookie' ,'$_request', '$_files');

  $FunctionEndChar = array(' ', '(', "\t", "\n");
  $FunctionsEndCharInvalid = array('=', '<', '>', '+', '-', '/', '\\', '*', '^', '.', ',', ':', ';', ')', '[', ']', '{', '}', '?', '!', '%', '&', '#', '\'', '"', '~', '|');
  $PHPKeyWordsFunc = array('__construct', '__destruct', '__call', '__callStatic', '__get', '__set', '__isset', '__unset', '__sleep', '__wakeup', '__toString', '__invoke', '__set_state', '__clone'
                           , 'init'
  );
  // -- init --
  $foundFunctions = array();

  $ClassesEndChar = array(' ', '{', "\t", "\n");
  $ClassesEndCharInvalid = array('=', '<', '>', '+', '-', '/', '\\', '*', '^', '.', ',', ':', ';', '(', ')', '[', ']', '}', '?', '!', '%', '&', '#', '\'', '"', '~', '|');
  $PHPKeyWordsClass = array();
  // -- init --
  $foundClasses = array();



  // -- begin of helpers --
  function _isEndChar($aStr)
  {
    global $VariableEndChar;
    return (in_array($aStr, $VariableEndChar)) ? true : false;
  }
  function _isNotPHPKeyWord($aStr)
  {
    global $PHPKeyWords;
    return (!in_array(strtolower($aStr), $PHPKeyWords)) ? true : false;
  }

  function _isEndCharFunctions($aStr)
  {
    global $FunctionEndChar;
    return (in_array($aStr, $FunctionEndChar)) ? true : false;
  }
  function _isNotPHPKeyWordFunctions($aStr)
  {
    global $PHPKeyWordsFunc;
    return (!in_array(strtolower($aStr), $PHPKeyWordsFunc)) ? true : false;
  }

  function _isEndCharClasses($aStr)
  {
    global $ClassesEndChar;
    return (in_array($aStr, $ClassesEndChar)) ? true : false;
  }
  function _isNotPHPKeyWordClasses($aStr)
  {
    global $PHPKeyWordsClass;
    return (!in_array(strtolower($aStr), $PHPKeyWordsClass)) ? true : false;
  }


  // -- sort the array by length --
  function _sortArrayByLength(&$array_of_data)
  {
    $number_of_entries = count($array_of_data);

    for($i = 0; $i < $number_of_entries; $i++)
    {
      $current_entry = $array_of_data[$i];
      $current_entry_length = strlen($current_entry);
      $appendable_entry_length = str_pad($current_entry_length, 10, '0', STR_PAD_LEFT);
      $array_of_data[$i] = $appendable_entry_length . " /|\/|\/|\ " . $array_of_data[$i];
    }

    rsort($array_of_data);

    for($i = 0; $i < $number_of_entries; $i++)
    {
      $current_entry = $array_of_data[$i];
      $current_entry_explosion = explode(" /|\/|\/|\ ", $current_entry);
      $array_of_data[$i] = $current_entry_explosion[1];
    }
  }
  // -- end of helpers --




  // -- convert linefeed into blank to avoid constuctions like <?phpecho ... (after a linefeed has been kicked out) --
  function addBlankBeforeLinefeed($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'performing addBlankBeforeLinefeed'."\n";

    // -- take whole file as one string --
    $bigLine = file_get_contents($aFile);
    $bigLine = str_replace("\n", " \n", $bigLine); // chr(32) . "\n" OR " \n"
    //$bigLine = str_replace(chr(10), chr(32) . chr(10), $bigLine); // chr(32) . "\n" OR " \n"
    if (unlink($aFile))
      file_put_contents($aFile, $bigLine);

    unset($bigLine);
  }

  // -- delete line until end from behind the comment on --
  // -- function detects if a // is inside a string --
  function deleteDoubleSlashComment($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'performing deleteDoubleSlashComment'."\n";

    $entry = '';
    // -- open file --
    $fp = @fopen($aFile, 'r') or die ("!!! can't read file: $aFile. STOP !!!\n"); // w+
    // -- read in file line for line --
    while($line = fgets($fp))
    {

      // -- init inline scan for the whole current line --
      $stackC1 = 0;
      $stackC2 = 0;
      $foundBy1 = 0; // do not delete this and use $stackC1 for it, because it could be found at 0 -> and then the whole mimic fuxxs up --
      $foundBy2 = 0;

      $lineLength = strlen($line);

      // -- begin of in line --
      for ($i = 0; $i < $lineLength; $i++)
      {

        // -- find begin of 'string' --
        if ($line[$i] == '\'' && $stackC1 == 0 && $stackC2 == 0)
        {
          $foundBy1 = $i;
          $stackC1++;
          continue;
        }

        // -- find end of 'string' --
        if ($stackC1 > 0 && $line[$i] == '\'')
        {
          // -- rule 2 --> check for \' so the ' has to be ignored!
          if ($line[$i-1] != '\\') // -- also ungleich \
            $stackC1--;
        }
        // -- find end with when 'string is not closed in the current line --
        if ($stackC1 > 0 && $i >= $lineLength - 1)
        {
          $foundCopen1 = false;
          // -- is a // in the rest of line --
          for ($j = $foundBy1; $j < $lineLength; $j++)
          {
            if ($line[$j] == '/' && $line[$j+1] == '/')
            {
              $entry .= substr($line, 0, $j);
              $foundCopen1 = true;
              break;
            }
          }
          // -- if not --
          if ($foundCopen1 == false)
          {
            $stackC1--;
          }
        }



        // -- find begin of "string" --
        if ($line[$i] == '"' && $stackC2 == 0 && $stackC1 == 0)
        {
          $stackC2++;
          continue;
        }

        // -- find end of "string" --
        if ($stackC2 > 0 && $line[$i] == '"')
        {
          if ($line[$i-1] != '\\') // -- also ungleich \   -> hier auch?!
           $stackC2--;
        }
        // -- find end with when "string is not closed in the current line --
        if ($stackC2 > 0 && $i >= $lineLength - 1)
        {
          $foundCopen2 = false;
          // -- is a // in the rest of line --
          for ($j = $foundBy2; $j < $lineLength; $j++)
          {
            if ($line[$j] == '/' && $line[$j+1] == '/')
            {
              $entry .= substr($line, 0, $j);
              $foundCopen2 = true;
              break;
            }
          }
          // -- if not --
          if ($foundCopen2 == false)
            $stackC2--;
        }



        // -- finally do something --
        if ($stackC1 == 0 && $stackC2 == 0)
        {
          if ($line[$i] == '/' && $line[$i+1] == '/')
          {
            // -- cut until the comment --
            $entry .= substr($line, 0, $i)  . "\n";
            break;
          }
          if ($i >= strlen($line) - 1)
          {
            // -- take the whole line --
            $entry .= substr($line, 0);
            break;
          }
        }

      }

  // -- would work, if there was no 'http://' --
  //    $pos = strpos($line, '//'); if ($pos !== false) $entry .= substr($line, 0, $pos); else $entry .= $line;

    }
    // --close file --
    fclose($fp);
    unlink($aFile);
    file_put_contents($aFile, $entry);
  }


  // -- delete content of a block comment --
  $newContentBlockComment = '';
  $offsetVar = 0;
  function deleteBlockComment($aFile)
  {
    global $debug;
    global $silent;

    $doAgain = false;
    global $newContentBlockComment;
    global $offsetVar;

    if ($newContentBlockComment == '')
    {
      if (!$silent) echo 'performing deleteBlockComment'."\n";
      $bigLine = file_get_contents($aFile);
    }
    else
    {
      $bigLine = $newContentBlockComment;
    }
    if ($bigLine)
    {
      $lineLength = strlen($bigLine);
      $stackC = false;
      $ComBeg = 0;
      $ComEnd = 0;
      for ($i = 0; $i < $lineLength; $i++)
      {
        // -- find begin of block comment --
        $posCom = strpos($bigLine, '/*', $i);
        if ($stackC == false && $posCom !== false)
        {
//           $checkNext = $bigLine[$posCom + strlen('/*')];
//           if ($checkNext == ' ' || $checkNext == "\n" || $checkNext == "\t")
          {
            $stackC = true;
            $ComBeg = $posCom + strlen('/*') - 1;
            $i = $ComBeg - 1; // !!!

//             $checkNext2 = substr($bigLine, $ComBeg, 1);
//             while ($checkNext2 == ' ' || $checkNext2 == "\n" || $checkNext2 == "\t")
//             {
              $ComBeg++;
              $i++;
              $checkNext2 = substr($bigLine, $ComBeg, 1);
//            }
          }
          continue;
        }

        // -- find end of block comment --
        if ($i + 1 < $lineLength)
        {
          $endChar = $bigLine[$i] . $bigLine[$i + 1];
          if ($stackC == true && ($lineLength > $i + 1) && $endChar == '*/')
          {
            //echo "found end of block comment at $i\n";
            $stackC = false;
            $ComEnd = $i;
          }
          else
          {
            //echo "still indside block comment\n";
            continue;
          }
        }
        else
          break 1;


        // -- finally do it --
        if ($stackC == false && $ComBeg > 0)
        {
          $curCom = substr($bigLine, $ComBeg, $ComEnd - $ComBeg);
          if (strlen($curCom) >= 0)
          {
            if ($debug) echo $curCom . "\n";
            $newContentBlockComment = substr_replace($bigLine, '', $ComBeg - 2, $ComEnd - $ComBeg + 4);
            $doAgain = true;
            $offsetVar = $ComBeg; // - 1 ??
            break 1;
          }
        }
      }
    }

    if ($newContentBlockComment == '')
      $newContentBlockComment = $bigLine; // -- avoids an empty file --
    // -- recursive call so every comment is replaced only were it has been found --
    if ($doAgain)
      deleteBlockComment($aFile);
    else
    {
      if (unlink($aFile))
        file_put_contents($aFile, $newContentBlockComment);
      unset($bigLine);
      $newContentBlockComment = '';
      $offsetVar = 0;
    }
  }



  // -- delete linux line feed --
  function deleteLinuxLineFeed($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'performing deleteLinuxLineFeed'."\n";


    $entry = '';
    $fp = @fopen($aFile, 'r') or die ("!!! can't read file: $aFile. STOP !!!\n"); // w+

    // -- datei zeilenweise auslesen --
    $FirstLine = false;
    while($line = fgets($fp))
    {

      // -- never touch first line because of stuff like those: (scripts would not be valid otherwise) --
      // -- #!/usr/bin/php --
      // -- <?xml version="1.0" encoding="UTF-8"?_> --
      if ($FirstLine == false)
      {
        $entry .= $line;// BULLSHIT: . "\n"; // -- to be save add a linefeed --
        $FirstLine = true;
        continue;
      }

// -- variante 1 doesn't work --
//      $lineLength = strlen($line);
//      for ($i = 0; $i < $lineLength; $i++)
//        if ($line[$i] == "\n" && $line[$i - 1] != ' ')
//          so in der Art $entry .= str_replace("\n", " \n", $line); // -- "\n" chr(10); --


// -- solved with variante 2 addBlankBeforeLinefeed() --
      // -- delete the linefeed only if there is at minimum one blank before to avoid errors like this : --
      // --   <?php --
      // -- if (CFG_PREVIEW) --
      // -- will return <?phpif (CFG_PREVIEW) --
      // -- to solve this, use addBlankBeforeLinefeed() --

      $lineXX = str_replace("\r", '', $line); // -- windows compatible -- // -- "\r" chr(13); --
      //$lineXX = $line; // -- not windows compatible --
      $entry .= str_replace("\n", '', $lineXX); // -- "\n" chr(10); --

    }
    // --close file --
    fclose($fp);
    unlink($aFile);
    file_put_contents($aFile, $entry);

  }



  // -- convert tabstop into blank --
  function convertTabstopToBlank($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'performing convertTabstopToBlank'."\n";

    // -- take whole file as one string --
    $bigLine = file_get_contents($aFile);
    $bigLine = str_replace("\t", ' ', $bigLine);
    if (unlink($aFile))
      file_put_contents($aFile, $bigLine);

    unset($bigLine);
  }


  // -- strip double blank spaces --
  function stripBlankSpace($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'performing stripBlankSpace'."\n";

    // -- take whole file as one string, that must be done recursive !!! --
    $bigLine = file_get_contents($aFile);
    $check = '  ';
    while($check !== false)
    {
      $bigLine = str_replace('  ', ' ', $bigLine);
      $check = strpos($bigLine, '  ');
    }
    if (unlink($aFile))
      file_put_contents($aFile, $bigLine);

    unset($bigLine);
 }

  // -- eliminate  php close and open --
  // -- solves the issue that empty space left after doing all other routines. Very important at "header-using" files like action or http_download --
  function eliminatePHPCloseOpen($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'performing eliminate PHP close and open'."\n";

    // -- take whole file as one string, that must be done recursive !!! --
    $bigLine = file_get_contents($aFile);
    $check = '?> <?php';
    while($check !== false)
    {
      $bigLine = str_replace('?> <?php', '', $bigLine);
      $check = strpos($bigLine, '?> <?php');
    }
    if (unlink($aFile))
      file_put_contents($aFile, $bigLine);

    unset($bigLine);
  }


  // -- eliminate  php open and close --
  function eliminatePHPEmptyOpenClose($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'performing eliminate PHP open and close'."\n";

    // -- take whole file as one string, that must be done recursive !!! --

    // -- round 1 --
    $bigLine = file_get_contents($aFile);
    $check = '<?php ?>';
    while($check !== false)
    {
      $bigLine = str_replace('<?php ?>', '', $bigLine);
      $check = strpos($bigLine, '<?php ?>');
    }
    if (unlink($aFile))
      file_put_contents($aFile, $bigLine);

    unset($bigLine);


    // -- round 2 --
    $bigLine = file_get_contents($aFile);
    $check = '<?php  ?>';
    while($check !== false)
    {
      $bigLine = str_replace('<?php  ?>', '', $bigLine);
      $check = strpos($bigLine, '<?php  ?>');
    }
    if (unlink($aFile))
      file_put_contents($aFile, $bigLine);

    unset($bigLine);

  }


  // -- find all files --
  function dir_rekursiv($directory, $aAnalyze = false)
  {
    global $debug;
    global $slowMotion;
    global $stepByStep;
    global $silent;
    global $fileTypes;
    global $blacklistDir;
    global $blacklistFile;
    global $blacklistUniqueFile;

    $handle =  opendir($directory);
    while ($file = readdir($handle))
    {
      if ($file != '.' && $file != '..' && substr($file, 0, 1) != '.')
      {
        // -- if directory entry is a directory --
        $currentFolder = str_replace(PURGESPACE, '', dirname($directory . $file));
        // -- and not in the directory blacklist --
        if (in_array($currentFolder, $blacklistDir) == false)
        {
          if (is_dir($directory . $file))
          {
            // -- Erneuter Funktionsaufruf, um das aktuelle Verzeichnis auszulesen --
            dir_rekursiv($directory . $file . '/', $aAnalyze);
          }
          else
          {
            // -- not in the unique file blacklist and not in the "anywhere" file blacklist --
            $currentUniqueFile = str_replace(PURGESPACE, '', $directory . $file);
            if (in_array($currentUniqueFile, $blacklistUniqueFile) == false && in_array($file, $blacklistFile) == false)
            {
              $ext = pathinfo($file, PATHINFO_EXTENSION);
              if (in_array(strtolower($ext), $fileTypes))
              {
                if (!$silent) echo str_replace(PURGESPACE, './', $directory . $file) . "\n";
  // -- ------------------------------------------------------------- --
  // -- begin -> comment out what you don't want to do with the files --

                // -- DON'T CHANGE THE ORDER of calls here unless you know what you are doing --

                // -- best all these three or none of them --
                addBlankBeforeLinefeed($directory . $file);
                deleteBlockComment($directory . $file);
                deleteDoubleSlashComment($directory . $file); // -- !!! convertLineFeedToBlank run before is mandatory for this function !!! --
// took out for debug at 2022-12-22: deleteLinuxLineFeed($directory . $file); // -- !!! deleteDoubleSlashComment run before is mandatory for this function !!! --
                // -- ------------------------------- --

                convertTabstopToBlank($directory . $file); // -- should be called before stripBlankSpace() --
                stripBlankSpace($directory . $file);
                eliminatePHPCloseOpen($directory . $file); // -- must come after the commen stuff --
                eliminatePHPEmptyOpenClose($directory . $file); // -- must come after the commen stuff --


  // -- end --------------------------------------------------------- --
  // -- ------------------------------------------------------------- --

                // -- slow motion --
                if ($slowMotion && !$stepByStep) // -- because if it is running with "step by step" it makes no sense to wait --
                  usleep(SLOWMOTION);
                // -- run step by step --
                if ($stepByStep)
                  fgets(STDIN);

              }
            }
          }
        }
      }
    }
    closedir($handle);
  }

  dir_rekursiv(PURGESPACE);



  // -- ------------ --
  // -- single tasks --
  // -- ------------ --

  /**
   * Patch base config.php regarding $cfg_isDocker
   */
  function patchBaseConfig($aFile)
  {
    global $debug;
    global $silent;
    if (!$silent) echo 'Patch base config.php regarding $cfg_isDocker'."\n";

    // -- take whole file as one string --
    $bigLine = file_get_contents($aFile);
    $bigLine = str_replace('$cfg_isDocker = false;', '$cfg_isDocker = true;', $bigLine);
    if (unlink($aFile))
      file_put_contents($aFile, $bigLine);

    unset($bigLine);
  }

  patchBaseConfig(PURGESPACE.'keep/config.php');

