#!/bin/bash

# -- build content --
##echo -n "PREPARE ASSET CONTENT..." has its own output!
./assetPrep.sh
##echo 'OK'

# -- detect architecture --
echo -n "DETECTING RASPBERRY PI OS..."
if [ -f '/etc/rpi-issue' ];
then
  TAG='arm64'
  echo "YES ($TAG)"
  SYSTEM='Raspi'
  ARGS='--restart always --device /dev/input/event0 --privileged  -v /sys/class/gpio/:/sys/class/gpio/'
else
  TAG='amd64'
  echo "NO ($TAG)"
  SYSTEM='Debian'
  ARGS=''
fi

# -- build --
echo -n "BUILDING IMAGE..."
docker build -t craft4fun/pimoo:${TAG} --build-arg SYSTEM=${SYSTEM} .
echo 'OK'


# -- cleanup content --
echo -n "CLEANING ASSET CONTENT..."
rm -R asset/*
echo 'OK'



CONTAINER_NAME='pimoo'

# -- stop and remove previously started container, if so ... --
echo "STOP AND REMOVE PREVOIUSLY STARTED CONTAINER..."
docker container stop ${CONTAINER_NAME} && docker container rm ${CONTAINER_NAME}
echo 'OK'


# - create volume once (permanent mediabase and feedback) --
# echo "CREATING VOLUME..."
# docker volume create vol-pimoo
# echo 'OK'


# -- run (realistic, with volume) --
echo "RUN CONTAINER..."
docker run --pull=never -d --name ${CONTAINER_NAME} \
  -p 8080:80 \
  -v /home/pi/music/:/music/ \
  -v vol-pimoo:/var/www/html/keep/ \
  --device /dev/snd \
  -e ADM_PASSWD='secret' \
  ${ARGS} craft4fun/pimoo:${TAG}
echo 'OK'


docker container ls -a


# -- connect to running container to insight in logs and so on --
# docker attach pimoo
# -- or execute command by --
# docker exec -it pimoo sh
# -- leave with
# CTRL+P followed by CTRL+Q

# -- run existing container --
# docker start pimoo
