#!/bin/bash

# -- History --
# 2022-02-15 Fork of piMoo-Installer script to init piMoo inside a docker image
# 2022-05-20 Add music path
# 2024-11-22 Use second parameter from dockerfile


# -- --------------- --
# -- set some basics --

fMusicPath='/music/'
fWWWUser='www-data'
#fWebroot='/var/www/html/'


# -- -------------------- --
# -- get passed arguments --

fSystem=$1 # -- Raspi or Debian --
fWebroot=$2


# -- ------------------------------------- --
# -- install required third party software --

apt update && apt upgrade -y

apt install apt-utils sudo --assume-yes

apt install mpg123 alsa-utils pulseaudio --assume-yes
# use command "speaker-test" to check whether sound is working

# -- if base image is "php:7.4-apache", the following line IS NOT required: --
apt install apache2 php8.2 php8.2-cli php8.2-mysql php8.2-xml php8.2-mbstring --assume-yes # -- prepared for future use: php-curl --
# -- php8.2-apache2 required since multistage build ? --

# -- required for mqtt playlist refresh --
apt install inotify-tools --assume-yes


# -- Raspberry Pi support --
if [ "${fSystem}" == "Raspi" ];
then

  # -- mouse control --
  apt install triggerhappy --assume-yes
  # cp more/piMoo-mouse-events.conf /etc/triggerhappy/triggers.d/ # -- is done by Dockerfile "COPY" --
  # service triggerhappy restart -> this must be done in the running container of course

fi


# -- useful tools --
#apt install mc vim --assume-yes
##apt install nano --assume-yes # -- nano is a bit too simple and performs bad on large one-line-xmls --

apt clean


# -- link containers origin php binaries to common debian ones --
# -- if base image is "php:7.4-apache", the following line IS required: --
#ln -s /usr/local/bin/php /usr/bin/php

# -- -------------------------------------------------------- --
# -- Create path inside container where host music is mounted --
mkdir ${fMusicPath}


# -- ------------------------------ --
# Copy project files to webroot ... --

# -- is done by Dockerfile "COPY" --


# -- ----------------------------- --
# -- ... then set owner and rights --

chown -R ${fWWWUser} ${fWebroot}
chmod -R 700 ${fWebroot}

# -- remove default index.html (required since multistage build?) --
#rm ${fWebroot}index.html

# -- ---------------------------------------------------- --
# -- Adding system user "www-data" (Debian) group "audio" --

usermod -G audio ${fWWWUser}


# -- making some things more comfortable --
#echo 'alias la="ls -lah"' >> /etc/bash.bashrc


exit 0
