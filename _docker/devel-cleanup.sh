#!/bin/bash

# -- detect architecture --
echo -n "DETECTING RASPBERRY PI OS..."
if [ -f '/etc/rpi-issue' ];
then
  fTAG='arm64'
  echo "YES ($fTAG)"
else
  fTAG='amd64'
  echo "NO ($fTAG)"
fi

CONTAINER_NAME='pimoo'

docker container stop ${CONTAINER_NAME}
docker container rm ${CONTAINER_NAME}
docker container ls -a

docker volume rm vol-pimoo

docker image rm craft4fun/pimoo:$fTAG
#docker image prune --force
docker image prune
docker image ls -a
