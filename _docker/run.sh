#!/bin/bash

#https://docs.docker.com/config/containers/multi-service_container/

# -- build mediabase if not already existing (also possible via webinterface later) --
if [ ! -f "${WEBROOT}mediaBase.xml" ];
then
  echo 'No mediabase found, creating new one!'
  if [ -f '/music/feedback.xml' ];
  then
    echo 'Found feedback.xml - cool - syncing it with mediabase!'
    cp /music/feedback.xml ${WEBROOT}keep/feedback.xml
    chmod 777 ${WEBROOT}keep/feedback.xml # -- fix Permission denied in /var/www/html/api_feedback.php on --
    #chown www-data ${WEBROOT}keep/feedback.xml # -- actually this should be enough, to be checked! --
  fi

  cd ${WEBROOT}admin/
  ${WEBROOT}admin/cli_importerXML.php
  chmod 777 ${WEBROOT}keep/mediaBase.xml # -- make newly created mediaBase.xml mutable for apache2 afterwards --
  #chown www-data ${WEBROOT}keep/mediaBase.xml # -- actually this should be enough, to be checked! --
fi

# -- start directly when container is launched --
cd /etc/init.d/
/etc/init.d/piMood start


# -- Raspberry Pi support --
# -- read by previously in Dockerfile set env var --
# -- detecting keyboard inside container doesn't work: if ls /dev/input/by-id/ | grep -q 'kbd'; then --
if [ "${SYSTEM}" == "Raspi" ];
then
  echo 'Raspberry Pi detected, trying to start triggerhappy daemon for mouse and keyboard control support if desired..'

  if [ -z "${TRIGGERHAPPY}" ]; # -- default is mouse support when no triggerhappy arg is passed --
  then
    echo "using piMoo-mouse-events.conf"
    cp /etc/triggerhappy/triggers.d/piMoo-mouse-events /etc/triggerhappy/triggers.d/piMoo-mouse-events.conf
    service triggerhappy start
  else
    if [ "${TRIGGERHAPPY}" == "keyboard" ];
    then
      echo "using piMoo-keyboard-events.conf"
      cp /etc/triggerhappy/triggers.d/piMoo-keyboard-events /etc/triggerhappy/triggers.d/piMoo-keyboard-events.conf
      service triggerhappy start
    fi
    # if [ "${TRIGGERHAPPY}" == "disabled" ];
    # then
      # -- not required to do something here --
    # fi
  fi
fi


# -- set backend admin password if provided --
if [ -z "${ADM_PASSWD}" ];
then

  echo 'no backend password provided, so I keep default'

else

  echo 'setting provided password as backend protection'
  # -- double magic ;) --
  md5=$(echo -n "${ADM_PASSWD}" | md5sum | awk '{print $1}')
  # replace md5 hashed "piMoo" '06074627064edaeb9f6a7cc31294fdde' with "$ADM_PASSWD"
  # in ${WEBROOT}keep/configDockerOverlay.php with sed
  sed -i "s/06074627064edaeb9f6a7cc31294fdde/$md5/g" ${WEBROOT}keep/configDockerOverlay.php

fi



# -- start main process "webinterface" of this container in a blocking manner instead of background
cd /usr/sbin/
/usr/sbin/apache2ctl -D FOREGROUND

# -- required ? to be sure! --
cd ${WEBROOT}

exit 0
