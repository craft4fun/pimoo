<?php

  interface interface_player
  {

    /**
     * returns the current player state
     * @return string
     */
    function getPlayerState();

    /**
     * returns the current player state
     * useable without object
     * @return string
     */
    static function static_getPlayerState($aDepth = NULLSTR);

    /**
     * startes the cli_player and cli_common script
     * @return string
     */
    function cmdTurnOn();

    /**
     * stoppes the cli_player and cli_common script at all
     */
    function cmdTurnOff();

    /**
     * next song command
     * if currently paused, it will be continued and then skipped
     */
    function cmdNext();

    function cmdVolumeUp();

    function cmdVolumeDown();

    function cmdPause();

    function cmdPlay($volUp = true);

    function cmdRewind($aSkipSeconds = -30);

    function cmdForward($aSkipSeconds = 30);




    // -- -------------- --
    // -- foreign helper --

    function GPIO_ON();

    function GPIO_OFF();

  }