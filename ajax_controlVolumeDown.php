<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_player.php';

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    echo LNG_ERR_ONLYADMIN;
  }
  else
  {
    $myPlayer = new class_player();
    $exec = $myPlayer->cmdVolumeDown();
    unset($myPlayer);

    $pos = strpos($exec, '%]');
    $output = substr($exec, $pos - 3, 4);
    echo 'turn down...<br>' . trim($output,' [');
  }

