<?php

  define('PROJECT_NAME','piMoo');
  define('PROJECT_VERSION','V0.31.0 "A²rea"');


  // -- basics --
  define('LINUX','linux');
  define('WINDOWS','windows');
  define('NO','no');
  define('YES','yes');
  define('OFF','off');
  define('ON','on');
  // define('OK','ok');
  // define('NOK','nok');
  define('UTF8','utf-8');

  define('MEDIABASETYPE_XML','mbt_xml');
  define('MEDIABASETYPE_MYSQL','mbt_mysql');


  // -- DEBIAN and RASPBERRY PI OS are the operating systems --
  // -- RasPi is used, when some additional hardware features like GPIOs are available, .. --
  // -- .. but the underlaying operating system is just RASPBERRY PI OS which is derived from DEBIAN --
  // -- ALPINE is the operating system, but just used when running inside a Docker container and some .. --
  // -- .. certain adjustments are required like a different webroot or package handling --

  define('SYSTEM_DEBIAN','Debian');
  define('SYSTEM_RASPI','Raspi');

  // define('DOCKER','Docker'); // -- for Docker container --
  define('SYSTEM_ALPINE','Alpine'); // -- Best for small docker images --

  define('DEBIAN_WEBROOT_BASIC','/var/www/html/'); // -- /var/www/html/ --
  define('ALPINE_WEBROOT_BASIC','/var/www/localhost/htdocs/');
  define('DEBIAN_WEBROOT_FULL','/var/www/html/'.PROJECT_NAME.'/'); // -- /var/www/html/piMoo/ --


  define('DOCKER_CONF_OVERLAY','configDockerOverlay.php');


  //define('GPIO17','gpio17');
  define('RASPI_CPU_MAX_TEMP',70);


  // -- player types --
  define('PLAYER_TYPE_CLASSIC','plt_classic'); // -- via script magic --
  define('PLAYER_TYPE_REMOTE','plt_remote');   // -- using remote mode and named pipes to control --


  // -- player states --
  define ('PS_UNKNOWN', 'unknown');
  define ('PS_OFF', 'off');
  define ('PS_PAUSED', 'paused');
  define ('PS_PLAYING', 'playing');

  // -- sessions --
  // mal schaun: define('BOOL_ADMIN','bool_admin'); define('INT_DL_LEVEL','int_DL_level'); define('STR_NICKNAME','str_nickname');

  // -- An MP3 frame always represents 26ms of audio, regardless of the bitrate. 1/0.026 = 38.46 frames per second. --
  define('FRAMES_PER_SEC',38.46);


  // -- alsa mixer control: Depends on sound-system configuration, but usually PCM --
  define('ALSA_CRTL_PCM','PCM');
  define('ALSA_CRTL_HEADPHONE','Headphone');
  define('ALSA_CRTL_MASTER','Master');


  define('SECONDS_PER_HOUR',3600);
  define('SECONDS_PER_DAY',86400);
  define('SECONDS_PER_WEEK',604800);
  define('SECONDS_PER_MONTH',2592000);
  define('SECONDS_PER_YEAR',31600800);

  // -- cache --
  define('CACHELIFETIME_NORM',60); // 60 -> 1min
  define('CACHELIFETIME_LONG',300); // 300 -> 5min
  define('CACHELIFETIME_VERYLONG',1800); // 1800 -> 30min
  define('CACHELIFETIME_ULTRALONG',SECONDS_PER_DAY); // 86400 -> 24h
  define('CACHELIFETIME_INFINITE',SECONDS_PER_YEAR); // 31600800 -> one year // -- don't worry about cache, it will be cleaned up on a (re-)import --

  define('CACHE_HITLIST','piMoo_hitlist');

  //define('CACHE_ITEMS_BY_GROUP','piMoo_ibg_'); // -- NOT REALLY USEFUL --
  define('CACHE_ITEMS_BY_SEARCH','piMoo_ibs_');
  define('CACHE_ITEMS_BY_USER','piMoo_ibu_');
  define('CACHE_ITEMS_BY_DATE','piMoo_ibd_');
  //define('CACHE_SONG_INFO','piMoo_si_'); // -- never because of feedback in song info --

  define('CACHE_RSLT_OFFSET','_o');
  define('CACHE_RSLT_MAX','_m'); // -- in classes often called "aLimit" --
  define('CACHE_RSLT_DAMLEV','_dl'); // -- damerau levenstein tolerance --
  define('CACHE_RSLT_CASESENSITIVITY','_cs'); // -- case sensitivity --
  define('CACHE_RSLT_CUTBLANK','_cb'); // -- cut blank --
  define('CACHE_RSLT_LANG','_l'); // -- language --

  define('CACHE_RSLT_VIEWFORM','_v'); // -- view form for the following --
  define('CACHE_RSLT_BLOCKS','B'); // -- block --
  define('CACHE_RSLT_TABLE','T'); // -- table --
  define('CACHE_RSLT_ROW','R'); // -- rows for iMoo --
  define('CACHE_RSLT_JSON','J'); // -- JSON for REST --


  // -- mediaBase stuff --
  /* -- NOT REALLY USEFUL --
  define('GRP_ALL','ALL');
  define('GRP_UNKNOWN','@');
  define('GRP_NUMBER','NUM');
  */
  define('GRP_RAND10','R10');
  define('GRP_RAND20','R20');
  // -- how many days is a song a newcomer --
  define('GRP_NEWCOMERDAYS14','N14'); // -- 2 weeks --
  define('GRP_NEWCOMERDAYS30','N30'); // -- 1 month --
  define('GRP_NEWCOMERDAYS60','N60'); // -- 2 months --
  define('GRP_NEWCOMERDAYS90','N90'); // -- 3 months --
  define('GRP_NEWCOMERDAYS180','N180'); // -- half a year --
  define('GRP_NEWCOMERDAYS365','N365'); // -- 1 year -- // until I found the bug why the first 15 are not in, the whole year is out


  define('LOW','low');
  define('MEDIUM','medium');
  define('HIGH','high');


  // -- xml entries --
  /* -- NOT REALLY USEFUL --
  define('XML_GRP','g');
  */
  define('XML_INDEX','id');
  define('XML_STRDITEM','i'); // -- this should keep, if wanna change, turn on brain before because all mediabase scripts must be changed --
  define('XML_BY','b');

//   define('XML_INTERPRET','i');
//   define('XML_TITLE','t');
  // and so on..?


  // -- common --
  define('WELCOMETIMEOUT','750'); // -- in milli seconds, default: 750 --

  define('COL_DOCKER','#086dd7;');


  define('NULLSTR','');
  define('NULLINT',0);
  define('ONEINT',1);
  define('EXCLMARK','!');
  define('QUESTMARK','?');
  define('AMPERSAND','&');
  define('PERCENT','%');
  define('DOT','.');
  define('DDOT','..');
  define('COLON',':');
  //define('DCOLON','::');
  define('SLASH','/');
  define('DSLASH','//');
  //define('BSLASH','\\'); // -- NEVER: trouble with phPurge.php --confuse --
  define('BLANK',' ');
  define('BLANK_HTML','&nbsp;');
  define('UNDSCR','_'); // -- underscrore --
  define('DASH','-');
  define('EQSIGN','='); // -- equal sign --
  define('SEMICOLON',';');
  define('COMMA',',');
  define('TILDE','~');
  define('PLUS_SIGN','+');
  define('CURDIR','./');
  define('ONEDIRUP','../');
  define('TWODIRUP','../../');
  define('HTML_LESS','&lt;');
  define('HTML_GREATER','&gt;');

//   define('CSVDLMT', SEMICOLON); // -- Obviously Windows Excel likes it most ";" --
//   define('CSVDLMT2', COMMA); // -- important in auth_levelcheck (ROLES), many others and obviously Marios Parammanager likes it most "," --
//   define('CSVDLMT3', "\t"); // -- in combination with Excel files and unicode --

  define('LFC', "\n"); // -- line feed console --
  define('LFH','<br>'); // -- line feed HTML --

  define('JSON_FILE_EXT','.json');


  define('TOKEN','token');
  //define('PERMS','perms');

  //define('DEVEL','devel');
  define('ADMIN','admin');
  define('USER','user');


  // define('DIR_TEMP','tmp/'); // -- must be used from config instead! $cfg_tmpPath --
  define('DIR_CACHE','cache/');
  define('DIR_KEEP','keep/');
  define('DIR_NICKNAMES','nicknames/');
  define('DIR_STASH','stash/');
  define('DIR_PIMOOREVERSE','iMoo/'); // -- comes from the legacy idea --

  define('NICK_AUTO','_auto_');
  define('NICK_TRIGGER','_trigger_');


  define('ISSUE_PLAYERSTATE','playerState');
  define('ISSUE_BTNSTART','btnStart');
  define('ISSUE_BTNSTOP','btnStop');
  define('ISSUE_ADMINCONTROLBAR','adminControlBar');

  // -- time zones --
  define('TZ_UTC','UTC');


  // -- ------------------ --
  // -- begin of languages --

  // -- supported languages must be added here --
  define('LANG_EN','EN');
  define('LANG_EN_PLAIN','english');
  define('LANG_DE','DE');
  define('LANG_DE_PLAIN','deutsch');


  define('STR_LANG','lang');
  define('ARROW',' &rarr; ');
  define('EMO_SAD','&#128542;'); // DISAPPOINTED FACE


  // -- include "language" message file --
  $lang = (isset($_COOKIE[STR_LANG])) ? $_COOKIE[STR_LANG] : LANG_EN;
   switch ($lang)
   {
     case LANG_EN:
    {
      include ('messages_'.LANG_EN.'.php');
      break;
    }
     case LANG_DE:
    {
      include ('messages_'.LANG_DE.'.php');
      break;
    }
  }

  // -- end of languages --
  // -- ---------------- --


  // -- search options --
  // define('SO_NONE',NULLSTR);
  define('SO_INTERPRET','~i~');
  define('SO_TITLE','~t~');
  define('SO_ALBUM','~a~'); // -- note: in internal code the typical char is l (lowercase L)--
  define('SO_GENRE','~g~');
  define('SO_COMMENT','~c~');




  // -- rating / voting --
  define('RATE1','~1~');
  define('RATE2','~2~');
  define('RATE3','~3~');
  define('RATE4','~4~');
  define('RATE5','~5~');

  define('RATE1_HUMAN',LNG_BAD);
  define('RATE2_HUMAN',LNG_TOLERABLY);
  define('RATE3_HUMAN',LNG_GOOD);
  define('RATE4_HUMAN',LNG_GREAT);
  define('RATE5_HUMAN',LNG_EXCELLENT);

  define('RATE1_DINGBAT','&#129326;'); // -- kotz: 129326 --
  define('RATE2_DINGBAT','&#128078;'); // -- thumb down: 128078
  // symbols for good emotion: &#129304; &#128076; &#128077; &#128079;
  define('RATE3_DINGBAT','&#128077;'); // -- former: &#128527; --
  define('RATE4_DINGBAT','&#128076;'); // -- former: &#128521; --
  define('RATE5_DINGBAT','&#128079;'); // -- former: &#128522; --

  define('SYM_CLEAN','CE'); // -- CE -> clear entry! Former writing hand: &#9997; --
  define('SYM_GO','&#128269;'); // -- tick: &#10003; Eyes: &#128064; --
  define('SYM_USER','&#128064;'); // -- former: &#9786; Sunglasses: &#128526;--
  define('SYM_STASH','&#128204;'); // -- S -> Stash; Pin: &#128204; --

  //define('ARROW_LEFT_DINGBAT','&#8656;'); // more: https://www.amp-what.com/unicode/search/left%20arrow

  //  $rating = array('~1~','~2~','~3~','~4~','~5~');
  //  $ratingHuman = array('bad','tolerably','good','great','excellent');

  define('COMMENT_DINGBAT','&#128172;'); // speak bubble: 128172 1F4AC




  // -- to be compatible with my good old Dreamplayer --
  define('VALUEOPERATOR_INI','ValueOperator.ini');
  define('FEEDBACK','feedback');
  define('VALUEOP','valueop');

  define('RATEDP1','+');
  define('RATEDP2','++');
  define('RATEDP3','+++');
  define('RATEDP4','++++');
  define('RATEDP5','+++++');




  // -- "enumeration" for damerau levenshtein distance --
  define('DLD_OFF', 0);
  define('DLD_LOW', 2);
  define('DLD_MEDIUM', 4);
  define('DLD_HIGH', 6);
  $DLD = array(DLD_OFF => LNG_OFF, DLD_LOW => LNG_LOW, DLD_MEDIUM => LNG_MEDIUM, DLD_HIGH => LNG_HIGH);




  // -- JSON return codes --
  define('JSON_RC','RC');              // -- used inside the response --
  define('JSON_RC_STATE','state');     // -- used inside the response --
  define('JSON_RC_ADMIN','admin');     // -- used inside the response --
  define('JSON_RC_ADDINFO','addInfo'); // -- additional info e.g. byCache --
  define('JSON_RC_NICKNAME','nickname');


  define('JSON_RC_OK',0); // -- everythings fine, no errors --
  define('JSON_RC_GENERAL_ERR',98);
  define('JSON_RC_UNDEF_ERR',99);

  //define('JSON_RC_BYCACHE',1); // -- additional info everythings fine, no errors, but fetched by cache --
  define('JSON_RC_NO_ADMIN',2);  // -- like: LNG_ERR_ONLYADMIN --
  define('JSON_RC_PARTYPOOPER',3);  // -- partypooper because no nickname given --

  define('JSON_RC_FILE_NOT_FOUND_OR_WRITEPROTECTED',7);

  define('JSON_RC_UNKNOWN_ID',10);
  define('JSON_RC_UNKNOWN_VALUE',11);
  define('JSON_RC_NO_MATCHING_VALUE',12);      // -- also tokens --
  define('JSON_RC_RESOURCE_IN_USE',13);        // -- e.g. nickname --
  define('JSON_RC_REQUIRED_PARAM_MISSING',23); // -- in relation with: define('E0_0023', 'E0+0023 Required parameter(s) missing'); --
  define('JSON_RC_TOKEN_MISSING',24);          // -- access token --
  define('JSON_RC_HASH_FAILURE',25);           // -- in relation with: define('E0_0025', 'E0+0025 Hash failure'); --
  define('JSON_RC_HASH_MISSING',26);           // -- in relation with: define('E0_0026', 'E0+0025 Hash missing'); --

  define('JSON_RC_FAILED_SONG_ADD_PLAYLIST',30);



  define('ANTI_DOS_DELAY',0);
