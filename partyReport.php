<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  require_once ('lib/utils.php');

  require_once ('api_mediabase.php');
  require_once ('api_playlist.php');
  require_once ('api_feedback.php'); // needed ?


  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();

  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">'.LNG_PARTY_REPORT.'</div>';

    echo '<span><a class="linkNaviNice" style="float: right;"href="index.php">'.LNG_BACK_TO_.PROJECT_NAME.'</a></span>';
    echo '<span><a class="linkNaviNice" href="myMooMore.php">'.LNG_BACK_TO_.LNG_MORE.'</a></span>';

  echo '</div>';

?>
<div style="margin-top: 2em;">

  <?php
    if (!isset($_GET['file']))
    {
      echo LNG_ERR_PARAM_MISSING;
    }
    else
    {
      echo  '<div class="plInfoRow modernCenterBackground" style="text-align: center; margin-bottom: 1em; color: red;">';

        if ($admin)
          echo '<a class="linkNice" style="float: right;" href="javascript:addHistoryPlaylistToCurrentPlaylist(\''.basename($_GET['file']).'\')">'.LNG_ADD_ALL_TO_PLAYLIST.'</a>';

        echo basename($_GET['file'], '.xml');

      echo  '</div>';

      // -- Attention: Use the actual unique main frame hulk ID "frame_hulk" where all ajax called stuff is pumped into, here the search result. --
      echo '<div id="frame_hulk">';
        $playlist = simplexml_load_file($_GET['file']);
        foreach ($playlist as $song)
        {
          //$escapedInterpret = str_replace('\'', '\\\'', $song->i);
          $escapedTitle = str_replace('\'', '\\\'', $song->t);

          echo '<div class="playListRowL boxShadow">';

            echo '<a class="grpLstItmR" href="javascript:songGetInfo(\''.base64_encode($song->a).'\')"><span style="text-align: center;" class="grpLstContR">i</span></a>';

            echo '<a class="grpLstItmR" href="javascript:songAdd(\''.base64_encode($song->a).'\',\''.base64_encode($song->i).'\',\''.base64_encode($song->t).'\',\''.$song->p.'\',\''.base64_encode($song->l).'\',\''. $escapedTitle.'\')">';

              echo '<div class="listItemInterpret">' . $song->i . '</div>';
              echo '<div class="listItemTitle">' . $song->t . '</div>';
              echo '<div class="listItemRemainTime">' . $song->s . '</div>'; // -- time stamp --
              echo '<div class="listItemWishedBy">' . $song->w . '</div>'; // -- wished by --

            echo '</a>';
          echo '</div>';
        }
      echo '</div>'; // -- end of frame_hulk --
    }
  ?>
</div>
<?php
  echo makeBottom();
  echo makeHTML_end();
