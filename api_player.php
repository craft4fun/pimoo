<?php
  require_once('defines.inc.php');
  require_once('lib/utils.php');

  require('api_playerInterface.php');

  class class_player implements interface_player
  {
    // -- stores the last msg of any action to have more information than a simple true or false boolean --
    private $lastMsg = NULLSTR;
    private $fc = 7; // -- fadecount --    $fc = 7;
    private $gpioHasBeenInit = false;
    //nope: $this->playerState = PS_UNKNOWN;
    private $depth = NULLSTR;
    private $playerStateFile = NULLSTR;
    private $system = NULLSTR;
    private $volStep = NULLINT;


    function __construct($aDepth = NULLSTR)
    {
      global $cfg_playerStateFile;
      global $cfg_system;
      global $cfg_volStep;

      $this->depth = $aDepth;
      $this->playerStateFile = $this->depth . $cfg_playerStateFile;
      $this->system = $cfg_system;
      $this->volStep = $cfg_volStep;
    }


    private function setLastMsg($lastMsg)
    {
      $this->lastMsg = $lastMsg;
    }

    public function getLastMsg()
    {
      return $this->lastMsg;
    }


    private function _setPlayerState($aState)
    {
      //$this->playerState = $aState;
      file_put_contents($this->playerStateFile, $aState);
    }


    function getPlayerParam($aPath, $aKeep = false)
    {
      global $cfg_playerParamFile;

      $result = NULLSTR;

      if (!file_exists($aPath.$cfg_playerParamFile))
      {
        // -- actially this is not a problem, if the pipe does exists. Nobody set a command before - so what --
        //eventLog('failed to load playerPipe file when reading player param', $this->depth);
      }
      else
      {

        $param = file_get_contents($aPath.$cfg_playerParamFile);
        // -- bring in a defines form form, e.g.: " --skip 2001 " --
        // -- the following removes blanks and linefeeds --
        $result = BLANK . trim($param, " \n") . BLANK;

        // -- cleanup param --
        if (!$aKeep)
        {
          file_put_contents($aPath.$cfg_playerParamFile, NULLSTR);
        }
      }

      return $result;
    }

    function pushPlayerParam($aParam)
    {
      global $cfg_playerParamFile;

      $result = file_put_contents($this->depth.$cfg_playerParamFile, $aParam);

      return $result;
    }

    /**
     * returns the current player state
     * @return string
     */
    function getPlayerState()
    {
      if (file_exists($this->playerStateFile))
      {
        return trim(file_get_contents($this->playerStateFile));
      }
      else
      {
        return PS_UNKNOWN;
      }
    }

    /**
     * returns the current player state
     * useable without object
     * @return string
     */
    static function static_getPlayerState($aDepth = NULLSTR)
    {
      global $cfg_playerStateFile;
      if (file_exists($aDepth.$cfg_playerStateFile))
      {
        return trim(file_get_contents($aDepth.$cfg_playerStateFile));
      }
      else
      {
        return PS_UNKNOWN;
      }
    }

    /**
     * return the current remain player time in full seconds
     * @param string $depth
     * @return boolean or integer
     */
    static function static_getRemainTimeInSeconds($depth = NULLSTR)
    {
      global $cfg_playerRemainTime_abs;
      $result = false;
      if (file_exists($depth.$cfg_playerRemainTime_abs))
      {
        $result = trim(file_get_contents($depth.$cfg_playerRemainTime_abs));
      }
      return $result;
    }

    /**
     * pushes the current or calculated remain time seconds to file
     * @param integer $seconds
     * @param string $depth
     * @return boolean
     */
    static function static_updateRemainTimeInSeconds($seconds, $depth = NULLSTR)
    {
      global $cfg_playerRemainTime_abs;
      $result = false;
      if (file_exists($depth.$cfg_playerRemainTime_abs))
      {
        $result = file_put_contents($depth.$cfg_playerRemainTime_abs, $seconds);
      }
      return $result;
    }


    private function _fadeVolDown($aSleepTime = 30000) // before 50000
    {
      global $cfg_alsacontrol;

      // -- fade out before next song --
      for ($i = $this->fc; $i > 0; $i--) // -- for now: drill the wood at the thinest place ;) --
      {
        // -- Raspbian on Raspberry Pi --
        if ($this->system == SYSTEM_RASPI)
        {
          exec("amixer set $cfg_alsacontrol".BLANK.(string)($this->volStep * 100) . '-');
        }

        // -- For normal systems --
        if ($this->system == SYSTEM_DEBIAN)
        {
          exec("amixer -c 0 set $cfg_alsacontrol".BLANK.(string)$this->volStep.'dB-');
        }

        usleep($aSleepTime);
      }
    }


    private function _fadeVolUp($aSleepTime = 10000)
    {
      global $cfg_alsacontrol;

      // -- "fade in" -> set before loudness immediately --
      for ($i = 0; $i <= $this->fc; $i++)
      {
        // -- Raspbian on Raspberry Pi --
        if ($this->system == SYSTEM_RASPI)
        {
          exec("amixer set $cfg_alsacontrol".BLANK.(string)($this->volStep * 100) . '+');
        }

        // -- For normal systems --
        if ($this->system == SYSTEM_DEBIAN)
        {
          exec("amixer -c 0 set $cfg_alsacontrol".BLANK.(string)$this->volStep.'dB+');
        }

        usleep($aSleepTime);
      }
    }




    /**
     * startes the cli_player and cli_common script
     * @return string
     */
    function cmdTurnOn()
    {
      if ($this->getPlayerState() == PS_OFF OR $this->getPlayerState() == PS_UNKNOWN)
      {
        $this->_setPlayerState(PS_PLAYING);
        return cntAsynchCommand('/etc/init.d/piMood run');
      }
    }


    /**
     * stoppes the cli_player and cli_common script at all
     */
    function cmdTurnOff()
    {
      // -- this should work without conditions to solve emergency cases --
      //if ($this->getPlayerState() != PS_OFF)

      $this->_setPlayerState(PS_OFF);

      $result = NULLSTR;

      // -- fade out before stopping --
      $this->_fadeVolDown(250000);

      // -- ------------- --
      // -- begin of core --
      // -- ------------- --
      $result .= cntAsynchCommand('/etc/init.d/piMood break');
      sleep(1); // -- otherwise the sound rises up hearable --
      // -- ----------- --
      // -- end of core --
      // -- ----------- --

      // -- "fade in" -> set before loudness immediately --
      $this->_fadeVolUp(1000);

      return $result;
    }





    /**
     * next song command
     * 1. if currently paused, it will be continued and then skipped (PS_PAUSED)
     * 2. if called by "skip" do not touch volume at all ($aVolNoTouch)
     */
    function cmdNext($aVolNoTouch = false)
    {
      global $cfg_playerProc;

      $result = false;

      if (
               $this->getPlayerState() == PS_UNKNOWN
            || $this->getPlayerState() == PS_OFF
          )
      {
        $this->setLastMsg(LNG_PLAYER_MUST_BE_RUNNING);
      }
      else
      {
        if (!$aVolNoTouch)
        {
          // -- default --
          $volDown = true;
        }
        else
        {
          // -- if called by "skip" --
          $volDown = false;
        }

        // -- if paused, play again --
        if ($this->getPlayerState() == PS_PAUSED)
        {
          $volDown = false;
          $this->cmdPlay(false);
          usleep(10000);
        }

        // -- --------------------- --
        // -- begin of the standard --
        if ($this->getPlayerState() != PS_PAUSED)
        {

          // -- fade out before next song --
          if ($volDown)
          {
            $this->_fadeVolDown(250000);
          }


          // -- ------------- --
          // -- begin of core --
          // -- ------------- --

          // -- that would be double --
          //    $myPlaylist = new class_playlist();
          //    $myPlaylist->removeSong($_GET['index']);
          //    $myPlaylist->getLastMsg();
          //    unset($myPlaylist);
          exec("pkill -USR1 -x $cfg_playerProc"); // would be possible :) -> exec('/etc/init.d/piMood next');
          if ($this->system == SYSTEM_RASPI)
          {
            sleep(1);
          }
          if ($this->system == SYSTEM_DEBIAN)
          {
            usleep(500000);
          }
          // -- ----------- --
          // -- end of core --
          // -- ----------- --

          // -- "fade in" -> set before loudness immediately --
          if (!$aVolNoTouch)
          {
            $this->_fadeVolUp(100000);
          }

          $result = true;
        }
        // -- end of the standard --
        // -- ------------------- --
      }

      return $result;
    }




    function cmdVolumeUp()
    {
      global $cfg_alsacontrol;

      $msg = NULLSTR;

      // -- Raspbian on Raspberry Pi --
      if ($this->system == SYSTEM_RASPI)
      {
        $msg .= (string)exec("amixer set $cfg_alsacontrol".BLANK.(string)($this->volStep * 100) . '+');
      }

      // -- For normal systems --
      if ($this->system == SYSTEM_DEBIAN)
      {
        $msg .= (string)exec("amixer -c 0 set $cfg_alsacontrol".BLANK.(string)$this->volStep.'dB+');
      }

      return $msg;
    }

    function cmdVolumeDown()
    {
      global $cfg_alsacontrol;

      $msg = NULLSTR;

      // -- Raspbian on Raspberry Pi --
      if ($this->system == SYSTEM_RASPI)
      {
        $msg .= (string)exec("amixer set $cfg_alsacontrol".BLANK.(string)($this->volStep * 100) . '-');
      }

      // -- For normal systems --
      if ($this->system == SYSTEM_DEBIAN)
      {
        $msg .= (string)exec("amixer -c 0 set $cfg_alsacontrol".BLANK.(string)$this->volStep.'dB-');
      }

      return $msg;
    }





    function cmdPause()
    {
      global $cfg_playerProc;

      $result = false;

      // -- only if state isn't already active --
      if ($this->getPlayerState() != PS_PAUSED)
      {
        $this->_setPlayerState(PS_PAUSED);

        $this->_fadeVolDown();

        // -- ------------- --
        // -- begin of core --
        // -- ------------- --

        exec ("pkill -STOP -x $cfg_playerProc");
        exec ('pkill -STOP -x cli_common.php'); // -- pause also calculating remain time --
        usleep(500000); // -- otherwise the sound rises up hearable --

        // -- ----------- --
        // -- end of core --
        // -- ----------- --

        $result = true;
      }

      return $result;
    }


    function cmdPlay($volUp = true)
    {
      global $cfg_playerProc;

      $result = false;

      // -- only if state isn't already active --
      if ($this->getPlayerState() != PS_PLAYING)
      {
        $this->_setPlayerState(PS_PLAYING);

        // -- ------------- --
        // -- begin of core --
        exec("pkill -CONT -x $cfg_playerProc");
        exec('pkill -CONT -x cli_common.php'); // -- continue also calculating remain time --
        // -- end of core --
        // -- ----------- --

        if ($volUp)
        {
          $this->_fadeVolUp();
        }

        $result = true;
      }

      return $result;
    }



    private function cmdSkip($aSkipSeconds)
    {
      global $cfg_playerExec;
      global $cfg_playerRemainTime;

      $result = false;

      // -- only if state isn't already active --
      if ($this->getPlayerState() != PS_PLAYING)
      {
        $this->setLastMsg(LNG_SKIP_ONLY_ON_PLAY);
      }
      else
      {
        /*
         An MP3 frame always represents 26ms of audio, regardless of the bitrate.
         1/0.026 = 38.46 frames per second.

         Hmm, it's strange but no one answer the question properly. I've been investigating, here's the formula:
         Frame length (in ms) = (samples per frame / sample rate (in hz)) * 1000
         The typical MP3 (an MPEG Layer III, version 1) has 1152 samples per frame and the sample rate is (commonly) 44100 hz.
         So (1152 / 44100) * 1000 = 26,122449 ms per frame.
         Notice the frame length (time) does not depend on the bitrate.
         */

        if (!file_exists($this->depth . $cfg_playerRemainTime))
        {
          eventLog('failed to load current remaintime from remainTimePipe', $this->depth);
          $this->setLastMsg(LNG_ERR_UNKNOWN_FUNNY);
        }
        else
        {
          require_once 'keep/config.php';
          require_once 'api_playlist.php';
          $myPlaylist = new class_playlist($this->depth);

          // -- works fine without --
          //$this->cmdPause();

          // -- get current song ... --
          $onAirSong = $myPlaylist->getWholePlaylist(true);
          if (isset($onAirSong))
          {
            $playTime = parseHumanMinSecStringToSeconds($onAirSong[0]['p']);

            // -- 1. determine current frame of playing song and init all requirements --
            $remainTime = file_get_contents($this->depth . $cfg_playerRemainTime);

            $currentTime = $playTime - $remainTime;
            $newTime = $currentTime + $aSkipSeconds;

            //if ( $newTime < 0 || $newTime >= $playTime )
            if ( $newTime < 5 || $newTime >= $playTime - 5) // -- to be save --
            {
              $this->setLastMsg(LNG_SKIP_TIME_TO_SHORT);
            }
            else
            {
              $newRemainTime = $playTime - $newTime;
              $newFrame = floor( $newTime * FRAMES_PER_SEC );

            /*
              // ... and add it again ... --
              $myPlaylist->setWishedBy($onAirSong[0]['w']);
              $myPlaylist->addSong(
                $onAirSong[0]['a'],
                $onAirSong[0]['i'],
                $onAirSong[0]['t'],
                $onAirSong[0]['p'],
                $onAirSong[0]['l'],
                true
              );
              // -- .. on top --
              $myPlaylist->moveBottomTopSong(true);
            */
              // -- alternatively have a new function for this --
              $myPlaylist->duplicateOnAirInCue();

              // -- push player parameter --
              if ($this->pushPlayerParam('--skip ' . $newFrame))
              {
                usleep(100000);

                // -- 3. load same song x frame later --
                $this->cmdNext(true);

                usleep(100000);
                if ($myPlaylist->pushPlayerRemainTime($newRemainTime))
                {
                  $result = true;
                }
              }
            }
          }
        }
      }
      return $result;
    }



    /**
     * skip rewind
     * @see interface_player::cmdRewind()
     */
    function cmdRewind($aSkipSeconds = -30)
    {
      $this->cmdSkip($aSkipSeconds);
    }

    /**
     * skip forward
     * @see interface_player::cmdForward()
     */
    function cmdForward($aSkipSeconds = 30)
    {
      $this->cmdSkip($aSkipSeconds);
    }




    // -- -------------- --
    // -- foreign helper --

    function GPIO_ON()
    {
      global $cfg_isDocker;
      global $cfg_GPIO_HiFi;
      global $cfg_GPIO_high;
      //global $cfg_GPIO_low;

      $result = NULLSTR;

      if ($this->system == SYSTEM_RASPI && $cfg_GPIO_HiFi != OFF)
      {
        if ($cfg_isDocker === true)
        {
          // -- native, independant solution but hell regarding sudo rules. But on Docker - it runs as root ;) --
          // -- init the gpio port ONCE! --
          // -- echo 17 > /sys/class/gpio/export --
          // -- configure as output --
          // -- echo out > /sys/class/gpio/gpio17/direction --
          // -- switch ON --
          // -- echo 1 > /sys/class/gpio/gpio17/value --

          // -- init the gpio port ONCE! --
          if (!$this->gpioHasBeenInit)
          {
            $result .= (string)shell_exec("echo $cfg_GPIO_HiFi > /sys/class/gpio/export");
            $this->gpioHasBeenInit = true;
          }
          // -- configure as output (enough to switch on with Low-Trigger-Releais) --
          $result .= (string)shell_exec("echo out > /sys/class/gpio/gpio$cfg_GPIO_HiFi/direction");
          // -- actually switch ON --
          /* test with Low Trigger relais 2022-12-22:
          $result .= (string)shell_exec("echo $cfg_GPIO_high > /sys/class/gpio/gpio$cfg_GPIO_HiFi/value"); */
        }
        else
        {
          // -- using pigpio since 2022 in native Raspberry environment, something like "pigs w 17 1", --
          // -- because it's easier to use regarding sudo rules --
          $result .= (string)shell_exec("pigs w $cfg_GPIO_HiFi $cfg_GPIO_high");
        }
      }

      eventLog('class_player::GPIO_ON() result: ' . $result, $this->depth);
      return $result;
    }


    function GPIO_OFF()
    {
      global $cfg_isDocker;
      global $cfg_GPIO_HiFi;
      //global $cfg_GPIO_high;
      global $cfg_GPIO_low;

      $result = NULLSTR;

      // -- turn on HiFi via hardware - relais --
      if ($this->system == SYSTEM_RASPI && $cfg_GPIO_HiFi != OFF)
      {
        if ($cfg_isDocker === true)
        {
          // -- native, independant solution but hell regarding sudo rules. But on Docker - it runs as root ;) --
          // -- switch OFF --
          // -- should be: .. --
          // -- echo 0 > /sys/class/gpio/gpio17/value --
          // .. but without a pulldown resistor the trick is to make it an input :) --
          // -- echo in > /sys/class/gpio/gpio17/direction --
          // -- disarm the gpio completally, this is the alternative to check before initializing --
          // -- but this has also to be checked before actually, therefore I keep the first option --
          // -- echo 17 > /sys/class/gpio/unexport --

          $cfg_GPIO_low = '1'; // -- force for my test with Low Trigger relais 2022-12-22 --
          $result .= (string)shell_exec("echo $cfg_GPIO_low > /sys/class/gpio/gpio$cfg_GPIO_HiFi/value");
                              //PROBLEM: sh: 1: cannot create /sys/class/gpio/gpio17/value: Permission denied
        }
        else
        {
          // -- using pigpio since 2022 in native Raspberry environment, something like "pigs w 17 0", --
          // -- because it's easier to use regarding sudo rules --
          $result .= (string)shell_exec("pigs w $cfg_GPIO_HiFi $cfg_GPIO_low");
        }
      }

      eventLog('class_player::GPIO_OFF() result: ' . $result, $this->depth);
      return $result;
    }

  }
