<?php
  // -- this script forces the whole page to deliver for a desktop browser --

  date_default_timezone_set('UTC'); // -- does not matter, no need to include all defines and config --
  if (!isset($_SESSION)) // -- session starting here is an exception as an emergency case --
    session_start();

  $_SESSION['forceIsMobile'] = false;

  if (isset($_SERVER['HTTP_REFERER']))
    header('location: ' . $_SERVER['HTTP_REFERER']);
  else
    header('location: frontend.php');