<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_player.php';

  // -- actually to cli_player.php --
  // -- because it must handle the signal to deliver it to mpg123 AND load a new song to playlist --
  // -- NO: because it runs synchronously with cli_player.php, if it ends, the cli_player.php picks the next song for it --

  $msg = NULLSTR;

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $msg =  cntErrMsg(LNG_ERR_ONLYADMIN);
  }
  else
  {
    $msg .= LNG_NOW_NEXT_SONG . LFH;

    // -- in this case the top song --
    // -- THAT IS NOT NECESSARY because of the same reason like above --
    //if (isset($_GET['index']))
    {
      $myPlayer = new class_player();
      if (!$myPlayer->cmdNext())
        $msg .= cntErrMsg($myPlayer->getLastMsg());
      unset($myPlayer);
    }
  }

  echo $msg;
