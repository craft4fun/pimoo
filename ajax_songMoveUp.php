<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_playlist.php';

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    echo cntErrMsg(LNG_ERR_ONLYADMIN);
  }
  else
  {
    if (isset($_GET['index']))
    {
      $myPlaylist = new class_playlist();
      $check = $myPlaylist->moveUpSong($_GET['index'], $admin);
      unset($myPlaylist);

      if ($check === false)
      {
        echo LNG_ERR_UNKNOWN;
      }
      else
      {
        echo LNG_SUC_MOVEDUP;
      }
    }
  }
