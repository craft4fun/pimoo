<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  if (!isset($_GET['value']))
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    //not here, because this would accept any language which maybe is not supported
    //setcookie(STR_LANG, $_GET['value'], time() + 2592000); // -- time()+60*60*24*30 -> one month --

    switch ($_GET['value']) // -- do not use the cookie here, it is not updated yet --
    {
      case LANG_EN:
       {
         if (setcookie(STR_LANG, LANG_EN, strtotime('+365 days')))
          $msg = LNG_LANGUAGE . ARROW . '<span style="font-weight: bold;">' . LANG_EN_PLAIN . '</span>';
         break;
       }
      case LANG_DE:
       {
         if (setcookie(STR_LANG, LANG_DE, strtotime('+365 days')))
          $msg = LNG_LANGUAGE . ARROW . '<span style="font-weight: bold;">' . LANG_DE_PLAIN. '</span>';
         break;
       }
    }
  }

  echo $msg;