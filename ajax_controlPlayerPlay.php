<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'lib/utils.php';
  require_once 'api_player.php';

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $msg = cntErrMsg(LNG_ERR_ONLYADMIN);
  }
  else
  {
    // -- actually no one is showing that - so what --
    $msg = 'try to play ...';
    $myPlayer = new class_player();
    $myPlayer->cmdPlay();
    unset($myPlayer);
  }

  echo $msg;