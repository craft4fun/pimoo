#!/usr/bin/php
<?php
  require ('defines.inc.php');
  require ('keep/config.php');
  require ('lib/utils.php');
  require ('api_player.php');

  $logOnce = false; // -- init --

  // -- needed, if it is to be controlled by runlevel scripts --
  define ('PLAYERSCRIPT', 'cli_player.php');


  $path = getCLIpathDependOnConfig($cfg_isDocker, $cfg_system, __DIR__);
  // -- finally set path - anyhow --
  chdir($path);


  // -- decrement the onAir remain time --
  function _handleRemainTime()
  {
    global $logOnce;

    $seconds = class_player::static_getRemainTimeInSeconds();
    if($seconds === false)
    {
      eventLog('got malformed remain time seconds in cli_common.php');
    }
    else
    {
      $seconds = $seconds - 1; //$cfg_playlistRefreshInterval;
      if ($seconds < 0)
      {
        // -- The following log is true, but it floods the event log! --
        if (!$logOnce)
        {
          eventLog('something went wrong in calculating remain time, so I try to fix it in emergency style');
          $logOnce = true;
        }

        $check = class_player::static_updateRemainTimeInSeconds(10);
        if ($check  === false)
          eventLog('cannot push remain time seconds (10) in cli_common.php');
      }
      else
      {
        $check = class_player::static_updateRemainTimeInSeconds($seconds);
        if ($check  === false)
          eventLog('cannot push remain time seconds (10) in cli_common.php');

        // nope: this will become true after fixing remain taime and then log again: $logOnce = false; // -- reset --
      }
    }
  }


  // -- ---------------- --
  // -- beg of main loop --

  function _continue()
  {
    // -- shortcut because this did not work as expected --
    // -- now solved in classic way via pkill STOP and CONT --
 return true;
    global $cfg_playerStateFile_abs;
    $state = file_get_contents($cfg_playerStateFile_abs);
    return ($state == PS_PLAYING) ? true : false;
  }


  // -- so the cli_player can do things at first --
  sleep(floor($cfg_playlistRefreshInterval / 2));

  while (_continue())
  {
    _handleRemainTime();

    sleep(1); // $cfg_playlistRefreshInterval
  }
  // -- end of main loop --
  // -- ---------------- --

  echo "Normally stopped by condition, NOT interrupted\n";
