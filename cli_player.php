#!/usr/bin/php
<?php
  require ('defines.inc.php');
  require ('keep/config.php');
  require ('lib/utils.php');

  require ('api_playlist.php');
  require ('api_player.php'); // -- class_player(); --


  // -- needed, if it is to be controlled by runlevel scripts --
  define ('PLAYERSCRIPT', 'cli_player.php');

  $path = getCLIpathDependOnConfig($cfg_isDocker, $cfg_system, __DIR__);
  // -- finally set path - anyhow --
  chdir($path);


  define('MAIN_INTERVAL_INIT', 0); // -- 10000 means 10ms --
  define('MAIN_INTERVAL_STEP', 100000); // -- 50000 means 50ms --
  define('MAIN_INTERVAL_MAX', 5000000); // -- 1000000 means 5s --

  define('SHUTDOWNDELAYCOUNTER_INIT',0);
  define('SHUTDOWNDELAYCOUNTER_EXEC',200); // -- 200 -> approx 15 minutes when called every 5th second (mixed with MAIN_INTERVAL_MAX) --


  // -- ------------------------- --
  // -- begin of fetch parameters --

  $debug = CFG_DEBUG; // -- init --
  if (isset($_SERVER['argv']))
  {
    foreach ($_SERVER['argv'] as $arg)
    {
       if ($arg == '--debug') $debug = true;
//       if ($arg == '--help' || $arg == '-h') $help = true;
    }
  }

  // -- end of fetch parameters --
  // -- ----------------------- --



  // -- ----------------------- --
  // -- begin of initialization --

  $goAhead = true;
  $myPlaylist = new class_playlist();

  // -- end of initialization --
  // -- --------------------- --



  // -- ------------- --
  // -- begin of core --
  // -- ------------- --


  // -- do synchronous line command --
  function synchPlayCommand($aSong)
  {
    global $cfg_playerPath;
    global $cfg_playerExec;

    global $path;

    $myPlayer = new class_player();

    $old = getcwd(); // -- really important --
    chdir($cfg_playerPath);

    system( $cfg_playerExec . $myPlayer->getPlayerParam($path) . cntPSS($aSong) );


    chdir($old); // -- really important --
  }

  // -- ----------- --
  // -- end of core --
  // -- ----------- --



  function _isPlayable($aSong)
  {
    return (file_exists($aSong) && !is_dir($aSong)) ? true : false;
  }


  // -- init shutdowntimer as a global var--
  $shutdownDelayCounter = SHUTDOWNDELAYCOUNTER_INIT;
  // -- init logOnce as global var --
  $logOnce = false;

  // -- ---------------------------- --
  // -- begin of main call functions --
  // -- ---------------------------- --
  function actionHandle(&$aDynLoop)
  {
    global $debug;
    global $myPlaylist;
    global $myMediabase;
    global $cfg_system;
    global $cfg_playerRemainTime; // -- no not remove, could be needed inside a following function --
    global $cfg_partyMode;
    global $cfg_partyReport;

    global $goAhead;
    global $shutdownDelayCounter;
    global $logOnce;

    if ($debug) echo "actionHandle with dynamic loop of: $aDynLoop\n";


    $nextSong = $myPlaylist->getNextSong();
    if (_isPlayable($nextSong))
    {
      // -- everything's alright, reset to "fastloop" --
      $aDynLoop = MAIN_INTERVAL_INIT;

      // -- reset shutdowntimer --
      $shutdownDelayCounter = SHUTDOWNDELAYCOUNTER_INIT;
      $logOnce = false;

      eventLog(PLAYERSCRIPT . ' going to play ' . $nextSong); // , PLAYERSCRIPT
      echo 'going to play ' . $nextSong . "\n";

      // -- push the song only if it is really played --
      if ($cfg_partyReport == true)
      {
        $myPlaylist->partyReport_pushSong();
      }

      // -- ------------------------------- --
      // -- the heart of this whole efforts --
      synchPlayCommand($nextSong);

      // -- remove previously played song from stock --
      $myPlaylist->removePlayedSong();
    }
    else
    {
      // -- make the main loop step by step slower to avoid panic (on user and hardware) --
      if ($aDynLoop <= MAIN_INTERVAL_MAX)
        $aDynLoop = $aDynLoop + MAIN_INTERVAL_STEP;

      // -- prevent flooding logfile --
      if (!$logOnce)
      {
        eventLog(PLAYERSCRIPT . ' I am waiting now because: was not play- or available: ' . $nextSong . ''); // , PLAYERSCRIPT
        $logOnce = true;
      }

      // -- "shutdown" --
      if ($cfg_partyMode == false)
      {
        // -- this is called approx. every 5th second --
        $shutdownDelayCounter++;
        if ($debug) echo eventLog("shutdownTimer: $shutdownDelayCounter");

        if ($shutdownDelayCounter >= SHUTDOWNDELAYCOUNTER_EXEC)
        {
          // -- do only turn of the amplifier via the GPIOs --
          if ($cfg_system == SYSTEM_RASPI)
          {
            eventLog(PLAYERSCRIPT . ' Last song has been played, I try to shutdown the amplifier now as wished...'); // , PLAYERSCRIPT
            echo "Last song has been played, I try to shutdown the amplifier now as wished...\n";
            $myPlayer = new class_player();

            // -- the order of the following both functions is important --
            // -- OKAY. Let me try to explain: --
            // -- $myPlayer->cmdTurnOff() does kill this script by /etc/init.d/piMoo stop --
            // -- But it doesn't matter, because the music is already of when the auto off function is triggered and so there's no sudden silence --

            // -- turn off relais --
            echo $myPlayer->GPIO_OFF();
            // -- turn off the player --
            echo $myPlayer->cmdTurnOff();
          }
          // -- do a real shutdown --
          if ($cfg_system == SYSTEM_DEBIAN)
          {
            eventLog(PLAYERSCRIPT . ' Last song has been played, I try to shutdown the system now as wished...'); // , PLAYERSCRIPT
            echo "Last song has been played, I try to shutdown the system now as wished...\n";
            util_systemShutdown(); //system('sudo /sbin/shutdown -h now');
          }

          //exit();
          $goAhead = false;
        }
      }
      // -- "spooling" --
      else
      {
        //if ($cfg_randomAfterLastSong)
        //{
          $myPlaylist->removePlayedSong();
          echo "was not play- or available: $nextSong! I try the next...\n";
          eventLog(PLAYERSCRIPT . ' was not play- or available: ' . $nextSong . 'I try the next...');
        //}
        //else
        //{
        //  // -- "waiting" --
        //  echo "playlist is empty or the top song is not a playable, I'm waiting...\n";
        //}
      }
    }
  }

  // -- -------------------------- --
  // -- end of main call functions --
  // -- -------------------------- --



  // -- ------------------ --
  // -- begin of main loop --
  // -- ------------------ --


  $mainInterval = MAIN_INTERVAL_INIT;

  // -- use this to stop the player later --
  while ($goAhead)
  {
    // -- player --
    actionHandle($mainInterval);

    // -- avoid 100% CPU --
    usleep($mainInterval);
  }
  // -- ---------------- --
  // -- end of main loop --
  // -- ---------------- --

  unset($myPlaylist);
  echo "CLI player normally stopped by condition, NOT interrupted\n";

