#!/bin/bash

clear

echo ''
echo '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
echo 'Installer script to install piMoo on your Linux environment'
echo 'Written by P.Höf'
echo ''
echo 'Known bugs: No real error handling'
echo ''
echo 'History'
echo '2013-12-17 - First attempt'
echo '2016-12-16 - Add debian XAMPP development environment as an option'
#echo '2017-03-23 - Adapt to Debian webroot standard /var/www/html/'
echo '2020-04-09 - Install a Daemon for starting piMoo on boot (Debian and Raspberry Pi OS only)'
echo '2020-04-17 - Install a Daemon for starting piMoo by mouse events and cleanup daemons'
echo '2020-06-20 - Remove openSuse support and split sudo rules into separate file'
echo '2021-12-22 - Add module "php-mysql" for database mode and change from "apt-get" to "apt" at all'
echo '2021-12-23 - Update to "bullseye" regarding php version (7.4)'
echo '2022-01-04 - Add missing module "php-xml" and install triggerhappy on Raspbian'
echo '2022-03-07 - Remove wiringpi, add pigpio'
echo '2022-05-25 - Second approach: Adapt to Debian webroot standard /var/www/html/html/'
echo '2022-07-18 - Optionally protect admin area'
echo '2022-11-28 - Read passwords silent'
echo '2022-12-06 - Add php-mbstring'
echo '2022-12-19 - Remove protecting admin area'
echo '2023-10-20 - Update to "bookworm" regarding php version (8.2)'
echo '2024-11-04 - add inotify-tools to apt install and add piMoo playlist change notifier daemon'
echo '2024-11-24 - Remove xampp'
echo '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'


# -----------------
# checking for root
# -----------------

echo ''
echo -n 'Checking for root (or sudo)...'
if [ "$(id -u)" != "0" ]; then
   echo "Failed -> this script must be run as root" 1>&2
   echo ''
   exit 1
fi
echo 'OK'





# ------------------------------------
# Ask for distribution
# Supported here: Debian, Raspberry Pi OS etc.
# ------------------------------------

echo ''
while [ -z "$fDistri" ]; do
    echo 'Gimme your Linux distribution: '
    echo '1. Raspberry PI OS (formerly known as: Raspian)'
    echo '2. Debian'
    echo '3. RESERVE - DO NOT USE'
  read fDistri
  fDistri=${fDistri##*[^0-9,' ',-]*}
  if [ -z "$fDistri" ]; then
    echo 'Wrong input, please repeat!'
  fi
done





# -----------------------------------------------
# QUESTION: Install required third party software
# -----------------------------------------------

echo 'Required third party software for piMoo are:'
echo '- apache with php support (also php-cli and mysql)'
echo '- mpg123'
echo '- alsa-utils (ALSA amixer)'

while [ -z "$fInstThirdParty" ]; do
    echo -n 'Shall I try to install required third party software? [yes|no]: '
  read fInstThirdParty
  fInstThirdParty=${fInstThirdParty%['yes','no']%}
  if [ -z "$fInstThirdParty" ]; then
    echo 'Wrong input, please repeat!'
  fi
done





# ---------------------------------------------------------------------------------------------------------------------
# QUESTION: Optionally add a RAM drive for temporariliy used files (useful on the Raspberry PI, avoids SD-Card damages)
# ---------------------------------------------------------------------------------------------------------------------

while [ -z "$fInstRAMDrive" ]; do
    echo -n 'Shall I create a RAM drive for temporariliy used files [yes|no]: '
  read fInstRAMDrive
  fInstRAMDrive=${fInstRAMDrive%['yes','no']%}
  if [ -z "$fInstRAMDrive" ]; then
    echo 'Wrong input, please repeat!'
  fi
done





# ----------------------------------------------------------------------------------------
# QUESTION: Patch index.php (so hostname or IP is enough URL to get access to this server)
# ----------------------------------------------------------------------------------------

while [ -z "$fPatchIndex" ]; do
    echo -n 'Shall I patch index.php (so hostname or IP is enough URL to get access to this server) [yes|no]: '
  read fPatchIndex
  fPatchIndex=${fPatchIndex%['yes','no']%}
  if [ -z "$fPatchIndex" ]; then
    echo 'Wrong input, please repeat!'
  fi
done





# --------------------------------------------
# QUESTION: Start webserver after installation
# --------------------------------------------

while [ -z "$fStartWebserver" ]; do
    echo -n 'Shall I try to start the webserver after installation[yes|no]?: '
  read fStartWebserver
  fStartWebserver=${fStartWebserver%['yes','no']%}
  if [ -z "$fStartWebserver" ]; then
    echo 'Wrong input, please repeat!'
  fi
done





# ---------------------
# QUESTION: Install now
# ---------------------

while [ -z "$fInstallNow" ]; do
    echo -n 'Begin the installation now? [yes|no]: '
  read fInstallNow
  fInstallNow=${fInstallNow%['yes','no']%}
  if [ -z "$fInstallNow" ]; then
    echo 'Wrong input, please repeat!'
  fi
done




# -------------------
# ACTION: Install now
# -------------------

if [ $fInstallNow != 'yes' ]; then
  echo ''
  echo 'Nothing installed. No modifications on your system done.'
  echo ''
  exit 0
fi





echo ''
echo '+++++++++++++++++++++++++++++++++++++++++'
echo 'Installing piMoo on your Linux machine now'
echo '+++++++++++++++++++++++++++++++++++++++++'


# ----------------------------
# -- 1.Raspberry PI OS and 2.Debian --

if ( [ $fDistri -eq 1 ] || [ $fDistri -eq 2 ]); then
  fWWWUser='www-data'
  #since debian8 actually fWebroot='/var/www/html/html/'
  #when using still /var/www/html/ you have to take care in apache.conf
  fWebroot='/var/www/html/'
fi


# -------------
# -- RESERVE --

if [ $fDistri -eq 3 ]; then
  fWWWUser='daemon'
  fWebroot='/opt/lampp/htdocs/'
fi


# ----------------------------------------------------
# ACTION: Stop a possibly running piMoo player instance
# ----------------------------------------------------

if [ -f '/etc/init.d/piMood' ];
then
  echo 'Going to stop a possibly running piMoo instance'
  /etc/init.d/piMood stop
  echo 'Going to stop a possibly running piMooPCN instance'
  /etc/init.d/piMooPCNd stop
fi







# ---------------------------------------------
# ACTION: Install required third party software
# ---------------------------------------------

ThirdPartyHint=0

if [ $fInstThirdParty == 'yes' ]; then


  # ----------------------------
  # -- 1.Raspberry PI OS and 2.Debian --

  if ( [ $fDistri -eq 1 ] || [ $fDistri -eq 2 ]); then
    echo 'Installing third party software using "apt install"...'

    apt update
    #apt ugrade -y
    apt install apache2 php8.2 php8.2-cli php8.2-mysql php8.2-xml php8.2-mbstring --assume-yes
    apt install mpg123 --assume-yes
    apt install alsa-utils --assume-yes
    apt install sudo --assume-yes
    # -- required for mqtt playlist refresh --
    apt install inotify-tools --assume-yes

    echo 'OK'
  fi

  # -- 1.Raspbian only --
  if [ $fDistri -eq 1 ]; then

    # -- triggerhappy --
    echo 'Addidionally installing triggerhappy for mouse support on Raspberry Pi...'
    apt install triggerhappy --assume-yes
    echo 'OK'

    # -- gpio handling --
    echo 'Addidionally installing (and enable) pigpio for gpio support on Raspberry Pi...'
    apt install pigpio --assume-yes
    systemctl start pigpiod
    systemctl enable pigpiod
    echo 'OK'

    echo 'init gpio 17 as output...'
    pigs modes 17 w
    echo 'OK'


  fi


  # ---------------
  # -- 3.RESERVE --

  if [ $fDistri -eq 3 ]; then
    echo 'RESERVE'
  fi


else
  ThirdPartyHint=1
fi




# --------------------------------------------------------
# Copy project files to webroot, then set owner and rights
# Possibly existing party reports and hitlist will be kept
# --------------------------------------------------------

if [ $fDistri -lt 3 ]; then # $fDistri -lt 3 means for Debian and Raspbian, but not for RESERVE

  echo -n "Copying piMoo project files into the distribution´s usual webroot path [$fWebroot]..."
  if [ ! -d $fWebroot"piMoo" ]; then
    mkdir $fWebroot"piMoo"
  fi
  cp -R piMoo/* $fWebroot"piMoo"
  echo 'OK'

  echo -n 'Setting owner and rights...'
  chown -R $fWWWUser $fWebroot"piMoo"
  chmod -R 700 $fWebroot"piMoo"
  echo 'OK'

else
  echo -e "RESERVE\n"

fi



# -------------------------------------------------------------------------------------------------------------------
# ACTION: Optionally add a RAM drive for temporariliy used files (useful on the Raspberry PI, avoids SD-Card damages)
# -------------------------------------------------------------------------------------------------------------------

if [ $fInstRAMDrive == 'yes' ]; then

  echo -n 'Making a entry in fstab to have a RAM Drive inside the piMoo project folder...'

  foundFstab=0

  fstabEntry="tmpfs "$fWebroot"piMoo/tmp/ tmpfs size=100M 0 0"

  while read line; do
    if [ "$line" == "$fstabEntry" ]; then
      foundFstab=1
    fi
  done < /etc/fstab

  if [ $foundFstab -eq 1 ]; then
    echo 'OK, already exists';
  else
    # echo "tmpfs "$fWebroot"piMoo/tmp/ tmpfs size=100M 0 0" >> /etc/fstab
    echo $fstabEntry >> /etc/fstab
    echo 'OK'
  fi

fi




# --------------------------------------------------------------------------------------
# ACTION: Patch index.php (so hostname or IP is enough URL to get access to this server)
# --------------------------------------------------------------------------------------

if [ $fPatchIndex == 'yes' ]; then

  echo -n 'Patching index.php in webroot...'

  cp more/index.php $fWebroot

  chown -R $fWWWUser $fWebroot"index.php"
  chmod 700 $fWebroot"index.php"

  if [ -f $fWebroot"index.html" ]; then
    mv $fWebroot"index.html" $fWebroot"index.html.bak"
  fi

  echo 'OK'
fi




# ------------------------------------------------------------------------------------------------
# Copy "runlevel_Debian" to "/etc/init.d/" and rename it to "piMood".
# ------------------------------------------------------------------------------------------------

echo -n 'Copying files to control piMoo with runlevel scripts...'


# ------------------------------------
# -- 1.Raspberry PI OS and 2.Debian --

if ( [ $fDistri -eq 1 ] || [ $fDistri -eq 2 ]); then

  # -- a real daemon service for further purposes with triggerhappy mouse events support ... --
  cp more/runlevel_Debian /etc/init.d/piMood
  chmod u+x /etc/init.d/piMood

  if [ $fDistri -eq 1 ]; then
     # -- ... which requires triggerhappy --
    cp more/piMoo-mouse-events.conf /etc/triggerhappy/triggers.d/
    service triggerhappy restart
  fi


  # -- additional daemon for playlis change notifier --
  cp more/runlevel_PCN_Debian /etc/init.d/piMooPCNd
  chmod u+x /etc/init.d/piMooPCNd

  # -- required to make newly copied runlevel scripts accessible --
  systemctl daemon-reload

fi

# --------------------------------
# -- 3.RESERVE Development --

if [ $fDistri -eq 3 ]; then
  echo 'RESERVE'
fi


if [ -f '/etc/init.d/piMood' ]; then
  echo 'OK'
else
  echo 'Something went wrong'
fi





# -------------------------------------------------------------
# Add system user "www-data" (Debian, Raspberry PI OS) to group "audio"
# -------------------------------------------------------------

echo -n 'Adding system user "www-data" (Debian, Raspberry PI OS) or "daemon" (XAMPP) to group "audio"...'
usermod -aG audio $fWWWUser
echo 'OK'





# -- ----------------------- --
# Add piMoo controls to sudoers
# -- ----------------------- --

echo -n 'Adding piMoo controls to sudoers...'


# ----------------------------
# -- 1.Raspberry PI OS and 2.Debian --

if ( [ $fDistri -eq 1 ] || [ $fDistri -eq 2 ] ); then

  cp more/sudoers_Debian /etc/sudoers.d/piMoo
  chmod 0440 /etc/sudoers.d/piMoo


### old school: patching the main sudoers file
###  foundSudoers=0
###  # this is only a very simple check
###  # www-data ALL=(ALL) NOPASSWD: /sbin/reboot
###  fsudoersEntry='www-data ALL=(ALL) NOPASSWD: /var/www/html/piMoo/cli_player.php'
###  # www-data ALL=(ALL) NOPASSWD: /var/www/html/piMoo/cli_common.php
###  while read line; do
###    if [ "$line" == "$fsudoersEntry" ]; then
###      foundSudoers=1
###    fi
###  done < /etc/sudoers
###  if [ $foundSudoers -eq 1 ]; then
###    echo 'OK, already exists';
###  else
###    ./more/modSudoers_Debian
###    echo 'OK'
###  fi

fi


# --------------------------------
# -- 3.RESERVE Development --

if [ $fDistri -eq 3 ]; then

  echo 'RESERVE'

fi





# -------------------------------------------
# ACTION: Start webserver after installation
# -------------------------------------------

if [ $fStartWebserver == 'yes' ]; then

  echo -n 'Starting the webserver...'

  if [ $fDistri -lt 3 ]; then
    /etc/init.d/apache2 start
  else
    /opt/lampp/lampp start
  fi

  echo 'OK'
fi





if [ $ThirdPartyHint -eq 1 ]; then
  echo ''
  echo 'REMEMBER TO INSTALL REQUIRED THIRD PARTY SOFTWARE IF NOT ALREADY DONE !!!'
  echo ''
fi





echo '+++++++++++++++++++++++++++++++'
echo 'Congratulation! Almost finished'
echo '+++++++++++++++++++++++++++++++'
echo ''
echo 'piMoo should now be installed on your Linux machine. Have fun with it!'
echo 'Do not forget to edit keep/config.php" inside the piMoo directory in webroot ->'
echo 'There you can set, where all your music is located,'
echo 'which Linux distribution you are using'
echo 'and set your backend admin password!'

# ---------------------------
# QUESTION: Patch keep/config
# ---------------------------

while [ -z "$fPatchConfig" ]; do
    echo -n 'Shall I try do that for you now (required php cli)? [yes|no]: '
  read fPatchConfig
  fPatchConfig=${fPatchConfig%['yes','no']%}
  if [ -z "$fPatchConfig" ]; then
    echo 'Wrong input, please repeat!'
  fi
done

# -------------------------------
# ACTION: Start Patch keep/config
# -------------------------------

if [ $fPatchConfig == 'yes' ]; then

  echo -n 'Where is your music located? '
  read fMusicLocation

  echo -n 'Set an backend admin password (also used by legacy api v1): '
  read -s fAdminPasswd

  ./more/patchConfig.php $fWebroot $fDistri $fMusicLocation $fAdminPasswd

  chown -R $fWWWUser $fWebroot"piMoo"
  chmod -R 700 $fWebroot"piMoo"

  echo 'OK'

else
  echo 'OK, then do it yourself'

fi



# ----------------------------
# QUESTION: protect admin area
# ----------------------------

echo 'I recommend to protect the admin area with an htaccess admin password additionally!'

# -- rest is prepared only --
# while [ -z "$fProtectAdminArea" ]; do
#   echo -n 'Shall I do that for you now? (This function is not stable - in case of doubt, set up .htaccess yourself) [yes|no]: '
#   read -s fProtectAdminArea
#   fProtectAdminArea=${fProtectAdminArea%['yes','no']%}
#   if [ -z "$fProtectAdminArea" ]; then
#     echo 'Wrong input, please repeat!'
#   fi
# done

# --------------------------
# ACTION: protect admin area
# --------------------------

# if [ $fProtectAdminArea == 'yes' ]; then

#   echo -n 'Set an admin-area-password: '
#   # read fAdminAreaPasswd

#   htpasswd -c $fWebroot"piMoo/admin/.htpasswd" admin

#   chown -R $fWWWUser $fWebroot"piMoo/admin/.htpasswd"
#   chmod -R 700 $fWebroot"piMoo/admin/.htpasswd"

#   echo 'OK'

# else
#   echo 'OK, then do it yourself if you wish.'

# fi







exit 0
