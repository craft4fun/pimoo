#!/usr/bin/php
<?php

//   echo $_SERVER['argv'][0] . "\n"; // -- ./more/patchConfig.php --
//   echo $_SERVER['argv'][1] . "\n"; // -- $fWebroot, e.g. /var/www/html/ --
//   echo $_SERVER['argv'][2] . "\n"; // -- $fDistri, e.g. 2 --
//   echo $_SERVER['argv'][3] . "\n"; // -- $fMusicLocation, e.g. /home/music/singles/ --

  // -- default --
  $distriStr = 'Raspi';

  $distri = $_SERVER['argv'][2];
  switch ($distri)
  {
    case 1:
      {
        $distriStr = 'Raspi';
        break;
      }
    case 2:
      {
        $distriStr = 'Debian';
        break;
      }
  // -- RESERVE doesn't matter in this context! --
  //     case 3:
  //       {
  //         $distriStr = 'RESERVE';
  //         break;
  //       }
  //     default:
  //       {
  //         $distriStr = 'Debian';
  //       }
  }


  // -- load the piMoo config --
  $config = file_get_contents($_SERVER['argv'][1] . 'piMoo/keep/config.php');

  // -- patch the distribution --
  $config = str_replace('$cfg_system = $cfg_systemDefault;', '$cfg_system = \''.$distriStr.'\';', $config);

  // -- patch the music location --
  $config = str_replace('$cfg_absPathToSongs = $cfg_absPathToSongsDefault;', '$cfg_absPathToSongs = \''.$_SERVER['argv'][3].'\';', $config);

  // -- patch the admin password --
  $config = str_replace('$cfg_admin_passwd = $cfg_admin_passwdDefault;', '$cfg_admin_passwd = \''.md5($_SERVER['argv'][4]).'\';', $config);

  // -- save the config.php --
  if (file_put_contents($_SERVER['argv'][1] . 'piMoo/keep/config.php', $config) !== false)
    exit(0);
  else
    exit(1);


