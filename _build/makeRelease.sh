#!/bin/bash

echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
echo 'Script for building a release of the latest source code of piMoo'
echo 'Written by P.Höf'
echo ''
echo 'History'
echo '2020-06-25 - Fork of good old iMuh project'
echo '2022-02-15 - Exclude "_docker"'
echo '2022-02-20 - Exclude "_testing"'
echo '2022-03-23 - Exclude "_*"'
echo '2022-12-06 - Exclude more stuff and command with line break'
echo '2023-08-25 - UNDO: Exclude "_*"'
echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'



# -------------------------
# init
# -------------------------

fMasterPath='../'

fTargetPathSource='piMoo_installer/piMoo/'

fTargetPath='piMoo_installer/'
fTargetArchive='piMoo_installer_'$(date "+%Y_%m_%d")




# --------------------------------------
# ACTION: Cleanup the target path before
# --------------------------------------

echo -n "Purging target source path '$fTargetPathSource'..."

rm -R -f $fTargetPathSource/*

echo 'OK'





# -----------------------------------------------------------------
# ACTION: Copy a source code files into target source code location
# -----------------------------------------------------------------

echo -n "Copying piMoo source files into the target source location '$fTargetPathSource'..."

if [ ! -d $fTargetPathSource ]; then
  mkdir $fTargetPathSource
fi

#rsync -r $fMasterPath* $fTargetPathSource --exclude=keep/mediaBase.xml --exclude=_build --exclude=_private --exclude=_testing --exclude=_music --exclude=_docker --exclude=keep/partyReport --exclude=keep/*.xml --exclude=tmpS/*.txt --exclude=keep/*.bak --exclude=tmpS/*.xml --exclude=tmp/cache/* --exclude=tmp/nicknames/* --exclude=tmp/player*

rsync -r $fMasterPath* $fTargetPathSource \
         --exclude=_build --exclude=_private --exclude=_testing --exclude=_music --exclude=_docker --exclude=_docker_alpine \
         --exclude=_addons --exclude=_mqtt \
         --exclude=keep/mediaBase.xml --exclude=keep/partyReport --exclude=keep/*.xml --exclude=keep/*.bak \
         --exclude=tmpS/*.txt --exclude=tmpS/*.xml \
         --exclude=tmp/cache/* --exclude=tmp/nicknames/* --exclude=tmp/player* --exclude=tmp/configShadow*

echo 'OK'




# ----------------------
# ACTION: purge the code
# ----------------------

echo -n "purging the code..."

./phPurger.php --silent -o $fTargetPath

echo 'OK'




# ----------------------------------
# ACTION: Make the installer archive
# ----------------------------------

echo -n "Making the installer archive '$fTargetArchive.tar.gz'..."

tar -czf $fTargetArchive.tar.gz $fTargetPath

echo 'OK'




# -------------------------------------
# ACTION: Cleanup the source path after
# -------------------------------------

echo -n "Purging target source path '$fTargetPathSource'..."

rm -R -f $fTargetPathSource/*

echo 'OK'

exit 0

