************************************************************
Steps to install "piMoo" manually
Examples are for Debian based systems (like Raspberry PI OS)
************************************************************


- 1 - Install required third party software

Install (if not already done)
 - apache with php support (also php-cli is required)
 - mpg123
 - alsa-utils (ALSA amixer)



- 2a - Copy project files

Copy the piMoo project folder into your webroot e.g. /var/www/html/ or whatever


- 2b - Set rights

on Debian based systems (like Raspberry PI OS) do:
chown -R www-data /var/www/html/piMoo
chmod -R 700 /var/www/html/piMoo



- 3 - Create a RAM Drive

Optionally add this line to /etc/fstab (takes effect a latest at the next reboot)
(creates a RAM Drive to temporariliy written files does not damage the SD-Card on the Raspberry PI)

tmpfs /var/www/html/piMoo/tmp/ tmpfs size=100M 0 0




- 4 - Add apache system user to group audio

Add system user "www-data" (Debian) to group "audio"



- 5 - Prepare runlevel scripts

Copy "runlevel_Debian" to "/etc/init.d/" and rename it to "piMood".
Make it executable with: chmod u+x piMood

(So piMoo can be controlled via runlevel scripts, even automattically started on system startup)



- 6 - Add requrired sudo commands to sudoers

Add the following lines to "sudoers" via "viduso".

www-data ALL=(ALL) NOPASSWD: /sbin/reboot
www-data ALL=(ALL) NOPASSWD: /var/www/html/piMoo/cli_player.php
www-data ALL=(ALL) NOPASSWD: /var/www/html/piMoo/cli_common.php


what about this? -> clarification
www-data ALL=(ALL) NOPASSWD: /etc/init.d/piMoo.sh




- 7 - Make your settings (always by hand, not by a installer)

Modify piMoo/keep/config.php as your requirements.


