<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  require_once ('lib/utils.php');

  require_once ('api_mediabase.php');
  require_once ('api_playlist.php');
  //require_once ('api_feedback.php');


  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();


  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">'.LNG_EVEN_MORE.'</div>';

    echo '<span><a class="linkNaviNice" href="myMooMore.php">'.LNG_BACK_TO_.LNG_MORE.'</a></span>';
    echo '<span style="float: right;"><a class="linkNaviNice" href="index.php">'.LNG_BACK_TO_.PROJECT_NAME.'</a></span>';

  echo '</div>';

?>
<div style="margin-top: 2em; text-align: center;">


    <div>
      <a class="linkNice" href="documentation.php"><?php echo LNG_DOCUMENTATION; ?></a>
    </div>

    <?php
      if ($cfg_songUpload !== false)
      {
    ?>
        <div style="margin-top: 2em;">
          <a class="linkNice" href="upload.php"><?php echo LNG_TITLE_UPLOAD_OWN_SONG; ?></a>
        </div>
    <?php
      }
      else
      {
    ?>
        <div style="margin-top: 2em;">
          <span class="linkNice" style="text-decoration: line-through;"><?php echo LNG_TITLE_UPLOAD_OWN_SONG; ?></span>
        </div>
    <?php
      }
    ?>

    <div style="margin-top: 2em;">
      <a class="linkNice" href="piMooReverse">piMoo-Reverse</a>
      <div style="margin-top: 0.5em;"><a class="linkNaviNice" target="_blank" href="piMooReverse">(<?php echo LNG_IN_NEW_TAB; ?>)</a></div>
      <div class="explainText">
        <?php echo LNG_EXPLAIN_iMoo; ?>
      </div>
    </div>

    <?php
      if (file_exists('piMooApp/www/'))
      {
    ?>
        <div  style="margin-top: 2em;">
          <a class="linkNice" target="_blank" href="piMooApp/www/">piMoo App</a>
          <div class="explainText">
            <?php echo LNG_EXPLAIN_piMooApp; ?>
          </div>
        </div>
    <?php
      }
    ?>

</div>
<?php
  echo makeBottom();
  echo makeHTML_end();
