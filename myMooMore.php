<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  require_once ('lib/utils.php');

  require_once ('api_mediabase.php');
  require_once ('api_playlist.php');
  //require_once ('api_feedback.php');


  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();


  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">'.LNG_HITLIST_N_PARTYREPORT.'</div>';

    echo '<span><a class="linkNaviNice" href="index.php">' . LNG_BACK_TO_ .  PROJECT_NAME.'</a></span>';
    echo '<span style="float: right;"><a class="linkNaviNice" href="myMooEvenMore.php">'.LNG_EVEN_MORE.'</a></span>';

  echo '</div>';

?>
<div style="padding: 0.5em;">

  <?php
    require_once 'hitlist.php';
    $myHitlist = new class_everlastingHitlist();
    echo $myHitlist->doOutput();
  ?>

  <div style="margin-top: 3em; text-align: center; font-size: larger;">
    <span style="color: #769cb1;"><?php echo BLANK.RATE4_DINGBAT; ?></span>
    <a class="linkNice" href="valuedSongs.php"><?php echo LNG_VALUED_SONGS; ?></a>
    <span style="color: #769cb1;"><?php echo BLANK.RATE5_DINGBAT; ?></span>
  </div>

  <div style="margin-top: 3em;">
    <label for="partyReport" style="font-style: italic;"><?php echo LNG_PARTY_REPORT; ?></label>
    <div id="partyReport" style="border-style: solid; border-color: grey; padding: 0.5em;">

      <?php
        // -- list all files in "../keep/partyReport/" as links --
        $files = scandir('keep/partyReport/');
        $files = array_reverse($files); // -- change order ascending --
        foreach($files as $fl)
        {
          if (substr($fl, 0, 1) != DOT) // -- show no hidden files or directories --
          {
            if ($fl != 'hitlist.txt')
            {
              $curFile = simplexml_load_file( 'keep/partyReport/'.$fl );
              echo '<div style="margin-bottom: 0.5em;">';
                echo '<span style="float: right;">'.count($curFile).'</span>';
                echo cntMBL('<a class="linkNice" href="partyReport.php?file=keep/partyReport/'.$fl.'">'.basename($fl, '.xml').'</a>');
              echo '</div>';  // faster without: unset($curFile);
            }
          }
        }
      ?>
    </div>
  </div>
</div>
<?php
  echo makeBottom();
  echo makeHTML_end();
