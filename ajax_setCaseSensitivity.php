<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  if (!isset($_GET['value']))
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    if (setcookie('bool_caseSensitive', $_GET['value'], strtotime('+365 days')))
    {
      //$msg = LNG_CASE_SENSTIVE . ARROW . '<span style="font-weight: bold;">' . $_GET['value'] . '</span>';

      switch ($_GET['value']) // -- do not use the cookie here, it is not updated yet --
      {
        case YES:
          {
            $msg =  LNG_CASE_SENSTIVE . ARROW . '<span style="font-weight: bold;">' . LNG_YES . '</span>';
            break;
          }
        case NO:
          {
            $msg = LNG_CASE_SENSTIVE . ARROW . '<span style="font-weight: bold;">' . LNG_NO . '</span>';
            break;
          }
      }
    }
  }

  echo $msg;