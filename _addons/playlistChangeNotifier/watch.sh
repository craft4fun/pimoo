#!/bin/sh

# -- path to the file to watch --
WATCHED_FILE="/vol-pimoo/$PLAYLIST_FILE"


# --function to generate a random alphanumeric string --
generate_random_string() {
    local length=${1:-10}  # Default length is 10 if not specified
    tr -dc 'A-Za-z0-9' </dev/urandom | head -c "$length"
}

# -- mqtt client id --
MQTT_CLIENT_ID="piMoo.backend.pcn.$(generate_random_string)"


# -- ensure the watched file exists, or wait until it is created --
while [ ! -f "$WATCHED_FILE" ]; do
  TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
  echo "$TIMESTAMP: Waiting for $WATCHED_FILE to be created one minute..."
  sleep 60
done

echo "Monitoring $WATCHED_FILE for changes..."

# -- loop to watch the file and send MQTT messages on modification --
while true; do
  TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
  echo "$TIMESTAMP: start watching playlist file"

  # -- wait for a modification event on the file --
  inotifywait -e modify "$WATCHED_FILE"

  # -- send MQTT message to the specified topic --
  mosquitto_pub -h "$MQTT_HOST" -p "$MQTT_PORT" -i "$MQTT_CLIENT_ID" -t "$MQTT_TOPIC" -m "playlist modified"

  TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
  echo "$TIMESTAMP: MQTT message sent to $MQTT_TOPIC: playlist modified"

  sleep 1
done

