#!/bin/bash


# -- check if architecture argument is provided (arm64 or amd64) --
if [ -z "$1" ]; then
  echo "Please specify the architecture as an argument (either amd64 or arm64)"
  exit 1
fi

ARCH=$1

# -- check and set the platform based on the provided architecture --
if [ "$ARCH" == "amd64" ]; then
  # PLATFORM="linux/amd64"
  PLATFORM="amd64"
elif [ "$ARCH" == "arm64" ]; then
  # PLATFORM="linux/arm64"
  PLATFORM="arm64"
else
  echo "Unsupported architecture: $ARCH. Use amd64 or arm64."
  exit 1
fi


# -- check if a Docker volume exists --
volume_exists() {
  docker volume inspect "$1" >/dev/null 2>&1
}


# -- check if the volume "vol-pimoo" exists --
if volume_exists "vol-pimoo"; then
  echo "Volume 'vol-pimoo' exists. Proceeding with build and container setup."

  # -- build image --
  docker build -t craft4fun/pimoo-pcn:$ARCH .

  # -- stop and remove prevoiusly started container, if so ... --
  echo "STOP AND REMOVE PREVOIUSLY STARTED CONTAINER..."
  docker container stop pimoo-pcn && docker container rm pimoo-pcn
  echo 'OK'

  docker run -d --name pimoo-pcn \
    -v vol-pimoo:/vol-pimoo \
    -e MQTT_HOST=192.168.2.41 \
    -e MQTT_PORT=1883 \
    -e MQTT_TOPIC="piMoo/playlist" \
    -e PLAYLIST_FILE="playlist.xml" \
    craft4fun/pimoo-pcn:$ARCH

  docker container ls -a

  exit 0

else
  echo "Error: Volume 'vol-pimoo' does not exist. Please create it before running this script."
  exit 1
fi




