#!/bin/bash

# -- check if architecture argument is provided (arm64 or amd64) --
if [ -z "$1" ]; then
  echo "Please specify the architecture as an argument (either amd64 or arm64)"
  exit 1
fi

ARCH=$1

# -- check and set the platform based on the provided architecture --
if [ "$ARCH" == "amd64" ]; then
  # PLATFORM="linux/amd64"
  PLATFORM="amd64"
elif [ "$ARCH" == "arm64" ]; then
  # PLATFORM="linux/arm64"
  PLATFORM="arm64"
else
  echo "Unsupported architecture: $ARCH. Use amd64 or arm64."
  exit 1
fi



# -- build and push --

echo "Building the image..."
docker build -t craft4fun/pimoo-pcn:$ARCH .
echo 'OK'

echo "PUSHING TO DOCKER HUB..."
docker push craft4fun/pimoo-pcn:$ARCH
echo 'OK'

exit 0



# -- idea for future when using buildx --

# docker manifest create craft4fun/pimoo-pcn:latest \
#   --amend craft4fun/pimoo-pcn:amd64 \
#   --amend craft4fun/pimoo-pcn:arm64
# docker manifest push craft4fun/pimoo-pcn:latest

