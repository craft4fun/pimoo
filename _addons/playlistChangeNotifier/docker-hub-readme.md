_craft4fun_ presents **piMooPCN**

# 1. What it is

piMooPCN (playlist change notifier) watches the playlist for changes and publishs them to a MQTT broker.


# 1. Basic steps to make it work

## 1.1 pre-conditions

The piMooPCN container requires a volume from the main piMoo container to get access to the playlist file.


## 1.2 Start piMooPCN

Modify the following for your needs and start the container

### 1.2.1 on raspberry

```
docker run -d --name pimoo-pcn \
  -v vol-pimoo:/vol-pimoo \
  -e MQTT_HOST=craft4fun.de \
  -e MQTT_PORT=1883 \
  -e MQTT_TOPIC="piMoo/playlist" \
  -e PLAYLIST_FILE="playlist.xml" \
  craft4fun/pimoo-pcn:arm64
```

### 1.2.2 on Debian like machine

```
docker run -d --name pimoo-pcn \
  -v vol-pimoo:/vol-pimoo \
  -e MQTT_HOST=craft4fun.de \
  -e MQTT_PORT=1883 \
  -e MQTT_TOPIC="piMoo/playlist" \
  -e PLAYLIST_FILE="playlist.xml" \
  craft4fun/pimoo-pcn:amd64
```


## 2. docker-compose example

## 2.1

```
docker compose -f docker-compose-arm64.yml -p pimoo --profile pimoo up -d
```
or
```
docker compose -f docker-compose-arm64.yml -p pimoo --profile pimoo400 up -d
```

## 2.2

docker-compose, filename: "docker-compose-arm64.yml"

```
services:

  pimoo:
    image: craft4fun/pimoo:arm64
    container_name: piMoo
    # network_mode: bridge  # Use Docker's default bridge network
    restart: always
    ports:
      - "8080:80"
    volumes:
      - /home/pi/music/:/music/
      - vol-pimoo:/var/www/html/keep/
      - /sys/class/gpio/:/sys/class/gpio/
    devices:
      - /dev/snd
      - /dev/input/event0
    environment:
      ADM_PASSWD: 'secret'
    privileged: true
    profiles:
      - pimoo

  pimoo400:
    image: craft4fun/pimoo:arm64
    container_name: piMoo400
    # network_mode: bridge  # Use Docker's default bridge network
    restart: unless-stopped
    ports:
      - "80:80"
    volumes:
      - /home/pi/music/:/music/
      - vol-pimoo:/var/www/html/keep/
    devices:
      - /dev/snd
      - /dev/input:/dev/input
    environment:
      ADM_PASSWD: 'secret'
      TRIGGERHAPPY: 'keyboard'
    profiles:
      - pimoo400

  pimoo-pcn:
    image: craft4fun/pimoo-pcn:arm64
    container_name: pimoo-pcn
    # network_mode: bridge  # Use Docker's default bridge network
    restart: unless-stopped
    volumes:
      - vol-pimoo:/vol-pimoo
    environment:
      MQTT_HOST: "pimoo-mosquitto"
      MQTT_PORT: "1883"
      MQTT_TOPIC: "piMoo/playlist"
      PLAYLIST_FILE: "playlist.xml"

  pimoo-mosquitto:
    image: eclipse-mosquitto
    container_name: mosquitto
    # network_mode: bridge  # Use Docker's default bridge network
    restart: unless-stopped
    ports:
      - "1883:1883"
      - "9001:9001"
    volumes:
      - vol-pimoo-mosquitto:/mosquitto/
      - ./mosquitto.conf:/mosquitto/config/mosquitto.conf  # Mount custom config file

volumes:
  vol-pimoo:
  vol-pimoo-mosquitto:
```

# 2.3

Very basic example mosquitto config, must be placed beside docker compose file.

```
# mosquitto.conf
allow_anonymous true

listener 1883
protocol mqtt

listener 9001
protocol websockets
```