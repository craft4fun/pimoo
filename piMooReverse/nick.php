<?php
  // -- enter the piMoo with this is giving the possibility to skip the "gimme nickname" procedure --
  // -- especcially makes sence if the piMoo is saved in a "Desktop" link on the smartphone --

  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once (ONEDIRUP . 'keep/config.php');
  require_once (ONEDIRUP . 'cnt/cnt_tools.php');


  if (!isset($_GET['name']))
  {
    //$_SESSION['str_nickname'] = $_SERVER['REMOTE_ADDR'];
    echo cntErrMsg(LNG_ERR_UNKNOWN_FUNNY);
  }
  else
  {
    $myNickname = new class_nickname();
    if ($myNickname->push($_GET['name']))
    {
      header('location: index.php');
    }
    else
    {
      include_once ('api_layout.php');
      echo makeHTML_begin();
      includeStyleSheet();
      includeJavascript();
      echo makeHTMLHeader_end();
      echo makeHTMLBody_begin();

        // -- head --
        echo '<div class="commonHead">';
          echo '<span><a href="index.php">back to iMoo</a></span>';
        echo '</div>';
        // -- the CORE message --
        echo cntErrMsg(LNG_TAKE_ANOTHER_NICK);

      echo makeHTML_end();
    }
    unset($myNickname);
  }

