<?php
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');

  if (!(isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR))
  {
    echo LNG_ERR_UNKNOWN;
  }
  else
  {
    $JSONfile = ONEDIRUP.$cfg_tmpPathStatic.DIR_PIMOOREVERSE.$_SESSION['str_nickname'].JSON_FILE_EXT;
    if (!file_exists($JSONfile))
    {
      //echo base64_encode('[]'); // -- an empty json file --
      echo '[]'; // -- an empty json file --
    }
    else
    {
      //$JSON = base64_encode(file_get_contents($JSONfile));
      $JSON = file_get_contents($JSONfile);

      // -- repair some ilegal char stuff --
      $JSON = str_replace('\u00b4', '´', $JSON);

      echo $JSON;
    }
  }
