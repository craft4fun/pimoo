<?php
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once '../lib/utils.php';

  if (!(isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR))
  {
    echo LNG_ERR_UNKNOWN;
  }
  else
  {
    $userPlaylistJSONfile = ONEDIRUP.$cfg_tmpPathStatic.DIR_PIMOOREVERSE.$_SESSION['str_nickname'].JSON_FILE_EXT;

    // -- clear really all --
    if (isset($_GET['clearAll']))
    {
      unlink($userPlaylistJSONfile);
      echo '[]';
    }
    // -- clear all except the cued --
    else
    {
      // -- load the current playlist --
      $curPlaylist = json_decode(file_get_contents($userPlaylistJSONfile));

      $killers = array();

      $i = 0;
      foreach ($curPlaylist as $song)
      {
        if ($song->c == false) // -- so not cued --
        {
          $killers[] = $i;
        }
        $i++;
      }

      // -- remove all, keep the current playing --
      foreach ($killers as $kill)
        unset($curPlaylist[$kill]);

      // -- that does the trick, removing whitespaces to avoid senseless json structure --
      $curPlaylist = array_merge($curPlaylist);

      $curPlaylistStr = json_encode($curPlaylist);
      file_put_contents($userPlaylistJSONfile, $curPlaylistStr);
      echo $curPlaylistStr;
    }
  }

