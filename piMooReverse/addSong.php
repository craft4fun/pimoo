<?php
  require_once '../defines.inc.php';
  //include_once '../keep/config.php';

  $randomSongs = (isset($_COOKIE['int_RandSrchResults'])) ? $_COOKIE['int_RandSrchResults'] : GRP_RAND20;
  $newcomerSongs = (isset($_COOKIE['int_NewcomerDays'])) ? $_COOKIE['int_NewcomerDays'] : GRP_NEWCOMERDAYS365;
?>

<section id="viewBody">

  <div id="addSongRow" class="listRow" style="text-align: center;">

    <span style="float: right;">
      <input class="menuButton" id="btn_news"                       type="button" value="<?php echo $newcomerSongs; ?>" onclick="callNewcomer(this.value)" title="newcomers" />
      <input class="menuButton" id="btn_rand"                       type="button" value="<?php echo $randomSongs; ?>"   onclick="callRand(this.value)"     title="random search results" />
    </span>

    <span style="float: left;">
      <input class="menuButton"                                     type="button" value="<?php echo RATE3_DINGBAT; ?>"  onclick="callSearchByStr('<?php echo RATE3; ?>')" />
      <input class="menuButton"                                     type="button" value="<?php echo RATE4_DINGBAT; ?>"  onclick="callSearchByStr('<?php echo RATE4; ?>')" />
      <input class="menuButton"                                     type="button" value="<?php echo RATE5_DINGBAT; ?>"  onclick="callSearchByStr('<?php echo RATE5; ?>')" />
    </span>

    <nobr>
      <input class="menuButton" id="btn_songSearchClean" type="button" value="<?php echo SYM_CLEAN; ?>"                 onclick="songSearchClean()" />
      <input class="menuButton" id="edt_songSearch"      type="text"   value=""                                         onkeyup="if (event.keyCode == 13) callSearchByEdtSongSearch(this.value)" />
      <input class="menuButton" id="btn_songSearchGo"    type="button" value="<?php echo SYM_GO; ?>"                    onclick="callSearchByEdtSongSearch()" />
      <input class="menuButton" id="btn_userSearchGo"    type="button" value="<?php echo SYM_USER; ?>"                  onclick="callSearchUserByEdtSongSearch()" title="search by user" />
    </nobr>

  </div>

  <div id="target_searchResults" style="clear: both;"></div>

</section>