<?php
  set_time_limit(2);

  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');

  if (!(isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR))
  {
    echo LNG_ERR_UNKNOWN;
  }
  else
  {
    $userPlaylistJSONfile = ONEDIRUP.$cfg_tmpPathStatic.DIR_PIMOOREVERSE.$_SESSION['str_nickname'].JSON_FILE_EXT;

    // -- load the current playlist --
    $curPlaylist = json_decode(file_get_contents($userPlaylistJSONfile));

    $i = 0;
    $found = 0;
    foreach ($curPlaylist as $song)
    {
      if ($song->c == true)
      {
        $found = $i;
        break;
      }
      $i++;
    }

    // -- pick previous song --
    if ($found > 0)
    {
      $curPlaylist[$found]->c = false;
      $curPlaylist[$found - 1]->c = true;
    }
    // -- if already the first, take the last again --
    elseif ($found == 0 && count($curPlaylist) > 1)
    {
      $curPlaylist[$found]->c = false;
      $curPlaylist[count($curPlaylist) - 1]->c = true;
    }

    $curPlaylistStr = json_encode($curPlaylist);
    file_put_contents($userPlaylistJSONfile, $curPlaylistStr);
    echo $curPlaylistStr;
  }

