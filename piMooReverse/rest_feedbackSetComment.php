<?php
  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  include_once ('../api_feedback.php');
  include_once ('../api_mediabase.php');

  //-- get nickame --
  $wishedBy =  (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname']: NULLSTR; // -- $_SERVER['REMOTE_ADDR'] --> do not fill the feedback.xml with obsolete information --

  $msg = NULLSTR;

  // -- is admin or registered user? --
  if (
         !(isset($_SESSION['bool_admin']) AND $_SESSION['bool_admin'] == true)
      && !(isset($_SESSION['str_nickname']) AND $_SESSION['str_nickname'] != NULLSTR)

      || !isset($_GET['song'])
      || !isset($_GET['comment'])
    )
  {
    $msg .= eventLog('ERROR in rest_feedbackSetComment.php, required parameter missing', ONEDIRUP);
  }
  else
  {
    $myFeedback = new class_feedback(ONEDIRUP);
    $myFeedback->setSong( base64_decode( $_GET['song'] )); // -- feedback must make an entry in EVERY case --
    if (!$myFeedback->setComment( base64_decode( /*htmlspecialchars done in setComment */ ( cntSubsituteCriticalJSChars( $_GET['comment'] ) ) ), $wishedBy))
    {
      $msg .= eventLog('ERROR in: myFeedback->setComment()', ONEDIRUP);
    }
    else
    {
      if (!$myFeedback->saveFeedback())
      {
        $msg .= eventLog('ERROR in: myFeedback->saveFeedback() in file rest_feedbackSetComment.php', ONEDIRUP);
      }
      else
      {
        // -- CLONE INTO MEDIABASE FOR DIRECT USE--
        $myMediaBase = new class_mediaBase(ONEDIRUP);
        $myMediaBase->setSong( base64_decode( $_GET['song'] )); // -- on mediabase it is ok to check if it exists --
        if (!$myMediaBase->setComment( base64_decode( ( cntSubsituteCriticalJSChars( $_GET['comment'] ) ) ), $wishedBy))
        {
          $msg .= eventLog('ERROR in: myMediaBase->setComment() in file rest_feedbackSetComment.php', ONEDIRUP);
        }
        else
        {
          if (!$myMediaBase->saveFeedback())
          {
            $msg .= eventLog('ERROR in: myMediaBase->saveFeedback() in file rest_feedbackSetComment.php', ONEDIRUP);
          }
          else
          {
              // -- as a result of all --
            $msg .= $myFeedback->getLastMsg();
          }
        }
      }
    }
  }

  echo $msg;
