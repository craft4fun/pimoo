<?php
  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  require_once ('../lib/utils.php');
  require_once ('../lib/class_cache.php');

  require_once ('api_layout.php');



  // -- standard check, if the required parameter is given --
  if (isset($_GET['val']))
  {
    // -- ------------------------------- --
    // -- begin of the the very main core --
    // -- ------------------------------- --

    require_once (ONEDIRUP . 'api_mediabase.php');
    $myMediaBase = new class_mediaBase(ONEDIRUP);
    $intermediateResult = $myMediaBase->getRandomSongs(intval(substr($_GET['val'], ONEINT)));
    $output = makeResultItems($intermediateResult);

    $output .= '<div style="clear: both;"></div>';

    /* -- ---------------------------------------------- --
    // -- beg of offer possibilty to add all to playlist --
    //-- get nickame --
    $user = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : $_SERVER['REMOTE_ADDR'];
    // -- result file reference --
    $resultFile = ONEDIRUP . DIR_TEMP . 'lastResult' . UNDSCR . 'random_' . $user;
    // -- use api_playlist --
    require_once (ONEDIRUP.'api_playlist.php');
    $myPlaylist = new class_playlist(ONEDIRUP);
    $myPlaylist::stat_lastResult_pushSongs($resultFile, $intermediateResult, $user);
    $output .= '<div class="listRow" style="text-align: center;" onclick="addAllAddOnce(\''.$resultFile.'\')">'.LNG_ADD_ALL_TO_PLAYLIST.'</div>';
    // -- end of offer possibilty to add all to playlist --
    // -- ---------------------------------------------- --*/

    //$output .= '<div class="listRow" style="text-align: center;" onclick="addAllAddOnce()">'.LNG_ADD_ALL_TO_PLAYLIST.'</div>';

    // -- must be here because the previous and next page procedure --
    $output .= '<div class="listRow" style="text-align: center;" onclick="$(\'btn_rand\').click()">'.LNG_MORE.'</div>';
    $output .= '<div class="listRow" style="text-align: center;" onclick="window.scrollTo(0, 0);">up!</div>';

    unset($myMediaBase);

    // -- ----------------------------- --
    // -- end of the the very main core --
    // -- ----------------------------- --

    // -- "execute" normal way --
    echo $output;
  }

