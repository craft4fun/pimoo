<?php
  //require_once '../keep/config.php';

  // -- ----------------------- --
  // -- begin of HTML standards --
  // -- ----------------------- --

  function makeHTML_begin()
  {
    global $cfg_DOJO;

    $result = '<!DOCTYPE HTML>
      <html>
        <head>
          <meta charset="UTF-8">
          <title>piMooReverse</title>
          <meta name="author" content="Patrick Höf" />
          <meta name="keywords" content="wskkerbborsch piMoo piMooReverse" />
          <meta name="description" content="piMooReverse" />
          <link type="text/css" rel="stylesheet" href="style.css" />
          <link type="text/css" rel="stylesheet" href="styleL.css" media="screen and (min-width: 800px)" />
          <script type="text/javascript" src="../'.$cfg_DOJO.'"
                  djConfig="parseOnLoad: false, isDebug: false, locale: \'en\'">
          </script>
          <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

          <!-- old favicon
          <link rel="icon" type="image/png" href="../res/favicon.png" /> -->
          <!-- new favicon with manifest -->
          <link rel="apple-touch-icon" sizes="180x180" href="../res/favicon/apple-touch-icon.png">
          <link rel="icon" type="image/png" sizes="32x32" href="../res/favicon/favicon-32x32.png">
          <link rel="icon" type="image/png" sizes="16x16" href="../res/favicon/favicon-16x16.png">
          <link rel="manifest" href="../res/favicon/site.webmanifest">
          <link rel="mask-icon" href="../res/favicon/safari-pinned-tab.svg" color="#5bbad5">
          <meta name="msapplication-TileColor" content="#da532c">
          <meta name="theme-color" content="#ffffff">


        ';


  //     <script type="text/javascript" src="script.js"></script>
  //     <script type="text/javascript" src="cnt/dojo1703/dojo/dojo.js"
  //       djConfig="parseOnLoad: true, isDebug: false, locale: \'en\'">
  //     </script>
  //     <script>
    //     dojo.require("dojo.dnd.Container");
    //     dojo.require("dojo.dnd.Manager");
    //     dojo.require("dojo.dnd.Source");
    //     dojo.require("dojo.dnd.Mover");
    //     dojo.require("dojo.dnd.Moveable");
    //     dojo.require("dojo.dnd.move");
  //     </script>
    return $result;
  }

//   function includeStyleSheet()
//   {
//     include ('style.css.php');
//   }

  function includeJavascript()
  {
    global $cfg_playlistRefreshInterval;
    global $cfg_codeWordQuestion;
    global $cfg_minimumSearchChars;
//    include ('../lib/cnt_tools.js.php');

    include ('script.js.php');
    //include (ONEDIRUP . 'cnt/cnt_tools_ext1.js.php');
  }

  function makeHTMLHeaderEndBodyBeg($aParam = NULLSTR)
  {
    return '</head><body'.$aParam.'>';
  }



  /**
   * makes HTML end tag
   * @return string
   */
  function makeHTML_end()
  {
    return '</body></html>';
  }

  // -- --------------------- --
  // -- end of HTML standards --
  // -- --------------------- --


  function makeBottomInfo()
  {
    $result = NULLSTR;

    // -- who is logged in? At the same time this gives the possibility to deal with several playlists! --
    if (isset($_SESSION['str_nickname']))
      $result .= '<a style="float: right; margin: 1em 1em 0em 0em; color: white; text-decoration: none; cursor: pointer;" href="javascript:promptNickname(\''.$_SESSION['str_nickname'].'\')">
                    logged in as: "<span style="font-weight: bold; ">'.$_SESSION['str_nickname'].'</span>"
                  </a>';


    // -- end of contentWrapper --
    $result .= '</div>';
    return $result;
  }


  function piMooReverse_cntStdMsg ($aMessage)
  {
    return '<span id="StdMsg">'.$aMessage.'</span>';
  }


  function piMooReverse_accessCheck()
  {
    global $cfg_codeWordAnswer;
    global $cfg_codeWordQuestion;

    $result = NULLSTR;

    // -- code word of the day --
    // -- the answer should be provided on the party flyer --
    if (
         $cfg_codeWordQuestion !== false &&
         (!isset($_SESSION['bool_codewordOK']) OR (isset($_SESSION['bool_codewordOK']) && $_SESSION['bool_codewordOK'] != $cfg_codeWordAnswer))
      )
    {
      $msg = '<div style="font-size: larger;">'.
               '<a style="color: black;" href="javascript:answerCodeword();">'.piMooReverse_cntStdMsg(LNG_CODEWORD_OF_THE_DAY_HINT).'</a>'.
             '</div>';
      die($msg);
    }

    // -- --------------------- --
    // -- logged in? "nickname" --
    $result .= '<div style="float: none; margin: 0.5em 1em 0.5em 0.5em;">';
      if (!isset($_SESSION['str_nickname']) || (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] == NULLSTR))
      {
         if (!isset($_SESSION['str_nickname']) || (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] == NULLSTR))
         {
           $msg = '<div style="font-size: larger;">'.
                    '<a style="color: black;" href="javascript:promptNickname();">'.piMooReverse_cntStdMsg(LNG_GIMMENICKNAME.'<div style="margin-top: 1em;">'.LNG_PARTYPOOPER).'</div></a>'.
                  '</div>';
           die($msg.'</div>');
         }
      }
    $result .= '</div>';

    return $result;
  }


  /**
   * the HTML rendering for given items per category
   * @param array
   * @return string (HTML)
   */
  function makeResultItems($aItems)
  {
    global $cfg_maxSearchResultsPerPage;

    // -- ask here directly for what is set in a individual browser --

    $count = count($aItems);
    if ( ($count >= $cfg_maxSearchResultsPerPage) ) //  && (!$cfg_cache)
      $result = sprintf('<div id="songCount" style="color: white; text-align: center;"><nobr>'.LNG_SONGS_FOUND_MORE_THAN_.'</nobr></div>', $count);
    else
      $result = sprintf('<div id="songCount" style="color: white; text-align: center;"><nobr>'.LNG_SONGS_FOUND_.'</nobr></div>', $count);


    $i = 0;
    foreach ($aItems as $item)
    {
      // -- prepared, but maybe too much in search results! --
      //$album = (true) ? '<img src="../lib/picDisp.php?pic='.$item['l'].'" />' : $item['l'];
      $album = $item['l'];

      // TODO paddy save results also in session storage to have them all at once in optional function addAllAtOnce()

      $add2PlaylistClick = BLANK.'onclick="pushSong(\'sr_'.$i.'\', \''.base64_encode($item['a']).'\', \''.base64_encode($item['i']).'\', \''.base64_encode($item['t']).'\', \''.$item['p'].'\')"';

      $result .= '
        <div id="sr_'.$i.'" class="listRow fadeOut500ms">

          <div class="playlistItemRightArea" style="overflow: hidden;" '.$add2PlaylistClick.'>
            <div style="float: right;">'.$item['p'].'</div>
            <div style="float: right; margin-top: 1.5em;">'.$album.'</div>
          </div>

          <div class="playlistItemCenterArea" onclick="showSongContext(\''.base64_encode($item['a']).'\', false, false)">
            &nbsp;
          </div>

          <div class="playlistItemLeftArea" style="overflow: hidden;" '.$add2PlaylistClick.'>
            <div style="margin-bottom: 0.5em; white-space: nowrap;">'.$item['i'].'</div>
            <div>'.$item['t'].'</div>
          </div>

        </div>
      ';
      $i++;
    }

    return $result;
  }


