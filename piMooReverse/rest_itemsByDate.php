<?php
  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  require_once ('../lib/utils.php');
  require_once ('../lib/class_cache.php');

  require_once ('api_layout.php');


  // -- standard check, if the required parameter is given --
  if (!isset($_GET['days']))
  {
    echo cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    // -- core of this file --
    $days = $_GET['days'];

    // -- piMooReverse style comes with rows --
    $viewForm = CACHE_RSLT_ROW;

    $offset = (isset($_GET['offset'])) ? $_GET['offset'] : NULLINT;

    $output = NULLSTR;

    // -- cache handling --
    $myCache = new class_outputCache(CACHELIFETIME_INFINITE); // -- because search results come from outer. Note: After importing a new mediabase, the cache will be cleared automatically  --
    $myCache->setCachePath(ONEDIRUP . $cfg_cachePath);
    $myCache->setCacheFile(CACHE_ITEMS_BY_DATE . $days . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $cfg_maxSearchResultsPerPage . $viewForm . CACHE_RSLT_LANG . $lang);

    // -- take it from cache --
    if ($myCache->getIsCacheFileAlive())
    {
      echo '<div style="text-align: center; color: silver;">... ' . LNG_LOADED_FROM_CACHE . ' ...</div>';
      echo file_get_contents($myCache->getCacheFile());
      unset($myCache);
    }

    // -- otherwise show it native and build cache --
    else
    {
      unset($myCache);

      // -- ------------------------------- --
      // -- begin of the the very main core --
      // -- ------------------------------- --
      require_once (ONEDIRUP.'api_mediabase.php');
      $myMediaBase = new class_mediaBase(ONEDIRUP);

      // -- calculate the days --
      $days2 = intval(substr($days, 1)); // -- N60 -> 60 --
      $daysAsSeconds = $days2 * 86400; // 24 * 60 * 60;
      $diff = time() - $daysAsSeconds;  // -- today minus x days --
      $past = date('Ymd', $diff);
      $today = date('Ymd', time());

      $tmp = $myMediaBase->getSongsByDate($past, $today, $offset, $cfg_maxSearchResultsPerPage);
      $output .= makeResultItems($tmp);

      $output .= '<div style="clear: both;">';
        if ($cfg_maxSearchResultsPerPage > NULLINT)
        {
          if (count($tmp) > 0 && count($tmp) >= $cfg_maxSearchResultsPerPage)
            $output .= '<div class="" style="float: right;"><div class="listRowItem"><a class="linkNaviNice" onclick="window.scrollTo(0, 0)" href="javascript:callNewcomer(\''.$days.'\', '.intval($offset + $cfg_maxSearchResultsPerPage).');">'.LNG_NEXT_RESULTS.'</a></div></div>';
          if ($offset > 0)
            $output .= '<div class="" style="float: left;"><div class="listRowItem"><a class="linkNaviNice" onclick="window.scrollTo(0, 0)" href="javascript:callNewcomer(\''.$days.'\', '.intval($offset - $cfg_maxSearchResultsPerPage).');">'.LNG_PREVIOUS_RESULTS.'</a></div></div>';
          else
            $output .= '<div class="" style="float: left;"></div>';
        }
      $output .= '</div>';

      // -- must be here because the previous and next page procedure --
      $output .= '<div class="listRow" style="text-align: center;" onclick="window.scrollTo(0, 0);">up!</div>';

      unset($tmp);
      unset($myMediaBase);
      // -- ----------------------------- --
      // -- end of the the very main core --
      // -- ----------------------------- --


      // -- "execute" normal way --
      echo $output;
      // -- build cache for next time --
      $myCache = new class_outputCache();
      $myCache->setCachePath(ONEDIRUP . $cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_DATE . $days . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $cfg_maxSearchResultsPerPage . $viewForm . CACHE_RSLT_LANG . $lang);
      $myCache->putCacheContent($output);
      unset($myCache);
      unset($output);
    }
  }


