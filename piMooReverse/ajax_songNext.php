<?php
  set_time_limit(2);

  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');

  if (!(isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR))
  {
    echo LNG_ERR_UNKNOWN;
  }
  else
  {
    $userPlaylistJSONfile = ONEDIRUP.$cfg_tmpPathStatic.DIR_PIMOOREVERSE.$_SESSION['str_nickname'].JSON_FILE_EXT;

    // -- load the current playlist --
    $curPlaylist = json_decode(file_get_contents($userPlaylistJSONfile));

    $i = 0;
    $found = 0;
    foreach ($curPlaylist as $song)
    {
      if ($song->c == true)
      {
        $found = $i;
        break;
      }
      $i++;
    }

    // -- pick next song --
    if (count($curPlaylist) > $found + 1)
    {
      $curPlaylist[$found]->c = false;
      $curPlaylist[$found + 1]->c = true;
    }
    // -- if already the last, take the first again --
    else
    {
      $curPlaylist[$found]->c = false;
      $curPlaylist[0]->c = true;
    }

    $curPlaylistStr = json_encode($curPlaylist);
    file_put_contents($userPlaylistJSONfile, $curPlaylistStr);
    echo $curPlaylistStr;
  }

