<?php
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');

  if (!isset($_GET['JSON']))
  {
    echo LNG_ERR_PARAM_MISSING;
  }
  else
  {
    $JSON = $_GET['JSON']; //$JSON = base64_decode($_GET['JSON']);
    if (!(isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR))
    {
      echo LNG_ERR_UNKNOWN;
    }
    else
    {
      // -- create folder if not exists --
      $tmpPath = ONEDIRUP.$cfg_tmpPathStatic.'iMoo';
      if (!is_dir($tmpPath))
        mkdir($tmpPath, 0700); //

      if (strlen($JSON) > 0)
      {
        $file = ONEDIRUP.$cfg_tmpPathStatic.DIR_PIMOOREVERSE.$_SESSION['str_nickname'].JSON_FILE_EXT;
        // -- simply --
        file_put_contents($file, $JSON);
      }
      else
        echo LNG_ERR_UNKNOWN;
    }
  }

