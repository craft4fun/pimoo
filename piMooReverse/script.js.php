<?php
  require_once '../defines.inc.php';
  require_once '../keep/config.php';

  $nickname = (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : NULLSTR;
?>
<script>

// -- defines --
var ppui = 100; // -- player progress update interval (default: 100) --

var directionPrevious = 'P';
var directionNext = 'N';

var previousPlayer = 'player1';
var player = 'player1';
var nextPlayer = 'player2';
var visuPlayer = player;


var playerSongLoaded = false;
var playerState_isPlaying = false;
var playerDuration = 0;

var playtimePlain = '0:00';


var previousSongStr = ''; // -- crossfade or even buffering --
var cuedSongStr = '';
var nextSongStr = ''; // -- crossfade or even buffering --

var crossfadeActive = false; // -- check for crossfade detected yes ... crossfade is starting soon --
//var crossing = false; // -- all conditions are ok and crossfade is going to fade over to next song --

var volUpTimer;
var volDownTimer;

var playlistCount;


// -- not really required --
//var playerTimer = null;

//-- When preview player is playing leaving page will ask for confirmation --
function HandleOnClose()
{
  if (playerState_isPlaying)
  {
    if (!confirm("Sure? That will stop the piMoo reverse player!"))
    {
      return false;
    }
  }
}


// -- basic utility like jQuery :o) --
function $(elemID)
{
  var elem = document.getElementById(elemID);
  if (elem)
  {
    return elem;
  }
  else
  {
    return false;
  }
}

// -- repair the broken DOM (required to make e.g. DND work again) --
//require(["dojo/parser"], function(parser){
//  parser.parse();
//});

//-- Delete a DOM node --
function killElement(element)
{
  if (element)
  {
    var papa = element.parentNode;
    if (papa)
    {
      papa.removeChild(element);
    }
  }
}


/**
 * songs with '&' causes trouble, because the filesystem needs the '&', but the HTML player the &amp;
 * clearly, the '&' is the divider for URL params
 * in combination with cnt_songDisp.php the issue occurs, because it needs the right filename for readfile()
 * here comes the solution:
 */
function base64_encode(aStr)
{
  //return window.btoa(aStr);
  return window.btoa(unescape(encodeURIComponent(aStr))); // -- also for UTF-8 --
}
function base64_decode(aStr)
{
  //return window.atob(aStr);
  return decodeURIComponent(escape(window.atob(aStr)));  // -- also for UTF-8 --
}

function decodeSongURL(aURL)
{
  return aURL;
/*
  var tmp1 = base64_decode(aURL);
  var tmp2 = tmp1.replace('\'', '&acute;');
  var tmp3 = tmp2.replace('&', '%26');
  return tmp3;
*/
}




function DojoAjax(aActionFile, aTargetLocation, aHandleAs, aSync, aPreventCache)
{
  // -- set default, possible values are: text (default), json (old: javascript, xml) HTML is NOT allowed! --
  if (!aHandleAs)
  {
    aHandleAs = 'text';
  }
  if (!aSync)
  {
    aSync = false;
  }
  if (!aPreventCache)
  {
    aPreventCache = true; // -- default is true --
  }

  // Look up the node we´ll stick the text under.
  var targetNode = dojo.byId(aTargetLocation);
  // The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
  var xhrArgs =
  {
    url: aActionFile,
    handleAs: aHandleAs,
    sync: aSync, // -- that is very imortant to have the result in the following functions by the caller --
    preventCache: aPreventCache,
    timeout: <?php echo $cfg_playlistRefreshInterval * 1000; /* 1000 -> give up after 10 seconds */ ?>,

    load: function(data)
    {
      if (aHandleAs == 'text')
      {
        if (targetNode)
        {
          targetNode.innerHTML = data;
        }
      }
      if (aHandleAs == 'json')
      {
        // NOPE: if (targetNode)
        {
          // NOPE: -- this functions needs a hidden elem like <span id="AJAXJSONRESP"> to hold the received data --
          var jsonStr = JSON.stringify(data);
          // NOPE: targetNode.textContent = jsonStr;
          window.sessionStorage.setItem('AJAXJSONRESP',jsonStr);
        }
      }
    },
    error: function(error)
    {
      <?php
        if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
        {
          echo 'if (targetNode) targetNode.innerHTML = ": " + error;';
        }
      ?>
    }
  };
  // Call the asynchronous xhrGet
  var deferred = dojo.xhrGet(xhrArgs);
}


//-- answer the code word of the day --
//-- "login" step 0 --
function answerCodeword()
{
  //var nickname = window.prompt("your wished nickname", aCurrent); // -- window. does not work on mobile devices --
  var codeword = prompt("<?php echo LNG_CODEWORD_OF_THE_DAY; ?>" + "\n(<?php echo $cfg_codeWordQuestion; ?>)");
  if (codeword)
  {
    DojoAjax("../ajax_answerCodeword.php?codeword=" + codeword, "body");
    window.setTimeout("window.location.reload()", 2000);
  }
}


// -- the very main ajax call for claiming or chaning a nickname --
function setNickname(nickname, current)
{
  DojoAjax("../ajax_setNickname.php?nickname=" + nickname + "&current=" + current, "targetDialog"); // , "target_feedback"
}

function promptNickname(aCurrent)
{
  if (!aCurrent)
  {
    aCurrent = "";
  }
  //var nickname = window.prompt("your wished nickname", aCurrent); // -- window. does not work on mobile devices --
  var nickname = prompt("<?php echo LNG_WISHED_NICKNAME; ?>", aCurrent);
  if (nickname)
  {
    //DojoAjax("../ajax_setNickname.php?nickname=" + nickname + "&current=" + aCurrent, "targetDialog"); // , "target_feedback"
    setNickname(nickname, aCurrent);
    window.setTimeout("window.location.reload()", 2000);
  }
}

function logout()
{
  var check = confirm('sure?');
  if (check == true)
  {
    DojoAjax('../ajax_logout.php', '', 'text', true);
    window.location.href='index.php';
  }
}

function showBusy(aParentElem)
{
  elem = $(aParentElem);
  if (elem)
  {
    // width="150" height="150"
    elem.innerHTML = '<div style="text-align: center;"><img src="../res/pleaseWait2.gif" alt="please wait" /></div>';
  }
}



// -- -------------------------------- --
// -- begin of all navi search callers --


function songSearchClean()
{
  var edt_songSearch = $("edt_songSearch");
  if (edt_songSearch)
  {
    localStorage["edt_songSearch"] = '';
    edt_songSearch.value = localStorage["edt_songSearch"];
    edt_songSearch.focus();
  }
}


// -- this function can be used by any caller --
function _callSearchByStr(aStr, aOffset, aTarget)
{
  if (!aOffset)
  {
    aOffset = 0;
  }

  var searchStr = aStr;
  if (searchStr && searchStr.length >= <?php echo $cfg_minimumSearchChars; ?>) // -- usually 3 --
  {
    showBusy(aTarget);
    // -- escape apostrophe (by simply use a double quote, which is normaly not used often in songs :o) hope so) --
    DojoAjax("rest_itemsBySearch.php?search=" + searchStr + "&offset=" + aOffset, aTarget);
  }
}

// -- 1. wrapper for _callSearchByStr --
function callSearchByStr(aStr, aOffset)
{
  _callSearchByStr(aStr, aOffset, 'target_searchResults');
}

//-- 2. wrapper for _callSearchByStr --
function callSearchByStrByContextDlg(aStr, aOffset)
{
  _callSearchByStr(aStr, aOffset, 'targetDialog');
}


// -- this function is to only be used by the free field search button --
function callSearchByEdtSongSearch (aElem)
{
  var edt_songSearch = $("edt_songSearch").value;
  // -- remember the last search to eventually modify an old search --
  localStorage["edt_songSearch"] = edt_songSearch;

  // -- core --
  callSearchByStr(edt_songSearch);
}


// -- searching for a user's comment only, not all other parts like album or title --
function _callSearchByUser(aUser, aOffset, aTarget)
{
  if (!aOffset)
  {
    aOffset = 0;
  }

  var userStr = aUser;
  if (userStr && userStr.length >= <?php echo $cfg_minimumSearchChars; ?>) // -- usually 3 --
  {
    showBusy(aTarget);
    DojoAjax("rest_itemsByUser.php?user=" + userStr + "&offset=" + aOffset, aTarget);
  }
}

//-- 1. wrapper for _callSearchByUser --
function callSearchByUser(aUser, aOffset)
{
  _callSearchByUser(aUser, aOffset, 'target_searchResults');
}

//-- 2. wrapper for _callSearchByUser --
function callSearchByUserByContextDlg(aUser, aOffset)
{
  _callSearchByUser(aUser, aOffset, 'targetDialog');
}

// -- this function is to only be used by the free field search USER button --
function callSearchUserByEdtSongSearch(aElem)
{
  var edt_songSearch = $("edt_songSearch").value;
  // -- remember the last search to eventually modify an old search --
  localStorage["edt_songSearch"] = edt_songSearch;

  // -- core --
  callSearchByUser(edt_songSearch);
}


function callNewcomer(aDays, aOffset)
{
  if (!aOffset)
  {
    aOffset = 0;
  }

  showBusy('target_searchResults');
  DojoAjax('rest_itemsByDate.php?days=' + aDays + "&offset=" + aOffset, "target_searchResults");
}


function callRand(aValue)
{
  showBusy('target_searchResults');
  DojoAjax('rest_itemByRandom.php?val=' + aValue, "target_searchResults");
}



function _loadSearchBar()
{
  if (typeof localStorage['edt_songSearch'] !== typeof undefined) // -- like isset() in PHP --
  {
    var edt_songSearch = $('edt_songSearch');
    if (edt_songSearch)
    {
      edt_songSearch.value = localStorage["edt_songSearch"];
    }
  }
}

// -- end of all navi search callers --
// -- ------------------------------ --





//-- ------------- --
//-- beg of dialog --

function dlg_close()
{
  var viewHead = $('viewHead');
  if ('viewHead')
  {
    viewHead.innerHTML = '<div id="RowAddSong" onclick="dlg_addSong()"><span style="cursor: pointer;">+ add song +</span></div>';
  }


  var targetDialog = $('targetDialog');
  if (targetDialog)
  {
    targetDialog.style.display = 'none';
    targetDialog.innerHTML = '';
  }

  var RowAddSong = $('RowAddSong');
  if (RowAddSong)
  {
    RowAddSong.style.display = 'block';
  }

  _addClearPlaylist();

  var hulk_playlist = $('hulk_playlist');
  if (hulk_playlist)
  {
    hulk_playlist.style.display = 'block';
    loadPlaylist();
  }

}

function dlg_addSong()
{
  var targetDialog = $('targetDialog');
  if (targetDialog)
  {
    targetDialog.style.display = 'block';
  }

  var hulk_playlist = $('hulk_playlist');
  if (hulk_playlist)
  {
    hulk_playlist.style.display = 'none';
    targetDialog.innerHTML = '';
  }

  var RowAddSong = $('RowAddSong');
  if (RowAddSong)
  {
    RowAddSong.style.display = 'none';
  }

  _removeClearPlaylist();

  var viewHead = $('viewHead');
  if ('viewHead')
  {
    //viewHead.innerHTML = '<a class="linkNaviNice" href="javascript:dlg_close()"><div style="width: 99%; height: 99%;">ok, back!</div></a>';
    //viewHead.innerHTML = '<div class="linkNaviNice" onclick="dlg_close()"><div style="width: 99%; height: 99%;">ok, back!</div></div>';
    viewHead.innerHTML = '<div onclick="dlg_close()"><span style="cursor: pointer;">ok, back!</span></div>';
  }

  DojoAjax('addSong.php', 'targetDialog');
  window.setTimeout(_loadSearchBar, 100);
}



function showSongContext(aSong, aSongID, aCurrent, aFirstHit = false)
{
  // -- needed if called e.g. by valuedSong.php to take the first hit of song name without intern mediabase path --
  if (aFirstHit == true)
  {
    aFirstHit = '&firstHit';
  }
  else
  {
    aFirstHit = '';
  }

  var targetDialog = $('targetDialog');
  if (targetDialog)
  {
    targetDialog.style.display = 'block';
  }

  var hulk_playlist = $('hulk_playlist');
  if (hulk_playlist)
  {
    hulk_playlist.style.display = 'none';
    targetDialog.innerHTML = '';
  }

  var RowAddSong = $('RowAddSong');
  if (RowAddSong)
  {
    RowAddSong.style.display = 'none';
  }

  _removeClearPlaylist();

  var viewHead = $('viewHead');
  if ('viewHead')
  {
    //viewHead.innerHTML = '<a class="linkNaviNice" href="javascript:dlg_close()"><div style="width: 99%; height: 99%;">ok, back!</div></a>';
    viewHead.innerHTML = '<span class="linkNaviNice" onclick="dlg_close()"><div style="width: 99%; height: 99%;">ok, back!</div></span>';
  }

  DojoAjax("ajax_showSongContext.php?song=" + aSong + '&songID=' + aSongID + '&current=' + aCurrent + aFirstHit, 'targetDialog');
}



// -- set comment --
function songSetComment(aSong) // , aComment
{
  var curComment = $('curComment');
  if (curComment)
  {
    if (curComment.innerHTML != "<?php echo LNG_NOCOMMENT_DO; ?>")
    {
      oldComment = curComment.innerHTML;
    }
    else
    {
      oldComment = '';
    }
  }

  var comment = prompt("<?php echo LNG_YOUR_COMMENT; ?>", oldComment); // aComment
  if (comment)
  {
    DojoAjax("rest_feedbackSetComment.php?song=" + aSong + "&comment=" + base64_encode( comment ), ''); // songInfo_comment
    if (curComment)
    {
      curComment.innerHTML = comment;
    }
  }
}

// -- set rating --
function songSetRating(aSong, aRating)
{
  DojoAjax("rest_feedbackSetRating.php?song=" + aSong + "&rating=" + aRating, ''); // songInfo_rating
}

//-- end of dialog --
//-- ------------- --





function _addClearPlaylist()
{
  var RowClearPlaylistCheck = $('RowClearPlaylist');
  if (!RowClearPlaylistCheck)
  {
    var newElem = document.createElement('div');
    newElem.textContent = '- clear playlist -';

    newElem.setAttribute('id', 'RowClearPlaylist');
    newElem.setAttribute('class', 'listRow');
    newElem.setAttribute('style', 'text-align: center; opacity: 1;');
    newElem.setAttribute('onclick', 'playlistClear();');

    var viewBody = $('viewBody');
    viewBody.appendChild(newElem);
  }
}

function _removeClearPlaylist()
{
  var RowClearPlaylist = $('RowClearPlaylist');
  if (RowClearPlaylist)
  {
    killElement(RowClearPlaylist);
  }
}







function songMoveUp(aSongID)
{
  // -- server stored playlist --
  DojoAjax('ajax_songMoveUp.php?songID=' + aSongID, '', 'json', true);
  // do it with javascript later ???
  loadPlaylist();
}

function songMoveDown(aSongID)
{
  // -- server stored playlist --
  DojoAjax('ajax_songMoveDown.php?songID=' + aSongID, '', 'json', true);
  // do it with javascript later ???
  loadPlaylist();
}

function songRemove(aSongID, aTitle)
{
  var check = confirm('remove "'+aTitle+'" from playlist?');
  if (check == true)
  {
    // -- server stored playlist --
    DojoAjax('ajax_songRemove.php?songID=' + aSongID, '', 'json', true);
    dlg_close();
  }
}


function playlistClear()
{
  var check = confirm('sure?');
  if (check == true)
  {
    // -- server stored playlist --
    if (playerState_isPlaying)
    {
      loadPlaylist('clear');
    }
    else
    {
      loadPlaylist('clearAll');
      playerSongLoaded = false;
      _removeClearPlaylist();
    }
    _updatePlayerControls();
  }
}

function getPlaylistCount(aByCache)
{
  if (!aByCache)
  {
    aByCache = true;
  }

  if (aByCache)
  {
    return playlistCount;
  }
  else
  {
    var objPlaylist = JSON.parse(_loadPlaylistStr());
    c = 0;
    for (var i in objPlaylist)
    {
      c++;
    }
    return c;
  }
}



function pushSong(aElemStr, aSong, aInterpret, aTitle, aPlaytime)
{
  //  -- give feedback with cursor --
//   var target_searchResults = $('target_searchResults'); //'target_searchResults'
//   if (target_searchResults)
//   {
//     target_searchResults.style.cursor  = 'progress';
//     window.setTimeout("target_searchResults.style.cursor  = 'auto'", 100); // 250
//   }

  var aElem = $(aElemStr);
  if (aElem)
  {
    // -- give feedback with color and let the color stay. So the user has feedback what he has added already --
    aElem.style.opacity  = '0.5'; // 0.25
    aElem.style.color = 'lime';
    //aElem.style.pointerEvents = 'none';
  }

  // -- get the highest ID --
  function _getHiID(aObject)
  {
    var h = 0;
    for (var i in aObject)
    {
      if (aObject[i].id > h)
      {
        h = aObject[i].id;
      }
    }
    return parseInt(h + 1);
  }

  // -- load current playlist storage --
  var objPlaylist = JSON.parse(_loadPlaylistStr());


//  aInterpret = base64_encode(aInterpret);
//  aTitle = base64_encode(aTitle);
//  aSong = base64_encode(aSong);


  //  -- create new entry --
  if (objPlaylist.length > 0)
  {
    var newObjEntry = {"id": _getHiID(objPlaylist), "c": false, "i": aInterpret, "t": aTitle, "p": aPlaytime, "a": aSong};
  }
  else
  {
    var newObjEntry = {"id": 1, "c": true, "i": aInterpret, "t": aTitle, "p": aPlaytime, "a": aSong};
  }

  // -- push into --
  objPlaylist.push(newObjEntry);

  // -- transform back to saveable string --
  var newPlaylistStr = JSON.stringify(objPlaylist);


  // -- save new playlist --
  //var base64 = base64_encode(newPlaylistStr);
  //DojoAjax('ajax_songPush.php?JSON=' + base64);
  DojoAjax('ajax_songPush.php?JSON=' + newPlaylistStr);
}

/*
  c -> currently cued        // boolean

  a -> absolute          // string
  i -> interpret          // string
  t -> title            // string
  p -> play time / (remain|ETA)  // integer
  l -> album            // string

  <div class="dojoDndItem listRow nowPlaying">
  <div style="float: right;">
      <img class="albumPic" src="cnt/cnt_picDisp.php?pic=music/Ride the lightning.jpg" alt="Ride the lightning" />
  </div>
    Metallica
  <audio id="previewPlayer" src="cnt/cnt_songDisp.php?song=music/For whom the bell tolls.mp3" controls onended="" >
    Oops...no audio element supported
  </audio>
    <br>
    <span style="float: right;">
      3:38
    </span>
    For whom the bell tolls
  </div>
*/

/*
function _getAlbumPicPath(aSong, aTarget)
{
  // -- have a look for a pic like file name --
  return aSong.substr(0, aSong.lastIndexOf(".")) + "< ?php echo DOT . $cfg_pictureFileType; ?>";
  // -- if not check an album name --
  // todo: song path + album name
  // -- but how to check if a file is there or not without php? not a bit! --
  //DojoAjax('ajax_songGetAlbumPic.php?song=' + aSong, aTarget);
}
*/

/*
function addAllAddOnce(aFilename)
{
  if (confirm("<?php echo LNG_SURE; ?>"))
  {
    // TODO paddy now? -> must be saved before in sessionStorage and then read back here
    pushSong(\'sr_'.$i.'\', \''.base64_encode($item['a']).'\', \''.base64_encode($item['i']).'\', \''.base64_encode($item['t']).'\', \''.$item['p'].'\');
  }
}
*/



function _buildPlaylistHTML(aObject)
{
  // -- init a string --
  var strPlaylist = '';

  playlistCount = aObject.length;
  var x = 0; // -- crossface or buffering --
  var xminus1 = 0;
  var xplus1 = 0;

  // -- build the playlist html hulk --
  for (var i in aObject)
  {
    strPlaylist = strPlaylist + '<div class="listRow'; // dojoDndItem

      if (aObject[i].c == true)
      {
        // -- add class for now playling --
        strPlaylist = strPlaylist + ' nowPlaying';
      }

      strPlaylist = strPlaylist + '"';

      if (aObject[i].c == true)
      {
        // -- add id for now playling --
        strPlaylist = strPlaylist + ' id="cuedSong" ';
      }

      strPlaylist = strPlaylist + ' data-songID="'+aObject[i].id+'" ';

    strPlaylist = strPlaylist + '>';

      // -- --------- --
      // -- right hulk --
      strPlaylist = strPlaylist + '<div onclick="songMoveDown('+aObject[i].id+')" class="playlistItemRightArea">';

          // -- play time --
          strPlaylist = strPlaylist + '<span style="float: right;">'+aObject[i].p+'</span>';

          // -- album name --
          //strPlaylist = strPlaylist + '<span style="float: right; margin-top: 1.5em;">'+aObject[i].l+'</span>';

      strPlaylist = strPlaylist + '</div>';


      // -- ----------- --
      // -- center hulk --
      //if (aObject[i].c != true) // -- "c"ued song: that blocked the current song --
      {
        strPlaylist = strPlaylist + '<div onclick="showSongContext(\''+aObject[i].a+'\','+aObject[i].id+','+aObject[i].c+')" class="playlistItemCenterArea">';

          // strPlaylist = strPlaylist + '&nbsp;';
          // -- album cover --
          strPlaylist = strPlaylist + '<span id="albPic'+i+'"></span>'; // -- for DojoAjax('ajax_songGetAlbumPic.php?song=' + aObject[i].a, 'albPic'+i); --

        strPlaylist = strPlaylist + '</div>';
      }


      // -- --------- --
      // -- left hulk --
      strPlaylist = strPlaylist + '<div onclick="songMoveUp('+aObject[i].id+')" class="playlistItemLeftArea">';

        strPlaylist = strPlaylist + '<nobr>';

          // -- interpret --
          strPlaylist = strPlaylist + base64_decode(aObject[i].i);

        strPlaylist = strPlaylist + '</nobr><br><nobr>';

          // -- title --
          strPlaylist = strPlaylist + base64_decode(aObject[i].t);

        strPlaylist = strPlaylist + '</nobr>';

      strPlaylist = strPlaylist + '</div>';



      if (aObject[i].c == true)
      {
        cuedSongStr = '../lib/songDisp.php?base64&song='+decodeSongURL(aObject[i].a)+'&hash=<?php echo md5($nickname); ?>';
        x = parseInt(i);

        document.title = 'piMoo#'+base64_decode(aObject[i].t);

        playtimePlain = aObject[i].p;

        /* -- progress bar must get the real max value before work correctly --
        strPlaylist = strPlaylist + '<div style="float: left;  width: 100%;">';
          strPlaylist = strPlaylist + '<progress id="playerProgress" min="0" max="100" value="0" style="opacity: 0.33; width: 100%;"></progress>';
        strPlaylist = strPlaylist + '</div>';*/
      }


      // -- that's it - pick the next (crossfade) song --

      // -- previous --
      xminus1 = x - 1;
      if (xminus1 >= 0)
      {
        previousSongStr = '../lib/songDisp.php?base64&song='+decodeSongURL(aObject[xminus1].a)+'&hash=<?php echo md5($nickname); ?>';
      }
      else
      {
        previousSongStr = '../lib/songDisp.php?base64&song='+decodeSongURL(aObject[playlistCount - 1].a)+'&hash=<?php echo md5($nickname); ?>';
      }


      // -- next --
      xplus1 = x + 1;
      if (xplus1 < playlistCount)
      {
        nextSongStr = '../lib/songDisp.php?base64&song='+decodeSongURL(aObject[xplus1].a)+'&hash=<?php echo md5($nickname); ?>';
      }
      else
      {
        nextSongStr = '../lib/songDisp.php?base64&song='+decodeSongURL(aObject[0].a)+'&hash=<?php echo md5($nickname); ?>';
      }

    strPlaylist = strPlaylist + '</div>'; // -- end of <div class="listRow --
  }




  // -- add row for clear playlist (only if at least one is in) --
  //   if (playlistCount > 1)
  //    _addClearPlaylist();

  // -- build the html --
  $("hulk_playlist").innerHTML = strPlaylist;

  // -- load the album pics --
  for (var i in aObject)
  {
    DojoAjax('ajax_songGetAlbumPic.php?base64&song=' + decodeSongURL(aObject[i].a), 'albPic'+i, 'text', false, false);
  }
}



function _loadPlaylistStr(aCommand)
{
  if (aCommand == 'next')
  {
    // -- prepare playlist for the next song and load back the modified one --
    DojoAjax('ajax_songNext.php', '', 'json', true  );
  }

  if (aCommand == 'previous')
  {
    // -- prepare playlist for the next song and load back the modified one --
    DojoAjax('ajax_songPrevious.php', '', 'json', true  );
  }

  if (aCommand == 'clear')
  {
    // -- clear all songs or except cued and load back the modified one --
    DojoAjax('ajax_playlistClear.php', '', 'json', true);
  }

  if (aCommand == 'clearAll')
  {
    // -- clear all songs or except cued and load back the modified one --
    DojoAjax('ajax_playlistClear.php?clearAll', '', 'json', true);
  }

  if (aCommand != 'next' && aCommand != 'previous')
  {
    // -- load in the normal way --
    DojoAjax('ajax_getPlaylist.php', '', 'json', true);
  }

  //var JSONplaylistStr = $('AJAXJSONRESP').textContent;
  var JSONplaylistStr = window.sessionStorage.getItem('AJAXJSONRESP');
  //JSONplaylistStr = base64_decode(JSONplaylistStr, true);
  return JSONplaylistStr;
}


function _updatePlayerControls()
{
  //function _delay()
  {
    if (playerState_isPlaying)
    {
      $('btn_play').style.visibility = 'hidden';
      $('btn_pause').style.visibility = 'visible';
      if (getPlaylistCount() <= 1)
      {
        $('btn_previous').style.visibility = 'hidden';
        $('btn_next').style.visibility = 'hidden';
      }
      else
      {
        $('btn_previous').style.visibility = 'visible';
        $('btn_next').style.visibility = 'visible';
      }
    }
    if (!playerState_isPlaying)
    {
      $('btn_play').style.visibility = 'visible';
      $('btn_pause').style.visibility = 'hidden';
      if (getPlaylistCount() > 1)
      {
        $('btn_previous').style.visibility = 'visible';
        $('btn_next').style.visibility = 'visible';
      }
      else
      {
        $('btn_previous').style.visibility = 'hidden';
        $('btn_next').style.visibility = 'hidden';
      }
    }
  }
  //window.setTimeout(_delay, 100);
}


function loadPlaylist(aCommand)
{
  if (!aCommand)
  {
    aCommand = '';
  }

  // -- load and parse it from anywhere --
  var objPlaylist = JSON.parse(_loadPlaylistStr(aCommand));

  // -- add row for clear playlist (only if at least one is in) --
  if (objPlaylist.length > 0)
  {
    _addClearPlaylist();
  }


  // -- build the html --
  _buildPlaylistHTML(objPlaylist);

  _updatePlayerControls();

  // -- repair the broken DOM (required to make e.g. DND work again) --
  //  require(["dojo/parser"], function(parser){
  //    parser.parse();
  //  });

}





// -- --------------------- --
// -- beg of player control --

// -- the volume control is for crossfade and not for buttons! --
function volUp(aPlayer)
{
  function _volUp()
  {
    if ($(aPlayer).volume < 0.9)
    {
      $(aPlayer).volume = $(aPlayer).volume + 0.1;
      //console.log("volUp" + $(aPlayer).volume);
    }
  }
  function _delay()
  {
    clearInterval(volUpTimer);
    $(aPlayer).volume = 1;
  }
  volUpTimer = window.setInterval(_volUp, 100);
  window.setTimeout(_delay, 1000);
}

function volDown(aPlayer)
{
  function _volDown()
  {
    if ($(aPlayer).volume > 0.2)
    {
      $(aPlayer).volume = $(aPlayer).volume - 0.1;
      //console.log("volUp" + $(aPlayer).volume);
    }
  }
  function _delay()
  {
    clearInterval(volDownTimer);
    $(aPlayer).volume = 0;
  }
  volDownTimer = window.setInterval(_volDown, 100);
  window.setTimeout(_delay, 1000);
}



//-- this still does not work on every browser --

//-- Rewinds the audio file by 10% --
function playerRewind()
{
  //console.log('rewind');
  var tenPercent = Math.floor( (playerDuration / 100) * 10 );
  if (playerSongLoaded)
  {
    $(player).currentTime -= tenPercent;
    _visualizeProgress($(player).currentTime, playerDuration); // -- required when player is paused --
  }
}

//-- Fast forwards the audio file by 10% --
function playerForward()
{
  console.log('forward');
  console.log('playerSongLoaded: ' + playerSongLoaded);


  var tenPercent = Math.floor( (playerDuration / 100) * 10 );
  //console.log('tenPercent: ' + tenPercent);
  if (playerSongLoaded)
  {
    /*
    var curTi = Math.floor($(player).currentTime);
    //console.log('currentTime before: ' + curTi);
    curTi += tenPercent;
    $(player).currentTime += curTi;
    //console.log('currentTime after: ' + curTi);

    $(player).currentTime += tenPercent;
    _visualizeProgress($(player).currentTime, playerDuration); // -- required when player is paused --

    */

    var curPlayerPos = Math.floor( $(player).currentTime );
    var playerPosPlusTenPercent = curPlayerPos + tenPercent;
    var checkEnd = playerDuration - tenPercent;

    console.log('player: ' + player);
    console.log('curPlayerPos: ' + curPlayerPos);
    console.log('playerPosPlusTenPercent: ' + playerPosPlusTenPercent);
    console.log('checkEnd: ' + checkEnd);

    if (playerPosPlusTenPercent < checkEnd)
    {
      $(player).currentTime = playerPosPlusTenPercent;
      _visualizeProgress($(player).currentTime, playerDuration); // -- required when player is paused --
    }
/*does not work for any reason
    else
    {
      // -- smaller steps now --
      var threePercent = Math.floor( (playerDuration / 100) * 3 );
      var playerPosPlusThreePercent = curPlayerPos + threePercent;

      console.log('curPlayerPos: ' + curPlayerPos);
      console.log('playerPosPlusThreePercent: ' + playerPosPlusThreePercent);

      if (playerPosPlusThreePercent < checkEnd)
        $(player).currentTime = playerPosPlusThreePercent;
    }
*/
  }
}


function playerPrevious()
{
  //console.log('previous');
  if (!crossfadeActive && playerState_isPlaying)
  {
    doCrossfade(directionPrevious);
    window.setTimeout(_previousSong, 1000);
  }
  else
  {
    _previousSong();
  }
}

function playerNext()
{
  //console.log('next');
  //console.log('crossfadeActive: ' + crossfadeActive);
  //console.log('playerState_isPlaying: ' + playerState_isPlaying);
  if (!crossfadeActive && playerState_isPlaying)
  {
    //console.log('doCrossfade: ' + doCrossfade);
    doCrossfade(directionNext);
    window.setTimeout(_nextSong, 1000);
  }
  else
  {
    _nextSong();
  }
}


/**
 * called by button "previous" only
 */
function _previousSong()
{
  console.log('!!! PREVIOUSSONG !!!');
  console.log('player: ' + player);

  // -- crossfade --
  if (crossfadeActive)
  {
    visuPlayer = nextPlayer;
  }

  var playedBefore = playerState_isPlaying;
  playerState_isPlaying = false;

  if (typeof playerTimer !== typeof undefined) // -- like isset() in PHP --
  {
    clearInterval(playerTimer);
  }

  // -- this has to be called together here --
  playerSongLoaded = false;
  loadPlaylist('previous'); // -- next song and reload the server side stored playlist is the same function to have only one request! --
  //_songPreviousPlaylistClient(); // -- do the previous mechanism for the client side stored --

  if (playedBefore)
  {
    playerPlay(crossfadeActive);
    playerState_isPlaying = true; // -- hm, russian - because set before  --
    playerSongLoaded = true; // -- hm, russian - because set before  --
  }

  // -- test --
  volDown(previousPlayer);

  crossfadeActive = false;
}




/**
 * called by event "end" or button "next"
 */
function _nextSong()
{
  console.log('!!! NEXTSONG !!!');
  console.log('player: ' + player);

  // -- crossfade --
  if (crossfadeActive)
  {
    visuPlayer = nextPlayer;
  }

  var playedBefore = playerState_isPlaying;
  playerState_isPlaying = false;

  if (typeof playerTimer !== typeof undefined) // -- like isset() in PHP --
  {
    clearInterval(playerTimer);
  }

  // -- this has to be called together here --
  playerSongLoaded = false;
  loadPlaylist('next'); // -- next song and reload the server side stored playlist is the same function to have only one request! --

  if (playedBefore)
  {
    playerPlay(crossfadeActive);
    playerState_isPlaying = true;  // -- hm, russian - because set before --
    playerSongLoaded = true; // -- hm, russian - because set before  --
  }

  // -- test --
  volDown(previousPlayer);

  crossfadeActive = false;
}


function showPlayerControlTab_buttons()
{
  $('playerControlTab_buttons').style.display = 'block';
  $('playerControlTab_crossfade').style.display = 'none';
}


function playerPlay(aCrossing)
{
  if (!aCrossing)
  {
    aCrossing = false;
  }

  //console.log('playerPlay: ' + player);

  if (!aCrossing)
  {
    // -- load song --
    if (!playerSongLoaded)
    {
      playerSongLoaded = true;
      $(player).src = cuedSongStr;
      //$(player).load(); // -- not needed and NOT good! --
      //$(player).preload = 'auto';
    }

    // hm? still required?
    if ($(player).currentTime > 0) // -- at the beginning of the song no fade in is required --
    {
      $(player).volume = 0;
    }

    $(player).play();
    playerState_isPlaying = true;

    // -- fade in --
    volUp(player);
  }

  // -- very important --
  $(player).addEventListener("ended", _nextSong, true);

  // -- calculate pure seconds progress --
  //var playtimePlain = getCurrentSongDuration();
  var pos = playtimePlain.indexOf(':');
  var minutes = playtimePlain.substr(0, pos);
  var seconds = playtimePlain.substr(pos + 1);

  playerDuration =  parseInt(minutes * 60) + parseInt(seconds);
  //$('playerProgress').max = playerDuration;


  $('btn_play').style.visibility = 'hidden';
  //if (getPlaylistCount() <= 1)
    //$('btn_next').style.visibility = 'hidden';
  $('btn_pause').style.visibility = 'visible';

  showPlayerControlTab_buttons();

  //if (typeof playerTimer !== typeof undefined) // -- like isset() in PHP --
    playerTimer = window.setInterval(_updateProgress, ppui);

}




function playerPause()
{
  //console.log('playerPause: ' + player);

  function _delay()
  {
    // -- prepared --
    //localStorage.setItem('currentTime', $(player).currentTime);

    $(player).pause();
    playerState_isPlaying = false;

    $('btn_play').style.visibility = 'visible';
    //if (getPlaylistCount() > 1)
      //$('btn_next').style.visibility = 'visible';
    $('btn_pause').style.visibility = 'hidden';

    clearInterval(playerTimer);
  }

  window.setTimeout(_delay, 1000);
  volDown(player);
}




function doCrossfade(aDirection)
{
  if (!aDirection)
  {
    aDirection = directionNext;
  }

  crossfadeActive = true;

  //console.log('!!! CROSSFADE !!!');
  $('playerControlTab_buttons').style.display = 'none';
  $('playerControlTab_crossfade').style.display = 'block';

  // -- switch player --
  if (player == 'player1')
  {
    previousPlayer = 'player1';
    player = 'player2';
    nextPlayer = 'player2';
  }
  else // else is important when switching on the own variable
  {
    previousPlayer = 'player2';
    player = 'player1';
    nextPlayer = 'player1';
  }

  //if (nextSongStr != '' || previousSongStr != '')
  {
    if (aDirection == directionPrevious)
    {
      cuedSongStr = previousSongStr;
    }
    if (aDirection == directionNext)
    {
      cuedSongStr = nextSongStr;
    }

    //playerPlay(crossfadeActive);
    // -- a part of "next play" --
    $(player).volume = 0; // -- start silent --
    volUp(player);
    playerSongLoaded = true;
    $(player).src = cuedSongStr;
    $(player).play();
    playerState_isPlaying = true;
  }

  // in _nextSong(): crossfadeActive = false;
}

function checkCrossfade()
{
  // -- calculate when to fade over and fire it --
   var onePerc = playerDuration / 100;
   var playerCurrentTime = Math.floor( $(player).currentTime );

   // -- crossfade begin --
   var crossSec = Math.floor( 5 * onePerc ); // -- 5% before song end --
   if ( playerCurrentTime >= parseInt(playerDuration - crossSec) )
   {
    if (!crossfadeActive)
    {
      doCrossfade(directionNext);
    }
   }

   /*
   // -- song end --
   var endSec = Math.floor( 1 * onePerc ); // -- 1% before song end --
   if ( playerCurrentTime >= parseInt(playerDuration - endSec) )
   {
     _nextSong();
   }
   */
}


function _updateProgress()
{
  // -- grafical visualization - do something awesome like fading the background color      and a progressbar --
  _visualizeProgress($(visuPlayer).currentTime, playerDuration);

  // -- crossfade --
  checkCrossfade();

  //console.log('progress: ' + $(player).currentTime);
  //console.log(player);
}


//-- end of player control --
//-- --------------------- --







function _visualizeProgress(aCurPos, aMaxValue)
{
  //   console.log("aCurPos: " + aCurPos);
  //   console.log("aMaxValue: " + aMaxValue);

  // -- progressbar --
  //$('playerProgress').value = aCurPos;


  // -- make aMaxValue to 255
  // -- make aCurPos to x
  var x = (255 * aCurPos) / aMaxValue;


  function getCol(aCur, aMax)
  {
  return 0;
    var result = 0;

    if (aCur < aMax / 2)
      result = aCur;
    if (aCur >= aMax / 2 && aCur <= aMax)
      result = aMax - aCur;

    console.log( Math.floor( result.toString() ) );
    return Math.floor( result.toString() );
  }



  //$('cuedSong').style.backgroundColor = 'rgb(50,'+(Math.floor(aCurPos)).toString()+',50)';
  //$('cuedSong').style.background = '-webkit-radial-gradient(center, black, rgb(50,'+(Math.floor(aCurPos)).toString()+',50))';

  // lime 00ff00
  // red ff0000



  // -- this does fade from red to lime --
  $('cuedSong').style.background = 'radial-gradient(' +
                    'center,' +
                    'black,' +
                    'rgb('+Math.floor(x).toString()+', '+Math.floor(255 - x).toString()+', '+getCol(x, 255)+')' +
                  ')';
  $('cuedSong').style.background = '-moz-radial-gradient(' +
                      'center,' +
                      'black,' +
                      'rgb('+Math.floor(x).toString()+', '+Math.floor(255 - x).toString()+', '+getCol(x, 255)+')' +
                    ')';
  $('cuedSong').style.background = '-webkit-radial-gradient(' +
                      'center,' +
                      'black,' +
                      'rgb('+Math.floor(x).toString()+', '+Math.floor(255 - x).toString()+', '+getCol(x, 255)+')' +
                    ')';

}

/*
function _resetCuedColor()
{
  // 4a4aff is a kind of blue
  // 58a658 is a dimmed lime
  $('cuedSong').style.background = 'radial-gradient(center, black, #58a658)';
  $('cuedSong').style.background = '-moz-radial-gradient(center, black, #58a658)';
  $('cuedSong').style.background = '-webkit-radial-gradient(center, black, #58a658)';
}
*/


/*
function getCurrentSongDuration()
{
  var duration = '0:00';
  var objPlaylist = JSON.parse(_loadPlaylistStr());
  for (var i in objPlaylist)
  {
    if (objPlaylist[i].c == true)
    {
      duration = objPlaylist[i].p;
      break;
    }
  }
  return duration;
}
*/


</script>
