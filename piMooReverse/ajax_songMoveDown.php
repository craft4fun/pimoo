<?php
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once '../lib/utils.php';

  if (!(isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR))
  {
    echo LNG_ERR_UNKNOWN;
  }
  else
  {
    if (!isset($_GET['songID']))
    {
      echo LNG_ERR_PARAM_MISSING;
    }
    else
    {
      $userPlaylistJSONfile = ONEDIRUP.$cfg_tmpPathStatic.DIR_PIMOOREVERSE.$_SESSION['str_nickname'].JSON_FILE_EXT;

      // -- load the current playlist --
      $curPlaylist = json_decode(file_get_contents($userPlaylistJSONfile));

      $count = count($curPlaylist);
      if ($count > 1)
      {
        $i = 0;
        $found = -1;
        foreach ($curPlaylist as $song)
        {
          if ($song->id == $_GET['songID'])
          {
            $found = $i;
            break;
          }
          $i++;
        }

        // -- move down requested song --
        if ($found >= 0 && $found < $count - 1)
          $curPlaylist = moveElementInArray($curPlaylist, $found, $found + 1);
        else
          $curPlaylist = moveElementInArray($curPlaylist, $found, 0);

        // -- that does the trick, removing whitespaces to avoid senseless json structure --
        $curPlaylist = array_merge($curPlaylist);
      }

      $curPlaylistStr = json_encode($curPlaylist);
      file_put_contents($userPlaylistJSONfile, $curPlaylistStr);
      echo $curPlaylistStr;
    }
  }

