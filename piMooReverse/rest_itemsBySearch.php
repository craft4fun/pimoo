<?php
  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  require_once ('../lib/utils.php');
  require_once ('../lib/class_cache.php');

  require_once ('api_layout.php');


  // -- standard check, if the required parameter is given --
  if (!isset($_GET['search']))
  {
    echo cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {

    // -- core of this file --
    $caseSensitive = (isset($_COOKIE['bool_caseSensitive']) && $_COOKIE['bool_caseSensitive'] == YES) ? true : false; // -- this is still required for cache handling --
    $search = $_GET['search'];

    $offset = (isset($_GET['offset'])) ? $_GET['offset'] : NULLINT;

    $DL_Level = (isset($_COOKIE['int_DL_level'])) ? $_COOKIE['int_DL_level'] : NULLINT;

    $cutBlank = (isset($_COOKIE['bool_cutBlank'])) ? $_COOKIE['bool_cutBlank'] : YES;

    // -- ppiMooReverseReverse style comes with rows --
     $viewForm = CACHE_RSLT_ROW;

    $output = NULLSTR;

    // -- cache handling --
    $myCache = new class_outputCache(CFG_SEARCH_CACHELIFETIME); // -- THE TRICK IS: the old votings are still in cache, but that's ok :o) So the new votings and the old be will found! --
    $myCache->setCachePath(ONEDIRUP . $cfg_cachePath);
    $myCache->setCacheFile(CACHE_ITEMS_BY_SEARCH . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $cfg_maxSearchResultsPerPage . CACHE_RSLT_VIEWFORM . $viewForm . CACHE_RSLT_LANG . $lang . CACHE_RSLT_DAMLEV . $DL_Level . CACHE_RSLT_CASESENSITIVITY . $caseSensitive . CACHE_RSLT_CUTBLANK . $cutBlank);

    // -- take it from cache --
    if ($myCache->getIsCacheFileAlive())
    {
      echo '<div style="text-align: center; color: silver;">... ' . LNG_LOADED_FROM_CACHE . ' ...</div>';
      echo file_get_contents($myCache->getCacheFile());
      unset($myCache);
    }

    // -- otherwise show it native and build cache --
    else
    {
      unset($myCache);

      // -- ------------------------------- --
      // -- begin of the the very main core --
      // -- ------------------------------- --
      //moved upper: $DL_Level = (isset($_COOKIE['int_DL_level'])) ? $_COOKIE['int_DL_level'] : 0;

      require_once (ONEDIRUP.'api_mediabase.php');
      $myMediaBase = new class_mediaBase(ONEDIRUP);
      $tmp = $myMediaBase->getSearchResult($search, $offset, $cfg_maxSearchResultsPerPage, $DL_Level, $caseSensitive); // , $cfg_searchFeedback
      $output .= makeResultItems($tmp);

      $output .= '<div style="clear: both;">';
      if ($cfg_maxSearchResultsPerPage > NULLINT)
        {
          if (count($tmp) > NULLINT && count($tmp) >= $cfg_maxSearchResultsPerPage)
            $output .= '<div class="" style="float: right;"><div class="listRowItem"><a class="linkNaviNice" onclick="window.scrollTo(0, 0)" href="javascript:callSearchByStr(\''.$search.'\', '.intval($offset + $cfg_maxSearchResultsPerPage).');">'.LNG_NEXT_RESULTS.'</a></div></div>';
          if ($offset > NULLINT)
            $output .= '<div class="" style="float: left;"><div class="listRowItem"><a class="linkNaviNice" onclick="window.scrollTo(0, 0)" href="javascript:callSearchByStr(\''.$search.'\', '.intval($offset - $cfg_maxSearchResultsPerPage).');">'.LNG_PREVIOUS_RESULTS.'</a></div></div>';
          else
            $output .= '<div class="" style="float: left;"></div>';
        }
      $output .= '</div>';

      // -- must be here because the previous and next page procedure --
      $output .= '<div class="listRow" style="text-align: center;" onclick="window.scrollTo(0, 0);">up!</div>';

      unset($tmp);
      unset($myMediaBase);
      // -- ----------------------------- --
      // -- end of the the very main core --
      // -- ----------------------------- --


      // -- "execute" normal way --
      echo $output;
      // -- build cache for next time --
      $myCache = new class_outputCache();
      $myCache->setCachePath(ONEDIRUP . $cfg_cachePath);
      $myCache->setCacheFile(CACHE_ITEMS_BY_SEARCH . $search . CACHE_RSLT_OFFSET . $offset . CACHE_RSLT_MAX . $cfg_maxSearchResultsPerPage . CACHE_RSLT_VIEWFORM . $viewForm . CACHE_RSLT_LANG . $lang . CACHE_RSLT_DAMLEV . $DL_Level . CACHE_RSLT_CASESENSITIVITY . $caseSensitive . CACHE_RSLT_CUTBLANK . $cutBlank);
      $myCache->putCacheContent($output);
      unset($myCache);
      unset($output);
    }

  }


