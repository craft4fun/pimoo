<?php
  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  require_once ('../lib/utils.php');
  include_once ('../api_feedback.php');
  include_once ('../api_mediabase.php');

  $msg = NULLSTR;

  if ($_GET['rating'] == NULLSTR)
  {
    $msg .= cntErrMsg(LNG_ERR_NO_RATING_SELECTED);
    eventLog('ERROR in rest_feedbackSetRating.php, required parameter missing', ONEDIRUP);
  }
  else
  {
    //-- get nickame --
    $wishedBy =  (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname']: NULLSTR; // -- $_SERVER['REMOTE_ADDR'] --> do not fill the feedback.xml with obsolete information --

    // -- is admin or registered user? --
    if (
        (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
        ||
        (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
      )
    {
      if (!isset($_GET['song']) || !isset($_GET['rating']))
      {
        $msg .= eventLog('ERROR, required parameter missing in rest_feedbackSetRating.php', ONEDIRUP);
      }
      else
      {
        $song = $_GET['song'];
        $rating = $_GET['rating'];

        $myFeedback = new class_feedback(ONEDIRUP);
        $myFeedback->setSong(base64_decode($song)); // -- feedback must make an entry in EVERY case --
        if (!$myFeedback->setRating($rating, $wishedBy))
        {
          $msg .= eventLog('ERROR in: myFeedback->setRating() in file rest_feedbackSetRating.php', ONEDIRUP);
        }
        else
        {
          if (!$myFeedback->saveFeedback())
          {
            $msg .= eventLog('ERROR in: myFeedback->saveFeedback() in file rest_feedbackSetRating.php', ONEDIRUP);
          }
          else
          {
            // -- CLONE INTO MEDIABASE FOR DIRECT USE--
            $myMediaBase = new class_mediaBase(ONEDIRUP);
            $myMediaBase->setSong(base64_decode($song)); // -- on mediabase it is ok to check if it exists --
            if (!$myMediaBase->setRating($rating, $wishedBy))
            {
              $msg .= eventLog('ERROR in: myMediaBase->setRating() in file rest_feedbackSetRating.php', ONEDIRUP);
            }
            else
            {
              if (!$myMediaBase->saveFeedback())
              {
                $msg .= eventLog('ERROR in function myMediaBase->saveFeedback() in file rest_feedbackSetRating.php', ONEDIRUP);
              }
              else
              {
                // -- as a result of all --
                $msg .= $myFeedback->getLastMsg();
              }
            }
          }
        }
      }
    }
  }

  echo $msg;
