<?php
  require_once '../defines.inc.php';
  require_once '../keep/config.php';

  require 'api_layout.php';

  echo makeHTML_begin();

  includeJavascript();

  echo makeHTMLHeaderEndBodyBeg(BLANK . 'onbeforeunload="return HandleOnClose()"' . BLANK);

  //STILL todo   echo piMoo_accessCheck();

  if (isset($_SESSION['str_nickname']))
  {
    echo '<img src="../res/logoutM.png" id="logoLogout" alt="logout" onclick="logout()" />';


  //<span id="AJAXJSONRESP" style="display: none; color: white;">AJAXJSONRESP</span>
?>

<img src="../res/piMoo_logoDark.png" id="logoBackTopiMoo" alt="back to piMoo" onclick="window.location.href='../index.php'" />


<div id="playerControlTab"> <!-- class="centerAlignedBlock"  -->

  <audio id="player1">Oops...no audio element supported</audio>
  <!-- later required for crossfade -->
  <audio id="player2">Oops...no audio element supported</audio>


  <span id="playerControlTab_buttons">
    <nobr>
      <img id="btn_rewind"   class="playerButton" src="../res/rewind.png"   onclick="playerRewind()"   alt="rewind"   style="visibility: visible;" />
      <img id="btn_forward"  class="playerButton" src="../res/forward.png"  onclick="playerForward()"  alt="forward"  style="visibility: visible; margin-right: 1em;" />

      <img id="btn_play"     class="playerButton" src="../res/play.png"     onclick="playerPlay()"     alt="play"     style="visibility: hidden;"  />
      <img id="btn_pause"    class="playerButton" src="../res/pause.png"    onclick="playerPause()"    alt="pause"    style="visibility: hidden;  margin-right: 1em;"  />

      <img id="btn_previous" class="playerButton" src="../res/previous.png" onclick="playerPrevious()" alt="previous" style="visibility: visible;" />
      <img id="btn_next"     class="playerButton" src="../res/next.png"     onclick="playerNext()"     alt="next"     style="visibility: visible;" />

      <!--
      <img id="btn_up"       class="playerButton" src="../res/arrowUp.png"  onclick="window.scrollTo(0, 0);" alt="up" />
       -->
    </nobr>
  </span>

  <span id="playerControlTab_crossfade" style="display: none;" ondblclick="showPlayerControlTab_buttons()">~ crossfading ~</span>

</div>


     <img id="btn_up"       class="playerButton" src="../res/arrowUp.png"  onclick="window.scrollTo(0, 0);" alt="up" />




<div id="contentWrapper">  <!-- in makeBottomInfo -->

  <header id="viewHead">
    <div id="RowAddSong" onclick="dlg_addSong()">
       <span style="cursor: pointer;">+ add song +</span>
    </div>
  </header>


  <section id="targetDialog" style="display: block;"></section>


  <section id="viewBody">

    <script type="text/javascript">
      dojo.addOnLoad(loadPlaylist);
    </script>

    <!-- preload the pleaseWait.gif -> so it is in cache for later use -->
    <img style="width: 0px; height: 0px; display: none;" src="../res/pleaseWait2.gif" />

    <div id="hulk_playlist"></div>

  </section>

<?php

    echo makeBottomInfo();
  }
  else
  {
    echo '<span id="errorMsg"><a style="text-decoration: none; color: black;" href="javascript:promptNickname()">'.LNG_GIMMENICKNAME.'</a></span>';
  }

  //require_once ('../defines.inc.php');
  //echo "LINUX2: " . LINUX;
//   require_once '../keep/config.php';
//   echo "cfg_logFile: $cfg_logFile";
//   require_once ('../lib/utils.php');
//   eventLog('XXXXXXXXXXXXXXXXXXXXXXXx', ONEDIRUP);



  echo makeHTML_end();
