<?php
  // -- special piMoo stuff --
  include_once ('../defines.inc.php');
  include_once ('../keep/config.php');
  include_once ('../lib/utils.php');
  include_once ('../api_mediabase.php');

  $msg = NULLSTR;

  if (isset($_GET['song']))
  {
    // -- needed if called e.g. by valuedSong.php to take the first hit of song name without intern mediabase path --
    $firstHit = (isset($_GET['firstHit'])) ? true : false;

    $songBase64 = $_GET['song'];
    $song = base64_decode($_GET['song']);

    $myMediabase = new class_mediaBase(ONEDIRUP);
    $info = $myMediabase->getSongInfo($song, $firstHit);

    // -- check if the song can be found in the mediabase --
    if ($info === false)
    {
      $msg .= cntErrMsg(LNG_ERR_NOSONGINFO);
    }
    else
    {
      // -- if found by luckiness, the first hit (without intern mediabase path, only filename) --
      if ($firstHit)
        $msg .= '<div style="text-align: center; color: red;">'.LNG_FIRST_HIT.'</div>';


      // -- ------------------ --
      // -- begin of song info --
      // -- ------------------ --

      // -- escape the apostrophe for further javascript issues --
      $escapedInterpret = str_replace('\'', '\\\'', $info['i']);
      $escapedTitle = str_replace('\'', '\\\'', $info['t']);
      $escapedAlbum = str_replace('\'', '\\\'', $info['l']);

      $escapedRatingBy = str_replace('\'', '\\\'', $info['rby']);
      $escapedComment = str_replace('\'', '\\\'', $info['c']);
      $escapedCommentBy = str_replace('\'', '\\\'', $info['cby']);

      // -- ------------------------------------------------------------------------------------------------------------------------ --
      // -- beg of detect state of song to vote or delete the song, but only if not the currently played or called by search results --
      $current = (isset($_GET['current']) && $_GET['current'] == 'true') ? true : false;
      $songID = (isset($_GET['songID']) && $_GET['songID'] != 'false') ? $_GET['songID'] : false;
      $allowDelete = (!$current && $songID !== false);
      $allowAddPlaylist = true;
      $allowVote = ($current && $songID !== false);
      // -- end of detect state of song to vote or delete the song, but only if not the currently played or called by search results --
      // -- ------------------------------------------------------------------------------------------------------------------------ --


      $pushSong = 'pushSong(\'sia\', \''.$songBase64.'\', \''.base64_encode($info['i']).'\', \''.base64_encode($info['t']).'\', \''.$info['p'].'\')';

      // -- -------------- --
      // -- add and delete --
      if ($allowAddPlaylist) // -- sia: SongInfoAdd --
        $msg .= '<div id="sia" class="listRow centerAligned fadeOut500ms" style="min-height: 2em; line-height: 2em;" onclick="'.$pushSong.'">'.LNG_ADD_TO_PLAYLIST.'</div>'; // songRemove('+aObject[i].id+', \''+aObject[i].t+'\')

      if ($allowDelete)
        $msg .= '<div class="listRow centerAligned" style="min-height: 2em; line-height: 2em;" onclick="songRemove('.$_GET['songID'].',\''.$escapedTitle.'\')">'.LNG_DELETE.'</div>'; // songRemove('+aObject[i].id+', \''+aObject[i].t+'\')


      $msg .= '
        <div class="listRow">
          <span style="float: right;"><a class="linkNice" href="javascript:callSearchByStrByContextDlg(\''.$escapedInterpret.'\')">'.$info['i'].'</a></span>
          <span>'.LNG_INTERPRET.'</span>
        </div>
        <div class="listRow">
          <span style="float: right;"><a class="linkNice" href="javascript:callSearchByStrByContextDlg(\''.$escapedTitle.'\')">'.$info['t'].'</a></span>
          <span>'.LNG_TITLE.'</span>
        </div>
        <div class="listRow">
          <span style="float: right;">'.$info['p'].'</span>
          <span>'.LNG_DURATION.'</span>
        </div>
        <div class="listRow">
          <span style="float: right;"><a class="linkNice" href="javascript:callSearchByStrByContextDlg(\''.$escapedAlbum.'\')">'.$info['l'].'</a></span>
          <span>'.LNG_ALBUM.'</span>
        </div>
        <div class="listRow">
          <span style="float: right;"><a class="linkNice" href="javascript:callSearchByStrByContextDlg(\''.$info['y'].'\')">'.$info['y'].'</a></span>
          <span>'.LNG_YEAR.'</span>
        </div>
        <div class="listRow">
          <span style="float: right;"><a class="linkNice" href="javascript:callSearchByStrByContextDlg(\''.$info['g'].'\')">'.$info['g'].'</a></span>
          <span>'.LNG_GENRE.'</span>
        </div>
        <div class="listRow" id="songInfo_rating">
          <span style="float: right;">';

      if ($allowVote)
      {
        function _isRateSel5($aRate) {
          if ($aRate == RATE5)
            return BLANK . 'selected' . BLANK; }
            function _isRateSel4($aRate) {
              if ($aRate == RATE4)
                return BLANK . 'selected' . BLANK; }
                function _isRateSel3($aRate) {
                  if ($aRate == RATE3)
                    return BLANK . 'selected' . BLANK; }
                    function _isRateSel2($aRate) {
                      if ($aRate == RATE2)
                        return BLANK . 'selected' . BLANK; }
                        function _isRateSel1($aRate) {
                          if ($aRate == RATE1)
                            return BLANK . 'selected' . BLANK; }

          $rateSelBox = '<select class="rating" onchange="songSetRating(\''.$songBase64.'\', this.value);" title="'.$escapedRatingBy.'">';
          $curRating = $info['r'];
          if ($curRating != RATE1 && $curRating != RATE2 && $curRating != RATE3 && $curRating != RATE4 && $curRating != RATE5)
            $rateSelBox .= '<option value="">'.LNG_NORATING_VOTE.'</option>';
          $rateSelBox .= '
            <option value="'.RATE5.'" '._isRateSel5($curRating).'>'.RATE5_HUMAN.'</option>
            <option value="'.RATE4.'" '._isRateSel4($curRating).'>'.RATE4_HUMAN.'</option>
            <option value="'.RATE3.'" '._isRateSel3($curRating).'>'.RATE3_HUMAN.'</option>
            <option value="'.RATE2.'" '._isRateSel2($curRating).'>'.RATE2_HUMAN.'</option>
            <option value="'.RATE1.'" '._isRateSel1($curRating).'>'.RATE1_HUMAN.'</option>
          </select>';

          $msg .= $rateSelBox;
        }
        else // -- in case it is not the current song, it serves as a search link only --
          $msg .= '<a class="linkNice" href="javascript:callSearchByStrByContextDlg(\''.$info['r'].'\')">'.cntGetHumanRating($info['r']).'</a>';


          if (strlen($info['r']) > NULLINT)
          {
            $msg .= '<span style="margin-left: 1em;">(by: <a href="javascript:callSearchByUserByContextDlg(\''.$escapedRatingBy.'\')">'.$info['rby'].'</a>)</span>';
          }

        $msg .= '
          </span>
          <span>'.LNG_RATING.'</span>
        </div>
        <div class="listRow" id="songInfo_comment">
          <span style="float: right;">';

        $commentExists = (strlen($info['c']) > NULLINT);
        if ($allowVote) // -- in case it is the current song, it can be changed --
        {
          if (!$commentExists)
            $msg .= '<span style="color: yellow;" id="curComment" onclick="songSetComment(\''.$songBase64.'\');">'.LNG_NOCOMMENT_DO.'</span>';
          else
            $msg .= '<span style="color: yellow;" id="curComment" onclick="songSetComment(\''.$songBase64.'\');">'.$info['c'].'</span>';
        }
        else // -- in case it is not the current song, it serves as a search link only --
          $msg .= '<a class="linkNice" href="javascript:callSearchByStrByContextDlg(\''.$escapedComment.'\')">'.$info['c'].'</a>';

        if ($commentExists)
          $msg .= '<span style="margin-left: 1em;">(by: <a href="javascript:callSearchByUserByContextDlg(\''.$escapedCommentBy.'\')">'.$info['rby'].'</a>)</span>';

        $msg .= '
          </span>
            <span>'.LNG_COMMENT.'</span>
          </div>
          <div class="listRow">
            <span style="float: right;">'.$info['d'].'</span>
            <span>'.LNG_FILE_DATE.'</span>
          </div>
          <div class="listRow">
            <span style="float: right;">'.$info['a'].'</span>
            <span>'.LNG_PATH_FILE.'</span>
          </div>
        ';


       // -- ----------- --
       // -- cover thumb --

       // -- only logged in can see preview? --
       if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
       {
         $picCheck = ONEDIRUP . cntGetAlbumPicURL($song, $info['l'], false, true);
         if (false !== $picCheck && $picCheck != ONEDIRUP)
           $msg .= '<div id="coverPicFrame"><img id="coverPic" src="'.$picCheck.'" /></div>';
       }

       // -- ------ --
       // -- lyrics --
       $msg .= '<style type="text/css">
         #lyric {
           width: 50%;
           max-width: 600px;
           color: white;
           text-shadow: 2px 1px 0 #333;
           margin-left: 5%;
           margin-top: 1em;
           float: left;
        }
      </style>';

      $pathToSong = dirname($cfg_absPathToSongs . $song) . SLASH;
      $lyricPrep = $pathToSong . basename($song, DOT . $cfg_musicFileType) . DOT . $cfg_textFileType;
      $lyric = (file_exists($lyricPrep)) ? '<pre>' . file_get_contents($lyricPrep)  . '</pre>' : LNG_NOLYRIC_AVAILABLE; // file_get_contents($lyricPrep)
      $msg .= '<article id="lyric">'.$lyric.'</article>';
    }

    unset($myMediabase);
  }

  echo $msg;
