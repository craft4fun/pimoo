<?php
  require_once ('../defines.inc.php');
  require_once ('../keep/config.php');
  require_once '../lib/utils.php';

  if (!(isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR))
  {
    echo LNG_ERR_UNKNOWN;
  }
  else
  {
    if (!isset($_GET['songID']))
    {
      echo LNG_ERR_PARAM_MISSING;
    }
    else
    {
      $userPlaylistJSONfile = ONEDIRUP.$cfg_tmpPathStatic.DIR_PIMOOREVERSE.$_SESSION['str_nickname'].JSON_FILE_EXT;
      // -- load the current playlist --
      $curPlaylist = json_decode(file_get_contents($userPlaylistJSONfile));

      $i = 0;
      $found = 0;
      foreach ($curPlaylist as $song)
      {
        if ($song->id == $_GET['songID'])
        {
          $found = $i;
          break;
        }
        $i++;
      }

      // -- remove the requested item --
      unset($curPlaylist[$found]);
      // -- that does the trick, removing whitespaces to avoid senseless json structure --
      $curPlaylist = array_merge($curPlaylist);
      $curPlaylistStr = json_encode($curPlaylist);
      file_put_contents($userPlaylistJSONfile, $curPlaylistStr);
      echo $curPlaylistStr;
    }
  }

