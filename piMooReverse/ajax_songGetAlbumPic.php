<?php
  require_once '../defines.inc.php';
  require_once '../keep/config.php';
  require_once '../api_mediabase.php';


  if (!isset($_SESSION['str_nickname']) || trim($_SESSION['str_nickname']) == NULLSTR)
  {
    echo LNG_ERR_UNKNOWN_FUNNY;
  }
  else
  {
    if (!isset($_GET['song']))
    {
      echo LNG_ERR_PARAM_MISSING;
    }
    else
    {
      // -- switches if the URL comes base64 encoded and has to be decoed before --
      $song = (isset($_GET['base64'])) ? base64_decode($_GET['song']) : $_GET['song'];

      // -- first attempt: take the song path and file name --
      $pathToSong = dirname($song) . SLASH; //old: $cfg_absPathToSongs . $aSong

      $cover = $pathToSong . basename($song, DOT . $cfg_musicFileType) . DOT . $cfg_pictureFileType;
      if (file_exists($cfg_absPathToSongs . $cover))
      {
        $cover = str_replace('&', '%26', $cover);
        $picLink = '<img class="albumPic" src="../lib/picDisp.php?pic='.$cover.'&hash='.md5($_SESSION['str_nickname']).'" />';
      }
      else
      {
        // -- second attempt: take the album name --
        $myMediaBase = new class_mediaBase(ONEDIRUP);
        $info = $myMediaBase->getSongInfo($song); //, true

        // -- check for the album name --
        $cover = $pathToSong . $info['l'] . DOT . $cfg_pictureFileType;

        // -- finally take something --
        if (file_exists($cfg_absPathToSongs . $cover))
        {
          $cover = str_replace('&', '%26', $cover);
          $picLink = '<img class="albumPic" src="../lib/picDisp.php?pic='.$cover.'&hash='.md5($_SESSION['str_nickname']).'" />';
        }
        else
          $picLink = $info['l'];

        unset($myMediaBase);
      }
      echo $picLink;
    }
  }



