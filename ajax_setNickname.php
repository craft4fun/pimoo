<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  $scriptname = basename(__FILE__);

  if (!isset($_GET['nickname']))
  {
    //    $_SESSION['str_nickname'] = $_SERVER['REMOTE_ADDR'];
    echo cntErrMsg(LNG_ERR_UNKNOWN_FUNNY);
    eventLog('missing parameter "nickname" in '.$scriptname);
  }
  else
  {
    $current = (isset($_GET['current'])) ? $_GET['current'] : NULLSTR;

    $myNickname = new class_nickname();
    if ($myNickname->push($_GET['nickname'], $current))
    {
      echo cntOkMsg(MSG_THANKS . ' <span style="font-weight: bold;">' . trim($_GET['nickname']) . '</span>'); // cntOkMsg
    }
    else
    {
      echo cntErrMsg('<span style="font-weight: bold;">' . LNG_TAKE_ANOTHER_NICK . '</span>'); //
    }
    unset($myNickname);
  }
