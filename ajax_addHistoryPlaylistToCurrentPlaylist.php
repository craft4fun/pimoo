<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  include_once ('api_playlist.php');


  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

  if (!$admin)
  {
    echo LNG_ERR_ONLYADMIN;
  }
  else
  {
    if (isset($_GET['playlist']))
    {
      //-- get nickame --
      if (isset($_SESSION['str_nickname']))
        $wishedBy = $_SESSION['str_nickname'];
      else
        $wishedBy = $_SERVER['REMOTE_ADDR'];

      $myPlaylist = new class_playlist();
      $myPlaylist->setWishedBy($wishedBy);

      $msg = NULLSTR;

      $historyPlaylist = simplexml_load_file(DIR_KEEP . 'partyReport' . SLASH . $_GET['playlist']);
      foreach ($historyPlaylist as $song)
      {
        $check = $myPlaylist->addSong($song->a, $song->i, $song->t, $song->p, $song->l, $admin);
        eventLog($myPlaylist->getLastMsg() . COLON . BLANK .  $song->t); // -- no longer required: strip_tags() --

        if ($check === true)
          $msg .= $myPlaylist->getLastMsg() . BLANK . '"' . $song->t . '"' . LFH;
        else
          $msg .= cntErrMsg($myPlaylist->getLastMsg() . BLANK . '"' . $song->t . '"' . LFH);

      }
      unset($historyPlaylist);
      unset($myPlaylist);

      echo $msg;
    }
  }


