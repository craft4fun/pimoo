#!/usr/bin/php
<?php
  require ('defines.inc.php');
  require ('keep/config.php');
  require ('lib/utils.php');

  require ('api_playlist.php');

  // -- needed, if it is to be controlled by runlevel scripts --
  define ('PLAYERSCRIPT', 'cli_player.php');

  $path = getCLIpathDependOnConfig($cfg_isDocker, $cfg_system, __DIR__);
  // -- finally set path - anyhow --
  chdir($path);


  /**
   * -- called by external trigger -> mouse event --
   */
  function addRandomSong()
  {
    global $cfg_playlistFile;

    if (!file_exists($cfg_playlistFile))
    {
      $newfile = fopen($cfg_playlistFile, 'w');
      fwrite($newfile, '<?xml version="1.0" encoding="UTF-8"?>'.LFC.'<playlist>'.LFC.'</playlist>');
      fclose($newfile);
      unset($newfile);
    }

    $myMediaBase = new class_mediaBase();
    $rand = $myMediaBase->getRandomSong();
    unset($myMediaBase);


    $myPlaylist = new class_playlist();

    $myPlaylist->setWishedBy(NICK_TRIGGER);

    // -- add randomly picked song to playlist --
    $myPlaylist->addSong($rand['a'], $rand['i'], $rand['t'], $rand['p'], $rand['l']);
    $myPlaylist->setWishedBy(NULLSTR);

    $myPlaylist->pushPlayerRemainTime(parseHumanMinSecStringToSeconds($rand['p']));

    // -- fill parallel the party report --
    if ($myPlaylist->partyReport)
      $myPlaylist->partyReport_addSong($rand['a'], $rand['i'], $rand['t'], $rand['p'], $rand['l'], $myPlaylist->wishedBy);

    unset($myPlaylist);
  }

  addRandomSong();
