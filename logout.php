<?php
  if (!isset($_SESSION)) // -- session starting here is an exception as an emergency case --
    session_start();
  $_SESSION['bool_admin'] = false;

  header('Location: index.php');
