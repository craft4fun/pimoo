<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_playlist.php';

  if (isset($_GET['index']))
  {
    // -- is admin? --
    $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);

    // -- the wisher can delete his own added song --
    if (isset($_GET['wishedBy']) && $_SESSION['str_nickname'] == $_GET['wishedBy'])
      $admin = true;

    $myPlaylist = new class_playlist();
    $myPlaylist->removeSong($_GET['index'], $admin);
    $msg = $myPlaylist->getLastMsg();
    //eventLog('removed from playlist [index]: ' . $_GET['index']);
    unset($myPlaylist);
    echo $msg;
  }
