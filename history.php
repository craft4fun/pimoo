<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();


  if (isset($_SERVER['HTTP_REFERER']))
    $back = $_SERVER['HTTP_REFERER'];
  else
    $back = 'index.php';


  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">'.LNG_HISTORY.'</div>';

    echo '<span><a class="linkNaviNice" href="'.$back.'">'.LNG_BACK.'</a></span>';

  echo '</div>';

?>
<div style="padding: 1em">

<div id="motto" style="float: right; color: orange; padding: 1em;">use is free for friends of me ;o)</div>

<h1 style="color: blue; font-style: italic; "><?php echo LNG_HISTORY; ?></h1>


<h2>2024-12-10 <span style="color: blue;">V0.31.0</span> <span style="color: grey;">"A²rea"</span></h2>
<ul>
  <li>Admin area window can be opened free to move in a separate window.</li>
</ul>

<h2>2024-11-29 <span style="color: blue;">V0.30.0</span> <span style="color: grey;">"SYSTEM"</span></h2>
<ul>
  <li>Some internal improvements regarding operating system handling.</li>
</ul>

<h2>2024-11-24 <span style="color: blue;">V0.29.0</span> <span style="color: grey;">"ALPINE"</span></h2>
<ul>
  <li>Basic Alpine support (best for small docker images).</li>
</ul>

<h2>2024-11-21 <span style="color: blue;">V0.28.0</span> <span style="color: grey;">"SEARCH"</span></h2>
<ul>
  <li>Add more options for search.</li>
</ul>

<h2>2024-11-02 <span style="color: blue;">V0.27.0</span> <span style="color: grey;">"MQTT"</span></h2>
<ul>
  <li>Realtime playlist reload and side reload via MQTT (broker required).</li>
</ul>

<h2>2024-06-09 <span style="color: blue;">V0.26.0</span> <span style="color: grey;">"F-KEYS"</span></h2>
<ul>
  <li>Add F5, F6 and F7 keys in addition to left, middle and right mouse button.</li>
</ul>

<h2>2023-11-23 <span style="color: blue;">V0.25.1</span> <span style="color: grey;">"HOME-PINNED"</span></h2>
<ul>
  <li>Add manifest for attaching link to mobiles home screen and working standalone.</li>
</ul>

<h2>2023-07-25 <span style="color: blue;">V0.24.2</span> <span style="color: grey;">"GENRE"</span></h2>
<ul>
  <li>Little change on dark style</li>
</ul>

<h2>2023-03-28 <span style="color: blue;">V0.23.1</span> <span style="color: grey;">"GENRE"</span></h2>
<ul>
  <li>Black- and Whitelist for rating when randomly pick sond in partymode</li>
</ul>

<h2>2023-03-28 <span style="color: blue;">V0.23.0</span> <span style="color: grey;">"GENRE"</span></h2>
<ul>
  <li>Whitelist genre template</li>
</ul>

<h2>2023-03-03 <span style="color: blue;">V0.22.1</span> <span style="color: grey;">"JOIN"</span></h2>
<ul>
  <li>Show comment bubble in search results.</li>
</ul>

<h2>2023-02-27 <span style="color: blue;">V0.22.0</span> <span style="color: grey;">"JOIN"</span></h2>
<ul>
  <li>Use join in queries when working in database mode.</li>
</ul>

<h2>2023-01-25 <span style="color: blue;">V0.21.3</span> <span style="color: grey;">"BACKENDPROTECTION"</span></h2>
<ul>
  <li>To top link right aligned.</li>
</ul>

<h2>2023-01-06 <span style="color: blue;">V0.21.2</span> <span style="color: grey;">"BACKENDPROTECTION"</span></h2>
<ul>
  <li>Show rating in default search result.</li>
</ul>

<h2>2023-01-02 <span style="color: blue;">V0.21.1</span> <span style="color: grey;">"BACKENDPROTECTION"</span></h2>
<ul>
  <li>Add genre whitelist to shadow config.</li>
</ul>

<h2>2022-12-19 <span style="color: blue;">V0.21.0</span> <span style="color: grey;">"BACKENDPROTECTION"</span></h2>
<ul>
  <li>Add built in password protection for backend.</li>
</ul>

<h2>2022-12-07 <span style="color: blue;">V0.20.1</span> <span style="color: grey;">"ROOTFONT"</span></h2>
<ul>
  <li>Initialize root font trick 62.5%</li>
  <li>Display zoom option</li>
</ul>

<h2>2022-12-05 <span style="color: blue;">V0.19.2</span> <span style="color: grey;">"STYLING"</span></h2>
<ul>
  <li>Simplify table and add emojis.</li>
</ul>

<h2>2022-12-04 <span style="color: blue;">V0.19.1</span> <span style="color: grey;">"STYLING"</span></h2>
<ul>
  <li>New, fresh styling.</li>
</ul>

<h2>2022-12-01 <span style="color: blue;">V0.18.1</span> <span style="color: grey;">"SHADOW"</span></h2>
<ul>
  <li>Split party-report-list in backend.</li>
</ul>

<h2>2022-11-25 <span style="color: blue;">V0.18.0</span> <span style="color: grey;">"SHADOW"</span></h2>
<ul>
  <li>Overwrite default configuration with shadow configuration temporarily.</li>
</ul>

<h2>2022-11-03 <span style="color: blue;">V0.17.1</span> <span style="color: grey;">"VISU"</span></h2>
<ul>
  <li>Center align non mobile page.</li>
</ul>

<h2>2022-10-29 <span style="color: blue;">V0.17.0</span> <span style="color: grey;">"VISU"</span></h2>
<ul>
  <li>Visualizate progress on omnipresent song bar.</li>
</ul>

<h2>2022-10-23 <span style="color: blue;">V0.16.0</span> <span style="color: grey;">"FASTNICK"</span></h2>
<ul>
  <li>Allow claim nickname by tapping onto existing ones.</li>
</ul>

<h2>2022-10-22 <span style="color: blue;">V0.15.2</span> <span style="color: grey;">"WEBROOT"</span></h2>
<ul>
  <li>Internal improvement, join several functions to have a single point of failure.</li>
</ul>

<h2>2022-06-15 <span style="color: blue;">V0.15.1</span> <span style="color: grey;">"WEBROOT"</span></h2>
<ul>
  <li>Add currently played song into the middle of the hitlist instead of bottom.</li>
</ul>

<h2>2022-05-25 <span style="color: blue;">V0.15.0</span> <span style="color: grey;">"WEBROOT"</span></h2>
<ul>
  <li>Adapt to Debian Apache default webroot /var/www/html/.</li>
</ul>

<h2>2022-05-23 <span style="color: blue;">V0.14.1</span> <span style="color: grey;">"GPIO"</span></h2>
<ul>
  <li>Fix wrong behaviour regarding add all at once.</li>
</ul>

<h2>2022-03-07 <span style="color: blue;">V0.14.0</span> <span style="color: grey;">"GPIO"</span></h2>
<ul>
  <li>Remove wiringpi and replace it with pigpio.</li>
</ul>

<h2>2022-02-25 <span style="color: blue;">V0.13.2</span> <span style="color: grey;">"DARKMODE"</span></h2>
<ul>
  <li>Save party-report upside down.</li>
</ul>

<h2>2022-01-01 <span style="color: blue;">V0.13.1</span> <span style="color: grey;">"DARKMODE"</span></h2>
<ul>
  <li>Finalize dark mode details.</li>
</ul>

<h2>2021-12-28 <span style="color: blue;">V0.13.0</span> <span style="color: grey;">"DARKMODE"</span></h2>
<ul>
  <li>In addition to piMooApp mode, added a common dark mode.</li>
</ul>

<h2>2021-12-23 <span style="color: blue;">V0.12.2</span> <span style="color: grey;">"DATABASE"</span></h2>
<ul>
  <li>Cleanup reload behaviour and fix a very old issue regarding random search results.</li>
</ul>

<h2>2021-12-23 <span style="color: blue;">V0.12.1</span> <span style="color: grey;">"DATABASE"</span></h2>
<ul>
  <li>Make tap-N-search function also available in party report and hitlist view.</li>
  <li>New background color in special admin area.</li>
</ul>

<h2>2021-12-21 <span style="color: blue;">V0.12.0</span> <span style="color: grey;">"DATABASE"</span></h2>
<ul>
  <li>Upgrade database strategy.</li>
  <li>Drop support for XML2 strategy.</li>
</ul>

<h2>2021-11-26 <span style="color: blue;">V0.11.3</span> <span style="color: grey;">"NANE"</span></h2>
<ul>
  <li>Disable calling song details from preview page.</li>
</ul>

<h2>2021-11-08 <span style="color: blue;">V0.11.2</span> <span style="color: grey;">"NANE"</span></h2>
<ul>
  <li>Use nicer font.</li>
</ul>

<h2>2021-10-21 <span style="color: blue;">V0.11.1</span> <span style="color: grey;">"NANE"</span></h2>
<ul>
  <li>Do not close song search bar automatically in mobile mode.</li>
</ul>

<h2>2021-10-06 <span style="color: blue;">V0.11.0</span> <span style="color: grey;">"IMPORT_RANGE"</span></h2>
<ul>
  <li>Give the possibilty to limit the time range for recently added music files also in backend import.</li>
</ul>

<h2>2020-12-26 <span style="color: blue;">V0.10.0</span> <span style="color: grey;">"LONG_SESSION"</span></h2>
<ul>
  <li>Enforce sessions to last longer to keep nicknames longer.</li>
</ul>

<h2>2020-12-24 <span style="color: blue;">V0.9.5</span> <span style="color: grey;">"XMAS"</span></h2>
<ul>
  <li>Move myMoo access link to the right.</li>
  <li>Enlarge system value bars.</li>
</ul>

<h2>2020-12-22 <span style="color: blue;">V0.9.4</span> <span style="color: grey;">"STASH"</span></h2>
<ul>
  <li>Show Raspberry Pi CPU temperature in admin area.</li>
  <li>Bugfixes in API response.</li>
</ul>

<h2>2020-11-30 <span style="color: blue;">V0.9.1</span> <span style="color: grey;">"STASH"</span></h2>
<ul>
  <li>Add a stash function. You can hide and fetch songs from your personal stash.</li>
  <li>Add possibility to delete previously made comments.</li>
  <li>Bugfixes.</li>
</ul>

<h2>2020-06-25 <span style="color: blue;">V0.9.0</span> <span style="color: grey;">"piMoo big-bang"</span></h2>
<ul>
  <li>First release of piMoo. A fork of my good old iMuh project!</li>
  <li>if you wish to see full history <a href="javascript:openOldHistory()">tap here</a></li>
</ul>

<div id="iMuhHistory" style="display: none;">
  <?php
    echo file_get_contents('history_old.php')
  ?>
</div>


<h1 style="color: red; font-style: italic; margin-top: 5em;">known bugs</h1>

<h2>2024</h2>
<ul>
  <li>All functions and features are well testet when <?php echo PROJECT_NAME; ?> works in mediabase variant "xml". Using a mariadb database could be buggy or differ in features</li>
  <li>For the rest, see <a target="blank" href="https://gitlab.com/craft4fun/pimoo/-/issues">project</a></li>
</ul>




<h1 style="color: purple; font-style: italic; margin-top: 5em;">licenses</h1>

<h2><?php echo PROJECT_NAME; ?> (this software)</h2>
<pre>
  <?php
    echo file_get_contents('LICENSE.md')
  ?>
</pre>


<h2>References</h2>
<ul>
  <li>piMoo <a href="https://craft4fun.de" target="_blank">Homepage</a></li>
  <li>Projekt page on <a href="https://gitlab.com/craft4fun/pimoo" target="_blank">GitLab</a></li>
  <li>Docker <a href="https://hub.docker.com/r/craft4fun/pimoo" target="_blank">Hub</a></li>
  <li>App using api v1 <a href="https://play.google.com/store/apps/details?id=de.craft4fun.pimooapp" target="_blank">Google Play Store</a></li>
</ul>


<h2 style="color: green; font-style: italic; margin-top: 5em;">included third party software</h2>

<h3>Dojo Toolkit</h3>
<pre>
  http://dojotoolkit.org/
</pre>

<h3>TableSort</h3>
<pre>
  // TableSort 8.16
  // Jürgen Berkemeier, 29. 4. 2013
  // www.j-berkemeier.de
</pre>

<h3>getID3</h3>
<pre>
  /////////////////////////////////////////////////////////////////
  /// getID3() by James Heinrich info@getid3.org                 //
  //  available at http://getid3.sourceforge.net                 //
  //            or http://www.getid3.org                         //
  /////////////////////////////////////////////////////////////////
</pre>

<h3>PHP Fortschrittsbalken</h3>
<pre>
  /*
  *  PHP Fortschrittsbalken v1.3
  *  (c) 2010 by Fabian Schlieper
  *  fabian@fabi.me
  *  http://fabi.me/
  */
</pre>


</div>
<?php
  echo makeHTML_end();
