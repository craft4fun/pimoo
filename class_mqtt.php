<?php

  class mqtt
  {
    private $depth = NULLSTR;
    private $mqtt = NULL;
    private $server;
    private $port;
    private $username;
    private $password;
    private $client_id;

    private $qualityOfService = 0;
    private $retain = false;

    private $topic = CFG_MQTT_USER;
    private $message;


    function __construct($depth = NULLSTR)
    {
      global $cfg_mqtt_broker_unified;
      global $cfg_mqtt_port;

      $this->depth = $depth;

      // require('../phpMQTT.php'); // composer style
      require('lib/phpMQTT/phpMQTT.php');

      $this->server = $cfg_mqtt_broker_unified;
      $this->port = $cfg_mqtt_port;
      $this->username = CFG_MQTT_USER;
      $this->password = CFG_MQTT_PASSWD;
      // -- make sure this is unique for connecting to sever - you could use uniqid() --
      $this->client_id = CFG_MQTT_CLIENTID_PREFIX.'.backend.'.uniqid();

      // $mqtt = new Bluerhinos\phpMQTT($server, $port, $client_id); // composer style
      $this->mqtt = new phpMQTT($this->server, $this->port, $this->client_id);
    }

    private function replaceUmlaute($string)
    {
      $search = array("Ä", "Ö", "Ü", "ä", "ö", "ü", "ß"); // , "´"
      $replace = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss"); // , ""
      return str_replace($search, $replace, $string);
    }

    function setDepth($value)
    {
      $this->depth = $value;
    }

    function setTopic($value)
    {
      $this->topic = $value;
    }

    function addSubTopic($value)
    {
      $this->topic .= SLASH . $value;
    }

    function setMessage($value)
    {
      $this->message = $this->replaceUmlaute($value);
    }



    /**
     * publish message to broker
     *
     * @return void
     */
    function publish($autoClose = false)
    {
      $result = false;

      if (!$this->mqtt->connect(true, NULL, $this->username, $this->password))
      {
        eventLog('Time out!', NULLSTR, 'mqtt', $this->depth);
      }
      else
      {
        // $mqtt->publish('bluerhinos/phpMQTT/examples/publishtest', 'Hello World! at ' . date('r'), 0, false);
        // $this->mqtt->publish($this->topic, $this->predictID.SLASH.$this->message, $this->qualityOfService, $this->retain);
        // $this->mqtt->publish($this->topic, $this->message, $this->qualityOfService, $this->retain);
        $this->mqtt->publish($this->topic, $this->message, $this->qualityOfService, $this->retain);

        if ($autoClose)
        {
          $this->mqtt->close();
        }

        $result = true;
      }

      return $result;
    }

    /**
     * short hand for publish
     *
     * @return void
     */
    function pub($autoClose = false)
    {
      $this->publish($autoClose);
    }


    function closeConnection()
    {
      $this->mqtt->close();
    }


  }