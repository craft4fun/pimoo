<style>

BODY {

  <?php
    define('COL_DARK','#303030');
    define('COL_BAIGE','#DBA901');
    define('COL_BLUEGRAY','#769CB1');
    define('COL_YELLOW','#FFFDEB');

    //if (file_exists('res/piMoo_bgDark.png') && !$isMobile)
      //echo 'background-image:url(\'res/piMoo_bgDark.png\');';
  ?>

  color: white;
  background-color: <?php echo COL_DARK; ?>;
}

/* -- all selection elements --*/
SELECT {
  background-color: <?php echo COL_DARK; ?>;
  color: white;
}

OPTION {
  background-color: <?php echo COL_DARK; ?>;
  /*color: white;*/
}


.commonStyleBGcolor {
  background-color: <?php echo COL_DARK; ?>;
}


/* this is partly dynamically overwritten while song is playing over time */
#omniPresentSong {
  background-color: <?php echo COL_DARK; ?>;
}

#adminControlBar {
  background-color: <?php echo COL_BLUEGRAY; ?>;
  background: radial-gradient(#FFFFFF, <?php echo COL_BAIGE; ?>);
  background: -moz-radial-gradient(#FFFFFF, <?php echo COL_BAIGE; ?>);
  background: -webkit-radial-gradient(#FFFFFF, <?php echo COL_BAIGE; ?>);
}



/* the basic template for all messages */
#target_feedback, #StdMsg, #okMsg, #errorMsg {
  color: white;

  border-color: blue;
  border-bottom-width: 2px;
  border-style: inset;
  box-shadow: 3px 3px 3px blue,
    -3px 3px 3px blue,
    3px -3px 3px navy,
    -3px -3px 3px silver;

  background-color: <?php echo COL_DARK; ?>;
  background: radial-gradient(<?php echo COL_DARK; ?>, black);
  background: -moz-radial-gradient(<?php echo COL_DARK; ?>, black);
  background: -webkit-radial-gradient(<?php echo COL_DARK; ?>, black);
}

#frame_navi {
  /* box-shadow: 2px 2px 2px <?php echo COL_BAIGE; ?>,
    -2px 2px 2px <?php echo COL_BAIGE; ?>,
    2px -2px 2px <?php echo COL_BAIGE; ?>,
    -2px -2px 2px <?php echo COL_BAIGE; ?>; */

  box-shadow: 2px 2px 2px <?php echo COL_BAIGE; ?>;
}

#ctl_hideNavi {
  border-color: <?php echo COL_BAIGE; ?>;
  border-style: dashed;
  border-width: 1px;
}


.menuButton:hover {
  background-color: black;
}


#edt_songSearch {
  color: white;

  box-shadow: 2px 2px 2px grey,
              2px 2px 2px grey,
              2px 2px 2px grey,
              2px 2px 2px grey;
}


#onAirTitle {
  color: #00ff0f;
  text-shadow: 3px 3px 10px #00ff0f, 1px 1px silver;
}
#onAirWaiting {
  color: orange;
  text-shadow: 2px 2px 7px #e3726b;
}


.modernCenterBackground {
  color: <?php echo COL_BAIGE; ?>;
}



<?php
  // -- ------------------------ --
  // -- begin of mediaBase group --
  // -- ------------------------ --
?>

.grpLstContL {
  /*box-shadow: 2px 2px 2px <?php echo COL_BLUEGRAY; ?>,
    -2px 2px 2px <?php echo COL_BLUEGRAY; ?>,
    2px -2px 2px <?php echo COL_BLUEGRAY; ?>,
    -2px -2px 2px <?php echo COL_BLUEGRAY; ?>;*/

  box-shadow:         inset 0 0 30px <?php echo COL_BLUEGRAY; ?>;
  -moz-box-shadow:    inset 0 0 30px <?php echo COL_BLUEGRAY; ?>;
  -webkit-box-shadow: inset 0 0 30px <?php echo COL_BLUEGRAY; ?>;


  color: white;
}
.grpLstContL:HOVER {
  box-shadow:         inset 0 0 30px <?php echo COL_BAIGE; ?>;
  -moz-box-shadow:    inset 0 0 30px <?php echo COL_BAIGE; ?>;
  -webkit-box-shadow: inset 0 0 30px <?php echo COL_BAIGE; ?>;
}

.grpLstItmL {
  text-decoration: none;
  color: black;
  cursor: default;
}

.grpLstContR {
  color: <?php echo COL_YELLOW; ?>;

  box-shadow:         inset 0 0 20px <?php echo COL_YELLOW; ?>;
  -moz-box-shadow:    inset 0 0 20px <?php echo COL_YELLOW; ?>;
  -webkit-box-shadow: inset 0 0 20px <?php echo COL_YELLOW; ?>;
}

.grpLstContR:HOVER {
  box-shadow:         inset 0 0 20px <?php echo COL_BAIGE; ?>;
  -moz-box-shadow:    inset 0 0 20px <?php echo COL_BAIGE; ?>;
  -webkit-box-shadow: inset 0 0 20px <?php echo COL_BAIGE; ?>;
}


.grpLstItmR {
  text-decoration: none;
  color: black;
  cursor: default;
}


.tableContent {
  border-bottom-style: solid;
  border-bottom-width: 1px;
  border-bottom-color: #fff79e;
}

.tableContentRight {
  border-top-right-radius: 7px;
  border-bottom-right-radius: 10px;
  background-color: #252525;
  border-right-style: solid;
  border-right-width: 1px;
  border-right-color: #fff79e;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  border-bottom-color: #fff79e;
}


/* Sortable tables */
table.sortable thead {
  background-color: #404040;
}


<?php
// -- ---------------------- --
// -- end of mediaBase group --
// -- ---------------------- --



// -- ----------------- --
// -- begin of playlist --
// -- ----------------- --
?>

.playListRowL {
  box-shadow:         inset 0 0 25px <?php echo COL_BLUEGRAY; ?>;
  -moz-box-shadow:    inset 0 0 25px <?php echo COL_BLUEGRAY; ?>;
  -webkit-box-shadow: inset 0 0 25px <?php echo COL_BLUEGRAY; ?>;
}


.listItemInterpret{
  color: #50acf2; /* #5275f7 #50acf2 #6ebbf5 */
}

.listItemTitle{
  color: white;
}

.listItemRemainTime {
  color: #f77c7c; /* #ff5454 */
}

.listItemWishedBy{
  color: #f7f78d; /* #ffff80 #f7f78d */
}


.playListRowR {
  float: right;
  margin: 10px 0px 0px 0px;
  padding: 5px;

  background-color: #fff79e;
  background: radial-gradient(#fff79e, silver);
  background: -moz-radial-gradient(#fff79e, silver);
  background: -webkit-radial-gradient(#fff79e, silver);

  min-width: 2em;
  min-height: 1.5em;

  transition: all 0.5s;

  box-shadow: 3px 3px 3px silver,
    -3px 3px 3px silver,
    3px -3px 3px silver,
    -3px -3px 3px silver;

  border-radius: 5px;
}
.playListRowR:HOVER {
  box-shadow: 6px 6px 6px #fff79e,
    -6px 6px 6px silver,
    6px -6px 6px silver,
    -6px -6px 6px #fff79e;
}

.playListItemR {
  color: white;
}



.plInfoRow{
  font-style: italic;
  cursor: default;
  text-align: center;
}


.comment {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 30px;';
    else
      echo 'padding: 15px 0px 0px 0px;';
  ?>
  cursor: default;
  background-image: url("res/comment<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;

}

.rating {
  margin-left: 1em;
  margin-bottom: 1em;
  background-color: transparent;
  border-radius: 3px;

  border-style: solid;
  border-width: 1px;
  border-left-color: gray;
  border-top-color: gray;
  border-right-color: black;
  border-bottom-color: black;

  <?php
    if ($isMobile)
      echo 'padding: 10px;';
    else
      echo 'padding: 5px;';
  ?>
}



.arrowDown {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowDown<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.arrowUp {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowUp<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.arrowUpTop {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowUpTop<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}


<?php
  // -- --------------- --
  // -- end of playlist --
  // -- --------------- --
?>



.watermarkCover {
  width: 90%;
  text-align: center;
  position: absolute;
  line-height: 99%;
  pointer-events: none;
}
.watermarkBgText{
  opacity: 0.1;
  color: white;
  text-shadow: 1px 1px 2px black, 0 0 25px red, 0 0 5px orange;
}



.textShadowBlur {
  text-shadow: 0px 0px 1px #808080;
}

.boxShadow {
  box-shadow: 3px 3px 3px grey,
     3px 3px 3px grey,
     3px 3px 3px grey,
     3px 3px 3px grey;
}

.explainText {
  color: gray;
  font-size: smaller;
  margin: 0.5em;
}


.watermark {
  background-color: <?php echo COL_DARK; ?>;
  text-shadow: rgba(150,150,150,0.75) 0px 2px 2px;
}

/* an optical nice link */
.linkNice {
  color: grey;
}

/* an optical nice link for navigation */
.linkNaviNice {
  color: silver;
  text-shadow: 2px 2px 10px <?php echo COL_BAIGE; ?>, 1px 1px #000;
}

.selectNice {
  color: grey;
  text-shadow: 1px 1px 5px #00bf0b, 1px 1px #000;
}

.textNice {
   color: grey;
   text-shadow: 1px 1px 5px #00bf0b, 1px 1px #000;
}


.btnBlue, .btnRed, .btnGreen, .btnGray {
  color: white;
}
.btnBlue {
  background-color: blue;
  background: radial-gradient(blue, aqua);
  background: -moz-radial-gradient(blue, aqua);
  background: -webkit-radial-gradient(blue, aqua);
}
.btnBlue:hover {
  background: radial-gradient(aqua, blue);
  background: -moz-radial-gradient(aqua, blue);
  background: -webkit-radial-gradient(aqua, blue);
}
.btnRed {
  background-color: red;
  background: radial-gradient(red, orange);
  background: -moz-radial-gradient(red, orange);
  background: -webkit-radial-gradient(red, orange);
}
.btnRed:hover {
  background: radial-gradient(orange, red);
  background: -moz-radial-gradient(orange, red);
  background: -webkit-radial-gradient(orange, red);
}
.btnGreen {
  background-color: green;
  background: radial-gradient(green, lime);
  background: -moz-radial-gradient(green, lime);
  background: -webkit-radial-gradient(green, lime);
}
.btnGreen:hover {
  background: radial-gradient(lime, green);
  background: -moz-radial-gradient(lime, green);
  background: -webkit-radial-gradient(lime, green);
}
.btnGray {
  box-shadow: 2px 2px 2px grey,
   2px 2px 2px grey,
   2px 2px 2px grey,
   2px 2px 2px grey;

}
.btnGray:hover {
  background: radial-gradient(black, <?php echo COL_DARK; ?>);
  background: -moz-radial-gradient(black, <?php echo COL_DARK; ?>);
  background: -webkit-radial-gradient(black, <?php echo COL_DARK; ?>);
}


.myMooOption {
  color: black;
  background-color: orange;
  background: linear-radial(center, orange, silver);
  background: -webkit-radial-gradient(center, orange, silver);
  background: -moz-radial-gradient(center, orange, silver);
}
.myMooOption:focus {
  color: white;
}
.selected {
  color: white;
  background-color: #00bf0b;
  background: linear-radial(center, #00bf0b, silver);
  background: -webkit-radial-gradient(center, #00bf0b, silver);
  background: -moz-radial-gradient(center, #00bf0b, silver);
}


.docuBoxInner {
  background-color: <?php echo COL_DARK; ?>;
}



</style>