<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_mediabase.php');
  require_once ('lib/utils.php');


  // -- in this context songs and player commands are the same --

  class class_playlist
  {

    // -- party report tmp store -> until pushed by cli_player.php --
    private $PR_Song = NULLSTR;
    private $PR_Interpret = NULLSTR;
    private $PR_Title = NULLSTR;
    private $PR_Playtime = NULLSTR;
    private $PR_WishedBy = NULLSTR;
    private $PR_Album = NULLSTR;

    // -- stores the last msg of any action to have more information than a simple true or false boolean --
    private $lastMsg = NULLSTR;
    public $wishedBy = NULLSTR;

    private $depth = NULLSTR;
    private $absSongLoc = NULLSTR;
    private $playlistFile = NULLSTR;
    private $allowMultibleEntries = NULLSTR;
    private $randomAfterLastSong = NULLSTR;
    private $maxEntries = NULLSTR;
    public $partyReport = NULLSTR;


    function __construct($aDepth = NULLSTR, $preventCreatingFiles = false)
    {
      global $cfg_absPathToSongs;
      global $cfg_playlistFile;
      global $cfg_allowMultiblePlaylistEntries;
      global $cfg_maxPlaylistEntries;
      global $cfg_partyReport;
      global $cfg_partyReportPath;
      global $cfg_partyMode;

      $this->depth = $aDepth;

      $this->absSongLoc = $cfg_absPathToSongs;
      $this->playlistFile = $aDepth . $cfg_playlistFile;
      $this->allowMultibleEntries = $cfg_allowMultiblePlaylistEntries;
      $this->randomAfterLastSong = $cfg_partyMode; //CFG_PARTY_MODE;
      $this->maxEntries = $cfg_maxPlaylistEntries;
      $this->partyReport = $cfg_partyReport;

      // -- in some special cases like starting from daemons, I don't want to create the required files automatically --
      if (!$preventCreatingFiles)
      {
        // -- if no playlist file exists, create a new one --
        if (!file_exists($this->playlistFile))
        {
          $this->_createNew($this->playlistFile);
        }
        if (!is_dir($aDepth.$cfg_partyReportPath))
        {
          mkdir($aDepth.$cfg_partyReportPath);
        }
      }

    }


    private function setLastMsg($lastMsg)
    {
      $this->lastMsg = $lastMsg;
    }

    public function getLastMsg()
    {
      return $this->lastMsg;
    }



    // -- ---------------- --
    // -- begin of helpers --
    // -- ---------------- --
    private function _createNew($aFile)
    {
      $newfile = fopen($aFile, 'w');
      if ($newfile === false)
      {
        echo eventLog('could not create new playlist file in: '.__FUNCTION__);
      }
      else
      {
        fwrite($newfile, '<?xml version="1.0" encoding="UTF-8"?>'.LFC.'<playlist>'.LFC.'</playlist>');
        fclose($newfile);
      }
      unset($newfile);
    }

    static private function _static_createNew($aFile)
    {
      $newfile = fopen($aFile, 'w');
      if ($newfile === false)
      {
        echo eventLog('could not create new playlist file in: '.__FUNCTION__);
      }
      else
      {
        fwrite($newfile, '<?xml version="1.0" encoding="UTF-8"?>'.LFC.'<playlist>'.LFC.'</playlist>');
        fclose($newfile);
      }
      unset($newfile);
    }



    /*
    // -- a very simple everlasting hitlist (current song is simply put to top) --
    private function _hitlistVar1($aSongFile)
    {
      global $cfg_partyReportPath;
      $hitListFile = $cfg_partyReportPath . 'hitlist.txt';
      if (!file_exists($hitListFile))
        file_put_contents($hitListFile, $aSongFile . "\n");
      $tmp = file_get_contents($hitListFile);
      $tmp = str_replace($aSongFile . "\n", '', $tmp);
      $tmp = $aSongFile . "\n" . $tmp;
      file_put_contents($hitListFile, $tmp);
    }
    */

    /**
     * everlasting hitlist, the current choosen song will be moved one step up
     * @param string $aSongFile
     */
    private function _hitlist($aSongFile)
    {
      global $cfg_hitlistLimit;
      global $cfg_partyReportPath;
      $hitListFile = $cfg_partyReportPath.'hitlist.txt';

      // -- if not existing, create one with first entry --
      if (!file_exists($hitListFile))
        file_put_contents($hitListFile, $aSongFile . LFC);
      else
      {
        // -- read file in array --
        $hitlist = file($hitListFile);

        $i = 0;
        // -- handle the special song --
        $found = false;
        foreach ($hitlist as $hl)
        {
          // -- if song is already in ... rise it up
          if (trim($aSongFile) == trim($hl))
          {
            $found = true;
            // -- only move to top if it is not already there --
            if ($i > 0)
            {
              $tmp = $hitlist[$i - 1];
              $hitlist[$i - 1] = $aSongFile . LFC;
              $hitlist[$i + 0] = $tmp;
            }
            break;
          }
          $i++;
        }
        // -- ... if not, attach it to the list --
        if (!$found)
        {
          // -- attach it to the bottom but cut the old --

          // -- cut hitlist to the limit --
          if (count ($hitlist) >  $cfg_hitlistLimit - 1)
            array_splice($hitlist, $cfg_hitlistLimit - 1);


          // -- -------------------- --
          // -- beg of three options --
          // -- -------------------- --

          // -- attach it to the bottom (tail) (replaced June 2022 with "middle") --
          //$hitlist[] = $aSongFile . "\n";

          // -- or to the top  --
          //array_unshift($hitlist, $aSongFile . "\n");

          // -- enter it in the middle (since June 2022) --
          $x = round($i / 2);
          array_splice($hitlist, $x, 0, $aSongFile . "\n");

          // -- -------------------- --
          // -- end of three options --
          // -- -------------------- --
        }

        // -- save the array to file --
        file_put_contents($hitListFile, $hitlist);
        unset($hitlist);
      }
    }

    /**
     * create a filename like playlist.2012-11-26.xml
     * @return string
     */
    private function _partyReport_getFileName()
    {
      global $cfg_partyReportPath;

      if (!is_dir($cfg_partyReportPath))
      {
        mkdir($cfg_partyReportPath);
      }
      $result = str_replace(DOT . cntGetFileExt($this->playlistFile), UNDSCR . date('Y-m-d') . '.xml', $this->playlistFile);
      return $cfg_partyReportPath . basename($result);
    }



    /**
     * prepare them only, push comes by the player, if it found that the song has been really played
     * @param string $aSong
     * @param string $aIN
     * @param string $aTI
     * @param string $aPT
     * @param string $aAL
     * @param string $aWB
     */
    function partyReport_addSong($aSong, $aIN, $aTI, $aPT, $aAL, $aWB)
    {
      $this->PR_Song = $aSong;
      $this->PR_Interpret = $aIN;
      $this->PR_Title = $aTI;
      $this->PR_Playtime = $aPT;
      $this->PR_WishedBy = $aWB;
      $this->PR_Album = $aAL;
    }


    /**
     * now push the song in real
     */
    function partyReport_pushSong()
    {
      // -- create a party report file if not exists already --
      $partyReportFile = $this->_partyReport_getFileName();
      if (!file_exists($partyReportFile))
      {
        $this->_createNew($partyReportFile);
      }

      // -- firstly add song at bottom ... --
      $xml = simplexml_load_file($partyReportFile);
      $item = $xml->addchild('item');
      $nextFreeIndex = (string)$this->_getNextFreeIndex($partyReportFile);
      $item->addAttribute(XML_INDEX, $nextFreeIndex);
      $item->addchild('a', htmlspecialchars($this->PR_Song));
      $item->addchild('i', htmlspecialchars($this->PR_Interpret));
      $item->addchild('t', htmlspecialchars($this->PR_Title));
      $item->addchild('p', $this->PR_Playtime);
      $item->addchild('w', htmlspecialchars($this->PR_WishedBy)); // -- wished by --
      $item->addchild('l', htmlspecialchars($this->PR_Album));
      $item->addchild('s', date('H:i:s')); // -- timestamp --

      // -- ... and then put it to top - without intermediate saving --

      // -- it's the first song --
      if ($nextFreeIndex == NULLINT)
      {
        // -- ... write to XML file now --
        $handle = fopen($partyReportFile, 'w');
        fwrite($handle, $xml->asXML());
        fclose($handle);
        unset($xml);
        // -- make party report file human readable --
        cntMakeHumanXML($partyReportFile); //$this->_humanPlaylist($partyReportFile);
      }

      // -- next song(s) add at top --
      if ($nextFreeIndex > NULLINT)
      {
        $dom = new DomDocument();
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false; // new
        $dom->loadXML($xml->asXML());
        $xpath = new DomXPath($dom);

        $parent = $xpath->query('/playlist');
        $queryOldTop = "//item[position() = 1]";
        $queryNewTop = "//item[@".XML_INDEX." = $nextFreeIndex]";
        $under = $xpath->query($queryNewTop);
        $above = $xpath->query($queryOldTop);
        $parent->item(0)->insertBefore($under->item(0), $above->item(0));
        $dom->save($partyReportFile);
      }

      // -- a simple everlasting hitlist --
      $this->_hitlist($this->PR_Song); // -- because the hitlist can be used for adding titles, the absolute path is needed -- //$this->_hitlist(basename($this->PR_Song));
    }


    /**
     * save the search results (in playlist format) to add it to playlist optionally
     * @param string $aFileRef
     * @param array $aResults
     * @param string $aWishedBy
     */
    public static function stat_lastResult_pushSongs($aFileRef, $aResults, $aWishedBy)
    {
      self::_static_createNew($aFileRef);
      $xml = simplexml_load_file($aFileRef);
      foreach ($aResults as $result)
      {
        $item = $xml->addchild('item');
        //$item->addAttribute(XML_INDEX, $this->_getNextFreeIndex($aFileRef));
        $item->addchild('a', htmlspecialchars($result['a']));
        $item->addchild('i', htmlspecialchars($result['i']));
        $item->addchild('t', htmlspecialchars($result['t']));
        $item->addchild('p', $result['p']);
        $item->addchild('w', htmlspecialchars($aWishedBy)); // -- wished by --
        $item->addchild('l', htmlspecialchars($result['l']));
        $item->addchild('s', date('H:i:s')); // -- timestamp --
        // -- ... write to XML file now --
        $handle = fopen($aFileRef, 'w');
        fwrite($handle, $xml->asXML());
        fclose($handle);
      }
    }


    // -- -------------- --
    // -- end of helpers --
    // -- -------------- --


      // -- for admin only --
    function clearPlaylist()
    {
      // -- Too simple! kills all, even the "on air" song --
      //       $this->_createNew($this->playlistFile);
      //       return LNG_SUC_KILLED;

      $result = LNG_ERR_UNKNOWN;
      $xml = simplexml_load_file($this->playlistFile);
      $count = count($xml->item);
      if ($xml)
      {
        $result = LNG_SUC;
        //$XMLstring = file_get_contents($this->playlistFile);
        //$doc = new SimpleXMLElement($XMLstring);
        for ($i = 0; $i < $count; $i++)
        {
          if ($i > 0) // -- on air --
          {
            if (isset($xml->item[$i]))
            {
              $dom = dom_import_simplexml($xml->item[$i]);
              $dom->parentNode->removeChild($dom);
              $result = LNG_SUC_KILLED;
              //$count--;
              $i--; // -- yeah, tricky --
            }
          }
        }
        // -- ... write to XML file now --
        $handle = fopen($this->playlistFile, 'wb');
        fwrite($handle, $xml->asXML());
        fclose($handle);
        unset($xml);
      }
      return $result;
    }


    /**
     * removes doubles from playlist
     */
    function removeDoubles()
    {
      $result = '<div style="font-weight: bold;">removing now double songs:</div><pre>';
      $xml = simplexml_load_file($this->playlistFile);
      if ($xml)
      {
        $killed = array();
        $attr = XML_INDEX;
        foreach ($xml->item as $item)
        {
          foreach ($xml->item as $item2)
          {
            $i1 = (string)$item->attributes()->$attr;
            $i2 = (string)$item2->attributes()->$attr;
            if ($i1 != $i2 && strcmp($item->a, $item2->a) === 0 && !in_array($i2, $killed))  // !array_key_exists($i2, $killed)
            {
              $this->removeSong($item->attributes()->$attr, true);
              if (!in_array($i1, $killed)) // if (!array_key_exists($i1, $killed))
                $result .= $item->a . ' i=' . $i1 . '<br>';
              $killed[] = $i1; // $killed[$i1] = $item->a;
            }
          }
        }
      }
      $result .= '</pre>';
      unset($killed);
      return $result;
    }

    /**
     * removes songs from playlist which are currently not available in the filesystem
     */
    function removeCurNotAvailInFilesys()
    {
      global $cfg_absPathToSongs;

      $result = '<div style="font-weight: bold;">removing now songs which are not available in filesystem:</div><pre>';
      $xml = simplexml_load_file($this->playlistFile);
      if ($xml)
      {
        $attr = XML_INDEX;
        foreach ($xml->item as $item)
        {
          if (!file_exists($cfg_absPathToSongs . $item->a))
          {
            $this->removeSong($item->attributes()->$attr, true);
            $result .= $item->a . LFH;
          }
        }
      }
      $result .= '</pre>';
      return $result;
    }


    // -- --------------------- --
    // -- begin of common stuff --
    // -- --------------------- --


    // -- check if song is already in --
    private function _allowSongPush($aSong)
    {
      if ($this->allowMultibleEntries)
        return true;
      else
      {
        $found = false;
         $xml = simplexml_load_file($this->playlistFile);
        if ($xml)
        {
          foreach ($xml->item as $item)
          {
            if ($item->a == (string)$aSong)
            {
              $found = true;
              break;
            }
          }
        }
        return !$found;
      }
    }


    /**
     * check for max entries. Returns TRUE if pushing a song is allowed
     * @return boolean
     */
    private function _checkForMaxEntries()
    {
      $result = false;

      $xml = simplexml_load_file($this->playlistFile);
      //nope, confused on an empty xml: if ($xml)
      {
        if (empty($xml->item)) // -- because of a new or empty file --
        {
          $result = true;
        }
        else
        {
          $countCurrentPlaylistEntries = count($xml->item);
          if ($countCurrentPlaylistEntries <= $this->maxEntries)
            $result = true;
        }
      }

      unset($xml);
      return $result;
    }

    /**
     * get next free index of a playlist-type file --
     * @param string $aPlaylistFile
     * @return number
     */
    private function _getNextFreeIndex($aPlaylistFile)
    {
      $nextIndex = 0;
      $xml = simplexml_load_file($aPlaylistFile);
      if ($xml)
      {
        $attr = XML_INDEX;
        foreach ($xml->item as $item)
        {
          $checkIndex = intval($item->attributes()->$attr);
          if ($checkIndex >= $nextIndex)
          {
            $nextIndex = $checkIndex + 1;
          }
        }
      }
      return $nextIndex;
    }


    function setWishedBy($aWishedBy = NULLSTR)
    {
      $this->wishedBy = $aWishedBy;
    }

    /**
     *  -- usually called by the frontend stuff --
     * @param string $aSong absolute song path
     * @param string $aIN interpret
     * @param string $aTI Title
     * @param string $aPT Playtime
     * @param string $aAL Album
     * @param boolean $aAdmin
     * @return string
     */
    function addSong($aSong, $aIN, $aTI, $aPT, $aAL, $aAdmin = false)
    {
      $result = false;

      if (!$aAdmin && !$this->_allowSongPush($aSong))
      {
        $this->lastMsg = '<span class="msgAddFail">'.LNG_ALREADY_IN.'</span>'; // -- return message that is aleady in --
      }
      else
      {
        if (!$aAdmin && !$this->_checkForMaxEntries())
        {
          $this->lastMsg = '<span class="msgAddFail">'.LNG_NO_MORE.'</span>'; // -- return message that no more songs are allowed --
        }
        else
        {
          $xml = simplexml_load_file($this->playlistFile);
          //if (!$xml)
            //$this->_createNew($this->playlistFile);
          //else
          {
            $item = $xml->addchild('item');
            $item->addAttribute(XML_INDEX, $this->_getNextFreeIndex($this->playlistFile));
            $item->addchild('a', htmlspecialchars($aSong));
            $item->addchild('i', htmlspecialchars($aIN));
            $item->addchild('t', htmlspecialchars($aTI));
            $item->addchild('p', $aPT);
            $item->addchild('w', htmlspecialchars($this->wishedBy));
            $item->addchild('l', htmlspecialchars($aAL));
            // -- ... write to XML file now --
            $handle = fopen($this->playlistFile, 'wb'); // wb
            if (fwrite($handle, $xml->asXML()))
              fclose($handle);
            else
              $this->lastMsg = cntErrMsg(LNG_ERR_WRITEFILE);
            unset($xml);
          }

          if (!cntMakeHumanXML($this->playlistFile))
          {
            $this->lastMsg = LNG_ERR_HUMANFORMAT;
          }
          else
          {
            $this->lastMsg = LNG_SUC_ADDED;
            $result = true;
          }
        }
      }

      return $result;
    }


    /**
     * remove song by its index (needed when multible entries are in)
     * @param string $aIndex
     * @param boolean $aAdmin
     * @return string
     */
    function removeSong($aIndex, $aAdmin)
    {
      $result = false;
      $this->lastMsg = LNG_ERR_UNKNOWN;

      if (!$aAdmin)
      {
        $this->lastMsg = cntErrMsg(LNG_ERR_ONLYADMIN);
      }
      else
      {
        $xml = simplexml_load_file($this->playlistFile);
        if ($xml)
        {
          //$XMLstring = file_get_contents($this->playlistFile);
          //$doc = new SimpleXMLElement($XMLstring);
          $attr = XML_INDEX;
          foreach ($xml->item as $item)
          {
            if ($item->attributes()->$attr == (string)$aIndex)
            {
              $dom = dom_import_simplexml($item);
              $dom->parentNode->removeChild($dom);
              $this->lastMsg = LNG_SUC_KILLED;
              $result = true;
              break;
            }
          }
          // -- ... write to XML file now --
          $handle = fopen($this->playlistFile, 'wb');
          fwrite($handle, $xml->asXML());
          fclose($handle);
          unset($xml);
        }
      }

      return $result;
    }


    /**
     * push a song from queue to stash
     * @param string $aIndex
     * @param string $aNickname
     * @param string $aA
     * @param string $aI
     * @param string $aT
     * @param string $aP
     * @param string $aL
     */
    function stashPushSong($aIndex, $aNickname, $aA, $aI, $aT, $aP, $aL)
    {
      global $cfg_tmpPathStatic;

      $result = false;

      $stashfile = $cfg_tmpPathStatic.DIR_STASH.$aNickname;
      if (!file_exists($stashfile))
      {
        if (!(is_dir($cfg_tmpPathStatic.DIR_STASH)))
          mkdir($cfg_tmpPathStatic.DIR_STASH);

        $this->_createNew($stashfile);
      }

      // -- add song to stash file --
      $xml = simplexml_load_file($stashfile);
      if ($xml)
      {
        $item = $xml->addchild('item');
        $item->addAttribute(XML_INDEX, $this->_getNextFreeIndex($stashfile));
        $item->addchild('a', htmlspecialchars($aA));
        $item->addchild('i', htmlspecialchars($aI));
        $item->addchild('t', htmlspecialchars($aT));
        $item->addchild('p', $aP);
        $item->addchild('w', htmlspecialchars($aNickname));
        $item->addchild('l', htmlspecialchars($aL));
        // -- ... write to XML file now --
        $handle = fopen($stashfile, 'wb'); // wb
        if (fwrite($handle, $xml->asXML()))
          fclose($handle);
        else
          $this->lastMsg = cntErrMsg(LNG_ERR_WRITEFILE);
        unset($xml);
      }

      if (!cntMakeHumanXML($stashfile))
      {
        $this->lastMsg = LNG_ERR_HUMANFORMAT;
      }
      else
      {
        $this->lastMsg = LNG_SUC_ADDED;
        $result = true;
      }

      return $result;
    }

    /**
     * pop a song from stash and add to queue
     * @param string $aNickname
     * @param integer|boolean $aIndex
     * @result array $result
     */
    function stashPopSong($aNickname, $aIndex = false)
    {
      global $cfg_tmpPathStatic;

      $result = array();
      $this->lastMsg = LNG_STASH_POP_FAILED;

      $stashfile = $cfg_tmpPathStatic.DIR_STASH.$aNickname;
      if (!file_exists($stashfile))
      {
        $this->lastMsg = LNG_STASH_NO_SONG;
      }
      else
      {
        $xml = simplexml_load_file($stashfile);
        if ($xml)
        {
          $count = count($xml); // --doesnt matter whether $xml->item or simply $xml --
          if (!($count > 0))
          {
            $this->lastMsg = LNG_STASH_NO_SONG;
          }
          else
          {
            // -- ------------------------------------------ --
            // -- read (last) song info of song to be popped --
            // -- this is an old case but it works and is still maintained --
            if ($aIndex === false)
            {
              $songID = $count-1; // -- the last song --
              $result['a'] = $xml->item[$songID]->a;
              $result['i'] = $xml->item[$songID]->i;
              $result['t'] = $xml->item[$songID]->t;
              $result['p'] = $xml->item[$songID]->p;
              $result['l'] = $xml->item[$songID]->l;
            }
            // -- --------------------------------------------- --
            // -- read (certain) song info of song to be popped --
            else
            {
              //$songID = intval($aIndex); // -- a certain song --
              $i = 0;
              $attr = XML_INDEX;
              foreach ($xml->item as $item)
              {
                if ($item->attributes()->$attr == (string)$aIndex) // -- index required as string --
                {
                  $result['a'] = $xml->item[$i]->a; // -- index required as integer --
                  $result['i'] = $xml->item[$i]->i;
                  $result['t'] = $xml->item[$i]->t;
                  $result['p'] = $xml->item[$i]->p;
                  $result['l'] = $xml->item[$i]->l;
                  break;
                }
                $i++;
              }
              $songID = $i;
            }

            // -- ----------------------------------------------------- --
            // -- now remove the last or certain song of the stash file --
            $dom = dom_import_simplexml($xml->item[$songID]);
            $dom->parentNode->removeChild($dom);
            $this->lastMsg = LNG_SUC_KILLED;
            // -- ... write to XML file now --
            $handle = fopen($stashfile, 'wb');
            fwrite($handle, $xml->asXML());
            fclose($handle);
            unset($xml);

            if (!cntMakeHumanXML($stashfile))
            {
              $this->lastMsg = LNG_ERR_HUMANFORMAT;
            }
            else
            {
              $this->lastMsg = LNG_SUC_ADDED;
            }
          }
        }
      }

      return $result;
    }

    /**
     * get a list with songs in stash
     * @param string $aNickname
     * @return NULL[][]
     */
    function stashGetContent($aNickname)
    {
      global $cfg_tmpPathStatic;

      $result = array();

      $stashfile = $cfg_tmpPathStatic.DIR_STASH.$aNickname;
      if (!file_exists($stashfile))
      {
        $this->lastMsg = LNG_STASH_NO_SONG;
      }
      else
      {
        $xml = simplexml_load_file($stashfile);
        if ($xml)
        {
          $count = count($xml); // --doesnt matter whether $xml->item or simply $xml --
          if (!($count > 0))
          {
            $this->lastMsg = LNG_STASH_NO_SONG;
          }
          else
          {
            $attr = XML_INDEX;
            foreach ($xml->item as $item)
            {
              $songIndex = intval($item->attributes()->$attr);
              $subArray = array();
              $subArray['a'] = $item->a; // -- absolute --
              $subArray['i'] = $item->i; // -- interpret --
              $subArray['t'] = $item->t; // -- title --
              $subArray['p'] = $item->p; // -- playtime --
              $subArray['l'] = $item->l; // -- album --
    //               $subArray['g'] = $item->g; // -- genre --
    //               $subArray['y'] = $item->y; // -- year --
    //               $subArray['d'] = $item->d; // -- file date --
              $result[$songIndex] = $subArray;
            }
          }
        }
      }

      return $result;
    }



    /**
     * -- usually called by cli_player.php --
     * -- gimme the next song and if not available take a random song if wished --
     * -- IF WHITELIST IS SET, IT IS TRIED (in getRandomSong) SEVERAL TIMES TO PICK THE RIGHT ONES, BUT IT CANNOT BE GUARANTEED! --
    */
    function getNextSong()
    {
      if (!file_exists($this->playlistFile))
        $this->_createNew($this->playlistFile);
      $xml = simplexml_load_file($this->playlistFile);
      //no, confused on an empty xml: if ($xml)
      {
        $result = $xml->item->a; // why does this work: $xml->item->a or $xml->item[0]->a, it should be $xml->item[1]->a;

        // -- if playlist empty and wished, take a random song ... --
        if (empty($result) && $this->randomAfterLastSong) //empty($result) && $this->randomAfterLastSong
        {
          $myMediaBase = new class_mediaBase();
          $rand = $myMediaBase->getRandomSong();
          unset($myMediaBase);
          $this->setWishedBy(NICK_AUTO);
          $this->addSong($rand['a'], $rand['i'], $rand['t'], $rand['p'], $rand['l']);
          $result = $rand['a'];

          $this->pushPlayerRemainTime(parseHumanMinSecStringToSeconds($rand['p']));
          // -- fill parallel the party report --
          if ($this->partyReport)
          {
            $this->partyReport_addSong($rand['a'], $rand['i'], $rand['t'], $rand['p'], $rand['l'], $this->wishedBy);
          }

          $this->setWishedBy(NULLSTR);
        }
        // -- ... if not, pick the next from playlist --
        else
        {
          $this->pushPlayerRemainTime(parseHumanMinSecStringToSeconds($xml->item->p));
          // -- fill parallel the party report --
          if ($this->partyReport)
          {
            $this->partyReport_addSong($xml->item->a, $xml->item->i, $xml->item->t, $xml->item->p, $xml->item->l, $xml->item->w);
          }
        }

        unset($xml);
        //return $this->absSongLoc . base64_decode( $result );
        return $this->absSongLoc . $result;
      }
    }

    /**
     * called by the cli_player
     */
    function removePlayedSong()
    {
      if (file_exists($this->playlistFile))
      {
        $xml = simplexml_load_file($this->playlistFile);
        if ($xml)
        {
          //$XMLstring = file_get_contents($this->playlistFile);
          if ($xml->item[0])
          {
            //$doc = new SimpleXMLElement($XMLstring);
            $dom = dom_import_simplexml($xml->item[0]);
            $dom->parentNode->removeChild($dom);
            // -- ... write to XML file now --
            $handle = fopen($this->playlistFile, 'wb');
            fwrite($handle, $xml->asXML());
            fclose($handle);
            unset($xml);
          }
        }
      }
    }




    /**
     * returns an array with all playlist song as single arrays with all information
     * this includes the currently playing "on air" song
     * @return multitype:multitype:NULL
     */
    function getWholePlaylist($onlyOnAir = false)
    {
      $result = array();

      try
      {
        // -- simplexml_load_file emits a warning when it fails to load a file, rather than throwing an exception. Warnings are not caught by try-catch blocks, which only handle exceptions. --
        $xml = @simplexml_load_file($this->playlistFile);
        if (!$xml)
        {
          throw new Exception("Failed to load XML file");
        }
        foreach ($xml->item as $item)
        {
          $subArray = array();
          $attr = XML_INDEX;
          $subArray[XML_INDEX] = $item->attributes()->$attr;
          //$subArray[] = base64_decode( $item->a );
          $subArray['a'] = $item->a; // -- absolut --
          $subArray['i'] = $item->i; // -- interpret --
          $subArray['t'] = $item->t; // -- title --  --> access like: $onAirSong[0]['t'] returns "Take me home" --
          $subArray['p'] = $item->p; // -- play time --> access like: $onAirSong[0]['p'] returns "1:23" --
          $subArray['w'] = $item->w; // -- wished by --
          $subArray['l'] = $item->l; // -- album --

          $result[] = $subArray;
          //unset($subArray);

          if ($onlyOnAir)
            break;
        }
        unset($xml);
      }
      catch (Exception $e)
      {
        eventLog('Failure in getWholePlaylist(): ' . $e->getMessage(), $this->depth);
      }


      return $result;
    }


    /**
     * return current playlist as json string with more info like player state
     *
     * @param class_player $player
     * @param boolean $asString
     * @return string
     */
    function getWholePlaylistAsJson(class_player $player, bool $asString = false): array|string
    {
      $json[JSON_RC] = JSON_RC_OK;

      $songs = $this->getWholePlaylist();
      $i = 0;
      foreach ($songs as $song)
      {
        // -- on air --
        if ($i == 0)
        {
          $onAirRealRemainTime = parseSecondsToHumanMinSecString($this->getPlayerRemainTime());
          if ($onAirRealRemainTime !== false)
          {
            $tmp = array('id' => intval($song[XML_INDEX]),
                          'a' => (string)$song['a'],
                          'i' => (string)$song['i'],
                          't' => (string)$song['t'],
                          'p' => (string)$onAirRealRemainTime,
                          'w' => (string)$song['w'],
                          'l' => (string)$song['l']
                        );
            $curPos = $onAirRealRemainTime;
          }
          else
          {
            $tmp = array('id' => intval($song[XML_INDEX]),
                          'a' => (string)$song['a'],
                          'i' => (string)$song['i'],
                          't' => (string)$song['t'],
                          'p' => (string)$song['p'],
                          'w' => (string)$song['w'],
                          'l' => (string)$song['l']
                        );
            $curPos = 0;
          }
          $json['onAir'] = $tmp;

          // -- player state --
          $json[JSON_RC_STATE] = $player->getPlayerState();

          // -- current position for visualizing --
          $json['onAirCurPos'] = getSecondsPerMinSec($curPos);
          $json['onAirLength'] = getSecondsPerMinSec($song['p']);
          $json['count'] = count($songs);
        }
        // -- playlist --
        else
        {
          $eta = $this->getEstimatedPlaytime($song[XML_INDEX]);
          $tmp = array('id' => intval($song[XML_INDEX]),
                      'a' => (string)$song['a'],
                      'i' => (string)$song['i'],
                      't' => (string)$song['t'],
                      'p' => (string)$eta,
                      'w' => (string)$song['w'],
                      'l' => (string)$song['l']
                      );
          $playlist[] = $tmp;
        }
        $i++;
      }
      if (!empty($playlist))
      {
        $json['playlist'] = $playlist;
      }

      if ($asString)
      {
        return json_encode($json);
      }
      else
      {
        return $json;
      }
    }



    /* returns simply the whole file count
    function getSongCount()
    {
      $result = '?';
      $xml = simplexml_load_file($this->playlistFile);
      if ($xml)
        $result = count($xml->item);
      unset($xml);
      return $result;
    }*/

    /**
     * handles the on air song
     */
    function getPlaylistCount()
    {
      $result = '0';
      $xml = simplexml_load_file($this->playlistFile);
      if ($xml)
      {
        $count = count($xml->item);
        $result = ($count > 0) ? $count - 1 : $count;
       }
      unset($xml);
      return $result;
    }




    /**
     * moves the song down in playlist
     * @param integer $aIndex
     * @param boolean $aAdmin
     * @return string
     */
    function moveDownSong($aIndex, $aAdmin)
    {
      $result = false; //LNG_ERR_UNKNOWN;
      if ($aAdmin)
      {
        $dom = new DomDocument();
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false; // new
        $dom->load($this->playlistFile);
        $xpath = new DomXPath($dom);
        $parent = $xpath->query('/playlist');
        $query1 = "//item[@".XML_INDEX." = $aIndex]";
        $under = $xpath->query($query1);
        try
        {
          //echo 'TRY1';
          @$parent->item(0)->insertBefore($under->item(0)->nextSibling, $under->item(0));
          //echo 'TRY2';
          if ($dom->save($this->playlistFile))
            $result = true; //LNG_SUC_MOVEDDOWN;
        }
        catch (Exception $e)
        {
          //echo 'CATCH' . $e->getMessage();
          //return $result;
        }
      }
      // else
      // {
      //   $result = cntErrMsg(LNG_ERR_ONLYADMIN);
      // }

      return $result;
    }


    /**
     * moves the song up in playlist
     * @param mixed $aIndex
     * @param boolean $aAdmin
     * @param string $aFile (optional)
     * @return string
     */
    function moveUpSong(mixed $aIndex, bool $aAdmin, string $aFile = NULLSTR): bool
    {
      $result = false;

      if ($aAdmin)
      {
        require_once $this->depth.'api_player.php';
        $playerState = class_player::static_getPlayerState();

        // -- allow even moving to on air --
        if ($playerState == PS_OFF)
        {
          $file = (empty($aFile)) ? $this->playlistFile : $aFile;
          $dom = new DomDocument();
          $dom->formatOutput = true;
          $dom->preserveWhiteSpace = false;
          $dom->load($file);
          $xpath = new DomXPath($dom);
          $parent = $xpath->query('/playlist');
          $query1 = "//item[@".XML_INDEX." = $aIndex]";
          $under = $xpath->query($query1);
          // -- Ensure the element is not already at the top --
          if ($under->length > 0 && $parent->item(0)->firstChild !== $under->item(0))
          {
            $parent->item(0)->insertBefore($under->item(0), $under->item(0)->previousSibling);
            if ($dom->save($file))
            {
              $result = true;
            }
          }
        }
        // -- allow maximum one below on air --
        else
        {
          $file = (empty($aFile)) ? $this->playlistFile : $aFile;
          $dom = new DomDocument();
          $dom->formatOutput = true;
          $dom->preserveWhiteSpace = false;
          $dom->load($file);
          $xpath = new DomXPath($dom);
          $parent = $xpath->query('/playlist');
          $query1 = "//item[@".XML_INDEX." = $aIndex]";
          $under = $xpath->query($query1);

          if ($under->length > 0)
          {
            $currentNode = $under->item(0);
            $previousSibling = $currentNode->previousSibling;
            // -- ensure the current node has a previous sibling (not already at the top) --
            if ($previousSibling !== null)
            {
              // -- check if the previous sibling is the first element --
              if ($parent->item(0)->firstChild !== $previousSibling)
              {
                // -- otherwise, move the current node one step up --
                $parent->item(0)->insertBefore($currentNode, $previousSibling);
              }
              if ($dom->save($file))
              {
                $result = true;
              }
            }
          }

        }
      }

      return $result;
    }


    /**
     * moves the song to the top of playlist
     * @param integer $aIndex
     * @param boolean $aAdmin
     * @return string
     */
    function moveTopSong($aIndex, $aAdmin)
    {
      $result = false; //LNG_ERR_UNKNOWN;
      if ($aAdmin) // $aAdmin
      {
        //if ($aIndex > 1) // 1 2 -- nonsense, this isn't the index beginning from top but the certain index anywhere in the list --
        {
          $dom = new DomDocument();
          $dom->formatOutput = true;
          $dom->preserveWhiteSpace = false; // new
          $dom->load($this->playlistFile);
          $xpath = new DomXPath($dom);
          $parent = $xpath->query('/playlist');
          $queryOldTop = "//item[position() = 2]"; // -- That does mean the second item aka the first in the playlist. The really first is the "on air" song --
          $queryNewTop = "//item[@".XML_INDEX." = $aIndex]";
          $under = $xpath->query($queryNewTop);
          $above = $xpath->query($queryOldTop);
          // -- TODO paddy Check whether it is NOT the on air song or the first in playlist. --
          if (true)
          {
            $parent->item(0)->insertBefore($under->item(0), $above->item(0));
            if ($dom->save($this->playlistFile))
            {
              $result = true; //LNG_SUC_MOVEDTOP;
            }
          }
        }
      }
      // else
      // {
      //   $result = cntErrMsg(LNG_ERR_ONLYADMIN);
      // }

      return $result;
    }


    /**
     * moves the bottom song to the top (directly after adding)
     * @param integer $aIndex
     * @param boolean $aAdmin
     * @return string
     */
    function moveBottomTopSong($aAdmin)
    {
      $result = false;//LNG_ERR_UNKNOWN;
      if ($aAdmin) // $aAdmin
      {
        $plc = $this->getPlaylistCount();
        // -- it has to be make sure, that a single song in the list could not be moved up: it will sent to nirvana :o) --
        if ($plc > 1)
        {
          $dom = new DomDocument();
          $dom->formatOutput = true;
          $dom->preserveWhiteSpace = false; // new
          $dom->load($this->playlistFile);
          $xpath = new DomXPath($dom);
          $parent = $xpath->query('/playlist');
          $queryOldTop = "//item[position() = 2]"; // -- That does mean the second item aka the first in the playlist. The first is the "on air" song --
          $lastSongPos = $plc + 1;
          $queryNewTop = "//item[position() = $lastSongPos]";
          $under = $xpath->query($queryNewTop);
          $above = $xpath->query($queryOldTop);
          // -- TODO paddy check whether it is NOT the on air song or the first in playlist --
          if (true)
          {
            $parent->item(0)->insertBefore($under->item(0), $above->item(0));
            if ($dom->save($this->playlistFile))
            {
              $result = true; //LNG_SUC_MOVEDTOP;
            }
          }
        }
      }
      // else
      // {
      //   $result = cntErrMsg(LNG_ERR_ONLYADMIN);
      // }

      return $result;
    }


    /**
     * duplicates to on air song in cue e.g. to perform a time skipping in currently played song
     * @return boolean
     */
    function duplicateOnAirInCue()
    {
      $result = false;

      // -- create document --
      $dom = new DomDocument();
      $dom->formatOutput = true;
      $dom->preserveWhiteSpace = false; // new
      $dom->load($this->playlistFile);
      $xpath = new DomXPath($dom);

      // -- build query --
      $parent = $xpath->query('/playlist');
      $queryOnAir = "//item[position() = 1]";
      $onAir = $xpath->query($queryOnAir);

      // -- clone and insert again --
      $clonenode = $onAir->item(0)->cloneNode(true);
      //$parent->item(0)->appendChild($clonenode);
      $parent->item(0)->insertBefore($clonenode, $onAir->item(0));

      // -- save --
      if ($dom->save($this->playlistFile))
        $result = true;

      return $result;
    }





    // -- ------------------- --
    // -- end of common stuff --
    // -- ------------------- --


    // -- Note: the following two functions are similary available in api_player.php! --

    /**
     * push remain time
     * @param integer $seconds
     * @return unknown
     */
    function pushPlayerRemainTime($seconds)
    {
      global $cfg_playerRemainTime_abs;
      return file_put_contents($this->depth.$cfg_playerRemainTime_abs, $seconds);
    }
    /**
     * get current remain time
     * @return integer
     */
    function getPlayerRemainTime()
    {
      global $cfg_playerRemainTime;
      if (!file_exists($this->depth.$cfg_playerRemainTime))
      {
        eventLog('file does not exist in getPlayerRemainTime(): '.$this->depth.$cfg_playerRemainTime, $this->depth);
        return 0; //false;
      }
      else
      {
        return trim(file_get_contents($this->depth.$cfg_playerRemainTime));
      }
    }



    private function _addPLTime($aTime1, $aTime2)
    {
      $h =  strtotime($aTime1);
      $h2 = strtotime($aTime2);
      $minute = date('i', $h2);
      $second = date('s', $h2);
      $hour = date('H', $h2);
      $convert = strtotime("+$minute minutes", $h);
      $convert = strtotime("+$second seconds", $convert);
      $convert = strtotime("+$hour hours", $convert);
      return date('H:i:s', $convert);
    }


    /**
     * returns the estimated remaintime until the song with given index is played
     * the summary of all titles to the index including the onAir song
     * @param integer $aIndex
     * @return string
     */
    function getEstimatedPlaytime($aIndex)
    {
      $result = TILDE;

      $count = '0:0:0';
      try
      {
        // -- simplexml_load_file emits a warning when it fails to load a file, rather than throwing an exception. Warnings are not caught by try-catch blocks, which only handle exceptions. --
        $xml = @simplexml_load_file($this->playlistFile);
        if (!$xml)
        {
          throw new Exception("Failed to load XML file");
        }
        $onAir = true;
        $attr = XML_INDEX;
        foreach ($xml->item as $item)
        {
          if ($item->attributes()->$attr == (string)$aIndex)
            break;

          if ($onAir)
          {
            $onAir = false;
            $count = $this->_addPLTime($count, '0:' . parseSecondsToHumanMinSecString($this->getPlayerRemainTime()));
          }
          else
          {
            // -- this would fuck up, if a song is longer than 59:99 ->    '0:' . $item->p[0] --
            $count = $this->_addPLTime($count, '0:' . $item->p[0]);
          }
        }
        $result = $count;
        unset($xml);
      }
      catch (Exception $e)
      {
        eventLog('Failure in getEstimatedPlaytime(): ' . $e->getMessage(), $this->depth);
      }

      return $result;
    }



  }

