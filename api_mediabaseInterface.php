<?php

  interface interface_mediabase
  {

    /**
     * helper
     */
    public function getLastMsg();

    /**
     * generates an array relating to the given search pattern (search)
     * $aFull for full informations
     * $aOffset for beginning result at
     * $aLimit limits the results
     * $aCaseSensitive boolean
     * @param a char as string
     * @return array
     */
    public function getSearchResult($aSeachStr, $aOffset = 0, $aLimit = 0, $aDL_level = false, $aCaseSensitive = false); // $aSeachStr, $aOffset = 0, $aLimit = 0, $aFeedbkSrch, $aDL_level = false

    /**
     * generates an array relating to the given search pattern (search)
     * it is searched for a user only. Not in the whole search string (Album, Title etc...)
     * $aFull for full informations
     * $aOffset for beginning result at
     * $aLimit limits the results
     * $aCaseSensitive boolean
     * @param a char as string
     * @return array
     */
    public function getSearchResultByUser($aUser, $aOffset = 0, $aLimit = 0, $aCaseSensitive = false);

    /**
     * generates an array relating to the given random count (e.g.10)
     * usually used by the gui
     * @param a char as string
     * @return array
     */
    public function getRandomSongs($aVal);

    /**
     * returns a random song
     * usually used only by api_playlist for cli_player.php
     * @return multitype:NULL
     */
    public function getRandomSong();

    /**
     * generates an array with songs in between two dates
     * step width: days
     * $aDate1 = past
     * $aDate2 = today
     * @params $aDate1, $aDate2 -> date('Ymd') -> e.g. 20060906
     * @return array
     */
    public function getSongsByDate($aDate1, $aDate2, $aOffset = 0, $aLimit = 0);

    /**
     *
     */
    public function getTotalSongCount();

    /**
     * Song information from mediabase.xml with comment and rating from feedback.xml (and if not found, older comment and rating from mediabase.xml, too)
     * @param string $aSong -> pure song filename WITHOUT path (even without the intern mediabase path)
     * @param boolean $aFirstHit -> if true, the first hit is taken, no matter in which sub-path it is found (useful in some cases e.g. valuedSong.php were a song must be found without intern mediabase path)
     * @return array with song info content and if cannot find the song false
     */
    public function getSongInfo($aSong, $aFirstHit = false);


    // -- FEEDBACK CLONE ------------------------------------- --
    // -- FEEDBACK CLONE - But the master is the feedback.xml! --
    // -- FEEDBACK CLONE ------------------------------------- --


    /**
     *
     * @param number $aOffset
     * @param number $aLimit
     */
    public function getVotedSongs($aOffset = 0, $aLimit = 0);

    // -- --------------------------------- --
    // -- begin of save a comment or rating --

    /**
     * THIS IS ALWAYS A CLONE OF THE FEEDBACK STORED VALUE
     * set the song which should be commented or rated
     * if it could not be found a new enty will be made
     * store $i in $this->curSongFound if it has been found, false if it has been created
     */
    public function setSong($aSong);

    /**
     *
     * @param string $aComment
     * @param string $aBy
     */
    public function setComment($aComment, $aBy = NULLSTR);

    /**
     *
     * @param string $aRating
     * @param string $aBy
     */
    public function setRating($aRating, $aBy = NULLSTR);

    /**
     * saves the mediabase file, after comment or rating has been set
     */
    public function saveFeedback();

    // -- end of save a comment or rating --
    // -- ------------------------------- --



    // -- --------------------------------- --
    // -- begin of find a comment or rating --

    /**
     * finds a wished song and returns rating and value in the following public functions getComment... and so on
     * if it could not be found the following code by the caller has to abort the rest by negative result here
     * @param string $aSong
     * @return boolean
     */
    public function findSong($aSong);

    /**
     *
     */
    public function getComment();

    /**
     *
     */
    public function getCommentBy();

    /**
     *
     */
    public function getRating();

    /**
     *
     */
    public function getRatingBy();

    /**
     *
     */
    public function findSongReset();


    // -- end of find a comment or rating --
    // -- ------------------------------- --
  }