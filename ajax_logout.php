<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  $myNickname = new class_nickname();
  if ($myNickname->drop($_SESSION['str_nickname']))
    echo LNG_GOODBYE;
  else
    echo LNG_ERR_UNKNOWN_FUNNY;
  unset($myNickname);

  /*  not needed, that comes by session. In difference to ajax_dropNickname, where I want to kick out another
  if (isset($_GET['nickname']))
  {
    $myNickname = new class_nickname();
    if ($myNickname->drop($_GET['nickname']))
      echo LNG_GOODBYE;
    else
      echo LNG_ERR_UNKNOWN_FUNNY;
    unset($myNickname);
  }
  else
  {
  //$_SESSION['str_nickname'] = $_SERVER['REMOTE_ADDR'];
    echo cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  */