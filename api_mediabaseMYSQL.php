<?php

  /**
   * class_mediaBase mySQL based
   * this mediaBase type should be the best in performance and data issues - but that's still open
   * @author paddy
   *
   * Preparations for using the database strategy
   * - Setup mySQL or MariaDB (if not already done)
   * - Create database named "piMoo"
   * - Import template "keep/mediaBase.sql"
   * - Create user "piMoo" with user "piMoo" or do something similar and make the relating changes in "keep/config.php",
   *   with this minimum permissions: SELECT,INSERT,UPDATE,DELETE
   * - have fun!
   *
   *
   */
  class class_mediaBase implements interface_mediabase
  {
    private $depth = NULLSTR;
    private $dbconnected = false;

    private $mySQL;

    private $curSongRating = NULLSTR;
    private $curSongRatingBy = NULLSTR;
    private $curSongComment = NULLSTR;
    private $curSongCommentBy = NULLSTR;

    private $genreWhitelist;
    private $genreWhitelistActive;
    private $genreBlacklist;

    private $rateWhitelist;
    private $rateWhitelistActive;
    private $rateBlacklist;


    function __construct($aDepth = NULLSTR)
    {
      global $cfg_mySQL_host;
      global $cfg_mySQL_port;
      global $cfg_mySQL_user;
      global $cfg_mySQL_passwd;
      global $cfg_mySQL_dataBase;

      global $cfg_genreWhitelist, $cfg_genreBlacklist;
      global $cfg_rateWhitelist, $cfg_rateBlacklist;

      $this->depth = $aDepth;

      // -- connect to database --
      try
      {
        $this->mySQL = @new mysqli($cfg_mySQL_host, $cfg_mySQL_user, $cfg_mySQL_passwd, $cfg_mySQL_dataBase, $cfg_mySQL_port);
        $this->dbconnected = true;
        // -- on some environments this is required - nobody knows why --
        mysqli_set_charset($this->mySQL, 'utf8');
        // -- in no case this (tested): --
        //mysqli_set_charset($this->mySQL, 'unicode');
      }
      catch (Exception $e)
      {
        // printf('<span style="color: red;">Connect failed: ' . "%s\n", mysqli_connect_error() . '</span>');
        echo eventLog('mysql connection failed: '.mysqli_connect_error(), $this->depth);
      }


      // -- FEEDBACK CLONE --
      // -- for writing --
      $this->curSong = NULLSTR;
      $this->curSongFound = false;
      // -- for reading --
      $this->findSongReset();

      $this->genreWhitelist = $cfg_genreWhitelist;
      $this->genreWhitelistActive = (count($this->genreWhitelist) > 0 && trim($this->genreWhitelist[0]) != NULLSTR);
      $this->genreBlacklist = $cfg_genreBlacklist;

      // -- recommendaction by nane --
      $this->rateWhitelist = $cfg_rateWhitelist;
      $this->rateWhitelistActive = (count($this->rateWhitelist) > 0 && trim($this->rateWhitelist[0]) != NULLSTR);
      $this->rateBlacklist = $cfg_rateBlacklist;
    }

    /**
     * get last message
     * @see interface_mediabase::getLastMsg()
     */
    function getLastMsg()
    {
      return $this->lastMsg;
    }


    // -- -------------- --
    // -- beg of helpers --


    /**
     * returns the interpret as string by given id
     * @param integer $aId
     * @return string
     */
    function query_getInterpretById($aId)
    {
      $queryInt = "SELECT `interpret` FROM `table_interpret` WHERE `id` = $aId";
      $queryResult = $this->mySQL->query($queryInt);
      $obj = $queryResult->fetch_object();
      if (isset($obj->interpret))
        return $obj->interpret;
      else
        return QUESTMARK;
    }

    /**
     * returns the album as string by given id
     * @param integer $aId
     * @return string
     */
    function query_getAlbumById($aId)
    {
      $queryInt = "SELECT `album` FROM `table_album` WHERE `id` = $aId";
      $queryResult = $this->mySQL->query($queryInt);
      $obj = $queryResult->fetch_object();
      if (isset($obj->album))
        return $obj->album;
      else
        return NULLSTR;
    }

    /**
     * returns the genre as string by given id
     * @param integer $aId
     * @return string
     */
    function query_getGenreById($aId)
    {
      $queryInt = "SELECT `genre` FROM `table_genre` WHERE `id` = $aId";
      $queryResult = $this->mySQL->query($queryInt);
      $obj = $queryResult->fetch_object();
      if (isset($obj->genre))
        return $obj->genre;
      else
        return NULLSTR;
    }


    // -- end of helpers --
    // -- -------------- --






    /**
     * generates an array relating to the given search pattern (search)
     * $aFull for full informations
     * $aOffset for beginning result at
     * $aLimit limits the results
     * $aCaseSensitive boolean
     * @param string $aSeachStr
     * @return array
     */
    function getSearchResult($aSeachStr, $aOffset = 0, $aLimit = 0, $aDL_level = false, $aCaseSensitive = false) // $aSeachStr, $aOffset = 0, $aLimit = 0, $aFeedbkSrch, $aDL_level = false
    {
      $result = array();

      if ($this->dbconnected)
      {
        $soPatLen = strlen(SO_INTERPRET);
        $checkSearchParam = substr($aSeachStr, 0, $soPatLen);
        switch ($checkSearchParam)
        {
          case SO_INTERPRET:
          {
            $seachStr = substr($aSeachStr, $soPatLen);
            $whereClause = "WHERE `interpret` LIKE '%$seachStr%'";
            break;
          }
          case SO_TITLE:
          {
            $seachStr = substr($aSeachStr, $soPatLen);
            $whereClause = "WHERE `title` LIKE '%$seachStr%'";
            break;
          }
          case SO_ALBUM:
          {
            $seachStr = substr($aSeachStr, $soPatLen);
            $whereClause = "WHERE `album` LIKE '%$seachStr%'";
            break;
          }
          case SO_GENRE:
          {
            $seachStr = substr($aSeachStr, $soPatLen);
            $whereClause = "WHERE `genre` LIKE '%$seachStr%'";
            break;
          }
          case SO_GENRE:
            {
              $seachStr = substr($aSeachStr, $soPatLen);
              $whereClause = "WHERE `comment` LIKE '%$seachStr%'";
              break;
            }
          default:
            $seachStr = $aSeachStr;
            $whereClause = "WHERE `interpret` LIKE '%$seachStr%'
                              OR `title` LIKE '%$seachStr%'
                              OR `album` LIKE '%$seachStr%'
                              OR `rating` LIKE '%$seachStr%'
                              OR `ratingBy` LIKE '%$seachStr%'
                              OR `comment` LIKE '%$seachStr%'
                              OR `commentBy` LIKE '%$seachStr%'
                              OR `year` LIKE '%$seachStr%'
                              OR `genre` LIKE '%$seachStr%'";
        }


        // -- in mySQL the wildcard is % --
        $seachStr = str_replace('*', '%', $seachStr);
        $limit = ($aLimit == 0) ? NULLSTR : ",$aLimit";

        $query = "SELECT
                    table_base.absolute AS absolute,
                    table_interpret.interpret AS interpret,
                    table_base.title AS title,
                    table_base.playtime AS playtime,
                    table_album.album AS album,
                    table_base.rating AS rating,
                    table_base.comment AS comment,
                    table_genre.genre AS genre
                  FROM
                    `table_base`
                    LEFT JOIN table_interpret ON (table_base.interpretID = table_interpret.id)
                    LEFT JOIN table_album ON (table_base.albumID = table_album.id)
                    LEFT JOIN table_genre ON (table_base.genreID = table_genre.id)

                  $whereClause

                  LIMIT $aOffset $limit";

        $queryResult = $this->mySQL->query($query);
        while ($row = $queryResult->fetch_object())
        {
          // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
          $genre = (isset($row->genre)) ? $row->genre : NULLSTR;
          if (   !in_array($genre, $this->genreBlacklist)
              && (!$this->genreWhitelistActive || in_array($genre, $this->genreWhitelist))
            )
          {
            $subArray = array();

            $subArray['a'] = (isset($row->absolute)) ? $row->absolute : NULLSTR;
            $subArray['i'] = (isset($row->interpret)) ? $row->interpret : QUESTMARK;
            $subArray['t'] = (isset($row->title)) ? $row->title : QUESTMARK;
            $subArray['p'] = (isset($row->playtime)) ? $row->playtime : NULLSTR;
            $subArray['l'] = (isset($row->album)) ? $row->album : NULLSTR;
            $subArray['r'] = (isset($row->rating)) ? $row->rating : NULLSTR;
            $subArray['c'] = (isset($row->comment)) ? $row->comment : NULLSTR;

            $result[] = $subArray;
          }
        }
        $queryResult->close(); // -- free result set --
      }

      return $result;
    }


    /**
     * generates an array relating to the given search pattern (search)
     * it is searched for a user only. Not in the whole search string (Album, Title etc...)
     * $aFull for full informations
     * $aOffset for beginning result at
     * $aLimit limits the results
     * $aCaseSensitive boolean
     * @param a char as string
     * @return array
     */
    function getSearchResultByUser($aUser, $aOffset = 0, $aLimit = 0, $aCaseSensitive = false)
    {
      $result = array();

      if ($this->dbconnected)
      {
        // -- in mySQL the wildcard is % --
        $aUser = str_replace('*', '%', $aUser);

        $limit = ($aLimit == 0) ? NULLSTR : ",$aLimit";

        $query = "SELECT
                    table_base.absolute AS absolute,
                    table_interpret.interpret AS interpret,
                    table_base.title AS title,
                    table_base.playtime AS playtime,
                    table_album.album AS album,
                    table_base.rating AS rating,
                    table_base.comment AS comment,
                    table_genre.genre AS genre
                  FROM `table_base`
                    LEFT JOIN table_interpret ON(table_base.interpretID = table_interpret.id)
                    LEFT JOIN table_album ON(table_base.albumID = table_album.id)
                    LEFT JOIN table_genre ON(table_base.genreID = table_genre.id)
                  WHERE `ratingBy` LIKE '%$aUser%'
                    OR `commentBy` LIKE '%$aUser%'

                  LIMIT $aOffset $limit";


        $queryResult = $this->mySQL->query($query);
        while ($row = $queryResult->fetch_object())
        {
          // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
          $genre = (isset($row->genre)) ? $row->genre : NULLSTR;
          if (   !in_array($genre, $this->genreBlacklist)
              && (!$this->genreWhitelistActive || in_array($genre, $this->genreWhitelist))
            )
          {
            $subArray = array();
            $subArray['a'] = (isset($row->absolute)) ? $row->absolute : NULLSTR;
            $subArray['i'] = (isset($row->interpret)) ? $row->interpret : QUESTMARK;
            $subArray['t'] = (isset($row->title)) ? $row->title : NULLSTR;
            $subArray['p'] = (isset($row->playtime)) ? $row->playtime : NULLSTR;
            $subArray['l'] = (isset($row->album)) ? $row->album : NULLSTR;
            $subArray['r'] = (isset($row->rating)) ? $row->rating : NULLSTR;
            $subArray['c'] = (isset($row->comment)) ? $row->comment : NULLSTR;
            $result[] = $subArray;
          }
        }
        $queryResult->close(); // -- free result set --
      }

      return $result;
    }



    /**
     * generates an array with songs in between two dates
     * step width: days
     * $aDate1 = past
     * $aDate2 = today
     * @params $aDate1, $aDate2 -> date('Ymd') -> e.g. 20060906
     * @return array
     */
    function getSongsByDate($aDate1, $aDate2, $aOffset = 0, $aLimit = 0)
    {
      $result = array();

      if ($this->dbconnected)
      {
        $limit = ($aLimit == 0) ? NULLSTR : ",$aLimit";

        $past = intval($aDate1);
        $today = intval($aDate2);

        $query = "SELECT
                    table_base.absolute AS absolute,
                    table_base.filedate AS filedate,
                    table_interpret.interpret AS interpret,
                    table_base.title AS title,
                    table_base.playtime AS playtime,
                    table_album.album AS album,
                    table_base.rating AS rating,
                    table_base.comment AS comment,
                    table_genre.genre AS genre
                  FROM `table_base`
                    LEFT JOIN table_interpret ON(table_base.interpretID = table_interpret.id)
                    LEFT JOIN table_album ON(table_base.albumID = table_album.id)
                    LEFT JOIN table_genre ON(table_base.genreID = table_genre.id)

                  ORDER BY `filedate` DESC

                  LIMIT $aOffset $limit";

        $queryResult = $this->mySQL->query($query);
        while ($row = $queryResult->fetch_object())
        {
          // $fileDate: <fd>2007-07-21 10:49:14</fd> -> 20070721
          $year  = substr($row->filedate, 0, 4);
          $month = substr($row->filedate, 5, 2);
          $day   = substr($row->filedate, 8, 2);
          $fileDate = intval($year.$month.$day);

          // -- file date as seconds now --
          if ($fileDate >= $past && $fileDate <= $today) // -- this works also for something like: get 90's --
          {
            // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
            $genre = (isset($row->genre)) ? $row->genre : NULLSTR;
            if (   !in_array($genre, $this->genreBlacklist)
                && (!$this->genreWhitelistActive || in_array($genre, $this->genreWhitelist))
              )
            {
              $subArray = array();

              $subArray['a'] = (isset($row->absolute)) ? $row->absolute : NULLSTR;
              $subArray['i'] = (isset($row->interpret)) ? $row->interpret : QUESTMARK;
              $subArray['t'] = (isset($row->title)) ? $row->title : NULLSTR;
              $subArray['p'] = (isset($row->playtime)) ? $row->playtime : NULLSTR;
              $subArray['l'] = (isset($row->album)) ? $row->album : NULLSTR;
              $subArray['r'] = (isset($row->rating)) ? $row->rating : NULLSTR;
              $subArray['c'] = (isset($row->comment)) ? $row->comment : NULLSTR;

              $result[] = $subArray;
            }
          }
        }
        // -- sort by newest --
        // -- that was causer of all problems with files by date - the filedate as index: --
        // -- that can be double of course!!! when songs have the same date!!! --
        //krsort($result); // -- sort by key reverse --
      }

      return $result;
    }


    /**
     * generates an array relating to the given random count (e.g.10)
     * usually used by the gui
     * @param integer
     * @return array
     */
    function getRandomSongs($value)
    {
      $result = array();

      if ($this->dbconnected)
      {
        $count = $this->getTotalSongCount();
        $max = ($count > $value) ? $value : $count;
        // -- avoid double delivery is done by mySQL itself--
        $query = "SELECT
                    table_base.absolute AS absolute,
                    table_interpret.interpret AS interpret,
                    table_base.title AS title,
                    table_base.playtime AS playtime,
                    table_album.album AS album,
                    table_base.rating AS rating,
                    table_base.comment AS comment,
                    table_genre.genre AS genre
                  FROM `table_base`
                    LEFT JOIN table_interpret ON(table_base.interpretID = table_interpret.id)
                    LEFT JOIN table_album ON(table_base.albumID = table_album.id)
                    LEFT JOIN table_genre ON(table_base.genreID = table_genre.id)

                  ORDER BY RAND()

                  LIMIT $max";

        $queryResult = $this->mySQL->query($query);
        while ($row = $queryResult->fetch_object())
        {
          // TODO paddy make white- and blacklist working
          /*$genre = (isset($row->genre)) ? $row->genre : NULLSTR;
          // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
          if (   !in_array($genre, $this->genreBlacklist)
              && (!$this->genreWhitelistActive || in_array($genre, $this->genreWhitelist))
            )*/
          {
            $subArray = array();

            $subArray['a'] = (isset($row->absolute)) ? $row->absolute : NULLSTR;
            $subArray['i'] = (isset($row->interpret)) ? $row->interpret : QUESTMARK;
            $subArray['t'] = (isset($row->title)) ? $row->title : NULLSTR;
            $subArray['p'] = (isset($row->playtime)) ? $row->playtime : NULLSTR;
            $subArray['l'] = (isset($row->album)) ? $row->album : NULLSTR;
            $subArray['r'] = (isset($row->rating)) ? $row->rating : NULLSTR;
            $subArray['c'] = (isset($row->comment)) ? $row->comment : NULLSTR;

            $result[] = $subArray;
          }
        }
        $queryResult->close(); // -- free result set --
      }

      return $result;
    }



    /**
     * returns a random song
     * usually used only by api_playlist for cli_player.php
     * @return multitype:NULL
     */
    function getRandomSong()
    {
      $result = array();

      // -- TODO paddy TRICKY make this working with preset values
      $simpleWhite = (true) ? NULLSTR : "WHERE rating == '~3~' OR rating == '~4~' OR rating == '~5~'";
      $simpleBlack = (true) ? NULLSTR : "WHERE rating != '~1~' AND rating != '~2~'";

      $query = "SELECT
                  table_base.absolute AS absolute,
                  table_interpret.interpret AS interpret,
                  table_base.title AS title,
                  table_base.playtime AS playtime,
                  table_album.album AS album,
                  table_base.rating AS rating,
                  table_genre.genre AS genre
                FROM `table_base`
                  LEFT JOIN table_interpret ON(table_base.interpretID = table_interpret.id)
                  LEFT JOIN table_album ON(table_base.albumID = table_album.id)
                  LEFT JOIN table_genre ON(table_base.genreID = table_genre.id)

                $simpleWhite
                $simpleBlack

                ORDER BY RAND()

                LIMIT 1";

      $queryResult = $this->mySQL->query($query);
      $obj = $queryResult->fetch_object();

      // -- allow only genres in whitelist and filter out genres of blacklist --
      // TODO paddy make white- and blacklist working
      /*$genre = (isset($obj->genre)) ? $obj->genre : NULLSTR;
      if (!in_array($genre, $this->genreBlacklist) && in_array($genre, $this->genreWhitelist))*/
      {
        $result['a'] = (isset($obj->absolute)) ? $obj->absolute : NULLSTR;
        $result['i'] = (isset($obj->interpret)) ? $obj->interpret : QUESTMARK;
        $result['t'] = (isset($obj->title)) ? $obj->title : NULLSTR;
        $result['p'] = (isset($obj->playtime)) ? $obj->playtime : NULLSTR;
        $result['l'] = (isset($obj->album)) ? $obj->album : NULLSTR;
      }
      $queryResult->close(); // -- free result set --

      return $result;
    }








    function getTotalSongCount()
    {
      if (!$this->dbconnected)
      {
        return NULLINT;
      }
      else
      {
        $query = "SELECT MAX(`id`) AS `id` FROM `table_base`";
        $queryResult = $this->mySQL->query($query);
        $obj = $queryResult->fetch_object();
        return $obj->id;
      }
    }


    /**
     * Song information from mediabase.xml with comment and rating from feedback.xml (and if not found, older comment and rating from mediabase.xml, too)
     * @param string $aSong -> pure song filename WITHOUT path (even without the intern mediabase path)
     * @param boolean $aFirstHit -> if true, the first hit is taken, no matter in which sub-path it is found (useful in some cases e.g. valuedSong.php were a song must be found without intern mediabase path)
     * @return array with song info content and if cannot find the song false
     */
    function getSongInfo($aSong, $aFirstHit = false)
    {
      $result = array();

      if ($this->dbconnected)
      {
        // -- in mySQL the wildcard is % --
        //$aSong = str_replace('*', '%', $aSong);

        // -- ??? $aFirstHit is not of interest in the context of a mySQl database ??? --
        $checkRef = ($aFirstHit) ? basename($aSong) : $aSong;

        $query = "SELECT *
                  FROM `table_base`
                    LEFT JOIN table_interpret ON(table_base.interpretID = table_interpret.id)
                    LEFT JOIN table_album ON(table_base.albumID = table_album.id)
                    LEFT JOIN table_genre ON(table_base.genreID = table_genre.id)
                  WHERE `absolute` = '$checkRef'";

        $queryResult = $this->mySQL->query($query);
        $obj = $queryResult->fetch_object();

        $result['a'] = (isset($obj->absolute)) ? $obj->absolute : NULLSTR;
        $result['i'] = (isset($obj->interpret)) ? $obj->interpret : QUESTMARK;
        $result['t'] = (isset($obj->title)) ? $obj->title : NULLSTR;
        $result['p'] = (isset($obj->playtime)) ? $obj->playtime : NULLSTR;
        $result['l'] = (isset($obj->album)) ? $obj->album : NULLSTR;
        $result['g'] = (isset($obj->genre)) ? $obj->genre : NULLSTR;
        $result['y'] = (isset($obj->year)) ? $obj->year : NULLSTR;
        $result['d'] = (isset($obj->filedate)) ? $obj->filedate : NULLSTR;
        $result['r'] = (isset($obj->rating)) ? $obj->rating : NULLSTR;
        $result['rby'] = (isset($obj->ratingBy)) ? $obj->ratingBy : NULLSTR;
        $result['c'] = (isset($obj->comment)) ? $obj->comment : NULLSTR;
        $result['cby'] = (isset($obj->commentBy)) ? $obj->commentBy : NULLSTR;

        $queryResult->close(); // -- free result set --
      }

      return $result;
    }








    // -- FEEDBACK CLONE ------------------------------------- --
    // -- FEEDBACK CLONE - But the master is the feedback.xml! --
    // -- FEEDBACK CLONE ------------------------------------- --


    /**
     *
     * @return multitype:NULL
     */
    function getVotedSongs($aOffset = 0, $aLimit = 0)
    {
      $result = array();

      $limit = ($aLimit == 0) ? NULLSTR : ",$aLimit";

      $query = "SELECT *
                FROM `table_base`
                  LEFT JOIN table_interpret ON(table_base.interpretID = table_interpret.id)
                  LEFT JOIN table_album ON(table_base.albumID = table_album.id)
                  LEFT JOIN table_genre ON(table_base.genreID = table_genre.id)
                WHERE `rating` != '' OR `comment` != ''

                LIMIT $aOffset $limit";

      $queryResult = $this->mySQL->query($query);
      while ($row = $queryResult->fetch_object())
      {
        $subArray = array();

        $subArray['a'] = (isset($row->absolute)) ? $row->absolute : NULLSTR;
        $subArray['i'] = (isset($row->interpret)) ? $row->interpret : QUESTMARK;
        $subArray['t'] = (isset($row->title)) ? $row->title : NULLSTR;
        $subArray['p'] = (isset($row->playtime)) ? $row->playtime : NULLSTR;
        $subArray['l'] = (isset($row->album)) ? $row->album : NULLSTR;
        $subArray['g'] = (isset($row->genre)) ? $row->genre : NULLSTR;
        $subArray['y'] = (isset($row->year)) ? $row->year : NULLSTR;
        $subArray['d'] = (isset($row->filedate)) ? $row->filedate : NULLSTR;
        $subArray['r'] = (isset($row->rating)) ? $row->rating : NULLSTR;
        $subArray['rby'] = (isset($row->ratingBy)) ? $row->ratingBy : NULLSTR;
        $subArray['c'] = (isset($row->comment)) ? $row->comment : NULLSTR;
        $subArray['cby'] = (isset($row->commentBy)) ? $row->commentBy : NULLSTR;

        $result[] = $subArray;
      }
      $queryResult->close(); // -- free result set --

      return $result;
    }



    // -- --------------------------------- --
    // -- begin of save a comment or rating --

    /**
     * THIS IS ALWAYS A CLONE OF THE FEEDBACK STORED VALUE
     * set the song which should be commented or rated
     * if it could not be found a new enty will be made
     * returns true if it has been found, false if it has been created
     */
    function setSong($aSong)
    {
      //$this->curSongFound = false; // -- if it could not be found, create a new entry in the following functions "setComment and/or setRating" --
      $this->curSong = $aSong;
    }


    /**
     * because a song could be available in several paths, every occurence will be updated
     * e.g.: "M83/M83 - Wait.mpg" -> LIKE "%M83 - Wait.mpg"
     * {@inheritDoc}
     * @see interface_mediabase::setComment()
     */
    function setComment($aComment, $aBy = NULLSTR)
    {
      $result = false;

      $comment = trim($aComment);

      $query = "UPDATE `table_base` SET `comment` = '$comment', `commentBy` = '$aBy'  WHERE `table_base`.`absolute` LIKE '%$this->curSong';";
      $queryResult = $this->mySQL->query($query);
      if ($queryResult)
      {
        eventLog('CHANGED COMMENT: "'.$comment.'" for song: '.$this->curSong);
        $result = true;
      }

      return $result;
    }


    /**
     * because a song could be available in several paths, every occurence will be updated
     * e.g.: "M83/M83 - Wait.mpg" -> LIKE "%M83 - Wait.mpg"
     * {@inheritDoc}
     * @see interface_mediabase::setRating()
     */
    function setRating($aRating, $aBy = NULLSTR)
    {
      $result = false;

      $query = "UPDATE `table_base` SET `rating` = '$aRating', `ratingBy` = '$aBy' WHERE `table_base`.`absolute` LIKE '%$this->curSong';";
      $queryResult = $this->mySQL->query($query);
      if ($queryResult)
      {
        eventLog('CHANGED RATING: "'.$aRating.'" for song: '.$this->curSong);
        $result = true;
      }

      return $result;
    }

    /**
     * saves the mediabase file, after comment or rating has been set when working in XML strategy.
     * Here in database stragedy it is only a facade (pattern)
     */
    function saveFeedback()
    {
      return true;
    }

    // -- end of save a comment or rating --
    // -- ------------------------------- --



    // -- --------------------------------- --
    // -- begin of find a comment or rating --

    /**
     * finds a wished song and returns rating and value in the following functions getComment... and so on
     * if it could not be found the following code by the caller has to abort the rest by negative result here
     * @param string $aSong
     * @return boolean
     */
    function findSong($aSong)
    {
      if ($this->dbconnected)
      {
        //$query = "SELECT `rating`,`ratingByID`,`comment`,`commentByID` FROM `table_base` WHERE `absolute` = '$aSong'";
        $query = "SELECT `rating`,`ratingBy`,`comment`,`commentBy` FROM `table_base` WHERE `absolute` LIKE '%$aSong'";
        $queryResult = $this->mySQL->query($query);
        $obj = $queryResult->fetch_object();

        //$this->curSongRating = (isset($obj->rating)) ? $obj->rating : NULLSTR;
        //$this->curSongRatingBy = (isset($obj->ratingByID)) ? $this->query_getUserById($obj->ratingByID) : NULLSTR;
        //$this->curSongComment = (isset($obj->comment)) ? $obj->comment : NULLSTR;
        //$this->curSongCommentBy = (isset($obj->commentByID)) ? $this->query_getUserById($obj->commentByID) : NULLSTR;

        $this->curSongRating = (isset($obj->rating)) ? $obj->rating : NULLSTR;
        $this->curSongRatingBy = (isset($obj->ratingBy)) ? $obj->ratingBy : NULLSTR;
        $this->curSongComment = (isset($obj->comment)) ? $obj->comment : NULLSTR;
        $this->curSongCommentBy = (isset($obj->commentBy)) ? $obj->commentBy : NULLSTR;

        $queryResult->close(); // -- free result set --
      }
    }


    function getComment()
    {
      return $this->curSongComment;
    }
    function getCommentBy()
    {
      return $this->curSongCommentBy;
    }

    function getRating()
    {
      return $this->curSongRating;
    }
    function getRatingBy()
    {
      return $this->curSongRatingBy;
    }


    function findSongReset()
    {
      $this->curSongComment = NULLSTR;
      $this->curSongCommentBy = NULLSTR;
      $this->curSongRating = NULLSTR;
      $this->curSongRatingBy = NULLSTR;
    }


    // -- end of find a comment or rating --
    // -- ------------------------------- --


  } // -- end of class_mediaBase "xml" --
