piMoo is a php-based party player using a console mpg player. It includes a webinterface and a REST-API for clients like piMooApp.
Where does the name come from? Raspi-Pi and the sound of a cow rhyming to music ;)

For installing have a look inside "_build" and howToInstallByHand.txt

Please also note the LICENSE. Basically it's free!