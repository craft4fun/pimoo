<h2>2020-06-20 <span style="color: blue;">V2.5.1</span> <span style="color: grey;">"LAST-iMUH"</span></h2>
<ul>
  <li>Removed openSuse support totally.</li>
  <li>Split required sudo rules in a separate file.</li>
</ul>

<h2>2020-06-19 <span style="color: blue;">V2.5.0</span> <span style="color: grey;">"LAST-iMUH"</span></h2>
<ul>
  <li>This is the last version of iMuh and with this the release candidate for piMoo!.</li>
</ul>

<h2>2020-06-09 <span style="color: blue;">V2.4.2</span> <span style="color: grey;">"MOUSECONTROL"</span></h2>
<ul>
  <li>Make alsamixer control configurable.</li>
</ul>

<h2>2020-04-30 <span style="color: blue;">V2.4.1</span> <span style="color: grey;">"MOUSECONTROL"</span></h2>
<ul>
  <li>Improve REST API for iMuh-App. An access token can take additional control of the user access.</li>
</ul>

<h2>2020-04-20 <span style="color: blue;">V2.4.0</span> <span style="color: grey;">"MOUSECONTROL"</span></h2>
<ul>
  <li>Start and stop iMuh by mouse events on raspberry pi using triggerhappy. Even skip a song with randomly add a new is possble.</li>
</ul>

<h2>2020-04-10 <span style="color: blue;">V2.3.1</span> <span style="color: grey;">"DAEMON"</span></h2>
<ul>
  <li>Improved setup script and add a real Daemon for running iMuh on system boot</li>
  <li>Add shutdown endpoint to api</li>
</ul>

<h2>2019-07-28 <span style="color: blue;">V2.2.5</span> <span style="color: grey;">"ADDRESULTS"</span></h2>
<ul>
  <li>Backend stuff: Add a feedback cleaner (depends on current mediabase)</li>
</ul>

<h2>2019-05-26 <span style="color: blue;">V2.2.4</span> <span style="color: grey;">"ADDRESULTS"</span></h2>
<ul>
  <li>Default Newcomer range is now 180 days</li>
</ul>

<h2>2019-05-25 <span style="color: blue;">V2.2.3</span> <span style="color: grey;">"ADDRESULTS"</span></h2>
<ul>
  <li>Bugfix missing player name variable</li>
</ul>

<h2>2019-02-16 <span style="color: blue;">V2.2.2</span> <span style="color: grey;">"ADDRESULTS"</span></h2>
<ul>
  <li>Inlcude iMoo and iMuhApp and make them available under myMuhEvenMore</li>
</ul>

<h2>2019-02-10 <span style="color: blue;">V2.2.1</span> <span style="color: grey;">"ADDRESULTS"</span></h2>
<ul>
  <li>remove center &quot;go up&quot; buttom</li>
  <li>restart playlist interval timer when open rating selection</li>
</ul>

<h2>2019-02-03 <span style="color: blue;">V2.2.0</span> <span style="color: grey;">"ADDRESULTS"</span></h2>
<ul>
  <li>Add search results to playlist at once</li>
</ul>

<h2>2018-04-23 <span style="color: blue;">V2.1.2</span> <span style="color: grey;">"SNDCTRL"</span></h2>
<ul>
  <li>Improved navigation</li>
</ul>

<h2>2017-11-11 <span style="color: blue;">V2.1.1</span> <span style="color: grey;">"SNDCTRL"</span></h2>
<ul>
  <li>Some smaller bugfixes</li>
</ul>

<h2>2016-12-20 <span style="color: blue;">V2.1.0</span> <span style="color: grey;">"SNDCTRL"</span></h2>
<ul>
  <li>Upgrade sound-control API</li>
</ul>

<h2>2016-12-20 <span style="color: blue;">V2.0.1</span> <span style="color: grey;">"SKIPPER"</span></h2>
<ul>
  <li>Some improvements</li>
</ul>

<h2>2016-12-18 <span style="color: blue;">V2.0.0</span> <span style="color: grey;">"SKIPPER"</span></h2>
<ul>
  <li>Move rewind or forward inside a song</li>
</ul>

<h2>2016-11-03 <span style="color: blue;">V1.9.7</span> <span style="color: grey;">"CLEANUP"</span></h2>
<ul>
  <li>Interal cleanup</li>
</ul>

<h2>2016-01-06 <span style="color: blue;">V1.9.6</span> <span style="color: grey;">"CUTBLANK"</span></h2>
<ul>
  <li>Optionaly do not cut the blank from search strings to find short stuff like "Sia" instead of "Asia"</li>
  <li>Optionally do not ignore case sensitivity for search strings</li>
</ul>

<h2>2016-01-01 <span style="color: blue;">V1.9.5</span> <span style="color: grey;">"QUICKSTART"</span></h2>
<ul>
  <li>New quickstart function in admin's area</li>
</ul>

<h2>2016-01-01 <span style="color: blue;">V1.9.4</span> <span style="color: grey;">"NEWYEAR"</span></h2>
<ul>
  <li>Bugfix in "bye"</li>
</ul>

<h2>2015-12-24 <span style="color: blue;">V1.9.3</span> <span style="color: grey;">"PARTYMODE"</span></h2>
<ul>
  <li>Improved the alignment in search results</li>
  <li>Bugfix in shutdown after last song</li>
  <li>Improved player behaviour regarding autoshutdown and pick random song</li>
</ul>

<h2>2015-11-23 <span style="color: blue;">V1.8.17</span> <span style="color: grey;">"MOREREST"</span></h2>
<ul>
  <li>More REST-API functions: add song at top if admin</li>
</ul>

<h2>2015-10-29 <span style="color: blue;">V1.8.16</span> <span style="color: grey;">"MOREREST"</span></h2>
<ul>
  <li>More REST-API endpoints: dorating and doComment</li>
  <li>Improvements on APIs</li>
</ul>

<h2>2015-09-25 <span style="color: blue;">V1.8.14</span> <span style="color: grey;">"REST-ALBUM"</span></h2>
<ul>
  <li>Deliver album picture and lyrics over REST-API for iMuhApp</li>
</ul>

<h2>2015-07-17 <span style="color: blue;">V1.8.13</span> <span style="color: grey;">"NANE"</span></h2>
<ul>
  <li>Little improvements on style</li>
</ul>

<h2>2015-05-18 <span style="color: blue;">V1.8.12</span> <span style="color: grey;">"NANE"</span></h2>
<ul>
  <li>Little improvements on CORS header</li>
</ul>


<h2>2015-05-05 <span style="color: blue;">V1.8.11</span> <span style="color: grey;">"NANE"</span></h2>
<ul>
  <li>User can upload own song(s) make available for party! (thanks to nane)</li>
  <li>User can delete own added song(s) from playlist</li>
</ul>

<h2>2015-04-29 <span style="color: blue;">V1.8.9</span> <span style="color: grey;">"REST_API"</span></h2>
<ul>
  <li>Bugfix for playing random after last song or playlist is empty</li>
</ul>

<h2>2015-04-10 <span style="color: blue;">V1.8.8</span> <span style="color: grey;">"api/1"</span></h2>
<ul>
  <li>Internal improvements.</li>
  <li>More REST API for iMuhApp</li>
  <li>Current api: api/1/</li>
  <li>Improved frontend_noJS</li>
</ul>

<h2>2015-03-27 <span style="color: blue;">V1.8.3</span> <span style="color: grey;">"FRONTEND_ADMIN"</span></h2>
<ul>
  <li>A special frontend which includes the common iMuh page and admin page side by side in one window.</li>
  <li>Little bugfixes</li>
  <li>A click onto the nickname performs a search for all votes song for this user</li>
</ul>

<h2>2015-03-15 <span style="color: blue;">V1.7.7</span> <span style="color: grey;">"MEDIABASE_UPDATE_XML"</span></h2>
<ul>
  <li>All settings in myMuh are stored as cookies instead of session now.</li>
</ul>

<h2>2015-03-06 <span style="color: blue;">V1.7.6</span> <span style="color: grey;">"COOKIE"</span></h2>
<ul>
  <li>All settings in myMuh are stored as cookies instead of session now.</li>
</ul>

<h2>2015-02-21 <span style="color: blue;">V1.7.5</span> <span style="color: grey;">"REST"</span></h2>
<ul>
  <li>Began of implement a REST interface to be prepared to controlled by a real app instead of the own web interface.</li>
</ul>

<h2>2015-01-14 <span style="color: blue;">V1.7.4</span> <span style="color: grey;">"iMoo_CRITICALCHARS"</span></h2>
<ul>
  <li>iMoo can handle now songs with critical chars like <?php echo AMPERSAND; ?> and ' by using base64.</li>
</ul>

<h2>2015-01-13 <span style="color: blue;">V1.7.3</span> <span style="color: grey;">"iNOJS"</span></h2>
<ul>
  <li>Improved iMuh console mode for browsers like "lynx".</li>
</ul>

<h2>2015-01-08 <span style="color: blue;">V1.7.2</span> <span style="color: grey;">"ALBUMCOVER"</span></h2>
<ul>
  <li>Smaller album dummy in playlist.</li>
  <li>iMoo can change nicknames: that gives the possibility to handle with playlists!</li>
</ul>

<h2>2015-01-02 <span style="color: blue;">V1.7.1</span> <span style="color: grey;">"ALBUMCOVER"</span></h2>
<ul>
  <li>option: show album cover in playlist if available.</li>
  <li>great enhancements in iMoo.</li>
</ul>

<h2>2015-01-01 <span style="color: blue;">V1.7</span> <span style="color: grey;">"HAPPYNEWYEAR"</span></h2>
<ul>
  <li>adminControlBar.</li>
  <li>cli_buildCache.</li>
</ul>

<h2>2014-12-30 <span style="color: blue;">V1.6.2</span> <span style="color: grey;">"iMoo"</span></h2>
<ul>
  <li>iMoo great steps.</li>
  <li>Omnipresent song title is handled better now. Only with ajax-request if not already available.</li>
</ul>

<h2>2014-12-27 <span style="color: blue;">V1.6</span> <span style="color: grey;">"ADMINCONTROLBAR"</span></h2>
<ul>
  <li>New: AdminControlBar always visible in every page if admin is logged in.</li>
</ul>

<h2>2014-12-20 <span style="color: blue;">V1.5</span> <span style="color: grey;">"MEDIABASETYPES"</span></h2>
<ul>
  <li>New: several mediaBase types for optimization to individual plattforms. e.g. optimized for speed or disk space. mySQL in progress.</li>
</ul>

<h2>2014-12-20 <span style="color: blue;">V1.4.4</span> <span style="color: grey;">"ETA"</span></h2>
<ul>
  <li>ETA and remaintime of onAir song are updated now via javascript every second and synchrounized via ajax every 10th second</li>
</ul>

<h2>2014-11-09 <span style="color: blue;">V1.4.3</span> <span style="color: grey;">"ADDALL"</span></h2>
<ul>
  <li>Add a whole history playlist to current playlist &rarr; only for admin</li>
</ul>

<h2>2014-10-12 <span style="color: blue;">V1.4.2</span> <span style="color: grey;">"iMoo"</span></h2>
<ul>
  <li>iMoo's getting better</li>
  <li>Result of latest songs is comes sorted by date - so latest on top</li>
</ul>

<h2>2014-10-11 <span style="color: blue;">V1.4</span> <span style="color: grey;">"iMoo"</span></h2>
<ul>
  <li>NEW: A very early version of "iMoo"</li>
</ul>

<h2>2014-10-09 <span style="color: blue;">V1.3</span> <span style="color: grey;">"STREAM"</span></h2>
<ul>
  <li>Possibility to disable preview player with streaming song and album cover</li>
</ul>

<h2>2014-10-06 <span style="color: blue;">V1.2.1</span> <span style="color: grey;">"DOJO1101"</span></h2>
<ul>
  <li>Upgrade to dojo toolkit 1.10.1</li>
</ul>

<h2>2014-10-01 <span style="color: blue;">V1.2</span> <span style="color: grey;">"iFEEDBACK"</span></h2>
<ul>
  <li>Improved feedback</li>
</ul>

<h2>2014-09-26 <span style="color: blue;">V1.1</span> <span style="color: grey;">"RELOAD"</span></h2>
<ul>
  <li>Faster reload of playlist after move or kill</li>
</ul>

<h2>2014-08-30 <span style="color: blue;">V1.0.1</span> <span style="color: grey;">"V1"</span></h2>
<ul>
  <li>Improved loading speed of voted songs by using feedback clone</li>
  <li>An always visible scroll-to-top-link</li>
  <li>Fade out before reboot or shutdown</li>
</ul>

<h2>2014-08-27 <span style="color: blue;">V0.9.22</span> <span style="color: grey;">"SHUTDOWN"</span></h2>
<ul>
  <li>Shutdown whole system</li>
  <li>Read back real playerstate in backend control</li>
</ul>

<h2>2014-08-24 <span style="color: blue;">V0.9.20</span> <span style="color: grey;">"PEANUTS"</span></h2>
<ul>
  <li>Quick exit for admin - user in admin's area</li>
  <li>Added "sudo" for debian install in install script</li>
  <li>Backend improvements like network infos</li>
</ul>

<h2>2014-08-03 <span style="color: blue;">V0.9.18</span> <span style="color: grey;">"TABLEPLUS"</span></h2>
<ul>
  <li>Show results as table with or without album column</li>
  <li>Show causer for comment and rating as HTML title in the on-air-block</li>
</ul>

<h2>2014-07-31 <span style="color: blue;">V0.9.17.1</span> <span style="color: grey;">"GOAHEAD"</span></h2>
<ul>
  <li>Tidied up playerstates</li>
  <li>If not playing, the on-air-song can be moved or killed also</li>
  <li>Tap onto the iMuh cow will reload the playlist much more fast</li>
  <li>Little styling issues</li>
</ul>

<h2>2014-06-15 <span style="color: blue;">V0.9.16.1</span> <span style="color: grey;">"BLACK_WHITELIST"</span></h2>
<ul>
  <li>The black and whitelists affect all: search filter and randomly picked songs</li>
  <li>No double delivery in random results</li>
</ul>

<h2>2014-06-13 <span style="color: blue;">V0.9.15</span> <span style="color: grey;">"BLACK_WHITELIST"</span></h2>
<ul>
  <li>If playlist is empty and the player is processing with random songs, one can define a whitelist for genres which are allowed to be player</li>
  <li>One can define a blacklist with genres which filteres out all songs relate to these genres</li>
</ul>

<h2>2014-04-03 <span style="color: blue;">V0.9.14.1</span> <span style="color: grey;">"VERY HAPPY"</span></h2>
<ul>
  <li>Feedback in mobile mode more margin to top</li>
</ul>

<h2>2014-04-03 <span style="color: blue;">V0.9.13</span> <span style="color: grey;">"VERY HAPPY"</span></h2>
<ul>
  <li>Why not being very happy, the preview player works even with songs with '&amp;' in path or filename.</li>
</ul>

<h2>2014-04-03 <span style="color: blue;">V0.9.12</span> <span style="color: grey;">"HAPPY"</span></h2>
<ul>
  <li>Why not being happy, the preview player works almost satisfactorily.</li>
</ul>

<h2>2014-04-03 <span style="color: blue;">V0.9.11.1</span> <span style="color: grey;">"APRILFOOL"</span></h2>
<ul>
  <li>Go ahead with development :o).</li>
</ul>

<h2>2014-03-28 <span style="color: blue;">V0.9.10</span> <span style="color: grey;">"SPEEDUP"</span></h2>
<ul>
  <li>Speedup by short tags.</li>
</ul>

<h2>2014-03-25 <span style="color: blue;">V0.9.9.1</span> <span style="color: grey;">"DOCUMENTATION"</span></h2>
<ul>
  <li>A documentation page.</li>
</ul>

<h2>2014-03-24 <span style="color: blue;">V0.9.9</span> <span style="color: grey;">"SEARCHUSER"</span></h2>
<ul>
  <li>Search for user's comment more clear. Even on a short name like "Tim" no confusing results like "time" appears.</li>
</ul>

<h2>2014-03-21 <span style="color: blue;">V0.9.8.3</span> <span style="color: grey;">"ST.PATRICK"</span></h2>
<ul>
  <li>TimeZone is setable in config.php.</li>
  <li>Mobile/desktop version works fine now in every sub-page, too.</li>
</ul>

<h2>2014-03-17 <span style="color: blue;">V0.9.8.2</span> <span style="color: grey;">"ST.PATRICK"</span></h2>
<ul>
  <li>New api_player.php.</li>
</ul>

<h2>2014-03-12 <span style="color: blue;">V0.9.7</span> <span style="color: grey;">"MORECONTROL"</span></h2>
<ul>
  <li>More control for player - new: pause/play.</li>
  <li>The preview asks on leaving for confirmation when preview player is running.</li>
</ul>

<h2>2014-03-03 <span style="color: blue;">V0.9.6.3</span> <span style="color: grey;">"FACELIFT"</span></h2>
<ul>
  <li>A small redesign.</li>
  <li>Cache also handles language.</li>
  <li>When preview player is playing leaving page will ask for confirmation.</li>
</ul>

<h2>2014-02-23 <span style="color: blue;">V0.9.5.5</span> <span style="color: grey;">"ELEPHANT"</span></h2>
<ul>
  <li>Remember the last search in search bar.</li>
  <li>A song can also be voted and rated in the preview page (finally one can hear the song there, too!).</li>
  <li>Improvements on caching.</li>
  <li>Improvements on page valuedSong.php.</li>
  <li>Enlarged the selects and inputs on mobile mode, too.</li>
</ul>

<h2>2014-02-11 <span style="color: blue;">V0.9.4.2</span> <span style="color: grey;">"LANGUAGE"</span></h2>
<ul>
  <li>Finishing to V0.9.0.</li>
  <li>Licenses listed at the bottom of this page.</li>
</ul>

<h2>2014-02-08 <span style="color: blue;">V0.9.0</span> <span style="color: grey;">"LANGUAGE"</span></h2>
<ul>
  <li>User interface languages are switchable now. Supported are English and German at the moment.</li>
  <li>Cache now compatible to maxSearchResultsPerPage.</li>
</ul>

<h2>2014-02-07 <span style="color: blue;">V0.8.2</span> <span style="color: grey;">"CODEWORD"</span></h2>
<ul>
  <li>Little improvements.</li>
</ul>

<h2>2014-02-06 <span style="color: blue;">V0.8.0</span> <span style="color: grey;">"CODEWORD"</span></h2>
<ul>
  <li>"Code word" of the day &rarr; required to get into iMuh. The answer should always be provided by the "party" flyer.</li>
  <li>Removed cfg_forceNickname &rarr; nickname is always required now.</li>
  <li>Removed cfg_feedbackAllowEverybody.</li>
</ul>

<h2>2014-02-06 <span style="color: blue;">V0.7.4.1</span> <span style="color: grey;">"PREVIEW"</span></h2>
<ul>
  <li>Some little improvements.</li>
</ul>

<h2>2014-02-05 <span style="color: blue;">V0.7.4</span> <span style="color: grey;">"PREVIEW"</span></h2>
<ul>
  <li>Some improvements to preview.</li>
  <li>Please wait message for long actions.</li>
  <li>More intern logging.</li>
</ul>

<h2>2014-02-04 <span style="color: blue;">V0.7.0</span> <span style="color: grey;">"PREVIEW"</span></h2>
<ul>
  <li>One can hear a preview of a song before adding it to the playlist. (Browser must support the HTML5 audio element).</li>
  <li>On preview page one can also see the lyrics N cover if available.</li>
</ul>

<h2>2014-02-03 <span style="color: blue;">V0.6.10</span> <span style="color: grey;">"JANITOR"</span></h2>
<ul>
  <li>Admin can purge doubles from playlist and songs which are not available in filesystem.</li>
</ul>

<h2>2014-02-02 <span style="color: blue;">V0.6.9</span> <span style="color: grey;">"MOVETOP"</span></h2>
<ul>
  <li>When adding a song, admin is able to move it top immediately.</li>
</ul>

<h2>2014-02-02 <span style="color: blue;">V0.6.8</span> <span style="color: grey;">"NEWCOMERS"</span></h2>
<ul>
  <li>Search easily for newcomers in the mediabase.</li>
  <li>In admin mode the welcome intro is set to 0 seconds.</li>
</ul>

<h2>2014-01-31 <span style="color: blue;">V0.6.7</span> <span style="color: grey;">"SVN"</span></h2>
<ul>
  <li>First release with versioncontrol support subversion "svn".</li>
</ul>

<h2>2014-01-30 <span style="color: blue;">V0.6.6.3</span> <span style="color: grey;">"TOPX"</span></h2>
<ul>
  <li>New songs enters the everlastig hitlist at the bottom.</li>
  <li>Hitlist entries are limited to a setable value.</li>
  <li>includeJavascript().</li>
</ul>

<h2>2014-01-19 <span style="color: blue;">V0.6.5.2</span> <span style="color: grey;">"HISTORY"</span></h2>
<ul>
  <li>Improvements on everlasting history.</li>
  <li>GPIO 17 as default.</li>
</ul>

<h2>2014-01-13 <span style="color: blue;">V0.6.4.2</span> <span style="color: grey;">"HIFI"</span></h2>
<ul>
  <li>Improvements on GPIO handling.</li>
</ul>

<h2>2014-01-08 <span style="color: blue;">V0.6.4</span> <span style="color: grey;">"HIFI"</span></h2>
<ul>
  <li>On Raspberry PI one can turn on the HiFi when starting iMuh.</li>
  <li>Cookie based settings in myMuh will kept for one month.</li>
</ul>

<h2>2013-12-30 <span style="color: blue;">V0.6.3.1</span> <span style="color: grey;">"NEWYEAR"</span></h2>
<ul>
  <li>Core script included as php (so defines can be used).</li>
  <li>Bugfixes.</li>
</ul>

<h2>2013-12-18 <span style="color: blue;">V0.6.2</span> <span style="color: grey;">"XMAS"</span></h2>
<ul>
  <li>Optionally show the current song always on top (omnipresent).</li>
</ul>

<h2>2013-12-18 <span style="color: blue;">V0.6.1</span> <span style="color: grey;">"MAKERELEASE"</span></h2>
<ul>
  <li>A script which makes a installer release in a quick way.</li>
</ul>

<h2>2013-12-17 <span style="color: blue;">V0.6</span> <span style="color: grey;">"INSTALLER"</span></h2>
<ul>
  <li>Some backend changes for temporary files.</li>
  <li>A installer script to bring iMuh to any Linux machine.</li>
</ul>

<h2>2013-12-13 <span style="color: blue;">V0.5.11.1</span> <span style="color: grey;">"TABLE"</span></h2>
<ul>
  <li>Search results as table if set in myMuh.</li>
  <li>Removed $cfg_addWithoutConfirm. It always has to be confirmed adding a song.</li>
</ul>

<h2>2013-11-04 <span style="color: blue;">V0.5.10</span> <span style="color: grey;">"NICK"</span></h2>
<ul>
  <li>
    You can enter iMuh directly with giving a nick name to skip the "gimme nickname" procedure.<br>
    A little more convenient especially if a nick name is required by force.
  </li>
</ul>

<h2>2013-11-01 <span style="color: blue;">V0.5.9</span> <span style="color: grey;">"MILESTONE"</span></h2>
<ul>
  <li>Show also all valued songs of feedback.</li>
</ul>

<h2>2013-10-31 <span style="color: blue;">V0.5.8.1</span> <span style="color: grey;">"STYLE"</span></h2>
<ul>
  <li>New styling</li>
</ul>

<h2>2013-10-30 <span style="color: blue;">V0.5.8</span> <span style="color: grey;">"PARTYREPORT"</span></h2>
<ul>
  <li>Everyone who's logged in (by given his nickname) can see the hitlist and partyreport (and pick titles of them)</li>
</ul>

<h2>2013-10-28 <span style="color: blue;">V0.5.7</span> <span style="color: grey;">"FADEOVER"</span></h2>
<ul>
  <li>Skip to next song fades out the volume before</li>
  <li>dto when stopping the whole player</li>
</ul>

<h2>2013-10-28 <span style="color: blue;">V0.5.7</span> <span style="color: grey;">"PEANUTS"</span></h2>
<ul>
  <li>Changed the order of the edit-playlist-arrows</li>
</ul>

<h2>2013-10-15 <span style="color: blue;">V0.5.6</span> <span style="color: grey;">"RASPIAN"</span></h2>
<ul>
  <li>Supporting Raspian regarding amixer</li>
</ul>

<h2>2013-10-15 <span style="color: blue;">V0.5.5</span> <span style="color: grey;">"PLAYERCONTROL"</span></h2>
<ul>
  <li>Remote control for the cli player (start and stop in admin's area)</li>
</ul>

<h2>2013-07-03 <span style="color: blue;">V0.5.4</span> <span style="color: grey;">"NANE30"</span></h2>
<ul>
  <li>A release just in time for nane's 30th birthday party!</li>
</ul>

<h2>2013-07-02 <span style="color: blue;">V0.5.3</span> <span style="color: grey;">"myMuh"</span></h2>
<ul>
  <li>A new myMuh page for individual setting for each user (e.g. amount of random search results).</li>
  <li>Tolerant searching for interpret, title, album and genre (with damerau levenshtein algo). Note: If a wildcard char is used, the tolerant search is skipped.</li>
</ul>

<h2>2013-06-11 <span style="color: blue;">V0.5.2.2</span> <span style="color: grey;">"JQUERY"</span></h2>
<ul>
  <li>Use $('') instead document.getElementById('').</li>
  <li>frame_logo added.</li>
</ul>

<h2>2013-06-11 <span style="color: blue;">V0.5.2.1</span> <span style="color: grey;">"IMPORTER"</span></h2>
<ul>
  <li>cli_importer.php and action_importer.php reporting only "mp3" files.</li>
  <li>Show no real feedback by cli when pushing next song.</li>
</ul>

<h2>2013-06-10 <span style="color: blue;">V0.5.2</span> <span style="color: grey;">"IMPORTER"</span></h2>
<ul>
  <li>New: cli_importer.php.</li>
  <li>Improvements on action_importer.php.</li>
  <li>Bugfix in show ALL relating to confirm load huge html.</li>
</ul>

<h2>2013-06-08 <span style="color: blue;">V0.5.1.1</span> <span style="color: grey;">"HISTORY"</span></h2>
<ul>
  <li>Backend returns to frontend_noJS if it came from there.</li>
</ul>

<h2>2013-06-06 <span style="color: blue;">V0.5.1</span> <span style="color: grey;">"HISTORY"</span></h2>
<ul>
  <li>Real browser history, every called page is saved by browser's history and reloadable.</li>
</ul>

<h2>2013-06-03 <span style="color: blue;">V0.4.1</span> <span style="color: grey;">"FALLBACK"</span></h2>
<ul>
  <li>Browser history is filled, even though not necessary but to avoid escaping the iMuh site by simply clicking on browers back button.</li>
</ul>

<h2>2013-06-03 <span style="color: blue;">V0.4.0</span> <span style="color: grey;">"FRIENDS"</span></h2>
<ul>
  <li>A larger iMuh cow. (Thanks Kruno for feedback)</li>
  <li>Navigation by A-Z, NUM and @ is obsolete now. Instead a single char in the search field will lead to the same results.</li>
  <li>Non reachable/playable songs are logged in error.txt.</li>
</ul>

<h2>2013-06-01 <span style="color: blue;">V0.3.13.2</span> <span style="color: grey;">"HUMANVOTING"</span></h2>
<ul>
  <li>Peanuts.</li>
</ul>

<h2>2013-05-31 <span style="color: blue;">V0.3.13</span> <span style="color: grey;">"HUMANVOTING"</span></h2>
<ul>
  <li>Rating is human readable instead of ~3~ and so far.</li>
  <li>History is readable for everybody.</li>
  <li>Links at bottom to switch quickly between mobile and desktop version.</li>
</ul>

<h2>2013-05-30 <span style="color: blue;">V0.3.12</span> <span style="color: grey;">"IMPROVED"</span></h2>
<ul>
  <li>Improved xml concept.</li>
</ul>

<h2>2013-05-29 <span style="color: blue;">V0.3.11</span> <span style="color: grey;">"iCACHE"</span></h2>
<ul>
  <li>Smarter cache system.</li>
  <li>Re-design of the feedback system. Searching and displaying deals with mediabase.xml and feedback.xml with priority on feedback.xml (because it could be newer).</li>
</ul>

<h2>2013-05-29 <span style="color: blue;">V0.3.9.3</span> <span style="color: grey;">"ERRORMSG"</span></h2>
<ul>
  <li>A feedback message for errors &rarr; comes in red.</li>
  <li>New favicon.</li>
</ul>

<h2>2013-05-28 <span style="color: blue;">V0.3.9.2</span> <span style="color: grey;">"DYNAMICLOOP"</span></h2>
<ul>
  <li>The master loop in cli_player increases it's wait time to a maximum of (e.g.) one second with every step where no song is playable (to avoid 100%CPU). It is reseted when the next playable songs comes into playlist.</li>
  <li>Push songs to party report and hitlist only if they are really played.</li>
  <li>Little improvements on design.</li>
  <li>Begin to maintain this history.</li>
</ul>

<h2>2013-05-27 <span style="color: blue;">V0.3.9</span> <span style="color: grey;">"PLAYER_FASTLOOP"</span></h2>
<ul>
  <li>The master loop in cli_player.php operates much faster now.</li>
</ul>

<h2>2013-05-26 <span style="color: blue;">V0.3.8</span> <span style="color: grey;">"HITLIST_NEW"</span></h2>
<ul>
  <li>Hitlist is handled in a new way &rarr; new songs are pushed to the hitlist from top. If in already they move up step by step.</li>
</ul>

<h2>2013-05-23 <span style="color: blue;">V0.3.6</span> <span style="color: grey;">"NEWLOGO"</span></h2>
<ul>
  <li>A new iMuh cow.</li>
</ul>

<h2>2013-05-19 <span style="color: blue;">V0.3.4</span> <span style="color: grey;">"VOLUMECONTROL"</span></h2>
<ul>
  <li>In admin mode one can adjust the loudness.</li>
</ul>

<h2>2013-05-13 <span style="color: blue;">V0.3.2</span> <span style="color: grey;">"SINA"</span></h2>
<ul>
  <li>The search results come in a more understandable way. Thanks to feedback by Sina.</li>
</ul>

<h2>2013-03-13 <span style="color: blue;">V0.3.1</span> <span style="color: grey;">"HITLIST"</span></h2>
<ul>
  <li>If partyreport is on, all played songs are gathered in form of a everlasting hitlist.</li>
</ul>

<h2>2013-03-09 <span style="color: blue;">V0.3</span> <span style="color: grey;">"REALREMAIN"</span></h2>
<ul>
  <li>The real remaining time and ETA times for playlist are shown in the main view.</li>
</ul>

<h2>2013-03-06 <span style="color: blue;">V0.2.4</span> <span style="color: grey;">"LYNX"</span></h2>
<ul>
  <li>A slim bypass without CSS and overhead to be fast with LYNX on the console.</li>
</ul>

<h2>2013-01-08 <span style="color: blue;">V0.2.3</span> <span style="color: grey;">"WILDCARD"</span></h2>
<ul>
  <li>The free filed search can handle wildcards as like * and ?.</li>
</ul>

<h2>2013-01-07 <span style="color: blue;">V0.2.2</span> <span style="color: grey;">"RANDOM"</span></h2>
<ul>
  <li>For wishy-washy search one can displayd (ten) random results of the whole available mediabase.</li>
</ul>

<h2>2012-12-21 <span style="color: blue;">V0.2.1</span> <span style="color: grey;">"MOBILEFORCE"</span></h2>
<ul>
  <li>One can force iMuh to dispatch for mobile- or desktop devices by using: "mobile.php"; "nobile.php" and return to automatic with "obile.php".</li>
</ul>


<h2>201?-??-?? <span style="color: blue;">V0.0.0.0</span> <span style="color: grey;">"BIG_BANG"</span></h2>
<ul>
  <li>... a few moons ago ...</li>
  <li>Ren&eacute;&acute;s idea with nicknames has been implemented in these days.</li>
</ul>


