<?php
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('api_layout.php');

  require_once ('lib/utils.php');

  require_once ('api_mediabase.php');
  require_once ('api_playlist.php');
  require_once ('api_feedback.php'); // needed ?


  echo makeHTML_begin(true);

  includeStyleSheet();
  includeJavascript();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  // -- make a global variable for mobile or not --
  $isMobile = (cntIsMobile()) ? true : false;

  // -- the order of this call before accessCheck is important --
  echo makeStandards($admin);
  // -- a generally access check, must be after makeStandards --
  echo accessCheck();


  // -- head --
  echo '<div class="commonHead">';

    echo '<div class="commonHeadText">'.LNG_VALUED_SONGS.' (feedback)</div>';
    //echo '<div style="text-size: smaller; text-align: center;">usually view only (no adding to playlist)</div>';

    echo '<span><a class="linkNaviNice" style="float: right;"href="index.php">'.LNG_BACK_TO_.PROJECT_NAME.'</a></span>';
    echo '<span><a class="linkNaviNice" href="myMooMore.php">'.LNG_BACK_TO_.LNG_MORE.'</a></span>';

  echo '</div>';
?>
<style type="text/css">
  .vby {
    color: black;
    font-size: smaller;
  }
</style>
<div style="margin-top: 2em;">
<?php

  // -- ---- --
  // -- Todo --
  // -- ---- --

  // -- show the history of the feedback for each song here, too --

  // -- ---- --
  // -- Todo --
  // -- ---- --

  $offset = (isset($_GET['offset'])) ? $_GET['offset'] : NULLINT;
  // -- limit to positive offset --
  if ($offset < NULLINT)
    $offset = NULLINT;

  // -- init navigation --
  $resultsPrevious = '<div class="resultNavi" style="float: left;">
                              <div class="grpLstItmL" title="'.LNG_PREVIOUS_RESULTS.'">
                                <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="valuedSongs.php?offset='.intval($offset - intval($cfg_maxSearchResultsPerPage*5)).'">'.HTML_LESS.HTML_LESS.'</a>
                                <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="valuedSongs.php?offset='.intval($offset - $cfg_maxSearchResultsPerPage).'">'.HTML_LESS.'</a>
                              </div>
                            </div>';
  $resultsNext = '<div class="resultNavi" style="float: right;">
                          <div class="grpLstItmL" title="'.LNG_NEXT_RESULTS.'">
                            <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="valuedSongs.php?offset='.intval($offset + $cfg_maxSearchResultsPerPage).'">'.HTML_GREATER.'</a>
                            <a class="grpLstContL" onclick="scrollTopCollapseNavi()" href="valuedSongs.php?offset='.intval($offset + intval($cfg_maxSearchResultsPerPage*5)).'">'.HTML_GREATER.HTML_GREATER.'</a>
                          </div>
                        </div>';


  $myMediabase = new class_mediaBase();
  $votedSongs = $myMediabase->getVotedSongs($offset, $cfg_maxSearchResultsPerPage);
  $count = count($votedSongs);

  // -- upper navigation --
  echo '<div style="width: 100%; float: left;">';
    if ($cfg_maxSearchResultsPerPage > NULLINT)
    {
      if ($count > NULLINT && $count >= $cfg_maxSearchResultsPerPage)
        echo $resultsNext;
      if ($offset > NULLINT)
        echo $resultsPrevious;
      else
        echo '<div class="resultNavi" style="float: left;"></div>';
    }
  echo '</div>';

  echo '<div style="margin-left: 1em;">'.LNG_VALUED_SONGS . COLON . BLANK . '<span style="color: red;">'.$count.'</span></div>';

  // -- Attention: Use the actual unique main frame hulk ID "frame_hulk" where all ajax called stuff is pumped into, here the search result. --
  echo '<div id="frame_hulk">';
    foreach ($votedSongs as $song)
    {
      $escapedInterpret = $song['i']; // str_replace('\'', '\\\'', $song['i']);
      $escapedTitle = $song['t']; //str_replace('\'', '\\\'', $song['t']);
      $escapedRatingBy = $song['rby']; //str_replace('\'', '\\\'', $song['rby']);
      $escapedComment = $song['c']; //str_replace('\'', '\\\'', $song['c']);
      $escapedCommentBy = $song['cby']; //str_replace('\'', '\\\'', $song['cby']);

       echo '<div class="playListRowL boxShadow">';
         // -- default --
         echo '<a class="grpLstItmR" href="javascript:songGetInfo(\''.base64_encode($song['a']).'\')"><span style="text-align: center;" class="grpLstContR">i</span></a>';
         //-- first hit --
         //echo '<a class="grpLstItmR" href="javascript:songGetInfo(\''.base64_encode(basename($song[a])).'\', true)"><span style="text-align: center;" class="grpLstContR">i</span></a>';

        echo '<a class="grpLstItmR" href="javascript:songAdd(\''.base64_encode($song['a']).'\',\''.base64_encode($song['i']).'\',\''.base64_encode($song['t']).'\',\''.$song['p'].'\',\''.base64_encode($song['l']).'\',\''.$escapedTitle.'\')">';

          echo '<div class="listItemInterpret">' . $escapedInterpret . '</div>';
          echo '<div class="listItemTitle">' . $escapedTitle . '</div>';

          // -- divider --
          echo '<div>&nbsp;</div>';

          echo '<span style="color: #ffff80;">' . cntGetHumanRating($song['r']) . ' <span class="vby">(by: '.$escapedRatingBy.')</span></span>';

          echo '<span style="float: right;">';
          echo '<span class="comment"><span style="margin-left: 1em; color: #ffff80;">' . $escapedComment . '</span><span class="vby"> (by: '.$escapedCommentBy.')</span></span>';
          echo '</span>';

        echo '</a>';
      echo '</div>';
    }
  echo '</div>'; // -- end of frame_hulk --

  // -- lower navigation --
  echo '<div style="width: 100%; float: left;">';
  if ($cfg_maxSearchResultsPerPage > NULLINT)
  {
    if ($count > NULLINT && $count >= $cfg_maxSearchResultsPerPage)
    {
      echo $resultsNext;
      if ($offset > NULLINT)
        echo $resultsPrevious;
      else
        echo '<div class="resultNavi" style="float: left;"></div>';
    }
  }
  echo '</div>';


  /* -- the old style: directly from feedback.xml and references from mediabase .xml --
  $myMediabase = new class_mediaBase();
  $attr = XML_BY;
  $playlist = simplexml_load_file('keep/feedback.xml');
  foreach ($playlist as $song)
  {
    $info = $myMediabase->getSongInfo((string)$song->a, true); // , true
    $escapedInterpret = str_replace('\'', '\\\'', $info['i']);
    $escapedTitle = str_replace('\'', '\\\'', $info['t']);
    $escapedRatingBy = str_replace('\'', '\\\'', $info['rby']);
    $escapedComment = str_replace('\'', '\\\'', $info['c']);
    $escapedCommentBy = str_replace('\'', '\\\'', $info['cby']);
    if ($info !== false)
    {
      echo '<div class="playListRowL boxShadow">';
        echo '<a class="grpLstItmR" href="javascript:songGetInfo(\''.base64_encode($song->a).'\', true)"><span style="text-align: center;" class="grpLstContR">i</span></a>';
        echo '<a class="grpLstItmR" href="javascript:songAdd(\''.base64_encode($song->a).'\',\''.base64_encode($info['i']).'\',\''.base64_encode($info['t']).'\',\''.$info['p'].'\',\''.$info['l'].'\',\''.$escapedTitle.'\')">';
          // -- if found by luckiness, the first hit (without intern mediabase path, only filename) --
          echo '<div style="text-align: center; color: red;">'.LNG_FIRST_HIT.'</div>';
          echo '<div style="color: black;">' . $escapedInterpret . '</div>';
          echo '<div style="color: blue;">' . $escapedTitle . '</div>';
          // -- divider --
          echo '<div>&nbsp;</div>';
          echo '<span style="color: #ffff80;">' . cntGetHumanRating($info['r']) . ' <span class="vby">(by: '.$escapedRatingBy.')</span></span>';
          echo '<span style="float: right;">';
            echo '<span class="comment"><span style="margin-left: 1em; color: #ffff80;">' . $escapedComment . '</span><span class="vby"> (by: '.$escapedCommentBy.')</span></span>';
          echo '</span>';
        echo '</a>';
      echo '</div>';
    }
    else
    {
      // -- file could not be found in the mediabase.xml, but ist voted in the feedback.xml --
      echo '<div class="playListRowL boxShadow">';
        echo '<div style="color: blue; ">' . $song->a . '</div>'; // text-decoration: line-through;
        // -- divider --
        echo '<div>&nbsp;</div>';
        echo '<span style="color: #ffff80;">' . cntGetHumanRating($song->r) . ' <span class="vby">(by: '.$song->ra->attributes()->$attr.')</span></span>';
        echo '<span style="float: right;">';
          echo '<span class="comment"><span style="margin-left: 1em; color: #ffff80;">' . $song->c . '</span><span class="vby"> (by: '.$song->c->attributes()->$attr.')</span></span>';
        echo '</span>';
      echo '</div>';
    }
  } */
?>
</div>
<?php
  echo makeBottom();
  echo makeHTML_end();
