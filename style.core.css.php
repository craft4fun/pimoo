<style>
<?php
  $imprisioned = (isset($_SESSION['bool_imprison']) && $_SESSION['bool_imprison'] == true) ? true : false;

  if (cntIsMobile())
  {
    $isMobile = true;
    $suffix = 'M';
  }
  else
  {
    $isMobile = false;
    $suffix = NULLSTR;
  }

  $displayZoom = (isset($_COOKIE['str_displayZoom'])) ? $_COOKIE['str_displayZoom'] : MEDIUM;

  // -- these basically built variables are also used in bright- and darkmode --
  $admin = isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true;
  $songOmnipres = (isset($_COOKIE['bool_SongOmnipresent']) && $_COOKIE['bool_SongOmnipresent'] == YES) ? true : false;

  // TODO paddy this should be replaced step by step using "root font rem"
  define('SCALE_DOUBLE', 'transform: scale(2); -moz-transform: scale(2); -webkit-transform: scale(2);');
  define('SCALE_ONEANDAHALF', 'transform: scale(1.5); -moz-transform: scale(1.5); -webkit-transform: scale(1.5);');

  define('COL_PURPLE_BRIGHT','#b92e82'); // -- some kind of bright purple --
?>

HTML {
  /* -- ROOT FONT = 10px -- */
  /* font-size: 62.5%; */
  /* -- use in elements later, e.g.: -- */
  /* font-size: 1.2rem; (1.2 * 10px = 12px) */

  <?php
    if (!isset($_COOKIE['str_displayZoom']))
    {
      echo 'font-size: 62.5%;';
    }
    else
    {
      switch ($_COOKIE['str_displayZoom'])
      {
        case LOW:
          echo 'font-size: 50%;';
          break;
        case MEDIUM:
          echo 'font-size: 62.5%;';
          break;
        case HIGH:
          echo 'font-size: 83.3%;';
          break;
      }
    }
  ?>

}

HTML, BODY {
  width: 100%;
  height: 99%;
  margin: 0px;
}

BODY {
  background-repeat: no-repeat;
  background-position: center;
  background-attachment: fixed;

  <?php
    if (!$isMobile)
    {
      // -- ADAPT ROOT FONT TO DEFAULT "16px"... --
      echo 'font-size: 1.6rem;';
    }
    else
    {
      // -- ... ENLARGE FOR MOBILES "32px"--
      echo 'font-size: 3.2rem;';
      //echo 'font-size: xx-large;'; // -- old school --
    }
  ?>

  font-family: 'CustomFont', Arial, sans-serif;
}

SELECT {
  text-align: center;
}


#isDebugTape {
  margin-top: 30px;
  transform: rotate(-40deg);
  -webkit-transform: rotate(-40deg);
  opacity: 0.8;
  z-index: 9001;
  position: absolute;
  text-align: center;
  left: -60px;
  top: 5px;
  width: 240px;
  padding: 4px;
  font-weight: bold;
  color: gray;
  border-style: none;
  background-color: red;
  background: radial-gradient(#FFFFFF, red);
  background: -moz-radial-gradient( #FFFFFF, red);
  background: -webkit-radial-gradient(#FFFFFF, red);
}


#omniPresentSong {
  position: fixed;
  z-index: 1000;

  height: 1.25em;
  width: 100%;
  left: 0px;
  text-align: center;
  font-size: larger;

  /*since overwritten party by song process over time, this is managed in ajax_getCurrentSong */
  /*padding: 0.25em;*/
  color: red;
  text-shadow: 2px 2px 10px red, 1px 1px #000;
}

#adminControlBar {
  position: fixed;
  z-index: 1000;

  <?php
    if ($songOmnipres)
      echo 'top: 2em;';
    else
      echo 'top: 0em;';
  ?>

  width: 100%;
  height: 1.1em;

  padding: 0.5em 0em;
  text-align: center;
}




#contentWrapper {
  position: absolute;
  padding: 0.25em 0.25em 0.25em 0.5em; /*2022-11-04: 0.5 left*/

<?php
  if (!$admin && !$songOmnipres)
  {
    $marginTop = 'margin-top: 0em;';
  }

  if ($admin && !$songOmnipres)
  {
    $marginTop = 'margin-top: 2.5em;';
  }

  if (!$admin && $songOmnipres)
  {
    $marginTop = 'margin-top: 2.5em;';
  }

  if ($admin && $songOmnipres)
  {
    $marginTop = 'margin-top: 4em;';
  }

  echo $marginTop;


  if ($isMobile || $imprisioned)
  {
    /*min-width: 98%;*/  // -- works for smaller smartphones --
    echo 'width: 98%;';  // -- works fine for desktop and tablets --
  }
  else
  {
    // -- new since 2022-Nov: I force the Desktop layout to be less wide --
    echo 'width: 55%;'; // 50 60
    echo 'left: 21%;';  // 23 18
  }
?>
}

/* -- hack for standalone admin area window -- */
@media (max-width: 800px) {
  #contentWrapper {
    width: 98%;
    left: 1%;
  }
}



<?php
  // -- enlarge all input elements, too --
  if ($isMobile)
  {
?>
    input {
      font-size: xx-large;
      min-width: 3em;
      padding: 0.5em 0em 0.5em 0em;
    }

    select {
      font-size: xx-large;
      min-width: 3em;
      padding: 0.5em 0em 0.5em 0em;
    }

<?php
  }
?>




#frame_logo {
  text-align: center;
  width: 100%;
}

#logo {
  <?php
    if ($isMobile)
    {
      if ($displayZoom == LOW)
        echo SCALE_ONEANDAHALF;
      else
        echo SCALE_DOUBLE;
      echo 'margin: 1.5em 0px 1.2em 0px;';
    }
    else
      echo 'margin: 0.5em 0px 0px 0px;';
  ?>
}

#welcomeLogo {
  position: absolute;
  top: 20%;
}

.stretchPic {
  position: relative;
  top: 0;
  left: 0;
  width: 95%;
  height: auto; /*95%*/
}

.CSS3_easeOutLong {
  /* needs document.getElementById("welcomeBG").style.opacity = "0.1"; to change the opacity status */
  opacity: 1;
  transition: opacity <?php echo WELCOMETIMEOUT; ?>ms ease-out;
}

#bottomHelper {
  clear: both;
  position: relative;
  float: none;
  margin: 100px 0px 0px 0px;
}

#frame_bottom {
  clear: both;
  position: relative;
  padding: 1em;
  <?php
    if ($isMobile)
      echo 'margin-top: 1em;';
    else
      echo 'margin-top: 3em;';
  ?>
}


/* the basic template for all messages */
#target_feedback, #StdMsg, #okMsg, #errorMsg {
  position: fixed;
  overflow: auto;
  margin: 0px;
  padding: 0.5em;
  z-index: 1000;
  cursor: default;

  transition: all 0.5s;

  border-radius: 10px;

  <?php
    if (!$isMobile)
    {
  ?>
    margin-right: 5%;
    top: 2em;
    left: 60%;
    /*max-width: 35%*/
  <?php
    }
    else
    {
      //margin: 0.75em 0.25em 0.25em 0.25em;
  ?>
    margin: 3em 0.3em 0.25em 0.25em;
    font-size: 2em;
    max-width: 90%;

    /* comes by javascript anywhere in "onload"
    height: auto; // pseudo screen.height -> height: 75%

    top: 0.1em;
    left: 0.1em;
    */
  <?php
    }
  ?>
}


/* belongs to and called with php->cntStdMsg() and js->quit_StdMsg() */
#StdMsg {
  z-index: 1001; /* irrelevant yet */
}

/* belongs to and called with php->cntOkMsg() */
#okMsg {
  z-index: 1002;  /* irrelevant yet */
  border-color: green;
  box-shadow: 3px 3px 3px green,
    -3px 3px 3px lime,
    3px -3px 3px green,
    -3px -3px 3px silver;
}

/* belongs to and called with php->cntErrMsg() */
#errorMsg {
  z-index: 1003;  /* irrelevant yet */
  border-color: red;
  box-shadow: 3px 3px 3px red,
    -3px 3px 3px #cf2b3e,
    3px -3px 3px red,
    -3px -3px 3px silver;
}

/* to have a failure color inside the feedback bubble when created via ajax */
.msgAddFail{
  color: red;
}


.takenNicknameArea {
  position: fixed;
  z-index: 1010; /*more than okMsg and so on: 1001 - 1003*/
  top: 75%;

  padding: 0.5em 0em 1em 0em;
  border-color: <?php echo COL_PURPLE_BRIGHT; ?>;
  box-shadow: 3px 3px 3px <?php echo COL_PURPLE_BRIGHT; ?>,
    -3px 3px 3px red,
    3px -3px 3px <?php echo COL_PURPLE_BRIGHT; ?>,
    -3px -3px 3px orange;

}
.takenNicknames {
  float: left;
  margin: 0em 1em 1em 1em;
}


.commonHead {
  padding: 0.75em;
  border-bottom: solid;
  border-bottom-width: 2px;
  border-bottom-color: gray;
}

.commonHeadText {
  text-align: center;
  font-weight: bold;
  text-shadow: 0px 0px 1px #808080; /* blur effect */
}

#frame_navi {
  position: static;
  float: left;
  width: 98%;
  padding: 0.25em;

  <?php
    if ($isMobile)
    {
      echo 'margin-top: 0.5em;';
    }
  ?>

  border-radius: 3px;
  /* will be set visible after intro */
  visibility: hidden;

}



#frame_hulk {
  padding-top: 1em;
}


/*this could be useful if text-align: center does not work*/
.centerAlignedBlock {
  margin-left: auto;
  margin-right: auto;
  width: inherit;
  /* must be possibly set at the individual place ("width: inherit" does not work always) */
  /*e.g. width: 250px;*/
  /*or   width: 75%;*/
}


#searchResultTable {
  padding-top: 1em;
  float: left;
  width: 99%;
  cursor: default;
  margin-left: 0.2em;
  <?php
    if ($isMobile)
    {
      echo 'font-size: smaller;';
    }
  ?>
}

#dlg_pleaseWait {
  text-align: center;
  margin: 1em;
  padding: 0.5em;
  cursor: default;
  border-radius: 5px;
}


.menuButton {
  border: none;
  background-color: transparent;
  font-size: larger;
}



/* -- very tricky stuff here :o) -- */

#searchbarGrid {
  margin: 0em 2em 0.2em 0.2em;
<?php if (!$isMobile) { ?>
  float: left; /* useful in desktop mode */
<?php } if ($displayZoom != HIGH) { ?>
  white-space: nowrap;
<?php } ?>
}

#btn_songSearchClean {
  background-color: transparent;
  /* border: none; removed 2022-12-02 */
  margin-right: 1em;
}
#edt_songSearch {
  /*z-index: 500*/
  margin-left: -1em;
}
#btn_songSearchGo {
  margin-right: 0px;
  <?php
    if ($isMobile)
    {
      echo 'padding: 0.5em 0em 0.5em 0px;';
      echo NULLSTR; // margin-left: -0.2em;
    }
    else
      echo NULLSTR; // margin-left: -0.3em;
  ?>
}

#onAirTitle, #onAirWaiting {
  float: left;
  margin: 0px 10px 0px 0px;
  padding: 0.25em;
  width: 50%;
  border-radius: 5px;

<?php
  if ($isMobile)
  {
    echo 'margin-left: 20%;';
    echo 'font-size: 1.5em;'; //  xx-large;
  }
  else
  {
    echo 'margin-left: 22%;';
    echo 'font-size: x-large;';
  }
?>

}


.modernCenterBackground {
  float: left;
  width: 95%;
  text-align: center;
  margin: 0px 10px 0px 10px;
  padding: 5px;
  font-size: larger;
  border-radius: 5px;
}


.resultNavi {
  position: relative;
  <?php
    if ($isMobile)
    {
      echo 'margin: 150px 20px 20px 20px;';
      echo 'font-size: 1.5em';
    }
    else
    {
      echo 'margin: 75px 10px 10px 10px;';
    }
  ?>
}


#songCount {
  position: absolute;
  left: 0.5em;
  <?php
    if ($isMobile)
    {
      echo 'top: 6em;';
    }
    else
    {
      echo 'top: 6em;'; // -- chrome works well with: 7em; Firefox needs less --
    }
  ?>
  border-radius: 2px;
}



<?php
  // -- ------------------------ --
  // -- begin of mediaBase group --
  // -- ------------------------ --
?>

.grpLstContX {
  float: left;

  <?php
    if ($isMobile)
    {
      echo 'margin: 0.3em;';
      echo 'font-size: 1.25em';
    }
    else
    {
      echo 'margin: 1em;';
    }
  ?>
}



.grpLstContL {
  margin: 1em 2em 1em 0em;
  padding: 0.75em;

  border-radius: 10px;
  transition: all 0.5s;
  text-decoration: none;
}

.grpLstItmL {
  text-decoration: none;
  color: black;
  cursor: default;
}

.grpLstContR {
  float: right;
  margin: 0.3em 0.25em 0em 0em;
  padding: 0.2em 0.2em 0em 0.2em; /*padding bottom 0em is a ugly fix for a larger space below the picture for any other reason*/
  min-width: 1.5em;
  /*min-height: 1.5em;*/
  transition: all 0.5s;
  border-radius: 3px;
}

.grpLstItmR {
  text-decoration: none;
  color: black;
  cursor: default;
}


.tableContent {
  padding: 0.5em 0.25em 0.5em 0.25em;
  <?php
    if ($isMobile)
      echo 'font-size: 1.2em;';
  ?>
}

.tableContentRight {
  <?php
    if (!$isMobile)
      echo 'font-size: smaller;';
    else
      echo 'font-size: 1em;';
  ?>
}

/* Sortable tables */
table.sortable thead {
  font-weight: bold;
  cursor: default;
  height: 2.5em;
}

<?php
  // -- ---------------------- --
  // -- end of mediaBase group --
  // -- ---------------------- --



  // -- ----------------- --
  // -- begin of playlist --
  // -- ----------------- --
?>

.playListRowL {
  margin-top: 2em;
  padding: 0em 0.5em 0.5em 0.5em;

  position: static;
  float: left;
    <?php
      if (!$isMobile)
        echo 'width: 98%;';
      else
        echo 'width: 96%;';
    ?>

  transition: all 0.5s;
  border-radius: 3px;
}


.playListItemL {
  text-decoration: none;
  cursor: default;
  /* -- moved to bright- and darkmode --
  color: black; */
}

.playListRowR {
  float: right;
  margin: 10px 0px 0px 0px;
  padding: 5px;

  background-color: #fff79e;
  background: radial-gradient(#fff79e, silver);
  background: -moz-radial-gradient(#fff79e, silver);
  background: -webkit-radial-gradient(#fff79e, silver);

  min-width: 2em;
  min-height: 1.5em;

  transition: all 0.5s;

  box-shadow: 3px 3px 3px silver,
    -3px 3px 3px silver,
    3px -3px 3px silver,
    -3px -3px 3px silver;

  border-radius: 5px;
}
.playListRowR:HOVER {
  box-shadow: 6px 6px 6px #fff79e,
    -6px 6px 6px silver,
    6px -6px 6px silver,
    -6px -6px 6px #fff79e;
}

.playListItemR {
  text-decoration: none;
  cursor: default;
}


.plInfoRow{
  font-style: italic;
  cursor: default;
  text-align: center;
}


.transAllEeaseFast{
  transition: all 0.25s ease;
}
  .transAllEeaseMedium{
  transition: all 0.5s ease;
}
.transAllEeaseSlow{
  transition: all 1.5s ease;
}


#ctrlVolumeDown {
  margin-left: 0em;
  cursor: default;
}
#ctrlVolumeUp {
  margin-left: 0.25em;
  cursor: default;
}

#ctrlRewind {
  margin-left: 0.75em;
  cursor: default;
}
#ctrlForward {
  margin-left: 0.25em;
  cursor: default;
}

#ctrlNext {
  margin-left: 0.75em;
  cursor: default;
}

#ctrlPlay {
  margin-left: 0.75em;
  cursor: default;
}
#ctrlPause {
  margin-left: 0.25em;
  cursor: default;
}





.kill {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 20px;';
    else
      echo 'padding: 10px;';
  ?>
  cursor: default;
  background-image: url("res/kill<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.stashPush {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowRight<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.stashPop {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowLeft<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.comment {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 30px;';
    else
      echo 'padding: 15px 0px 0px 0px;';
  ?>
  cursor: default;
  background-image: url("res/comment<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;

  }

.rating {
  margin-left: 1em;
  margin-bottom: 1em;
  background-color: transparent;
  border-radius: 3px;

  border-style: solid;
  border-width: 1px;
  border-left-color: gray;
  border-top-color: gray;
  border-right-color: black;
  border-bottom-color: black;


  <?php
    if ($isMobile)
      echo 'padding: 10px;';
    else
      echo 'padding: 5px;';
  ?>
}



.arrowDown {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowDown<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.arrowUp {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowUp<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}

.arrowUpTop {
  margin-left: 1em;
  <?php
    if ($isMobile)
      echo 'padding: 22px;';
    else
      echo 'padding: 11px;';
  ?>
  cursor: default;
  background-image: url("res/arrowUpTop<?php echo $suffix; ?>.png");
  background-repeat: no-repeat;
}



<?php
  // -- --------------- --
  // -- end of playlist --
  // -- --------------- --
?>



#toTopLink {
  position: fixed;
  z-index: 100001; /*that is realy strong, but to be sure it is also created at the bottom of the body*/
  text-decoration: none;
  font-weight: bold;
  top: 85%;
  opacity: 0.25;
  transition: opacity 0.5s ease-in-out;

  <?php if (!$isMobile) { ?>
    /* -- left aligned -- */
    left: 0px;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  <?php } else { ?>
    /* -- left aligned -- */
    right: 0px;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
  <?php } ?>

  padding: 1em;
  height: 1em;
  width: 0.8em;

  color: white;
  background-color: gray;

}
#toTopLink:HOVER {
  opacity: 0.75;
}


#hitlist {
  clear: both;
  position: relative;
  padding-top: 3em;
}

#hitlistHeader {
  text-align: center;

  font-size: larger;
  color: red;
  text-shadow: 3px 3px 10px red;
}



.watermarkArea {
/*   position: absolute; */
/*   display: block; */
/*   min-height: 50%; */
/*   min-width: 50%; */
/*   z-index: 0; */
}
.watermarkCover {
  width: 90%;
  text-align: center;
  position: absolute;
  line-height: 99%;
  pointer-events: none;
}
.watermarkBgText{
  z-index: -1;

  font-family: Arial, Helvetica, sans-serif;
  font-size: 5em;
  font-weight: bold;
}



.textShadowBlur {
  text-shadow: 0px 0px 1px #808080;
}

.boxShadow {
  box-shadow: 3px 3px 3px grey,
     3px 3px 3px grey,
     3px 3px 3px grey,
     3px 3px 3px grey;
}

.explainText {
  color: gray;
  font-size: smaller;
  margin: 0.5em;
}


.watermark {
  text-align: center;
  float: right;
  margin-right: 1em;
  background-clip: text;
  -webkit-background-clip: text;
  -moz-background-clip: text;
  color: transparent;
}

.link {
<?php if ($isMobile) { ?>
  font-size: 4rem;
<?php } ?>
}


/* an optical nice link */
.linkNice {
<?php if ($isMobile) { ?>
  font-size: 4rem;
<?php } ?>
}

/* an optical nice link for navigation */
.linkNaviNice {
<?php if ($isMobile) { ?>
  font-size: 4rem;
<?php } ?>
}

.selectNice {
  color: grey;
  text-shadow: 1px 1px 5px #00bf0b, 1px 1px #000;
}

.textNice {
  color: grey;
  text-shadow: 1px 1px 5px #00bf0b, 1px 1px #000;
}


.btnBlue, .btnRed, .btnGreen, .btnGray, .btnYellow {
  color: white;
}
.btnBlue {
  background-color: blue;
  background: radial-gradient(blue, aqua);
  background: -moz-radial-gradient(blue, aqua);
  background: -webkit-radial-gradient(blue, aqua);
}
.btnBlue:hover {
  background: radial-gradient(aqua, blue);
  background: -moz-radial-gradient(aqua, blue);
  background: -webkit-radial-gradient(aqua, blue);
}
.btnRed {
  background-color: red;
  background: radial-gradient(red, orange);
  background: -moz-radial-gradient(red, orange);
  background: -webkit-radial-gradient(red, orange);
}
.btnRed:hover {
  background: radial-gradient(orange, red);
  background: -moz-radial-gradient(orange, red);
  background: -webkit-radial-gradient(orange, red);
}
.btnGreen {
  background-color: green;
  background: radial-gradient(green, lime);
  background: -moz-radial-gradient(green, lime);
  background: -webkit-radial-gradient(green, lime);
}
.btnGreen:hover {
  background: radial-gradient(lime, green);
  background: -moz-radial-gradient(lime, green);
  background: -webkit-radial-gradient(lime, green);
}
.btnYellow {
  background-color: yellow;
  background: radial-gradient(orange, yellow);
  background: -moz-radial-gradient(orange, yellow);
  background: -webkit-radial-gradient(orange, yellow);
}
.btnYellow:hover {
  background: radial-gradient(yellow, orange);
  background: -moz-radial-gradient(yellow, orange);
  background: -webkit-radial-gradient(yellow, orange);
}

.myMooOption, .adminOption {
  float: right;
  border-style: solid;
  border-width: 1px;
  padding: 0.2em;
  border-radius: 5px;
  font-size: 1.2em;
}
.adminOption {
  height: 2.2em;
  background-color: silver;
  color: white;
}

.pleaseWaitAdmin {
<?php
  if ($isMobile)
  {
    //echo SCALE_DOUBLE;
    echo 'width: 300px;';
    echo 'height: 50px;';
  }
?>
}

.uploadFile {
<?php
  if ($isMobile)
  {
    echo 'max-width: 8em;';
  }
  else
  {
    echo 'max-width: 9em;';
  }
?>
}

.adminBoxOuter {
  margin-top: 3em;
}

.adminBoxLabel {
  margin-left: 1em;
  text-transform: uppercase;
  font-style: italic;
}

.adminBoxInner, .docuBoxInner {
  padding: 1em;
  border-radius: 5px;
  border-style: solid;
  border-color: gray;
}


#backend_login{
  border-color: brown;
}


.dockerDisabled {
  text-decoration: line-through;
}

.blinking {
  animation: blinker 1.5s cubic-bezier(.5, 0, 1, 1) infinite alternate;
}
@keyframes blinker { to { color: red; } }

/*
  column grid - Spaltensatz - must be used this way:

  <div class="columnGrid02">
    <img src="dummy.png" alt="dummy" />
  </div>

  <div class="columnGrid02">
    <img src="dummy.png" alt="dummy" />
  </div>

*/

/* column grid - Spaltensatz */
.columnGrid02 {
  width: 50%;
  float: left;
  text-align: center;
}
.columnGrid03 {
  width: 33.3%;
  float: left;
  text-align: center;
}
.columnGrid04 {
  width: 25%;
  float: left;
  text-align: center;
}
.columnGrid05 {
  width: 20%;
  float: left;
  text-align: center;
}

/* -- modern column grid -- */
.columnGridFlex {
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
}

</style>
