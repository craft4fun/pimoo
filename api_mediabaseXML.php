<?php

  /**
   * class_mediaBase XML based
   * XML -> optimized for fast queries (but larger mediaBase data)
   * @author paddy
   */
  class class_mediaBase implements interface_mediabase
  {
    private $depth = NULLSTR;
    private $mediabaseFile = NULLSTR;
    private $mediaBaseXML;

    // -- FEEDBACK CLONE --
    // -- for writing --
    private $curSong = NULLSTR;
    private $curSongFound = false;
    private $lastMsg = NULLSTR;

    private $genreWhitelist;
    private $genreWhitelistActive;
    private $genreBlacklist;

    private $rateWhitelist;
    private $rateWhitelistActive;
    private $rateBlacklist;


    function __construct($aDepth = NULLSTR)
    {
      global $cfg_mediabaseFile;
      global $cfg_genreWhitelist, $cfg_genreBlacklist;
      global $cfg_rateWhitelist, $cfg_rateBlacklist;

      $this->depth = $aDepth;
      $this->mediabaseFile = $this->depth . $cfg_mediabaseFile;

      if (file_exists($this->mediabaseFile))
      {
        $this->mediaBaseXML = simplexml_load_file($this->mediabaseFile);
      }
      else
      {
        //echo ('<span style="color: red;">' . $this->mediabaseFile . ' is not available, nothing more to do...</span>');
        echo ('<span style="color: red;">' . $this->mediabaseFile . ' is not available, please import your songs with the <span style="color: blue; font-weight: bold;">blue button</span>...</span>');
      }

      // -- for reading --
      $this->findSongReset();

      $this->genreWhitelist = $cfg_genreWhitelist;
      $this->genreWhitelistActive = (count($this->genreWhitelist) > 0 && trim($this->genreWhitelist[0]) != NULLSTR);
      $this->genreBlacklist = $cfg_genreBlacklist;

      // -- recommendaction by nane --
      $this->rateWhitelist = $cfg_rateWhitelist;
      $this->rateWhitelistActive = (count($this->rateWhitelist) > 0 && trim($this->rateWhitelist[0]) != NULLSTR);
      $this->rateBlacklist = $cfg_rateBlacklist;
    }




    /**
     * get last message
     * @see interface_mediabase::getLastMsg()
     */
    function getLastMsg()
    {
      return $this->lastMsg;
    }



    /**
     * generates an array relating to the given search pattern (search)
     * -- DO NOT CHANGE IN HECTIC RUSH (almost the same procedure like in getSongInfo but with less code) --
     * $aFull for full informations
     * $aOffset for beginning result at
     * $aLimit limits the results
     * $aCaseSensitive boolean
     * @param a char as string
     * @return array
     */
    function getSearchResult($aSeachStr, $aOffset = 0, $aLimit = 0, $aDL_level = false, $aCaseSensitive = false)
    {
      global $cfg_minimumSearchChars;

      $result = array();

      $limitCount = $aOffset;
      $refCounter = 0;

      if (strlen($aSeachStr) >= $cfg_minimumSearchChars)
      {
        if ($this->mediaBaseXML)
        {
          $soPatLen = strlen(SO_INTERPRET);
          $checkSearchParam = substr($aSeachStr, 0, $soPatLen);
          switch ($checkSearchParam)
          {
            case SO_INTERPRET:
            case SO_TITLE:
            case SO_ALBUM:
            case SO_GENRE:
            case SO_COMMENT:
            {
              $seachStr = substr($aSeachStr, $soPatLen);
              break;
            }
            default:
            {
              $seachStr = $aSeachStr;
            }
          }


          // -- search in mediabase ONLY --
          $attr = XML_BY;
          foreach($this->mediaBaseXML->i as $item)
          {
            if ($aLimit > 0 && $limitCount >= $aLimit + $aOffset)
            {
              break;
            }

            // -- build string in which is being searched at depending on special search parameters --
            switch ($checkSearchParam)
            {
              case SO_INTERPRET:
              {
                $songHulk = ($item->i)
                . BLANK . ($item->r); // -- in addition also rating which is addressed with e.g. ~3~ --
                break;
              }
              case SO_TITLE:
              {
                $songHulk = ($item->t)
                . BLANK . ($item->r); // -- in addition also rating which is addressed with e.g. ~3~ --
                break;
              }
              case SO_ALBUM:
              {
                $songHulk = ($item->l)
                . BLANK . ($item->r); // -- in addition also rating which is addressed with e.g. ~3~ --
                break;
              }
              case SO_GENRE:
                {
                  $songHulk = ($item->g)
                  . BLANK . ($item->r); // -- in addition also rating which is addressed with e.g. ~3~ --
                  break;
                }
              case SO_COMMENT:
                {
                  $songHulk = ($item->c)
                  . BLANK . ($item->r); // -- in addition also rating which is addressed with e.g. ~3~ --
                  break;
                }
              // -- default is searching in all fields --
              default:
              {
                $songHulk = ($item->i) . BLANK . ($item->t) . BLANK . ($item->l) . BLANK .
                ($item->g) . BLANK . ($item->y) . BLANK .
                ($item->r) . BLANK . ($item->r->attributes()->$attr) . BLANK .
                ($item->c) . BLANK . ($item->c->attributes()->$attr);
              }
            }


            // -- -------------------------------- --
            // -- begin of the heart of the search --
            $songHulkDecoded = html_entity_decode($songHulk, ENT_QUOTES | ENT_IGNORE, 'UTF-8');
            //$check = stripos($songHulkDecoded , cntTrim($aSeachStr));
            //$caseSensitive = (!isset($_COOKIE['bool_caseSensitive']) || $_COOKIE['bool_caseSensitive'] != YES) ? FNM_CASEFOLD : NULLINT;
            $caseSensitive = ($aCaseSensitive) ? NULLINT : FNM_CASEFOLD;
            $check = (fnmatch('*' . cntTrim($seachStr) . '*', $songHulkDecoded, $caseSensitive)); // -- a differentation is required because of cache handling  --
            // -- end of the heart of the search --
            // -- -------------------------------- --


            // -- -------------------------------------------------------------------------------------- --
            // -- begin of optionally with damerau levenshtein distance to make the search more tolerant --
            if ($aDL_level > 0)
            {
              $DL_in = levenshtein($seachStr, html_entity_decode($item->i, ENT_QUOTES | ENT_IGNORE, 'UTF-8'));
              $DL_ti = levenshtein($seachStr, html_entity_decode($item->t, ENT_QUOTES | ENT_IGNORE, 'UTF-8'));
              $DL_al = levenshtein($seachStr, html_entity_decode($item->l, ENT_QUOTES | ENT_IGNORE, 'UTF-8'));
              $DL_ge = levenshtein($seachStr, html_entity_decode($item->g, ENT_QUOTES | ENT_IGNORE, 'UTF-8'));

              if ($DL_in <= $aDL_level || $DL_ti <= $aDL_level || $DL_al <= $aDL_level || $DL_ge <= $aDL_level)
              {
                $check = true;
              }
            }
            // -- end of optionally with damerau levenshtein distance to make the search more tolerant --
            // -- ------------------------------------------------------------------------------------ --


            if ($check !== false)
            {
              $refCounter++;
              // -- go on when counter is greater than offset --
              if ($refCounter <= $aOffset)
              {
                continue;
              }

              // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
              if (   !in_array($item->g, $this->genreBlacklist)
                  && (!$this->genreWhitelistActive || in_array($item->g, $this->genreWhitelist))
                )
              {
                $subArray = array();
                $subArray['a'] = $item->a;
                $subArray['i'] = $item->i;
                $subArray['t'] = $item->t;
                $subArray['p'] = $item->p;
                // -- additional data (for table and iMoo) --
                $subArray['l'] = $item->l;
                $subArray['r'] = $item->r;
                $subArray['c'] = $item->c;
                $result[] = $subArray;
                // unset($subArray); faster without that
              }

              $limitCount++;
            }
          }
        }
      }

      return $result;
    }


    /**
     * generates an array relating to the given search pattern (search)
     * it is searched for a user only. Not in the whole search string (Album, Title etc...)
     * $aFull for full informations
     * $aOffset for beginning result at
     * $aLimit limits the results
     * $aCaseSensitive boolean
     * @param a char as string
     * @return array
     */
    function getSearchResultByUser($aUser, $aOffset = 0, $aLimit = 0, $aCaseSensitive = false)
    {
      global $cfg_minimumSearchChars;

      $limitCount = $aOffset;
      $refCounter = 0;
      $result = array();

      if (strlen($aUser) >= $cfg_minimumSearchChars)
      {
        if ($this->mediaBaseXML)
        {
          $attr = XML_BY;
          foreach($this->mediaBaseXML->i as $item)
          {
            if ($aLimit > 0 && $limitCount >= $aLimit + $aOffset)
              break;

            // -- search in mediabase --
            $songHulk = ($item->r->attributes()->$attr) . BLANK . ($item->c->attributes()->$attr);

            // -- -------------------------------- --
            // -- begin of the heart of the search --
            $songHulkDecoded = html_entity_decode($songHulk, ENT_QUOTES | ENT_IGNORE, 'UTF-8');

            // -- old: the name MUST match completely --
            //$check = stripos($songHulkDecoded , cntTrim($aUser));

            // -- with wildcard like the normal search -> better, so one can found something like Tobi*  (as) --
            //$caseSensitive = (!isset($_COOKIE['bool_caseSensitive']) || $_COOKIE['bool_caseSensitive'] != YES) ? FNM_CASEFOLD : NULLINT;
            $caseSensitive = ($aCaseSensitive) ? NULLINT : FNM_CASEFOLD;
            $check = (fnmatch('*' . cntTrim($aUser) . '*', $songHulkDecoded, $caseSensitive)); // -- a differentation is required because of cache handling  --
            // -- end of the heart of the search --
            // -- -------------------------------- --

            if ($check !== false)
            {
              $refCounter++;
              // -- go on when counter is greater than offset --
              if ($refCounter <= $aOffset)
                continue;

              // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
              if (   !in_array($item->g, $this->genreBlacklist)
                  && (!$this->genreWhitelistActive || in_array($item->g, $this->genreWhitelist))
                )
              {
                $subArray = array();
                $subArray['a'] = $item->a;
                $subArray['i'] = $item->i;
                $subArray['t'] = $item->t;
                $subArray['p'] = $item->p;
                // -- additional data (for table and iMoo) --
                $subArray['l'] = $item->l;
                $subArray['r'] = $item->r;
                $subArray['c'] = $item->c;
                $result[] = $subArray;
                // unset($subArray); faster without that
              }
              $limitCount++;
            }
          }
        }
      }
      return $result;
    }



    /**
     * generates an array with songs in between two dates
     * step width: days
     * $aDate1 = past
     * $aDate2 = today
     * @params $aDate1, $aDate2 -> date('Ymd') -> e.g. 20060906
     * @return array
     */
    function getSongsByDate($aDate1, $aDate2, $aOffset = 0, $aLimit = 0)
    {
      $result = array();

      $limitCount = $aOffset;
      $refCounter = 0;

      $past = intval($aDate1);
      $today = intval($aDate2);

      if ($this->mediaBaseXML)
      {
        foreach($this->mediaBaseXML->i as $item)
        {
          if ($aLimit > 0 && $limitCount >= $aLimit + $aOffset)
            break;

          // $fileDate: <fd>2007-07-21 10:49:14</fd> -> 20070721
          // $aDate1: 20070601
          // $aDate2: 20070801

          $year  = substr($item->d, 0, 4);
          $month = substr($item->d, 5, 2);
          $day   = substr($item->d, 8, 2);
          $fileDate = intval($year.$month.$day); // -- e.g. 20070701 --

          // -- file date as seconds now --
          //$fileDate = mktime(0, 0, 0, $month, $day, $year);
          //echo LFH."past: $past" . LFH;
          //echo "file: $fileDate" . LFH;
          //echo "today: $today" . LFH;

          if ($fileDate >= $past && $fileDate <= $today) // -- this works also for something like: get 90's --
          {
            $refCounter++;
            if ($refCounter <= $aOffset) // -- go on when counter is greater than offset --
              continue;

            // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
            if (   !in_array($item->g, $this->genreBlacklist)
                && (!$this->genreWhitelistActive || in_array($item->g, $this->genreWhitelist))
              )
            {
              $subArray = array();
              $subArray['a'] = $item->a;
              $subArray['i'] = $item->i;
              $subArray['t'] = $item->t;
              $subArray['p'] = $item->p;
              // -- additional data --
              $subArray['l'] = $item->l;
              $subArray['r'] = $item->r;
              $subArray['c'] = $item->c;
              // -- that was causer of all problems with files by date - the filedate as index: --
              // -- that can be double of course!!! when songs have the same date!!! --
              //$result[$fileDate] = $subArray; // $fileDate required to sort by newest --
              $result[] = $subArray;
              // unset($subArray); faster without that
            }

            $limitCount++;
          }
        }
        // -- sort by newest --
        //krsort($result); // -- sort by key reverse --
      }

      return $result;
    }



    /**
     * generates an array relating to the given random count (e.g.10)
     * usually used by the gui
     * @param integer $number
     * @return array
     */
    function getRandomSongs($number)
    {
      $result = array();

      $check = array(); // -- avoid double delivery --
      if ($this->mediaBaseXML)
      {
        $count = count($this->mediaBaseXML->i);
        $max = ($count > $number) ? $number : $count;
        // -- emergency counter --
        $ec = 0;
        // -- emergency limit: give up after this count of takes and deliver any song --
        $el = util_calcMaxRetryCounter($count); //$el = $count * 10;
        while (count($result) < $max && $ec < $el)
        {
          $rand = mt_rand(0, $count - 1);
          $curGenre = $this->mediaBaseXML->i[$rand]->g;
          if (
              // -- deliver no song which is already in results --
                 (!in_array($curGenre, $check))

              // -- allow only genres in whitelist (if active) and filter out genres of blacklist --
              && (!in_array($curGenre, $this->genreBlacklist))
              && (!$this->genreWhitelistActive || in_array($curGenre, $this->genreWhitelist))
            )
          {
            $subArray = array();
            $subArray['a'] = $this->mediaBaseXML->i[$rand]->a;
            $subArray['i'] = $this->mediaBaseXML->i[$rand]->i;
            $subArray['t'] = $this->mediaBaseXML->i[$rand]->t;
            $subArray['p'] = $this->mediaBaseXML->i[$rand]->p;
            // -- additional data (for table and iMoo) --
            $subArray['l'] = $this->mediaBaseXML->i[$rand]->l;
            $subArray['r'] = $this->mediaBaseXML->i[$rand]->r;
            $subArray['c'] = $this->mediaBaseXML->i[$rand]->c;
            $result[] = $subArray;
            // unset($subArray); faster without that
            $check[] = $this->mediaBaseXML->i[$rand]->a;
          }
          $ec++;
        }
      }

      return $result;
    }



    /**
     * returns a random song
     * usually used only by api_playlist for cli_player.php
     * IF WHITELIST IS SET, IT IS TRIED SEVERAL TIMES TO PICK THE RIGHT ONES, BUT IT CANNOT BE GUARANTEED!
     * @return multitype:NULL
     */
    function getRandomSong()
    {
      $result = array();

      if ($this->mediaBaseXML)
      {
        $count = count($this->mediaBaseXML->i);
        // -- create rundom number --
        $rand = mt_rand(0, $count - 1); // -- old: srand(microtime() * $count); $rand = rand(0, $count - 1);
        $ec = 0; // -- emergency counter --
        // -- emergency limit, give up after this count of takes and deliver any song --
        $el = util_calcMaxRetryCounter($count); //$el = $count * 10;
        while ($ec < $el && (

              // -- allow only genres in whitelist and filter out genres of blacklist --
                 in_array($this->mediaBaseXML->i[$rand]->g, $this->genreBlacklist)
              || ($this->genreWhitelistActive && !in_array($this->mediaBaseXML->i[$rand]->g, $this->genreWhitelist))

              // -- And as a very unique special here (recommendaction by nane),  --
              // -- try to find only well rated songs! --
              || in_array($this->mediaBaseXML->i[$rand]->r, $this->rateBlacklist)
              || ($this->rateWhitelistActive && !in_array($this->mediaBaseXML->i[$rand]->r, $this->rateWhitelist))
            )
          )
        {
          // -- create rundom number --
          $rand = mt_rand(0, $count - 1);
          $ec++;
        }

        //$result['a'] = base64_decode( $this->mediaBaseXML->i[$rand]->a );
        $result['a'] = $this->mediaBaseXML->i[$rand]->a;
        $result['i'] = $this->mediaBaseXML->i[$rand]->i;
        $result['t'] = $this->mediaBaseXML->i[$rand]->t;
        $result['p'] = $this->mediaBaseXML->i[$rand]->p;
        $result['l'] = $this->mediaBaseXML->i[$rand]->l; // -- additional data (for tablePlus and iMoo) --
      }

      // -- debug --
      // echo 'RATING: '.$this->mediaBaseXML->i[$rand]->r;
      // echo 'GENRE: '.$this->mediaBaseXML->i[$rand]->g;


      return $result;
    }





    function getTotalSongCount()
    {
      $result = QUESTMARK;
      if (@$this->mediaBaseXML)
        $result = count($this->mediaBaseXML->i);
      return $result;
    }


    /**
     * Song information from mediabase.xml with comment and rating from feedback.xml (and if not found, older comment and rating from mediabase.xml, too)
     * @param string $aSong -> pure song filename WITHOUT path (even without the intern mediabase path)
     * @param boolean $aFirstHit -> if true, the first hit is taken, no matter in which sub-path it is found (useful in some cases e.g. valuedSong.php were a song must be found without intern mediabase path)
     * @return array with song info content and if cannot find the song false
     */
    function getSongInfo($aSong, $aFirstHit = false)
    {
      $attr = XML_BY;
      $found = false;
      $result = array();
      if ($this->mediaBaseXML)
      {
        //$myFeeback = new class_feedback();
        foreach($this->mediaBaseXML->i as $item)
        {
          $checkRef = ($aFirstHit) ? basename($item->a) : $item->a;
          if ($checkRef == $aSong)
          {
            //$myFeeback->findSong( ( basename($item->a) ) ); // htmlspecialchars
            $result['a'] = $item->a; // -- absolute --
            $result['i'] = $item->i; // -- interpret --
            $result['t'] = $item->t; // -- title --
            $result['p'] = $item->p; // -- playtime --
            $result['l'] = $item->l; // -- album --
            $result['g'] = $item->g; // -- genre --
            $result['y'] = $item->y; // -- year --
            $result['d'] = $item->d; // -- file date --


// -- now the mediabase value is as new as the feedback.xml entry --
// -- it is not required now, that the feedback.xml is opened --

//             if ($myFeeback->getRating() != NULLSTR)
//             {
//               // -- prio 1 feedback --
//               $subArray['r'] = $myFeeback->getRating();
//               $subArray['r2'] = $myFeeback->getRatingBy();
//             }
//             else
            {
              // -- prio 2 mediabase (previously set) --
              $result['r'] = $item->r;
              $result['rby'] = $item->r->attributes()->$attr;
            }

//             if ($myFeeback->getComment() != NULLSTR)
//             {
//               // -- prio 1 feedback --
//               $subArray['c'] = $myFeeback->getComment();
//               $subArray['c2'] = $myFeeback->getCommentBy();
//             }
//             else
            {
              // -- prio 2 mediabase (previously set) --
              $result['c'] = $item->c;
              $result['cby'] = $item->c->attributes()->$attr;
            }

             // -- end of the feedback handling --

            //$myFeeback->findSongReset();
            $found = true;
            break;
          }
        }
        //unset($myFeeback);
      }
      return ($found) ? $result : $found;
    }








    // -- FEEDBACK CLONE ------------------------------------- --
    // -- FEEDBACK CLONE - But the master is the feedback.xml! --
    // -- FEEDBACK CLONE ------------------------------------- --


    /**
     *
     * {@inheritDoc}
     * @see interface_mediabase::getVotedSongs()
     */
    function getVotedSongs($aOffset = 0, $aLimit = 0)
    {
      $result = array();

      $limitCount = $aOffset;
      $refCounter = 0;

      $attr = XML_BY;
      if ($this->mediaBaseXML)
      {
        foreach($this->mediaBaseXML->i as $item)
        {
          if ($aLimit > 0 && $limitCount >= $aLimit + $aOffset)
            break;

          // -- if one of these are set, show the feedback --
          // -- the feedback clone in mediabse is used --
          // -- this is very faster than the old style with feedback and mediabase  --

          if (trim($item->r) != NULLSTR OR  trim($item->c) != NULLSTR)
          {
            $refCounter++;
            if ($refCounter <= $aOffset) // -- go on when counter is greater than offset --
              continue;

            $subArray = array();

            $subArray['a'] = $item->a;

            $subArray['i'] = $item->i; // -- interpret --
            $subArray['t'] = $item->t; // -- title --
            $subArray['p'] = $item->p; // -- playtime --
            $subArray['l'] = $item->l; // -- album --
            $subArray['g'] = $item->g; // -- genre --
            $subArray['y'] = $item->y; // -- year --
            $subArray['d'] = $item->d; // -- file date --

            $subArray['r'] = $item->r;
            $subArray['rby'] = $item->r->attributes()->$attr;
            $subArray['c'] = $item->c;
            $subArray['cby'] = $item->c->attributes()->$attr;

            $result[] = $subArray;

            $limitCount++;
          }
        }
      }

      return $result;
    }





    // -- --------------------------------- --
    // -- begin of save a comment or rating --


    /**
     * THIS IS ALWAYS A CLONE OF THE FEEDBACK STORED VALUE
     * set the song which should be commented or rated
     * if it could not be found a new enty will be made
     * store $i in $this->curSongFound if it has been found, false if it has been created
     */
    function setSong($aSong)
    {
      //$result = false;
      if (CFG_DEBUG)
        echo eventLog('DEBUG aSong: '.$aSong, $this->depth);

      $this->curSongFound = false; // -- if it could not be found, create a new entry in the following functions "setComment and/or setRating" --
      $this->curSong = $aSong;
      if ($this->mediaBaseXML)
      {
        $i = 0;
        foreach ($this->mediaBaseXML->i as $item)
        {
          //PROBLEM   the first hit is taken
          //          $checkRef = ($aFirstHit) ? basename($item->a) : $item->a;
          //          if ($checkRef == $aSong)

          //if (strcmp(basename($item->a), $this->curSong) == 0) // -- orig here, where the entry is absolute --
          //if (strcmp($item->a, $this->curSong) == 0) // -- like feedback, where every entry is valid for all --
          // -- The simple file matters, whereever in which album! Works also for piMoo AND piMooReverse. --
          if (strcmp(basename($item->a), basename($this->curSong)) == 0)
          {
            $this->curSongFound = $i;
            //$result = true;
            break;
          }
          $i++;
        }
      }
      //eventLog('DEBUG SONG (mediaBase): ' . $aSong, $this->depth);
      //return $result;
    }

    /**
     *
     * {@inheritDoc}
     * @see interface_mediabase::setComment()
     * @return boolean
     */
    function setComment($aComment, $aBy = NULLSTR)
    {
      $result = false;

      $attr = XML_BY;
      // -- take the existing entry, no new creation, that's only in feedback.xml --
      if ($this->curSongFound !== false)
      {
        $this->mediaBaseXML->i[$this->curSongFound]->c = htmlspecialchars( trim($aComment) );
        $this->mediaBaseXML->i[$this->curSongFound]->c->attributes()->$attr = $aBy;

        eventLog('CHANGED COMMENT in mediaBase: "' . trim($aComment) . '" for song: ' . $this->curSong, $this->depth);
        $result = true;
      }
      else
      {
        // -- Yes, return also true - Because not finding a song is not a failure! --
        $result = true;
      }

      return $result;
    }


     /**
      *
      * {@inheritDoc}
      * @see interface_mediabase::setRating()
      * @return boolean
      */
    function setRating($aRating, $aBy = NULLSTR)
    {
      $result = false;

      $attr = XML_BY;
      // -- take the existing entry, no new creation, that's only in feedback.xml --
      if ($this->curSongFound !== false)
      {
        $this->mediaBaseXML->i[$this->curSongFound]->r = $aRating;
        $this->mediaBaseXML->i[$this->curSongFound]->r->attributes()->$attr = $aBy;

        eventLog('CHANGED RATING in mediaBase: "' . $aRating . '" for song: ' . $this->curSong, $this->depth);
        $result = true;
      }
      else
      {
        // -- Yes, return also true - Because not finding a song is not a failure! --
        $result = true;
      }

      return $result;
    }


    /**
     * saves the mediabase file, after comment or rating has been set
     * @return boolean
     */
    function saveFeedback()
    {
      $result = false;

      // -- ... write to XML file now --
      $handle = fopen($this->mediabaseFile, 'wb');
      if (!fwrite($handle, $this->mediaBaseXML->asXML()))
      {
        $this->lastMsg = LNG_ERR_UNKNOWN;
        eventLog('ERROR in function saveFeedback() in file api_mediabaseXML.php');
      }
      else
      {
        $result = true;
      }

      fclose($handle);

      if ($result && CFG_DEBUG)
        cntMakeHumanXML($this->mediaBaseXML);

      return $result;
    }


    // -- end of save a comment or rating --
    // -- ------------------------------- --



    // -- --------------------------------- --
    // -- begin of find a comment or rating --


    /**
     * finds a wished song and returns rating and value in the following functions getComment... and so on
     * if it could not be found the following code by the caller has to abort the rest by negative result here
     * @param string $aSong
     * @return boolean
     */
    function findSong($aSong)
    {
      $result = false;

      if ($this->mediaBaseXML) //$xml = simplexml_load_file($this->feedbackXMLFile);
      {
        $attr = XML_BY;
        foreach ($this->mediaBaseXML->i as $item)
        {
          if (strcmp($item->a, $aSong) == 0) //if ($item->a == $aSong)
          {
            if (isset($item->c))
            {
              $this->curSongComment = $item->c;
              $this->curSongCommentBy = $item->c->attributes()->$attr;
            }
            if (isset($item->r))
            {
              $this->curSongRating = $item->r;
              $this->curSongRatingBy = $item->r->attributes()->$attr;
            }
            $result = true;
            break;
          }
        }
      }

      return $result;
    }


    function getComment()
    {
      return $this->curSongComment;
    }
    function getCommentBy()
    {
      return $this->curSongCommentBy;
    }

    function getRating()
    {
      return $this->curSongRating;
    }
    function getRatingBy()
    {
      return $this->curSongRatingBy;
    }


    function findSongReset()
    {
      $this->curSongComment = NULLSTR;
      $this->curSongCommentBy = NULLSTR;
      $this->curSongRating = NULLSTR;
      $this->curSongRatingBy = NULLSTR;
    }


    // -- end of find a comment or rating --
    // -- ------------------------------- --


  } // -- end of class_mediaBase "xml" --
