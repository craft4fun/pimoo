<?php
  /*
   * Keep in mind, that this script is only called, if not frontpage is focused.
   * E.G. only myMoo or admin page is calling it.
  */

  // -- special piMoo stuff --
  require_once ('defines.inc.php');
  require_once ('keep/config.php');
  require_once ('lib/utils.php');
  require_once ('api_playlist.php');
  require_once ('api_player.php');

  $myPlaylist = new class_playlist();

  $onAirSong = $myPlaylist->getWholePlaylist(true); // -- only onAir song --
  $playerState = class_player::static_getPlayerState();
  if ($playerState != PS_PLAYING) // || $playerState == PS_PAUSED)
  {
    echo '<div style="padding-top: 0.25em;">'.TILDE.BLANK.$playerState.BLANK.TILDE.'</div>';
  }
  else
  {
    if (isset($onAirSong[0]['t']))
    {
      // -- song length --
      $length = parseHumanMinSecStringToSeconds($onAirSong[0]['p'], true);

      // -- generate gradient --
      $grad = cntGradientFromTo('ff0000', '00ff00', $length);

      // -- get actual values with static function! --
      $seconds = class_player::static_getRemainTimeInSeconds();

      // -- catch nonsense --
      if ($seconds > $length - 1)
      {
        $seconds = $length - 1;
      }

      // -- html output --
      $style = 'height: 120%; padding-top: 0.25em;
                color: white;
                background-color: #'.$grad[$seconds].';
                background: radial-gradient(black, #'.$grad[$seconds].');
                background: -moz-radial-gradient(black, #'.$grad[$seconds].');
                background: -webkit-radial-gradient(black, #'.$grad[$seconds].');';
      // -- background: radial-gradient(center, black, #'.$grad[$seconds].'); -> center seems to be default --

      echo '<div style="'.$style.'">'.$onAirSong[0]['t'].'</div>';
    }
  }

  unset($myPlaylist);
