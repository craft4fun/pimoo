<?php
  // -- piMoo basic stuff --
  require ('defines.inc.php');
  require ('keep/config.php');
  require ('lib/utils.php');
  require ('lib/class_cache.php');

  // -- special piMoo stuff --
  require ('api_layout.php');
  require ('api_playlist.php');

  echo makeHTML_begin();

  echo makeHTMLHeader_end();
  echo makeHTMLBody_begin();

  echo '<div>Welcome to piMoo in console mode (without javascript requirements)</div>';

  echo makeTopNavi_noJS();


  // -- ------------------- --
  // -- beg of admin's area --

  // -- admin --
  if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
  {
    $admin = true;
    echo BLANK_HTML.cntMBL('<a href="admin/backend.php"><nobr>goAdmin</nobr></a>');
    echo BLANK_HTML.cntMBL('<a href="admin/logout.php">logout</a>');
  }
  else
  {
    $admin = false;
    echo BLANK_HTML.cntMBL('<a href="admin/backend.php">becomeAdmin</a>');
  }
  // -- end of admin's area --
  // -- ------------------- --


  if ($cfg_allowExternControl OR $admin)
    echo makeSearchBar_noJS();
  else
    echo BLANK_HTML.'the song navigation has been disabled by admin'.LFH;

  $myPlaylist = new class_playlist();
  // -- --------------------- --
  // -- begin of POST ACTIONS --
  echo '<form name="FORM_PLAYLIST" method="POST" action="action_noJS.php">';
    echo makeCurrentPlaylistView_noJS($myPlaylist->getWholePlaylist());
  echo '</form>';
  // -- end of POST ACTIONS --
  // -- ------------------- --
  unset($myPlaylist);

  echo LFH;

  echo makeHTML_end();

