<?php
  // -- special piMoo stuff --
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'api_player.php';

  $msg = NULLSTR;

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    $msg .= cntErrMsg(LNG_ERR_ONLYADMIN);
  }
  else
  {
    $seconds = (isset($_GET['seconds'])) ? intval($_GET['seconds']) : -30;

    $msg .= $seconds . 's' . LFH;

    $myPlayer = new class_player();
    if (!$myPlayer->cmdRewind($seconds))
      $msg .= cntErrMsg($myPlayer->getLastMsg()); //
    unset($myPlayer);
  }

  echo $msg;