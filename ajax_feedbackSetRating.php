<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  include_once ('api_feedback.php');
  include_once ('api_mediabase.php');


  //-- get nickame --
  $nickname =  (isset($_SESSION['str_nickname'])) ? $_SESSION['str_nickname'] : NULLSTR; // -- $_SERVER['REMOTE_ADDR'] --> do not fill the feedback.xml with obsolete information --

  $msg = NULLSTR;

  // -- is admin or registered user? --
  if (!(
      (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
      ||
      (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
    ))
  {
    eventLog('ERROR in ajax_feedbackSetComment.php, no user or admin');
  }
  else
  {
    if (!isset($_GET['song']))
    {
      eventLog('ERROR in ajax_feedbackSetRating.php, required parameter missing: song');
    }
    else
    {
      $song = $_GET['song'];

      if (!isset($_GET['rating']))
      {
        eventLog('ERROR in ajax_feedbackSetRating.php, required parameter missing: rating');
      }
      else
      {
        $rating = $_GET['rating'];

        $myFeedback = new class_feedback();
        $myFeedback->setSong(base64_decode($song)); // -- feedback must make an entry in EVERY case --
        if (!$myFeedback->setRating($rating, $nickname))
        {
          eventLog('ERROR in: myFeedback->setRating() in file ajax_feedbackSetRating.php');
        }
        else
        {
          if (!$myFeedback->saveFeedback())
          {
            eventLog('ERROR in: myFeedback->saveFeedback() in file ajax_feedbackSetRating.php');
          }
          else
          {
            // -- CLONE INTO MEDIABASE FOR DIRECT USE--
            $myMediaBase = new class_mediaBase();
            $myMediaBase->setSong(base64_decode($song)); // -- on mediabase it is ok to check if it exists --
            if (!$myMediaBase->setRating($rating, $nickname))
            {
              eventLog('ERROR in: $myMediaBase->setRating() in file ajax_feedbackSetRating.php');
            }
            else
            {
              if (!$myMediaBase->saveFeedback())
              {
                eventLog('ERROR in function $myMediaBase->saveFeedback() in file ajax_feedbackSetRating.php');
              }
              else
              {
                // -- as a result of all --
                $msg .= $myFeedback->getLastMsg();
              }
            }
          }
        }
      }
    }
  }

  echo $msg;
