<?php
  // -- special piMoo stuff --
  include_once ('defines.inc.php');
  include_once ('keep/config.php');
  require_once ('lib/utils.php');

  if ($_GET['value'] == NULLSTR)
  {
    $msg = cntErrMsg(LNG_ERR_PARAM_MISSING);
  }
  else
  {
    if (setcookie('int_NewcomerDays', $_GET['value'], strtotime('+365 days')))
      $msg = LNG_DAYS_SONG_NEWCOMMER . ARROW . '<span style="font-weight: bold;">' . $_GET['value'] . '</span>';
  }

  echo $msg;