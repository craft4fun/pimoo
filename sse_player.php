<?php
  header('Content-Type: text/event-stream');
  header('Cache-Control: no-cache');

  // -- special piMoo stuff --
//   include_once ('defines.inc.php');
//   include_once ('keep/config.php');

//   require_once ('lib/utils.php');

//   include_once ('api_playlist.php');
//   include_once ('api_layout.php');

//   $myPlaylist = new class_playlist();

//   $content = makeCurrentPlaylistView($myPlaylist->getWholePlaylist());
//   echo "data: $content \n\n";
//   flush();



  // -- so wie hier oben zu sehen, geht es schon mal prinzipiell --
//   $time = date('r');
//   echo "data: The server time is: {$time}\n\n";
//   flush();


  // https://www.html5rocks.com/en/tutorials/eventsource/basics/

  /**
   * Constructs the SSE data format and flushes that data to the client.
   *
   * @param string $id Timestamp/id of this connection.
   * @param string $msg Line of text that should be transmitted.
   */
  function sendMsg($id, $msg) {
    echo "id: $id" . PHP_EOL;
    echo "data: $msg" . PHP_EOL;
    echo PHP_EOL;
    ob_flush();
    flush();
//sleep(10);
  }

  $serverTime = time();

  sendMsg($serverTime, 'server time: ' . date("h:i:s", time()));
