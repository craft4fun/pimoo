#!/usr/bin/php
<?php

  // -- INFO there is also a separate addon docker tool (pimoo-pcn), --
  // -- which watches changes on playlist and notifies via mqtt all frontend clients --

  // -- --------------------------------------- --
  // -- check if the inotifywait command exists --
  if (!shell_exec('command -v inotifywait'))
  {
    echo "inotifywait not found, please install it with: apt install inotify-tools" . PHP_EOL;
    exit(1);
  }

  require 'defines.inc.php';
  require 'lib/utils.php';
  require 'keep/config.php';
  require 'api_player.php';
  require 'api_playlist.php';
  require 'class_mqtt.php';


  $abs_playlistFile = __DIR__ . SLASH . $cfg_playlistFile;


  echo "cfg_mqtt_broker_unified: $cfg_mqtt_broker_unified" . PHP_EOL;
  echo "abs_playlistFile: $abs_playlistFile" . PHP_EOL;


  // -- ----------------------- --
  // -- create required objects --
  $myPlaylist = new class_playlist(NULLSTR, true); // -- prevent creating files automatically here --
  $myPlayer = new class_player();
  $mqtt = new mqtt();
  $mqtt->setTopic(CFG_MQTT_TOPIC_PLAYLIST);

  // -- ------------------------------------------------ --
  // -- beg of observe and watch for changes in playlist --
  while (true)
  {
    // -- not required, because including class_playlist cares about missing playlist file? --
    if (!file_exists($abs_playlistFile))
    {
      $timestamp = date('Y-m-d H:i:s');
      echo "$timestamp $abs_playlistFile not found, waiting one minute" . PHP_EOL;
      sleep(60);
    }
    else
    {
      $timestamp = date('Y-m-d H:i:s');
      echo "$timestamp start watching playlist file" . PHP_EOL;

      // -- watch for changes to the file using inotifywait --
      exec('inotifywait -e modify '.$abs_playlistFile, $output, $return_var);

      // -- if inotifywait detected a modification, publish a mqtt topic --
      if ($return_var == 0)
      {
        // -- give backend time to handle file changes like loading playlist --
        // usleep(500000); // -- 0.5s --
        // -- trigger now --
        $playlist = $myPlaylist->getWholePlaylistAsJson($myPlayer, true);
        $mqtt->setMessage($playlist);
        $mqtt->publish(true);

        $timestamp = date('Y-m-d H:i:s');
        echo "$timestamp publish current playlist to topic: " . CFG_MQTT_TOPIC_PLAYLIST . PHP_EOL;
      }

      sleep(1);
    }
  }
  // -- end of observe and watch for changes in playlist --
  // -- ------------------------------------------------ --
