<?php
  require_once 'defines.inc.php';
  require_once 'keep/config.php';
  require_once 'lib/utils.php';
  require_once 'api_playlist.php';


  // -- moves the bottom song to the top (directly after adding) --

  // -- is admin? --
  $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true);
  if (!$admin)
  {
    echo cntErrMsg(LNG_ERR_ONLYADMIN);
  }
  else
  {
    $myPlaylist = new class_playlist();
    $check = $myPlaylist->moveBottomTopSong($admin);
    unset($myPlaylist);

    if ($check)
      echo LNG_SUC_MOVEDUP;
    else
      echo LNG_ERR_UNKNOWN;
  }

