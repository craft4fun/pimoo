<?php
  require_once ('api_feedback.php');
  require_once ('api_mediabase.php');
  require_once ('api_player.php');


  // -- ----------------------- --
  // -- begin of HTML standards --
  // -- ----------------------- --

  function makeHTML_begin($aLinks = true, $aDepth = NULLSTR)
  {
    global $cfg_DOJO;
    global $cfg_mqtt;

    function _isMobile()
    {
      global $isMobile;
      return $isMobile;
    }


    $result = '<!DOCTYPE HTML>
      <html>
        <head>
          <title>'.PROJECT_NAME.'</title>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <meta name="author" content="Patrick Höf">
          <meta name="keywords" content="'.PROJECT_NAME.' web based party player">
          <meta name="description" content="'.PROJECT_NAME.'">
      ';

//      if (_isMobile())
//      {
//        $result .= '
//         ';
//      }

    // -- that leads to massive problems in mobile view because it works against the other scaling technologies in my style --
    //<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.25,  user-scalable=yes">

    if ($aLinks)
    {
      // -- for any unknown reason this must be loaded before dojo --
      if ($cfg_mqtt)
      {
        // $result .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/mqtt/4.3.8/mqtt.min.js"></script>';
        $result .= '<script src="'.$aDepth.'lib/jsMQTT/mqtt.4.3.8.min.js"></script>';
      }

      $result .= '
        <script type="text/javascript" src="'.$aDepth . $cfg_DOJO.'"
                djConfig="parseOnLoad: false, isDebug: false, locale: \'en\'">
        </script>';
      //$result .= '<script type="text/javascript" src="'.$aDepth.'lib/utils.js.php"></script>';
      //$result .= '<script type="text/javascript" src="'.$aDepth.'lib/utils_ext1.js"></script>';
      //old: $result .= '<link rel="shortcut icon" href="'.$aDepth.'res/favicon.ico" type="image/x-icon">';
      //$result .= '<link rel="stylesheet" href="'.$aDepth.'style.css" type="text/css" media="screen">';

      // -- old favicon --
      //$result .= '<link rel="icon" type="image/png" href="'.$aDepth.'res/favicon.png" />';
      // -- new favicon with manifest for working standalone when pinned on home screen --
      $result .= '
        <link rel="apple-touch-icon" sizes="180x180" href="res/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="res/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="res/favicon/favicon-16x16.png">
        <link rel="manifest" href="res/favicon/site.webmanifest">
        <link rel="mask-icon" href="res/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
      ';


    }
    return $result;
  }

  /**
   * forceBright can be used for exceptions, e.g. when calles by admin area
   * @param boolean $forceBright
   */
  function includeStyleSheet($forceBright = false)
  {
    global $cfg_isDocker; // -- used in style.bright --

    include ('style.core.css.php');

    if ($forceBright)
    {
      include ('style.bright.css.php');
    }
    else
    {
      $darkMode = (isset($_COOKIE['bool_darkMode']) && $_COOKIE['bool_darkMode'] == YES) ? true : false;
      if ($darkMode)
        include ('style.dark.css.php');
      else
        include ('style.bright.css.php');
    }
  }

  function includeJavascript($aDepth = NULLSTR)
  {
    global $cfg_playlistRefreshInterval;
    global $cfg_codeWordQuestion;
    global $cfg_minimumSearchChars;
    global $cfg_mqtt;
    global $cfg_mqtt_broker_unified;

    include ($aDepth . 'lib/utils.js.php');
    include ($aDepth . 'lib/utils_ext1.js.php');
  }



  function makeHTMLHeader_end()
  {
    return '</head>';
  }

  /**
   * HTML body begin with optionally param oder style string
   * @param string $aParam
   * @return string
   */
  function makeHTMLBody_begin($aParam = NULLSTR)
  {
    //return "<body $aParam>" . '<script type="text/javascript">dojo.back.init();</script>';
    return '<body'.BLANK.$aParam.'>';
  }


  /**
   * creates some standards on top like feedback and so on
   * @return string
   */
  function makeStandards($aAdmin = false, $aDepth = NULLSTR)
  {
    global $cfg_playlistRefreshInterval;
    global $isMobile;

    $result = NULLSTR;

    if (CFG_DEBUG)
      $result .= '<div id="isDebugTape">...DEBUG...</div>';

    // -- show song omnipresent on top --
    $user = (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR) ? true : false;
    if ($aAdmin || $user)
    {
      $result .= '<div id="omniPresentSong" style="display: none;"></div>'; // -- always prepared --
      // -- normally show the omnipresent bar if wished, but not in admin adrea when imprissoned - that would be twice! --
      if ($aAdmin || !isset($_SESSION['bool_imprison']))
      {
        $songOmnipres = (isset($_COOKIE['bool_SongOmnipresent']) && $_COOKIE['bool_SongOmnipresent'] == YES) ? true : false;
        if ($songOmnipres)
        {
          $result .= '<script>
            //omnipresentCallerInit();
            omnipresentCallerStart("'.$aDepth.'");
          </script>';
        }
      }
    }


    // -- player control bar --
    if ($aAdmin)
    {
      $result .= '<span id="targetAdminControlBar">';
        $result .= makeAdminControlBar($aDepth);
      $result .= '</span>';
    }


    // -- beg of contentWrapper --
    $result .= '<div id="contentWrapper">';


    // -- standard feedback --
    $result .= '<div id="target_feedback" style="opacity: 0; display: none;" onclick="quit_feedback()"></div>';
  /* maybe in the future
    // -- OK message --
    $result .= '<div id="okMsg"></div>'; //  onclick="quit_feedback()"
    // -- error message --
    $result .= '<div id="errorMsg"></div>'; // onclick="quit_feedback()"
  */

    if ($isMobile)
    {
      $result .= '<script>
        function setFeedbackHeight()
        {
          var windowInnerHeight = window.innerHeight; // -- on mobile also ok: screen.height --
          var targetHeight = (windowInnerHeight / 2) + (windowInnerHeight / 4); // 75%
          $("target_feedback").style.maxHeight = targetHeight + "px";
          //console.log(targetHeight);
        }
        dojo.ready(setFeedbackHeight);

        window.addEventListener("resize", setFeedbackHeight);
      </script>';
    }



    // -- detect if cli player is currently playing when entering the main page --
    $playerState = class_player::static_getPlayerState();
    if ($playerState == PS_PLAYING)
      $isPlaying = 'var isPlaying = true;';
    else
      $isPlaying = 'var isPlaying = false;';


    // -- counting down the eta via javascript --
    $result .= '<script>
          '.$isPlaying.'
          function update_RT_ETA()
          {
            // -- call only if player is playing --
            if (isPlaying == true)
            {
              var onAirRT = $("onAirRT");
              if (onAirRT)
              {
                var oldRT = onAirRT.textContent;
                var newRT = calcRTtime(oldRT);
                onAirRT.textContent = newRT;
              }
              // find all elements with data-eta attribute and decrease them 1s
              dojo.query("[data-eta]").forEach(function(elem)
              {
                var oldETA = elem.getAttribute("data-eta");
                var newETA = calcETAtime(oldETA);
                elem.setAttribute("data-eta", newETA);
                elem.textContent = "ETA: " + newETA;
              });
            }
          }

          /* change browser tab name */
          function update_documentTitle()
          {
            let omniPresentSong = $(\'omniPresentSong\');
            if (omniPresentSong)
            {
              window.document.title = \''.PROJECT_NAME.'#\' + omniPresentSong.innerText; // -- not: innerHTML --
            }
            else
            {
              window.document.title = \''.PROJECT_NAME.'\';
            }
          }

          function mainLoop()
          {
            update_RT_ETA();
            update_documentTitle();
          }
          var mainLoop = window.setInterval(mainLoop, 1000);
        </script>';


    // WHAT THE HELL IS THAT?
    // -- onAirRemainTime for special use with initial value --
    $result .= '<span id="onAirRemainTime" style="display: none; opacity: 0;">'.(string)($cfg_playlistRefreshInterval).'</span>'; // style="visibility: hidden";

    // -- moved to makeBottom() since Dec 2021 --
    //$result .= '<a href="javascript:window.scrollTo(0, 0);" id="toTopLink">&uarr;</a>';

    return $result;
  }



  /**
   * Spawn the main frame hulk where all ajax called stuff is pumped into.
   * e.g. http://localhost/piMoo/frontend.php?hulk=ajax_itemsByGroup.php&group=T
   * http://localhost/piMoo/frontend.php?hulk=ajax_itemsBySearch.php&search=~4~&offset=100
   * @return string
   */
  function makeAJAXframeHulk($includeHulk = false)
  {
    // -- preload the pleaseWait.gif -> so it is in cache for later use --
    $hulkDIV = '<div id="frame_hulk"><img style="width: 0px; height: 0px; display: none;" src="res/pleaseWait.gif" />'; // -- open --

    if ($includeHulk)
    {
      if (isset($_GET['hulk']) && $_GET['hulk'] != NULLSTR)
      {
        $hulkPage = $_GET['hulk'];

        if (!file_exists($hulkPage))
        {
          $hulkDIV .= LNG_ERR_NOPAGE . COLON . BLANK . $hulkPage;
        }
        else
        {
          $hulkRef = $_SERVER['QUERY_STRING']; // -- get all URL parameters, including search string for example --
          //include_once $hulkRef; //@include $_REQUEST['hulk'];
          //$hulk .= '<script>callHulk(\''.$hulkPage.QUESTMARK.$hulkRef.'\');</script>';
          $hulkDIV .= '<script>DojoAjax(\''.$hulkPage.QUESTMARK.$hulkRef.'\', \'frame_hulk\');</script>';
        }
      }
    }
    $hulkDIV .= '</div>'; // -- close --

    return $hulkDIV;
  }


  /**
   * a generally access check
   * @return string (HTML) or die
   */
  function accessCheck($aAccessBar = false) // , $aDepth = NULLSTR
  {
    global $isMobile;

    $result = NULLSTR;

    $admin = (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true) ? true : false;
    $nickname = (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR) ? true : false;


    // -- ------------------------ --
    // -- right part of access bar --
    if ($aAccessBar)
    {
      $result .= '<div style="float: right; margin-right: 1em; padding: 1em;">';

      if ($nickname)
      {
        $result .= '<div style="margin: 1em 0em 1.5em 0em;"><a class="linkNaviNice" href="myMoo.php">myMoo</a></div>';
      }

      // -- admin's area, the backend --
      if (!$admin)
      {
        $result .= '<span style="opacity: 0.1;" margin-top: 1em;>'.cntMBL('<a class="linkNice" href="admin/backend.php">become admin</a>').'</span>';

        // if (!$isMobile)
        // {
        //   $result .= '<input style="opacity: 0.5; margin-left: 0.5rem;" type="button" id="openAdminAreaButton" />';
        // }
      }

      $result .= '</div>';
    }


    // -- -------------------- --
    // -- code word of the day --

    global $cfg_codeWordAnswer;
    global $cfg_codeWordQuestion;

    if (!$admin)
    {
      // -- code word of the day --
      // -- the answer should be provided on the party flyer --
      if (
             $cfg_codeWordQuestion !== false
          && (!isset($_SESSION['bool_codewordOK']) || (isset($_SESSION['bool_codewordOK']) && $_SESSION['bool_codewordOK'] != $cfg_codeWordAnswer))
       )
      {
        $msg = '<div style="font-size: larger;">'
                 .'<a style="color: black;" href="javascript:answerCodeword();">'.cntStdMsg(LNG_CODEWORD_OF_THE_DAY_HINT).'</a>'.
               '</div>';
        die($msg); // -- do net delete die()! --
      }
    }


    // -- --------------------- --
    // -- logged in? "nickname" --
    $result .= '<div style="float: left; margin: 0.5em 1em 0.5em 0.5em;">';

      if (!$nickname)
      {
        $msg = '<div style="font-size: larger;">'.
                 '<a style="color: black;" href="javascript:promptNickname();">'.cntStdMsg(LNG_GIMMENICKNAME.'<div style="margin-top: 1em;">'.LNG_PARTYPOOPER).'</div></a>';
        $msg .= '</div>';
        // -- as a convenience quick login I offer the currently logged in users --
        $msg .= class_nickname::showCurrentNicknames();
        die($msg); // -- do not delete die()! --
      }
      else
      {
        // -- ----------------------- --
        // -- left part of access bar --
        if ($aAccessBar)
        {
          $result .= '<div style="float: none; margin: 1.5em 0em 0em 0.5em;">';
            $result .= LNG_HELLO.BLANK.'<span style="font-weight: bold;"><nobr><a class="linkNice" href="javascript:callSearchByUser(\''.$_SESSION['str_nickname'].'\')">'.$_SESSION['str_nickname'].'</a></nobr></span>';
          $result .= '</div>';
        }
      }

    $result .= '</div>';

    return $result;
  }


  /**
   * makes HTML bottom
   * @return string
   */
  function makeBottom($aDepth = NULLSTR)
  {
    global $isMobile;
    global $imprison;

    if (!isset($isMobile))
    {
      $isMobile = cntIsMobile();
    }

    $result = '
      <div id="bottomHelper">&nbsp;</div>
      <div id="frame_bottom">
        <span style="float: right;"><a class="linkNaviNice" href="'.$aDepth.'history.php">'.PROJECT_VERSION.'</a></span>
    ';
    if (isset($_SESSION['forceIsMobile']) && $_SESSION['forceIsMobile'] == true)
    {
      $result .= '<a class="linkNaviNice" href="'.$aDepth.'nobile.php">desktop version</a>';
    }
    else
    {
      if ($isMobile)
      {
        $result .= '<a class="linkNaviNice" href="'.$aDepth.'nobile.php">desktop version</a>';
      }
      else
      {
        $result .= '<a class="linkNaviNice" href="'.$aDepth.'mobile.php">mobile version</a>';
      }
    }
    $result .= '</div>';

    // -- to bring it to the end of the body is even stronger then a high z-index --
    $result .= '<a href="javascript:window.scrollTo(0, 0);" id="toTopLink">&uarr;</a>';


    // -- open admin area button must be initialized and then called by a users button click due to modern browser security policy --
    if (!$isMobile && (isset($imprison) &&! $imprison))
    {
      $result .= '<script>
                    document.getElementById("openAdminAreaButton").addEventListener("click", () => {
                      window.open("backend.php?imprison", "AdminArea",
                      "width=640, height=800, menubar=no, toolbar=no, location=no, status=no, scrollbars=yes, resizable=yes");
                    });
                  </script>';
    }

    return $result;
  }


  /**
   * makes HTML end tag
   * @return string
   */
  function makeHTML_end()
  {
    $result = NULLSTR;
    $result .= '</div>';  // -- end of contentWrapper --
    $result .= '</body></html>';
    return $result;
  }

  // -- --------------------- --
  // -- end of HTML standards --
  // -- --------------------- --




  // -- --------------------------- --
  // -- begin of basic layout stuff --
  // -- --------------------------- --

  function makeAdminControlBar($aDepth = NULLSTR) // -- topmost --
  {
    //global $isMobile;
    $isMobile = (cntIsMobile()) ? true : false;
    $mobileSuffix = ($isMobile) ? 'M' : NULLSTR;

    $result = NULLSTR;

    $result .= '<div id="adminControlBar">';

      $playerState = class_player::static_getPlayerState($aDepth);

      $result .= '<div style="float: left; margin-left: 0.2em;">';
        if (isset($_SESSION['str_nickname']))
          $result .= '<span style="float: right; opacity: 0.25;">'.cntMBL('<a onclick="return confirm(\''.LNG_SURE.'\')" style="color: red;" href="javascript:stopNlogout(\''.$aDepth.'\')">'.LNG_LOGOUT_SHORT.'</a>').'</span>';
      $result .= '</div>';

      $result .= '<div style="float: right; margin-right: 0.2em;">';

        if ($aDepth != ONEDIRUP && !isset($_SESSION['bool_imprison']))
          $result .= cntMBL('<a href="admin/backend.php"><nobr><span class="link" style="color: red;">admin</span></nobr></a>');

        if (!isset($_SESSION['bool_imprison']))
          $result .= '<span style="margin-left: 0.2em;">'.cntMBL('<a href="logout.php" onclick="return confirm(\''.LNG_SURE.'\');">logout</a>').'</span>';

        if (isset($_SESSION['bool_imprison']))
          $result .= '<span class="watermark" style="margin-left: 0.2em;" title="'.LNG_ADMIN_BREAK.'">'.LNG_ADMIN_IMPRISONED.'</span>';

      $result .= '</div>';


      $result .= '<div class="centerAlignedBlock" style="width: 500px;"><nobr>';


        $result .= '<span class="transAllEeaseFast" id="ctrlElemVolumeDown"><a href="javascript:controlVolumeDown(\''.$aDepth.'\');"><img id="ctrlVolumeDown" src="'.$aDepth.'res/volumeDown'.$mobileSuffix.'.png" title="volume down"/></a></span>';
        $result .= '<span class="transAllEeaseFast" id="ctrlElemVolumeUp"><a href="javascript:controlVolumeUp(\''.$aDepth.'\');"  ><img id="ctrlVolumeUp"   src="'.$aDepth.'res/volumeUp'.$mobileSuffix.'.png"   title="volume up"/></a></span>';


        //if ($playerState == PS_PLAYING)  // OR $playerState == PS_UNKNOWN
        {
          $result .= '<span class="transAllEeaseSlow" id="ctrlElemRewind"><a href="javascript:controlPlayerRewind(-30, \''.$aDepth.'\');" ><img id="ctrlRewind"      src="'.$aDepth.'res/rewind'.$mobileSuffix.'.png"       title="rewind" /></a></span>';
          $result .= '<span class="transAllEeaseSlow" id="ctrlElemForward"><a href="javascript:controlPlayerForward(30,\''.$aDepth.'\');"><img id="ctrlForward"     src="'.$aDepth.'res/forward'.$mobileSuffix.'.png"      title="forward" /></a></span>';
        }


        //if ($playerState != PS_UNKNOWN && $playerState != PS_OFF)
          $result .= '<a href="javascript:controlPlayerNext(\''.$aDepth.'\');" onclick="return confirm(\''.LNG_SURE_NEXT_SONG_IMMEDIATELLY.'\');"><img id="ctrlNext" src="'.$aDepth.'res/next'.$mobileSuffix.'.png" title="next" /></a>';


        if ($playerState == PS_PAUSED)
        {
          $result .= '<span class="transAllEeaseSlow" id="ctrlElemPlay"  style="opacity: 1;"><a href="javascript:controlPlayerPlay(\''.$aDepth.'\');" ><img id="ctrlPlay"      src="'.$aDepth.'res/play'.$mobileSuffix.'.png"       title="play" /></a></span>';
          $result .= '<span class="transAllEeaseSlow" id="ctrlElemPause" style="opacity: 0.2;"><a href="javascript:controlPlayerPause(\''.$aDepth.'\');"><img id="ctrlPause"     src="'.$aDepth.'res/pause'.$mobileSuffix.'.png"      title="pause" /></a></span>';
        }

        if ($playerState == PS_PLAYING) // OR $playerState == PS_UNKNOWN
        {
          $result .= '<span class="transAllEeaseSlow" id="ctrlElemPlay"  style="opacity: 0.2;"><a href="javascript:controlPlayerPlay(\''.$aDepth.'\');" ><img id="ctrlPlay"      src="'.$aDepth.'res/play'.$mobileSuffix.'.png"       title="play" /></a></span>'; // onclick="return confirm(\''.LNG_SURE.'\');"
          $result .= '<span class="transAllEeaseSlow" id="ctrlElemPause" style="opacity: 1;"><a href="javascript:controlPlayerPause(\''.$aDepth.'\');"><img id="ctrlPause"     src="'.$aDepth.'res/pause'.$mobileSuffix.'.png"      title="pause" /></a></span>'; // onclick="return confirm(\''.LNG_SURE.'\');"
        }


      $result .= '</nobr></div>';

     $result .= '</div>';

     return $result;
  }


  function makeTopNavi()
  {
    $darkMode = (isset($_COOKIE['bool_darkMode']) && $_COOKIE['bool_darkMode'] == YES) ? true : false;
    $logoPicRef = (!$darkMode) ? 'res/piMoo_logo.png' : 'res/piMoo_logoDark.png';
    return '<script>
        function resetToMain()
        {
          //_setBrowserHistory(\'frontend.php\');

          // -- ------- --
          // -- classic --
          //window.location.href = \'frontend.php\';

          // -- ------ --
          // -- faster --

          // -- reset and reload the interval playlist refresh --
          resetAjaxCurPlaylistCall();
          ajaxInterval = window.setInterval("ajaxCurPlaylistCall();", '.getPLRI().');

          // -- this is very clever: load the playlist hulk but write only frontend into browsers history --
          callHulk(\'ajax_getCurrentPlaylist.php\', \'frontend.php\');
        }

        // -- goto piMooReverse by tap and hold on piMoo logo for approx. one second --
        var longClickTimer;
      </script>

      <div id="frame_logo">
        <img id="logo" alt="logo" src="'.$logoPicRef.'"
          onclick="resetToMain()"
          onmousedown="longClickTimer=setTimeout(function(){window.location.href = \'piMooReverse/index.php\';}, 750);"
          onmouseup="clearTimeout(longClickTimer);"
        />
      </div>
    ';
  }


  function makeTopNavi_noJS()
  {
    return '<a href="frontend_noJS.php">home</a>';
  }


  function makeSearchBar($aEnabled)
  {
    global $cfg_minimumSearchChars;
    global $cfg_maxSearchResultsPerPage;
    global $cfg_cache;
    //global $cfg_naviShowExtras;
    global $cfg_stash;

    $result = NULLSTR;

    if ($aEnabled)
    {
      $result .= '<div id="frame_navi">';

      $result .= '<div id="ctl_hideNavi" onclick="hideNaviClick()" style="text-align: center; cursor: default; font-size: larger;">&nbsp;</div>'; // &uArr; border-style: solid; border-color: white; border-width: 1px;
        $result .= '<div id="ctl_showNavi" onclick="showNaviClick()" style="text-align: center; cursor: default; display: none; font-size: smaller;">&nbsp;</div>'; // &dArr;

        $result .= '<div id="frame_navi_content">';

          $result .= '<div id="searchbarGrid">
                        <input class="menuButton btnGray" id="btn_songSearchClean" type="button" value="'.SYM_CLEAN.'"  onclick="songSearchClean()" />
                        <input class="menuButton"         id="edt_songSearch"      type="text"   value=""               onkeyup="if (event.keyCode == 13) callSearchByEdtSongSearch(this.value)" />
                        <input class="menuButton btnGray" id="btn_songSearchGo"    type="button" value="'.SYM_GO.'"     onclick="callSearchByEdtSongSearch()" />
                        <input class="menuButton btnGray" id="btn_userSearchGo"    type="button" value="'.SYM_USER.'"   onclick="callSearchUserByEdtSongSearch()" title="'.LNG_SEARCH_USER.'" />
                      </div>';

          $result .= '<span style="white-space: nowrap; margin: 0.2em;">
                        <input class="menuButton btnGray" type="button" value="'.RATE3_DINGBAT.'" onclick="callSearchByStr(\''.RATE3.'\')" title="'.RATE3_HUMAN.'" />
                        <input class="menuButton btnGray" type="button" value="'.RATE4_DINGBAT.'" onclick="callSearchByStr(\''.RATE4.'\')" title="'.RATE4_HUMAN.'" />
                        <input class="menuButton btnGray" type="button" value="'.RATE5_DINGBAT.'" onclick="callSearchByStr(\''.RATE5.'\')" title="'.RATE5_HUMAN.'" />
                      </span>';

          $RSR = (isset($_COOKIE['int_RandSrchResults'])) ? $_COOKIE['int_RandSrchResults'] : GRP_RAND10;
          $DAYS = (isset($_COOKIE['int_NewcomerDays'])) ? $_COOKIE['int_NewcomerDays'] : GRP_NEWCOMERDAYS180;
          $stash = $cfg_stash ? '<input class="menuButton btnGray" type="button" value="'.SYM_STASH.'" onclick="stashShow()" title="'.LNG_STASH_SHOW.'" />' : NULLSTR;
          $result .= '<span style="white-space: nowrap; margin: 0em 0.2em 0.2em 0.2em; float: right;">
                        '.$stash.'
                        <input class="menuButton btnGray" type="button" value="'.$DAYS.'" onclick="callNewcomer(this.value)" title="'.LNG_NEWCOMMER.'" />
                        <input class="menuButton btnGray" type="button" value="'.$RSR.'"  onclick="callRand(this.value)"     title="'.LNG_RANDOM_SEARCH_RESULTS.'" />
                      </span>';

          $result .= '</div>';

        $result .= '</div>';
    }
    else
    {
      $result .= '<div style="margin: 30px; color: red; border-style: ridge; padding: 1em; text-align: center;">
                   '.LNG_SSB_DISABLEDBYADMIN.'
                  </div>';
    }

    return $result;
  }


  function showGroupNavi()
  {
    return 'elem2 = $("frame_navi");
      if (elem2)
        elem2.style.visibility = \'visible\';';
  }


  function makeSearchBar_noJS()
  {
    $result = '<form method="POST" name="FORM_SONG_SEARCH" action="action_noJS.php">';

      // -- search --
      $searchStr = (isset($_COOKIE['strSearch'])) ? $_COOKIE['strSearch'] : NULLSTR;
      $result .= BLANK_HTML. '<input name="edt_search" type="text" value="'.$searchStr.'" />
                              <input name="btn_songSearch" type="submit" value="searchSong" />
                              <input name="btn_userSearch" type="submit" value="searchUser" />';

      // -- R10 --
      $result .= BLANK_HTML. '<input name="btn_byRandom" type="submit" value="R10" />';

      // -- N180 --
      $result .= BLANK_HTML. '<input name="btn_byDate" type="submit" value="N180" />';

    $result .= '</form>';

    return $result;
  }


  function makeWelcomeLogo($aVisible)
  {
    if ($aVisible)
      return '<img id="welcomeLogo" class="stretchPic CSS3_easeOutLong" src="res/'.PROJECT_NAME.'_welcome.png"  />';
    else
      return '<img id="welcomeLogo" style="display: none;" class="stretchPic CSS3_easeOutLong" src="res/'.PROJECT_NAME.'_welcome.png"  />';
  }

  // -- ------------------------- --
  // -- end of basic layout stuff --
  // -- ------------------------- --





  // -- ---------------------- --
  // -- all other layout stuff --
  // -- ---------------------- --



  /**
   * the HTML rendering for given items per category
   * @param array
   * @return string (HTML)
   */
  function makeResultItems($aItems) // , $aBuildCache = false -> removed 2023-04-27
  {
    global $cfg_maxSearchResultsPerPage;
    global $cfg_cache;
    global $cfg_musicFileType;
    global $cfg_system;

    $result = NULLSTR;

    // -- ask here directly for what is set in a individual browser --
    $AsTable = (isset($_COOKIE['bool_SrchRsltsAsTable']) && ($_COOKIE['bool_SrchRsltsAsTable'] == YES)) ? true : false;

    $count = count($aItems);

    //$result .= '<div style="float: left; min-width: 99.9%; margin-bottom: 1em;">';
    if (($count >= $cfg_maxSearchResultsPerPage)) //  && (!$cfg_cache)
    {
      $result .= sprintf('<span id="songCount"><nobr>'.LNG_SONGS_FOUND_MORE_THAN_.'</nobr></span>', $count);
    }
    else
    {
      $result .= sprintf('<span id="songCount"><nobr>'.LNG_SONGS_FOUND_.'</nobr></span>', $count);
    }

    //$result .= '</div>';

    if ($AsTable)
    {
      $result .= '<table id="searchResultTable" class="sortable" border="0">
                    <thead onclick="makeTableSortable()">
                    <tr>
                      <th>'.LNG_INTERPRET.'</th>
                      <th>'.LNG_TITLE.'</th>
                      <th>'.LNG_ALBUM.'</th>
                      <th style="font-size: smaller;">'.LNG_RATING.'</th>
                      <th>'.LNG_DURATION.'</th>
                      <th>Song</th>
                    </tr>';
        $result .= '</thead><tbody>';
    }

    foreach ($aItems as $item)
    {
      // -- ------------------------------ --
      // -- the common way in tiled blocks --
      // -- ------------------------------ --
      if (!$AsTable)
      {
        $info = '<a class="grpLstItmR"
                  href="javascript:songGetInfo(\''.base64_encode($item['a']).'\')">
                  <span style="text-align: center;" class="grpLstContR">i</span>
                 </a>';

        $result .= '<div class="grpLstContX">'.$info .'
                      <a class="grpLstItmL"
                        href="javascript:songAdd(\''.base64_encode($item['a']).'\',
                                                  \''.base64_encode($item['i']).'\',
                                                  \''.base64_encode($item['t']).'\',
                                                  \''.$item['p'].'\',
                                                  \''.base64_encode($item['l']).'\',
                                                  \''.cntSubsituteCriticalJSChars($item['t']).'\')" >
                        <div class="grpLstContL">
                          <div class="listItemInterpret">
                            <nobr>'.$item['i'].'</nobr>
                          </div>
                          <div class="listItemTitle">
                            <nobr>'.$item['t'].'</nobr>
                          </div>
                          <span style="float: right;">'.cntGetEmojiRating($item['r']).BLANK.cntMakeCommentBubble($item['c']).'</span>
                          <span class="listItemRemainTime">'.$item['p'].'</span>
                        </div>
                      </a>
                    </div>';

      }
      // -- ------------------- --
      // -- as a sortable table --
      // -- ------------------- --
      else
      {
        $result .= '<tr>';
          $addSongSnippet = 'onclick="songAdd(\''.base64_encode($item['a']).'\',\''.base64_encode($item['i']).'\',\''.base64_encode($item['t']).'\',\''.$item['p'].'\',\''.base64_encode($item['l']).'\' , \''.cntSubsituteCriticalJSChars($item['t']).'\')"';
          $result .= '<td '.$addSongSnippet.' class="tableContent">'.$item['i'].'</td>
                      <td '.$addSongSnippet.' class="tableContent">'.$item['t'].'</td>';
          // $escapedAlbum = str_replace('\'', '\\\'', $item['l']);
          $result .= '<td '.$addSongSnippet.' class="tableContent">'. $item['l'].'</td>';
          $result .= '<td '.$addSongSnippet.' class="tableContent" style="text-align: center;">'
                           .cntGetEmojiRating($item['r']).BLANK
                           .cntMakeCommentBubble($item['c'])
                    .'</td>';
          $result .= '<td '.$addSongSnippet.' class="tableContent" style="text-align: right;">'.$item['p'].'</td>
                      <td onclick="songGetInfo(\''.base64_encode($item['a']).'\')" class="tableContent tableContentRight">'.basename($item['a'], ".$cfg_musicFileType").'</td>';
        $result .= '</tr>';
      }
    }

    if ($AsTable)
    {
      $result .= '</tbody></table>';
    }

    return $result;
  }


  function makeResultItems_noJS($aItems)
  {
    $count = count($aItems);
    $result = '<nobr>found '.$count.' song(s)</nobr><p>';
    $result .= '<form name="FORM_ADD_SONG" method="POST" action="action_noJS.php">'; // frontend_noJS.php
    $result .= '<input type="hidden" name="hid_count" value="'.$count.'" />';
    $i = 0;
    foreach ($aItems as $item)
    {
       $result .= '
                  <input type="hidden" name="hid_a'.$i.'"  value="'.$item['a'].'" />
                  <input type="hidden" name="hid_in'.$i.'" value="'.$item['i'].'" />
                  <input type="hidden" name="hid_ti'.$i.'" value="'.$item['t'].'" />
                  <input type="hidden" name="hid_pt'.$i.'" value="'.$item['p'].'" />
                  <input type="hidden" name="hid_al'.$i.'" value="'.$item['l'].'" />

                  <input type="submit" name="btn_add'.$i.'" value="+" />
                              '.$item['i'].'<br>
                  &nbsp;&nbsp;'.$item['t'].'<br>
                  &nbsp;&nbsp;'.$item['p'].'<br>
                  &nbsp;&nbsp;'.$item['l'].'<p>
                ';
       $i++;
    }
    $result .= '</form>';
    return $result;
  }







  /**
   * the HTML rendering for given items per category
   * this includes the currently playing "on air" song
   * @param array
   * @return string (HTML)
   */
  function makeCurrentPlaylistView($aItems)
  {
    global $cfg_timeZone;
    global $cfg_musicFileType;
    global $cfg_stash;
    global $cfg_mqtt;

    $timeZone = (isset($cfg_timeZone)) ? $cfg_timeZone : TZ_UTC;
    date_default_timezone_set($timeZone);

    if (!isset($_SESSION)) // -- session starting here is an exception as an emergency case --
      session_start();

    $playerState = class_player::static_getPlayerState();

    $count = count($aItems);
    $result = NULLSTR;

    //$grad = cntGradientFromTo('019e1a', 'd0218f', $count);  // 00DF20 to BF0040 // then use: //style="color: #'.$grad[$c].';"

    $onAir = true; // -- aka top most song in the playlist --
    $topSong = false;
    $lastSong = false;
    $c = 0;
    if (is_array($aItems))
    {
      foreach ($aItems as $item)
      {
        $topSong = ($c == 1) ? true : false;
        $lastSong = ($c == $count - 1) ? true : false;

        $gtSecondSong = ($c > 2) ? true : false;

        $escapedInterpret = str_replace('\'', '\\\'', $item['i']);
        $escapedTitle = str_replace('\'', '\\\'', $item['t']);

        if (!$onAir)
        {
          $result .= '<div class="playListRowL">';
        }
        else
        {                                    // style="background-color: #'.$grad[$c].';"
          $result .= '<div class="playListRowL" style="margin-bottom: 3em; height: 8.5em;">'; // -- "Mogeloffset" height: 8.5em; is a trick to set the height correct. It should be possible without this but hard to find who's the causer of the bottom space inside (padding?!) --
          // -- admin stuff --
          if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true && !empty($item[XML_INDEX]) && $item[XML_INDEX] != NULLSTR)
          {
            // -- on air but not playing --
            if ($playerState == PS_OFF || $playerState == PS_UNKNOWN)
            {
              $result .= '<a class="playListItemR kill" style="float: left;" title="kill"      href="javascript:songKill(\''.$item[XML_INDEX].'\');" onclick="return confirm(\''.sprintf(LNG_SURE_DELETE_SONG_FROM_PLAYLIST, $escapedTitle).'\');"></a>';
              if (!$lastSong)
              {
               $result .= '<a class="arrowDown"          style="float: left;" title="move down" href="javascript:songMoveDown(\''.$item[XML_INDEX].'\');"></a>';
              }
            }
          }
          // -- really "on Air" --
          if ($playerState == PS_PLAYING)
          {
            $result .= '<div class="plInfoRow" style="position: absolute;" id="onAirTitle"><nobr>'.LNG_ONAIR.'</nobr></div>';
          }
          else
          {
            $result .= '<div class="plInfoRow" style="position: absolute;" id="onAirWaiting"><nobr>'.LNG_WAITING.'</nobr></div>';
          }

        }


        // -- song info --
        $thumgSize = (cntIsMobile()) ? 'width="120px" height="120px"' : 'width="75px" height="75px"';
        //if (isset($_COOKIE['bool_ShowAlbumCoverInPlaylist']) && $_COOKIE['bool_ShowAlbumCoverInPlaylist'] == YES && isset($_SESSION['str_nickname']) && trim($_SESSION['str_nickname']) != NULLSTR)
        if (isset($_COOKIE['bool_ShowAlbumCoverInPlaylist']) && $_COOKIE['bool_ShowAlbumCoverInPlaylist'] == YES)
          $infoSymbol = '<img '.$thumgSize.' src="'.cntGetAlbumPicURL($item['a'], $item['l'], true).'" title="info" />';
        else
          $infoSymbol = 'i';

        $result .= '<a class="grpLstItmR" href="javascript:songGetInfo(\''.base64_encode( $item['a'] ).'\')">
                        <span style="text-align: center;" class="grpLstContR">'.$infoSymbol.'</span>
                    </a>';


        // -- admin stuff --
        if (!$onAir)
        {
          if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true && !empty($item['i']) && $item['i'] != NULLSTR)
          {
             $result .= '<a class="playListItemR kill" title="kill" onclick="return confirm(\''.sprintf(LNG_SURE_DELETE_SONG_FROM_PLAYLIST, $item['t']).'\');" href="javascript:songKill(\''.$item[XML_INDEX].'\');"></a>';

            // -- move songs --
            if (!$lastSong)
            {
              $result .= '<a class="arrowDown"  title="move down" href="javascript:songMoveDown(\''.$item[XML_INDEX].'\');"></a>';
            }
            if (!$topSong || $playerState == PS_OFF || $playerState == PS_UNKNOWN)
            {
              $result .= '<a class="arrowUp"    title="move up"   href="javascript:songMoveUp(\''.$item[XML_INDEX].'\');"></a>';
            }
            if ($gtSecondSong)
            {
              $result .= '<a class="arrowUpTop" title="move top"  href="javascript:songMoveTop(\''.$item[XML_INDEX].'\');"></a>';
            }
          }
          else
          {
            // -- the wisher can delete his own added song --
            if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] == $item['w'])
            {
              $result .= '<a class="playListItemR kill" title="kill" onclick="return confirm(\''.sprintf(LNG_SURE_DELETE_SONG_FROM_PLAYLIST, $item['t']).'\');" href="javascript:songKill(\''.$item[XML_INDEX].'\', \''.$item['w'].'\');"></a>';
            }
          }

          // -- stash function - independant from admin or not --
          if ($cfg_stash)
          {
            if (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] == $item['w'])
            {
              $result .= '<a class="playListItemR stashPush" title="stash push" href="javascript:stashPushSong(\''.$item[XML_INDEX].'\', \''.$item['w'].'\', \''.base64_encode($item['a']).'\', \''.base64_encode($item['i']).'\', \''.base64_encode($item['t']).'\', \''.$item['p'].'\', \''.base64_encode($item['l']).'\');"></a>';
            }
          }
        }


         // -- working with <br> is not lazy, but a trick. Otherwise, working with <div>, the whole line would be clickable which would be not fine --
        $result .= LFH;
        // -- this is really ugly, but it works :o) --
        if ($onAir)
          $result .= '<div style="float: none;">&nbsp;</div>';

        $result .= '<span class="playListItemL listItemInterpret" class=""onclick="callSearchByStr(\''.SO_INTERPRET.$escapedInterpret.'\')">' . $item['i'] . '</span>';
          $result .= LFH;
        $result .= '<span class="playListItemL listItemTitle" id="curOnAirSongTitle" onclick="callSearchByStr(\''.SO_TITLE.$escapedTitle.'\')">' . $item['t'] . '</span>';


        $result .= '<div class="watermark">'.basename($item['a'],DOT.$cfg_musicFileType).'</div>';

        $myPlaylist = new class_playlist();
        if ($onAir)
        {
          $playlistRemainTime = $myPlaylist->getPlayerRemainTime();

          // -- hidden field for omniPresentSong progress visualization --
          $result .= '<span id="hid_onAirSongCurPos" style="display: none;">'.$playlistRemainTime.'</span>';
          $onAirSongLength = parseHumanMinSecStringToSeconds($item['p']);
          $result .= '<span id="hid_onAirSongLength" style="display: none;">'.$onAirSongLength.'</span>';

          // -- what the user sees --
          if ($myPlaylist->getPlayerRemainTime() !== false)
            $result .= '<div class="playListItemL listItemRemainTime" id="onAirRT">'.parseSecondsToHumanMinSecString($playlistRemainTime).'</div>';
          else
            $result .= '<div class="playListItemL listItemRemainTime" id="onAirRT">'.$item['p'].'</div>';
        }
        else
        {
          //style="color: #'.$grad[$i].';"
          $eta = $myPlaylist->getEstimatedPlaytime($item[XML_INDEX]);
          $result .= '<div class="playListItemL listItemRemainTime" data-eta="'.$eta.'">ETA:'.BLANK.$eta.'</div>';
        }
        unset($myPlaylist);

        // -- wished by --
        if ($item['w'] != NULLSTR)
        {
          //  $result .= '<div class="playListItemL" style="color: #ffff80;">wished by ' . $item['w] . '</div>';
          $escapedUser = str_replace('\'', '\\\'', $item['w']);
          $result .= '<span class="playListItemL listItemWishedBy" onclick="callSearchByUser(\''.$escapedUser.'\')">'.LNG_WISHED_BY.' <span style="font-weight: bold;">' . $item['w'] . '</span></span>';
          //$result .= '<br>';
        }

        // -- --------------------------- --
        // -- begin of rating and comment --
        if ($onAir)
        {
          if (
              (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'] == true)
               ||
              (isset($_SESSION['str_nickname']) && $_SESSION['str_nickname'] != NULLSTR)
            )
          {
            // -- take it from mediabase in future. Even first in order --
            $myMediabase = new class_mediaBase();
            if ($myMediabase->findSong($item['a'])) // -- without basename() because of full path --
            {
              $curComment = $myMediabase->getComment() . ' (by: ' . $myMediabase->getCommentBy() . ')';
              $curRating = $myMediabase->getRating() . ' (by: ' . $myMediabase->getRatingBy() . ')';
            }
            else
            {
              $curComment = NULLSTR;
              $curRating = NULLSTR;
            }

            function _isRateSel5($aRate) {
              if ($aRate == RATE5)
                return BLANK . 'selected' . BLANK; }
            function _isRateSel4($aRate) {
              if ($aRate == RATE4)
                return BLANK . 'selected' . BLANK; }
            function _isRateSel3($aRate) {
              if ($aRate == RATE3)
                return BLANK . 'selected' . BLANK; }
            function _isRateSel2($aRate) {
              if ($aRate == RATE2)
                return BLANK . 'selected' . BLANK; }
            function _isRateSel1($aRate) {
              if ($aRate == RATE1)
                return BLANK . 'selected' . BLANK; }

            $escapedFile = base64_encode(basename(str_replace('\'', '\\\'', $item['a'])));

            $result .= '<span style="float: right; white-space: nowrap;">';

              // -- comment --
              $curComment = ($myMediabase->getComment() != NULLSTR) ? $myMediabase->getComment() : LNG_NOCOMMENT_DO; // old $myFeedback
              $curCommentBy = ($myMediabase->getCommentBy() != NULLSTR) ? $myMediabase->getCommentBy() : NULLSTR;
              $result .= '<span style="border-style: outset; border-width: 1px; margin-left: 2em; padding: 0.5em 0.75em 0.5em 0px; border-radius: 3px;">';

                if ($cfg_mqtt)
                {
                  $previewSongC = 'href="preview.php?song='.base64_encode($item['a']).'&i='.base64_encode($item['i']).'&t='.base64_encode($item['t']).'&p='.base64_encode($item['p']).'&l='.base64_encode($item['l']).'"';
                  // $previewSongR = 'onclick="location.href="preview.php?song='.base64_encode($item['a']).'&i='.base64_encode($item['i']).'&t='.base64_encode($item['t']).'&p='.base64_encode($item['p']).'&l='.base64_encode($item['l']).'"';
                  // $previewSongC = 'alert("use");';
                  $previewSongR = '';
                }

                if (!$cfg_mqtt)
                  $result .= '<a class="playListItemR comment" href="javascript:songSetComment(\''.$escapedFile.'\');" title="'.$curCommentBy.'">';
                else
                  $result .= '<a disabled class="playListItemR comment" '.$previewSongC.' title="'.$curCommentBy.'">';

                $result .= '<span id="curComment" style="font-style: italic; margin-left: 0.5em;">'.$curComment.'</span></a>';
              $result .= '</span>';

              // -- rating --
              $curRating = $myMediabase->getRating(); // old $myFeedback
              $curRatingBy = $myMediabase->getRatingBy();

              if (!$cfg_mqtt)
                $result .= '<select class="playListItemR rating" onfocus="restartPlaylistInterval()" onchange="songSetRating(\''.$escapedFile.'\', this.value);" title="'.$curRatingBy.'">';
              else
                $result .= '<select disabled class="playListItemR rating" '.$previewSongR.' title="'.$curRatingBy.'">';

              if ($curRating != RATE1 && $curRating != RATE2 && $curRating != RATE3 && $curRating != RATE4 && $curRating != RATE5)
                $result .= '<option value="">'.LNG_NORATING_VOTE.'</option>';
              $result .= '
                            <option value="'.RATE5.'" '._isRateSel5($curRating).'>'.RATE5_HUMAN.'</option>
                            <option value="'.RATE4.'" '._isRateSel4($curRating).'>'.RATE4_HUMAN.'</option>
                            <option value="'.RATE3.'" '._isRateSel3($curRating).'>'.RATE3_HUMAN.'</option>
                            <option value="'.RATE2.'" '._isRateSel2($curRating).'>'.RATE2_HUMAN.'</option>
                            <option value="'.RATE1.'" '._isRateSel1($curRating).'>'.RATE1_HUMAN.'</option>
                          </select>';

            $result .= '</span>';

            unset($myMediabase);
          }
        }
        // -- end of rating and comment --
        // -- ------------------------- --

        $result .= '</div>';


        // -- show this hint only below the on air song --
        if ($onAir)
        {
          $songCount = $count;
          if ($count >= 1)
            $songCount--;
          else
            $songCount = 0;

          $result .= '<div class="plInfoRow modernCenterBackground">';
            $result .= sprintf('<nobr>'.LNG_PLAYLIST_SONGS_IN_.'<nobr>', $songCount);
          $result .= '</div>';
        }

        $onAir = false;
        $c++;
      }
    }

    // -- when playlist is completely empty show a hint how to add a song --
    if ($count == NULLINT)
    {
      $result .= '<div class="plInfoRow modernCenterBackground">';
        $result .= '<nobr>'.LNG_PLAYLIST_NO_SONGS_IN_HELP.'<nobr>';
      $result .= '</div>';
    }

    return $result;
  }




  function makeCurrentPlaylistView_noJS($aItems)
  {
    global $cfg_timeZone;

    $timeZone = (isset($cfg_timeZone)) ? $cfg_timeZone : 'UTC';
    date_default_timezone_set($timeZone);

    if (!isset($_SESSION)) // -- session starting here is an exception as an emergency case --
      session_start();

    $count = count($aItems);
    $indexCount = $count;
    $result = NULLSTR;
    $onAir = true;
    $c = 0;
    if (is_array($aItems))
    {
      foreach ($aItems as $item)
      {
        $indexCount = ($item[XML_INDEX] > $count) ? $item[XML_INDEX] : $indexCount;

        $result .=  NULLSTR;
        if ($onAir)
        {
          $result .= '<div><nobr>'.LNG_ONAIR.'</nobr></div>';
          if (isset($_SESSION['bool_admin']) && $_SESSION['bool_admin'])
            $result .= '<input type="submit" name="btnNext" value="&rarr;" />&nbsp;' . $item['i'].LFH;
          else
            $result .= '&nbsp;&nbsp;'.$item['i'].LFH;
        }
        else
          $result .= '<input type="submit" name="btnKill'.$item[XML_INDEX].'" value="-" />&nbsp;'.$item['i'].LFH;

        if ($onAir)
          $result .= '&nbsp;&nbsp;'.$item['t'].LFH;
        else
        {
          if ($c < 2)
            $result .= '&nbsp;&nbsp;'.$item['t'].LFH;
          else
            $result .= '<input type="submit" name="btnMoveTop'.$item[XML_INDEX].'" value="&uarr;" />&nbsp;'.$item['t'].LFH;
        }


        $myPlaylist = new class_playlist();
        if ($onAir)
        {
          if ($myPlaylist->getPlayerRemainTime() !== false)
            $result .= '&nbsp;&nbsp;'.parseSecondsToHumanMinSecString($myPlaylist->getPlayerRemainTime()).LFH;
          else
            $result .= '&nbsp;&nbsp;'.$item['p'].LFH;
        }
        else
          $result .= '&nbsp;&nbsp;ETA: '.$myPlaylist->getEstimatedPlaytime($item[XML_INDEX]).LFH;
        unset($myPlaylist);

        // -- wished by --
        $result .= '&nbsp;&nbsp;wished by '.$item['w'].'<p>';


        if ($onAir)
        {
          $songCount = $count;
          if ($count >= 1)
            $songCount--;
          else
            $songCount = 0;
          $result .= sprintf('<div><nobr>'.LNG_PLAYLIST_SONGS_IN_.'<nobr></div>', $songCount);
        }
        $onAir = false;
        $c++;
      }
      $result .= '<input type="hidden" name="hid_count" value="'.$indexCount.'" />';
    }
    return $result;
  }



